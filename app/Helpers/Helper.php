<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Helper {

	public static function sidenavAccess($permission) {
		$admin = Auth::guard('admin')->user();
		if ($admin->is_admin == 1) {
			return true;
		} elseif ($admin->is_admin == 2) {
			if (!$admin->hasAccess([$permission])) {
				return false;
			}
			return true;
		}
	}

	public static function checkIsReviewer() {
		$admin = Auth::guard('admin')->user();
		$roleArray = $admin->roles()->pluck('slug')->toArray();
		if (in_array('adjudicator', $roleArray)) {
			return $admin->facultyPayment()->checkRenumeration($admin);
		} else {
			return false;
		}
	}

}
