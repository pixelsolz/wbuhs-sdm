<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentData extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'name' => $this->name,
			'email' => $this->email,
			'phone' => $this->phone,
			'institute' => $this->institute,
			'registration_no' => $this->registration_no,
			'academic_year' => $this->ac_year,
			'course_id' => $this->course_id,
			'is_complete' => $this->is_dissertation_complete,
			'complete_date' => $this->is_dissertation_complete == 1 ? $this->complete_date : '',
		];
	}
}
