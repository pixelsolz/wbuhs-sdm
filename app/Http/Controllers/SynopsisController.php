<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Models\Category;
use App\Models\CollegeMaster;
use App\Models\Synopsis;
use App\Models\SynopsisStudentData;
use App\Models\SynopsisTitle;
use App\Models\User;
use App\Notifications\SendNotify;
use App\Services\MPdfService;
use Auth;
use Carbon\Carbon;
use DB;
//use File;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Image;
use Mail;
use Session;
use Validator;
use View;

class SynopsisController extends Controller {

	public $mPDF;
	public function __construct(MPdfService $mPDF) {
		$this->mPDF = $mPDF;
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 0);
		ini_set('upload_max_filesize', '1024M');
		ini_set('post_max_size', '1024M');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Student | Synopsis';
		$parentCategory = Category::where('parent', 0)->get();
		$categories = $this->categoryTree();
		$college_masters = CollegeMaster::all();
		return view('frontend.synopsis-form', compact('title', 'categories', 'parentCategory', 'college_masters'));
	}

	public function uploadPDF(Request $request) {
		ini_set('memory_limit', '25600M');
		ini_set('max_execution_time', 0);
		ini_set('upload_max_filesize', '25600M');
		ini_set('post_max_size', '25600M');
		$studentPdf = $request->file('student_pdf');
		$filename = time() . $studentPdf->getClientOriginalName();
		//Storage::disk('upload_pdf')->put($filename, file_get_contents($studentPdf->getRealPath()));
		$studentPdf->move(public_path('pdfs'), $filename);
		return $filename;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$validator = Validator::make($request->all(), [
			'category_id' => 'required',
			'name_of_student' => 'required',
			'registration_no' => 'required',
			//'registration_year' => 'required',
			'institute_name' => 'required',
			'mobile_landline' => 'required',
			'guide_name' => 'required',
			'email' => 'email',
			'guide_designation' => 'required',
			'proposed_title_thesis' => 'required',
			'proposed_work_place' => 'required',
			'student_pdf_file' => 'required',
			'dob' => 'required',
			//'student_pdf_more'=>'mimes:pdf',
			'signUpload' => 'mimes:jpeg,png,jpg | max:2048',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		if (!Auth::user()->userDetail->registration_no) {
			$studentData = SynopsisStudentData::where('unique_id', $request->registration_no)->where(DB::raw("TRIM(name)"), $request->name_of_student)->first();
			if (!$studentData) {
				Session::flash('error', 'Unique Id and/or your name mismatch as per records.');
				return redirect()->back()->withInput();
			}
		}

		$checkHasEnabled = SynopsisStudentData::where('unique_id', $request->registration_no)->latest('id')->first();

		if ($checkHasEnabled && $checkHasEnabled->status == 0) {
			Session::flash('error', 'Submission period has been end');
			return redirect()->back()->withInput();
		}

		if (Synopsis::where('registration_no', $request->registration_no)->first()) {
			Session::flash('error', 'This registration no has already used!');
			return redirect()->back()->withInput();
		}
		$mytime = Carbon::now('Asia/Kolkata');
		$currentDate = $mytime->timestamp;
		$lateSubmitDate = Carbon::parse('2022-01-01')->timestamp;
		if ($currentDate >= $lateSubmitDate) {
			Session::flash('error', 'Submission period has been end');
			return redirect()->back()->withInput();
		}

		$mytime = Carbon::now('Asia/Kolkata');
		$currentDate = $mytime->timestamp;
		$lateSubmitDate = Carbon::parse('2022-01-26')->timestamp;
		$lateStudents = [];
		/*if (!in_array(\Auth::user()->email, $lateStudents) && $currentDate >= $lateSubmitDate) {
			Session::flash('error', 'Submission period has been end');
			return redirect()->back()->withInput();
		}*/

		\DB::beginTransaction();
		try {
			$user_id = Auth::user()->id;
			$userDetail = Auth::user()->userDetail;
			$userDetail->dob = Carbon::parse($request->dob)->format('Y-m-d');
			$userDetail->current_submission = 'synopsis';
			$userDetail->save();
			$synopsis = Synopsis::create($request->except('student_pdf', 'signUpload', 'institute_name', 'category_id', 'dob') + ['user_id' => $user_id, 'category_id' => $request->course_name]);
			if (!empty($request->institute_name)) {
				$college = CollegeMaster::where('id', $request->input('institute_name'))->first();
				$synopsis->institute_id = $college->id;
				$synopsis->institute_name = $college->college_name;
				$synopsis->institute_code = $college->college_code;
				$synopsis->save();
			}
			if ($request->hasFile('signUpload')) {
				$signImage = $request->file('signUpload');
				$signfilename = time() . $signImage->getClientOriginalName();
				$image_resize = Image::make($signImage->getRealPath());
				//$signImage->move(public_path('signatures'), $signfilename);
				$image_resize->resize(480, 100, function ($constraint) {
					//$constraint->aspectRatio();
				})->save(public_path('signatures/' . $signfilename));
				//$signImage->move(public_path('signatures'), $signfilename);
				$synopsis->student_signature = $signfilename;
				$synopsis->save();
			}

			if ($request->hasFile('student_pdf_more')) {
				if (count($request->file('student_pdf_more')) > 0) {
					$filenames = '';
					foreach ($request->file('student_pdf_more') as $value) {
						$file = $value;
						$filename = time() . 'additional_' . $file->getClientOriginalName();
						$filenames .= $filename . ',';
						$file->move(public_path('pdfs'), $filename);
					}
					$synopsis->more_pdfs = rtrim($filenames, ',');
					$synopsis->save();
				}
			}

			if ($request->has('student_pdf_file')) {
				$synopsis->student_pdf = $request->student_pdf_file;
				$synopsis->save();
			}

			/*if ($request->hasFile('student_pdf')) {
				$pdfFile = $request->file('student_pdf');
				$filename = time() . $pdfFile->getClientOriginalName();
				$pdfFile->move(public_path('pdfs'), $filename);
				$synopsis->student_pdf = $filename;
				$synopsis->save();
			}*/

			$pdfName = time() . 'synopsis.pdf';
			$file = View::make('pdf.synopsis-pdf', compact('synopsis'));
			$this->mPDF->savePDF($file, $pdfName);
			$synopsis->form_pdf = $pdfName;
			$synopsis->save();

			$sessionArray = ['pay_data' => $synopsis, 'pay_type' => 'App\Models\Synopsis'];
			\DB::commit();
			session($sessionArray);
			return redirect('synopsis/payment/create');
		} catch (\Exception $e) {
			\DB::rollback();
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$title = 'Student | Synopsis View';
		$categories = $this->categoryTree();
		$synopsis = Auth::user()->studentSynopsis;
		//$synopsis = Synopsis::find($id);
		$course = Category::where('id', $synopsis->category_id)->first();
		return view('frontend.synopsis-form-edit', compact('title', 'categories', 'synopsis', 'course'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Student | Synopsis Edit';
		$categories = $this->categoryTree();
		$synopsis = Auth::user()->studentSynopsis;
		//$synopsis = Synopsis::find($id);
		$course = Category::where('id', $synopsis->category_id)->first();
		return view('frontend.synopsis-form-edit', compact('title', 'categories', 'synopsis', 'course'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		try {
			$synopsis = Synopsis::find($id);
			$synopsis->fill($request->except('student_pdf', 'student_pdf_more', 'signUpload'));

			if ($request->hasFile('signUpload')) {
				$signImage = $request->file('signUpload');
				$signfilename = time() . $signImage->getClientOriginalName();
				$signImage->move(public_path('signatures'), $signfilename);
				$synopsis->student_signature = $signfilename;
				$synopsis->save();
			}

			if ($request->hasFile('student_pdf_more')) {
				if (count($request->file('student_pdf_more')) > 0) {
					$filenames = '';
					foreach ($request->file('student_pdf_more') as $value) {
						$file = $value;
						$filename = time() . 'additional_' . $file->getClientOriginalName();
						$filenames .= $filename . ',';
						$file->move(public_path('pdfs'), $filename);
					}
					$synopsis->more_pdfs = rtrim($filenames, ',');
					$synopsis->save();
				}
			}

			if ($request->has('student_pdf_file')) {
				$synopsis->student_pdf = $request->student_pdf_file;
				$synopsis->save();
			}

			$synopsis->status = 1;
			$synopsis->is_resubmitted = $synopsis->is_resubmitted + 1;
			$synopsis->save();

			$synopsis->transaction()->update(['review_status' => 0]);

			$data = ['type' => 1, 'synopsis' => $synopsis];
			$view = 'emails.resubmit-synopsis';
			$subject = 'Synopsis Resubmitted';

			$superadmins = User::where('is_admin', 1)->get();
			foreach ($superadmins as $key => $value) {
				Mail::to($value->email)->send(new SendMail($data, $view, $subject));
				$value->notify(new SendNotify('Synopsis resubmitted'));
			}

			$secrtearyBOS = User::whereHas('roles', function ($q) {
				$q->where('slug', 'secretarybos');
			})->get();

			foreach ($secrtearyBOS as $value) {
				Mail::to($value->email)->send(new SendMail($data, $view, $subject));
				$value->notify(new SendNotify('Synopsis resubmitted'));
			}

			Session::flash('msg', 'Sucessfully updated synopsis');
			return redirect()->route('dashboard.index');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	protected function formValidation($data) {
		$validator = Validator::make($data, [
			'category_id' => 'required',
			'name_of_student' => 'required',
			'registration_no' => 'required',
			//'registration_year' => 'required',
			'institute_name' => 'required',
			'mobile_landline' => 'required',
			'guide_name' => 'required',
			'email' => 'email',
			'guide_designation' => 'required',
			'proposed_title_thesis' => 'required',
			'proposed_work_place' => 'required',
			'student_pdf' => 'required | mimes:pdf',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
	}

	protected function categoryTree($parent = 0, $spacing = '', $user_tree_array = '') {
		if (!is_array($user_tree_array)) {
			$user_tree_array = array();
		}

		$query = DB::select(DB::raw("select * from `wb_categories` WHERE 1 AND `parent` = $parent order by id asc"));

		if (count($query) > 0) {
			foreach ($query as $row) {
				$user_tree_array[] = array("id" => $row->id, "name" => $spacing . $row->name);
				$user_tree_array = $this->categoryTree($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);

			}
		}
		return $user_tree_array;

	}

	public function addCategory(Request $request) {
		$message = [
			'parent.required' => 'The cateogry field required.',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'parent' => 'required',
		], $message);
		if ($validator->fails()) {
			return response()->json(array(
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray(),

			), 422);
		}
		try {
			$parent = Category::find($request->parent);
			if ($parent->has_child != 1) {
				$parent->has_child = 1;
				$parent->save();
			}
			$category = Category::create($request->all());
			return response()->json(['status' => 200, 'data' => $category]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_error' => $e->getMessage()]);
		}
	}

	/*
		        check synopsis exists
	*/

	public function checkCategorywiseSynopsis(Request $request) {

	}

	public function saveSignature(Request $request) {
		try {
			$image = $request->image;
			$image = str_replace('data:image/jpeg;base64,', '', $image);
			$image = str_replace(' ', '+', $image);
			$imageName = time() . '.' . 'jpeg';
			\File::put(public_path('signatures') . '/' . $imageName, base64_decode($image));
			return response()->json(['status' => 200, 'image' => $imageName]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}

	}

	public function viewPdf(Request $request) {

		ini_set("memory_limit", -1);
		$synposis = Synopsis::find($request->id);
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download(public_path('pdfs/' . $synposis->student_pdf), 'synopsis.pdf', $headers);

		/*$mpdf = new \Mpdf\Mpdf();
					$mpdf->SetImportUse();

			        $pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $synposis->form_pdf));
			        	$tplId = $mpdf->ImportPage($pagecount);
						$mpdf->UseTemplate($tplId);
						$mpdf->AddPage();

						$pagecount1 = $mpdf->SetSourceFile(public_path('pdfs/' . $synposis->student_pdf));
					for ($i = 1; $i < $pagecount1; $i++) {
						$tplId1 = $mpdf->ImportPage($i);
						$mpdf->UseTemplate($tplId1);
						$mpdf->AddPage();
					}

					$pagecount = $mpdf->SetSourceFile(public_path('pdfs/' . $synposis->student_pdf));
					for ($i = 1; $i < $pagecount; $i++) {
						$tplId = $mpdf->ImportPage($i);
						$mpdf->UseTemplate($tplId);
						$mpdf->AddPage();
					}
		*/
	}

	public function viewOherpdf($pdf) {
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetImportUse();
		$pagecount = $mpdf->SetSourceFile(public_path('pdfs/' . $pdf));
		if ($pagecount == 1) {
			$tplId = $mpdf->ImportPage($pagecount);
			$mpdf->UseTemplate($tplId);
		} else {
			for ($i = 1; $i < $pagecount; $i++) {
				$tplId = $mpdf->ImportPage($i);
				$mpdf->UseTemplate($tplId);
				$mpdf->AddPage();
			}
		}

		$mpdf->Output();
	}

	public function test() {

		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetImportUse();

		$pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/pdf.pdf'));
		for ($i = 1; $i < $pagecount; $i++) {
			$tplId = $mpdf->ImportPage($i);
			$mpdf->UseTemplate($tplId);
			//$mpdf->AddPage();
		}

		$pagecount1 = $mpdf->SetSourceFile(public_path('pdfs/synopsis/1540641636synopsis.pdf'));
		$tpl1Id = $mpdf->ImportPage($pagecount1);
		$mpdf->UseTemplate($tpl1Id);
		//$mpdf->WriteHTML();

		$mpdf->Output();
	}

	public function oldSynopsisPayment($id) {
		$synopsis = Auth::user()->studentSynopsis;
		//$synopsis = Synopsis::find($id);
		$sessionArray = ['pay_data' => $synopsis, 'pay_type' => 'App\Models\Synopsis'];
		session($sessionArray);
		return redirect('synopsis/payment/create');
	}

	public function setUniqueNo(Request $request) {
		$value = trim(strtoupper($request->value));
		$studentData = SynopsisStudentData::where(DB::raw("TRIM(name)"), $value)->latest('id')->first();
		if ($studentData && $studentData->status == 1) {
			return response()->json(['status' => 200, 'data' => $studentData->unique_id]);
		} else {
			return response()->json(['status' => 422, 'status_text' => 'Student name does not found']);
		}
	}

	public function downloadSynossisManual(Request $request) {
		$file = public_path() . "/manuals/Student-Synopsis-Submission-Manual.docx";
		$headers = array(
			'Content-Type: application/octet-stream',
		);
		return Response::download($file, 'Synopsis-manual.docx', $headers);

	}

	public function updateSynossisFile($id, Request $request) {
		$synopsis = Synopsis::find($id);
		if (!empty('student_pdf_file')) {
			$synopsis->student_pdf = $request->student_pdf_file;
			$synopsis->save();
		}

		if ($request->hasFile('student_pdf_more')) {
			if (count($request->file('student_pdf_more')) > 0) {
				$filenames = '';
				foreach ($request->file('student_pdf_more') as $value) {
					$file = $value;
					$filename = time() . 'additional_' . $file->getClientOriginalName();
					$filenames .= $filename . ',';
					$file->move(public_path('pdfs'), $filename);
				}
				$synopsis->more_pdfs = $synopsis->more_pdfs . ',' . rtrim($filenames, ',');
				$synopsis->save();
			}
		}
		$synopsis->resubmit_enable = 0;
		$synopsis->save();

		Session::flash('msg', 'Sucessfully updated synopsis');
		return redirect()->route('dashboard.index');
	}

	public function changeEnableStatus() {
		SynopsisStudentData::whereDate('updated_at', '2020-03-02')->update(['status' => 0]);
	}

	/* Synopsys Title Management */

	public function synopsysTitle() {
		$synopsistitles = SynopsisTitle::paginate(10);
		return view('frontend.synopsis-title', compact('synopsistitles'));
	}
}
