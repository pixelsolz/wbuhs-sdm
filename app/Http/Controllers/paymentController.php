<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Models\Category;
use App\Models\Payment;
//use App\Models\PaymentResponse;
use App\Models\StudentData;
use App\Models\Synopsis;
use App\Models\SynopsisTransaction;
use App\Models\Thesis;
use App\Models\ThesisTransaction;
use App\Models\User;
use App\Notifications\SendNotify;
use App\Services\MPdfService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Mail;
use Session;
use View;

class paymentController extends Controller {

	public $mPDF;
	public function __construct(MPdfService $mPDF) {
		$this->mPDF = $mPDF;
		ini_set('memory_limit', '1024M');
		//ini_set('max_execution_time', 0);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$mytime = Carbon::now('Asia/Kolkata');
		$currentDate = $mytime->toDateString();
		$lateSubmitData = '2021-12-16';
		$title = 'Student | Payment';
		$merchantId = 'WBUHSEXAM';
		$securityId = 'wbuhsexam';
		$checksum_key = 'oV6OMBZUuGKL';
		$amount = '2000.00';
		if ($currentDate >= $lateSubmitData) {
			$amount = '3000.00';
		} else {
			$amount = '2000.00';
		}
		$appUrl = 'https://wbuhs.ac.in/sdm/return-payment';
		$str = $merchantId . '|' . \Auth::user()->id . '|NA|' . $amount . '|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|WBUHSSDM2018|' . $appUrl;
		$checksum = hash_hmac('sha256', $str, $checksum_key, false);
		$checksum_value = ucwords(strtoupper($checksum));
		$newstr = $str . "|" . $checksum_value;
		$msg = $merchantId . '|' . \Auth::user()->id . '|NA|' . $amount . '|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|WBUHSSDM2018|' . $appUrl;

		/*$merchantId = 'WBUHSEXAM';
			$securityId = 'wbuhsexam';
			$checksum_key = 'oV6OMBZUuGKL';
			$str = $merchantId . '|1234|NA|2.00|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://wbuhs.ac.in/sdm/return-payment';
			$checksum = hash_hmac('sha256', $str, $checksum_key, false);
			$checksum_value = ucwords(strtoupper($checksum));
			$newstr = $str . "|" . $checksum_value;
		*/

		return view('frontend.payment', compact('merchantId', 'securityId', 'checksum_key', 'checksum', 'checksum_value', 'msg', 'newstr', 'title'));
	}

	public function createSynopsis() {
		$mytime = Carbon::now('Asia/Kolkata');
		$currentDate = $mytime->toDateString();
		$lateSubmitData = '2021-12-16';
		$amount = '600.00';
		if ($currentDate >= $lateSubmitData) {
			$amount = '600.00';
		} else {
			$amount = '400.00';
		}
		$title = 'Student | Payment';
		$merchantId = 'WBUHSEXAM';
		$securityId = 'wbuhsexam';
		$checksum_key = 'oV6OMBZUuGKL';
		$amount = '600.00';
		/*if (in_array(\Auth::user()->email, $lateStudents)) {
			$amount = '600.00';
		}*/
		$appUrl = 'https://wbuhs.ac.in/sdm/return-payment';
		$str = $merchantId . '|' . \Auth::user()->id . '|NA|' . $amount . '|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|WBUHSSDM2018|' . $appUrl;
		$checksum = hash_hmac('sha256', $str, $checksum_key, false);
		$checksum_value = ucwords(strtoupper($checksum));
		$newstr = $str . "|" . $checksum_value;
		$msg = $merchantId . '|' . \Auth::user()->id . '|NA|' . $amount . '|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|WBUHSSDM2018|' . $appUrl;

		/*$merchantId = 'WBUHSEXAM';
			$securityId = 'wbuhsexam';
			$checksum_key = 'oV6OMBZUuGKL';
			$str = $merchantId . '|1234|NA|2.00|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://wbuhs.ac.in/sdm/return-payment';
			$checksum = hash_hmac('sha256', $str, $checksum_key, false);
			$checksum_value = ucwords(strtoupper($checksum));
			$newstr = $str . "|" . $checksum_value;
		*/

		return view('frontend.payment', compact('merchantId', 'securityId', 'checksum_key', 'checksum', 'checksum_value', 'msg', 'newstr', 'title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		\DB::beginTransaction();
		try {

			if ($request->session()->has('pay_data') && $request->session()->has('pay_type')) {

				$payment = Payment::create(['pay_id' => session('pay_data')->id, 'pay_type' => session('pay_type'), 'pay_amount' => '500', 'type' => $request->pay_type, 'is_paid' => ($request->pay_type == 'online' ? 1 : 0), 'status' => 1]);

				//$payment = Payment::create(['pay_id' => session('pay_data')->id, 'pay_type' => session('pay_type'), 'pay_amount' => '500', 'type' => 'online', 'is_paid' => ($request->pay_type == 'online' ? 1 : 0), 'status' => 1]);

				$synopsisOrthesis = $payment->pay;

				//$pdf = PDF::loadView('pdf.synopsis-pdf', compact('synopsis'));
				$pdfName = time() . (session('pay_type') == 'App\Models\Synopsis' ? 'synopsis.pdf' : 'thesis.pdf');

				/*$this->mPDF->mergePDF($pdfName, $synopsisOrthesis->student_pdf, $synopsisOrthesis->form_pdf);

				$synopsisOrthesis->form_pdf = $pdfName;*/

				if (session('pay_type') == 'App\Models\Synopsis') {
					$synopsisOrthesis->status = 1;
				} else {
					$synopsisOrthesis->status = 1;
				}

				$synopsisOrthesis->save();

				$request->session()->forget('pay_data');
				$request->session()->forget('pay_type');
				$type = ($payment->pay_type == 'App\Models\Synopsis') ? 1 : 2;

				//$paidType = $request->pay_type;
				$paidType = 'online';

				$data = ['type' => $type, 'synopsis' => $synopsisOrthesis];
				$view = 'emails.synopsis-mail';
				$attach = public_path('pdfs/synopsis/' . $synopsisOrthesis->form_pdf);
				$subject = ($payment->pay_type == 'App\Models\Synopsis') ? 'Synopsis' : 'Dissertation';
				$today_add_three = Carbon::now()->addDay(3)->format('Y-m-d');

				$notifyData = ($payment->pay_type == 'App\Models\Synopsis') ? 'New Synopsis' : 'New Dissertation';

				$secrtearyBOS = User::whereHas('roles', function ($q) {
					$q->where('slug', 'secretarybos');
				})->get();

				$userId = \Auth::user()->id;

				$studentSql = Synopsis::where('user_id', $userId)->first();

				$thesis = Thesis::find($studentSql->id);
				$studentCategory = $studentSql->category_id;

				$bossAll = array();

				$bosSql = User::whereHas('roles', function ($q) {
					$q->where('slug', 'bos');
				})->get();

				foreach ($bosSql as $valueBos) {
					$thesisTransUserCount = ThesisTransaction::where('to_user_id', $valueBos->id)->where('status', '<>', 4)->where('is_active', 1)->get();
					if (count($thesisTransUserCount) < 3) {
						$bossAll[] = $valueBos->id;
					}

				}

				$bosRandSql = User::whereHas('category', function ($q) use ($studentCategory, $bossAll) {
					$q->where('category_id', $studentCategory)->whereIn('user_id', $bossAll);
				})->inRandomOrder()->limit(3)->get();

				$string = 15;
				$result = bin2hex(random_bytes($string));
				foreach ($bosRandSql as $valueRandBos) {

					$to = $valueRandBos->email;
					$confirmationLink = $result . $valueRandBos->id . date('YmdHis');

					$thesisTrans = ThesisTransaction::where('to_user_id', $valueRandBos->id)->where('thesis_id', $thesis->id)->where('is_active', 1)->first();
					if ($thesisTrans) {

						$thesisTrans->accknowledge_email_date = Carbon::now();
						$thesisTrans->accknowledge_link = $confirmationLink;
						$thesisTrans->count_send = $thesisTrans->count_send + 1;
						$thesisTrans->updated_by = Auth::guard('admin')->user()->id;
						$thesisTrans->save();
					} else {

						ThesisTransaction::create(['thesis_id' => $thesis->id, 'to_user_id' => $valueRandBos->id, 'trans_type' => 1, 'send_to' => $valueRandBos->email, 'accknowledge_link' => $confirmationLink, 'count_send' => 1, 'accknowledge_email_date' => Carbon::now(), 'is_active' => 1]);

						$data = ['type' => $type, 'synopsis' => $synopsisOrthesis, 'confirmationLink' => $confirmationLink, 'userId' => $studentSql->id];

						Mail::send('emails.dissertation-assign-confirmation', $data, function ($message) use ($to, $subject) {
							$message->to($to)->subject
								($subject);
							$message->from(config('mail.from.address'), 'WBUHS');
						});

					}

					sleep(3);
				}

				//if ($type == 1) {
				//	foreach ($secrtearyBOS as $value) {
				//		Mail::to($value->email)->send(new SendMail($data, $view, $subject));
				//		$value->notify(new SendNotify($notifyData));
				//	}
				//}

				//$superadmins = User::where('is_admin', 1)->get();
				//foreach ($superadmins as $key => $value) {
				//	Mail::to($value->email)->send(new SendMail($data, $view, $subject));
				//	$value->notify(new SendNotify($notifyData));

				//	sleep(2);

				//}
				//$std_view = 'emails.student-synpoOrthesis';
				// logged in student
				//Mail::to(\Auth::user()->email)->send(new SendMail($data, $std_view, $subject));

				\DB::commit();
				Session::flash('msg', 'Sucessfully Paid');

				return redirect('success-message/' . $type . '/' . $paidType);
			}
		} catch (\Exception $e) {
			\DB::rollback();
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function testPDF() {

		$this->mPDF->mergePDFFiles([public_path('pdfs/1540623711sample.pdf')], 'test.pdf');
		exit();
		$synopsis = Synopsis::find(4);

		/*$file = View::make('pdf.synopsis-pdf', compact('synopsis'));
		$this->mPDF->streamPDF($file);*/
	}

	public function successMessage($type, $paid_type) {
		$title = 'Success Message';
		return view('frontend.success-message', compact('title', 'type', 'paid_type'));
	}

	public function check() {
		$synopsisOrthesis = Thesis::find(2);
		$Notavailablefaculties = $synopsisOrthesis->category->user()->whereHas('roles', function ($q) {
			$q->where('slug', 'reviewer');
		})->wherehas('thesisTransction', function ($q) {
			$q->where('status', '>', 1);
		})->pluck('id')->toArray();

		$availablefaculties = $synopsisOrthesis->category->user()
			->where(function ($q) use ($Notavailablefaculties) {
				$q->whereHas('roles', function ($q) {
					$q->where('slug', 'reviewer');
				});
				if (count($Notavailablefaculties)) {
					$q->whereNotIn('id', $Notavailablefaculties);
				}
			})->get();

		echo $availablefaculties;
	}

	public function getSubCategory(Request $request) {

		if (Category::where('parent', $request->id)->first()->has_child == 1) {
			$categories = Category::where('parent', $request->id)->get();
			return response()->json(['status' => 200, 'is_sub_cat' => 1, 'data' => $categories]);
		} elseif (Category::where('parent', $request->id)->first()->has_child == 0) {
			$courses = Category::where('parent', $request->id)->get();
			return response()->json(['status' => 200, 'is_sub_cat' => 0, 'data' => $courses]);
		}

	}

	public function paymentForm() {
		$merchantId = 'WBUHSEXAM';
		$securityId = 'wbuhsexam';
		$checksum_key = 'oV6OMBZUuGKL';
		$amount = '600.00';
		$appUrl = 'http://wbuhs.ac.in/sdm/return-payment';
		$str = $merchantId . '|' . \Auth::user()->id . '|NA|' . $amount . '|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|' . $appUrl;
		$checksum = hash_hmac('sha256', $str, $checksum_key, false);
		$checksum_value = ucwords(strtoupper($checksum));
		$newstr = $str . "|" . $checksum_value;
		$msg = $merchantId . '|' . \Auth::user()->id . '|NA|' . $amount . '|NA|NA|NA|INR|NA|R|' . $securityId . '|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|' . $appUrl;
		return view('frontend.test-payment', compact('merchantId', 'securityId', 'checksum_key', 'checksum', 'checksum_value', 'msg', 'newstr'));

	}

	public function testCheck(Request $request) {
		try {
			if ($request->session()->has('pay_data') && $request->session()->has('pay_type')) {
				$payment = Payment::create(['pay_id' => session('pay_data')->id, 'pay_type' => session('pay_type'), 'pay_amount' => 400, 'transaction_no' => 'fefeff#', 'type' => 'online', 'is_paid' => 1, 'status' => 1]);

				$synopsisOrthesis = $payment->pay;

				//$pdf = PDF::loadView('pdf.synopsis-pdf', compact('synopsis'));
				$pdfName = time() . (session('pay_type') == 'App\Models\Synopsis' ? 'synopsis.pdf' : 'thesis.pdf');
				$synopsisData = session('pay_data');

				/*$this->mPDF->mergePDF($pdfName, $synopsisOrthesis->student_pdf, $synopsisOrthesis->form_pdf);

				$synopsisOrthesis->form_pdf = $pdfName;*/
				if (session('pay_type') == 'App\Models\Synopsis') {
					$synopsisOrthesis->status = 1;
				} else {
					$synopsisOrthesis->status = 1;
				}

				$synopsisOrthesis->save();

				$request->session()->forget('pay_data');
				$request->session()->forget('pay_type');
				$type = ($payment->pay_type == 'App\Models\Synopsis') ? 1 : 2;
				$paidType = 1;

				$data = ['type' => $type, 'synopsis' => $synopsisOrthesis];
				$view = 'emails.synopsis-mail';
				$attach = public_path('pdfs/synopsis/' . $synopsisOrthesis->form_pdf);
				$subject = ($payment->pay_type == 'App\Models\Synopsis') ? 'Synopsis' : 'Dissertation';
				$today_add_three = Carbon::now()->addDay(3)->format('Y-m-d');

				$notifyData = ($payment->pay_type == 'App\Models\Synopsis') ? 'New Synopsis' : 'New Dissertation';

				if ($type == 1) {

					$BOS = User::whereHas('roles', function ($q) {
						$q->where('slug', 'bos');
					})->whereHas('category', function ($q) use ($synopsisData) {
						$q->where('id', $synopsisData->category_id);
					})->get();

					$countAssign = 0;
					foreach ($BOS as $value) {
						$checktransaction = SynopsisTransaction::where('to_user_id', $value->id)->where('review_status', 0)->count();
						if ($checktransaction < 3 && $countAssign < 3) {
							SynopsisTransaction::create(['synopsis_id' => $synopsisData->id, 'to_user_id' => $value->id, 'trans_type' => 1, 'send_to' => $value->email, 'review_status' => 0]);
							Mail::to($value->email)->send(new SendMail(['type' => $type, 'synopsis' => $synopsisOrthesis, 'to_user' => $value], $view, $subject));
							$value->notify(new SendNotify($notifyData));
							$countAssign++;
							sleep(3);
						}

					}
				}

				$superadmins = User::where('is_admin', 1)->get();
				foreach ($superadmins as $key => $value) {
					Mail::to($value->email)->send(new SendMail($data, $view, $subject));
					$value->notify(new SendNotify($notifyData));
				}
				$std_view = 'emails.student-synpoOrthesis';
				// logged in student
				Mail::to(\Auth::user()->email)->send(new SendMail($data, $std_view, $subject));

				\DB::commit();
				Session::flash('msg', 'Sucessfully Paid');

				return redirect('success-message/' . $type . '/' . $paidType);
			}
		} catch (\Exception $e) {
			\DB::rollback();
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	public function returnPayment(Request $request) {
		$msg = explode("|", $_REQUEST['msg']);
		$status = $msg[14];
		if ($status == '0300') {

			\DB::beginTransaction();
			$transaction_no = $msg[2];
			$amount = $msg[4];
			$transDate = $msg[13];
			$appId = !empty($msg[22]) ? $msg[22] : 'WBUHSSDM2018';
			$currentUser = User::find($msg[1]);
			try {
				if ($currentUser && $currentUser->userDetail->current_submission) {
					$payType = $currentUser->userDetail->current_submission == 'synopsis' ? 'App\Models\Synopsis' : 'App\Models\Thesis';
					$payId = $currentUser->userDetail->current_submission == 'synopsis' ? Synopsis::where('user_id', $currentUser->id)->first()->id : Thesis::where('user_id', $currentUser->id)->first()->id;
					$payment = Payment::create(['pay_id' => $payId, 'pay_type' => $payType, 'pay_amount' => $amount, 'transaction_no' => $transaction_no, 'application_id' => $appId, 'transaction_date' => Carbon::parse($transDate)->format('Y-m-d H:i:s'), 'bill_desk_response' => @$_REQUEST['msg'], 'type' => 'online', 'is_paid' => 1, 'status' => 1]);

					$synopsisOrthesis = $payment->pay;

					$pdfName = time() . ($currentUser->userDetail->current_submission == 'synopsis' ? 'synopsis.pdf' : 'thesis.pdf');
					$synopsisData = $currentUser->userDetail->current_submission == 'synopsis' ? Synopsis::where('user_id', $currentUser->id)->first() : Thesis::where('user_id', $currentUser->id)->first();

					if ($currentUser->userDetail->current_submission == 'synopsis') {
						$synopsisOrthesis->status = 1;
					} else {
						$synopsisOrthesis->status = 1;
					}

					$synopsisOrthesis->save();
					if ($request->session()->has('pay_data')) {
						$request->session()->forget('pay_data');
					}
					if ($request->session()->has('pay_type')) {
						$request->session()->forget('pay_type');
					}

					$type = ($currentUser->userDetail->current_submission == 'synopsis') ? 1 : 2;
					$paidType = 1;

					$data = ['type' => $type, 'synopsis' => $synopsisOrthesis];
					$view = 'emails.synopsis-mail';
					$attach = public_path('pdfs/synopsis/' . $synopsisOrthesis->form_pdf);
					$subject = ($payment->pay_type == 'App\Models\Synopsis') ? 'Synopsis' : 'Dissertation';
					$today_add_three = Carbon::now()->addDay(3)->format('Y-m-d');

					$notifyData = ($payment->pay_type == 'App\Models\Synopsis') ? 'New Synopsis' : 'New Dissertation';

					if ($type == 1) {
						$getBos = $this->getBosForAssingSynopsis($synopsisData->category_id, $synopsisData->institute_code);
						$checkExist = SynopsisTransaction::where('synopsis_id', $synopsisData->id)->first();
						$attached_path = public_path('files/Help_manual_for_BOS_review_status.docx');
						if ($getBos && !$checkExist) {
							SynopsisTransaction::create(['synopsis_id' => $synopsisData->id, 'to_user_id' => $getBos->id, 'trans_type' => 1, 'send_to' => $getBos->email, 'review_status' => 0]);
							Mail::to($getBos->email)->send(new SendMail(['type' => 1, 'synopsis' => @$synopsisOrthesis, 'to_user' => $getBos], $view, $subject, $attached_path));

						}
					}
					// for Thesis Assign
					else {
						if (Category::where('id', $synopsisOrthesis->category_id)->parent != 60) {
							$getInternalAdjudicator = $this->getAdjudicatorForAssingThesis($synopsisOrthesis->category_id, $synopsisOrthesis->institute_code, $isState = true);
							if ($getInternalAdjudicator) {
								$checkExistInternal = ThesisTransaction::where('thesis_id', $synopsisOrthesis->id)->where('to_user_id', $getInternalAdjudicator->id)->first();

								if ($getInternalAdjudicator && !$checkExistInternal) {
									if ($synopsisOrthesis->transaction()->count() < 3) {
										$this->sendThesisEmail($getInternalAdjudicator, $synopsisOrthesis);
									}
								}
							}
							$getExternale1Adjudicator = $this->getAdjudicatorForAssingThesis($synopsisOrthesis->category_id, $synopsisOrthesis->institute_code, $isState = false);
							if ($getExternale1Adjudicator) {
								$checkExistExternal1 = ThesisTransaction::where('thesis_id', $synopsisOrthesis->id)->where('to_user_id', $getExternale1Adjudicator->id)->first();

								if ($getExternale1Adjudicator && !$checkExistExternal1) {
									if ($synopsisOrthesis->transaction()->count() < 3) {
										$this->sendThesisEmail($getExternale1Adjudicator, $synopsisOrthesis);
									}
								}
							}

							$getExternal2Adjudicator = $this->getAdjudicatorForAssingThesis($synopsisOrthesis->category_id, $synopsisOrthesis->institute_code, $isState = false);
							if ($getExternal2Adjudicator) {
								$checkExistExternal2 = ThesisTransaction::where('thesis_id', $synopsisOrthesis->id)->where('to_user_id', $getExternal2Adjudicator->id)->first();

								if ($getExternal2Adjudicator && !$checkExistExternal2) {
									if ($synopsisOrthesis->transaction()->count() < 3) {
										$this->sendThesisEmail($getExternal2Adjudicator, $synopsisOrthesis);
									}

								}
							}
						}

						$superadmins = User::where('is_admin', 1)->get();
						foreach ($superadmins as $key => $value) {
							$value->notify(new SendNotify($notifyData));
						}
						$std_view = 'emails.student-synpoOrthesis';
						Mail::to($currentUser->email)->send(new SendMail($data, $std_view, $subject));
					}

					\DB::commit();
					Session::flash('msg', 'Sucessfully Paid');

					return redirect('success-message/' . $type . '/' . $paidType);
				}
			} catch (\Exception $e) {
				\DB::rollback();
				Session::flash('error', $e->getMessage());
				return redirect()->back();
			}

		} elseif ($status == '0399') {
			Session::flash('error', 'Your transaction has been canceled!');
			return (session('pay_type') == 'App\Models\Synopsis') ? redirect('synopsis/payment/create') : redirect('dissertation/payment/create');
			// Invalid Authentication

		} elseif ($status == 'NA') {
			Session::flash('error', 'Your transaction has been canceled!');
			return (session('pay_type') == 'App\Models\Synopsis') ? redirect('synopsis/payment/create') : redirect('dissertation/payment/create');
		} elseif ($status == '0002') {
			Session::flash('error', 'Your transaction has been canceled!');
			return (session('pay_type') == 'App\Models\Synopsis') ? redirect('synopsis/payment/create') : redirect('dissertation/payment/create');
		} elseif ($status == '0001') {
			Session::flash('error', 'Your transaction has been canceled!');
			return (session('pay_type') == 'App\Models\Synopsis') ? redirect('synopsis/payment/create') : redirect('dissertation/payment/create');
		}

	}

	public function setRegistrationNo(Request $request) {
		$value = trim(strtoupper($request->value));
		$studentData = StudentData::where(DB::raw("TRIM(name)"), $value)->where('status', 1)->first();
		if ($studentData) {
			return response()->json(['status' => 200, 'data' => $studentData->registration_no]);
		} else {
			return response()->json(['status' => 422, 'status_text' => 'Student name does not found']);
		}
	}

	public function checkRegistration(Request $request) {
		$studentData = StudentData::where('registration_no', $request->value)->first();
		if ($studentData) {
			return response()->json(['status' => 200, 'status_text' => 'Registration no. has matched!']);
		} else {
			return response()->json(['status' => 422, 'status_text' => 'Registration no. does not match!']);
		}
	}

	public function getSudentPayment() {
		$title = 'Payment History';
		$synopsisPayment = \Auth::user()->studentSynopsis ? \Auth::user()->studentSynopsis->payements()->first() : '';
		$dissertationPayment = \Auth::user()->studentThesis ? \Auth::user()->studentThesis->payements()->where('is_paid', 1)->first() : '';
		return view('frontend.student-payment-history', compact('title', 'synopsisPayment', 'dissertationPayment'));
	}

	public function sendPaymentReport() {
		$file_name = 'payment-transaction.txt';
		/*$file = fopen(storage_path('app/' . $file_name), "w");
			fwrite($file, "");
		*/
		file_put_contents(storage_path('app/' . $file_name), '');
		$previousday = Carbon::yesterday()->format('Y-m-d');
		$previousDayPayments = Payment::whereDate('transaction_date', $previousday)->where('status', 1)->get();
		foreach ($previousDayPayments as $value) {
			$data = $value->transaction_no . ',' . $value->pay->user_id . ',' . sprintf('%0.2f', $value->pay_amount) . ',' . Carbon::parse($value->transaction_date)->format('Ymd');
			$file = fopen(storage_path('app/' . $file_name), "a");
			fwrite($file, $data . "\n");
			fclose($file);
			//file_put_contents(storage_path('app/' . $file_name), $data . "\n", FILE_APPEND);
		}
		if (count($previousDayPayments)) {
			$attachedPath = storage_path('app/payment-transaction.txt');
			$data = [];
			$view = 'emails.payment-transaction-detail';
			$subject = 'Payment Report on ' . Carbon::yesterday()->format('Y-m-d') . ' for SDM';
			Mail::to('sandipan.deb@billdesk.com')->cc(['suvhankarpal@wtl.co.in', 'asst.registrar@wbuhs.ac.in', 'registrar@wbuhs.ac.in', 'bappaditya.dasgupta@gmail.com', 'iamindrajitmondal@gmail.com', 'afowbuhs@gmail.com', 'prog.officer@wbuhs.ac.in', 'abinash.misra@billdesk.com', 'abinashmisra@yahoo.com', 'jayanta.spider@gmail.com', 'soma.biswas@billdesk.com', 'bhanja.shamba@gmail.com', 'supriyo.ghosh97@gmail.com', 'subhajit360@gmail.com', 'saha.chiranjit1985@gmail.com', 'arnab.paul@billdesk.com'])->send(new SendMail($data, $view, $subject, $attachedPath));
		}

	}

	private function getBosForAssingSynopsis($category, $college) {
		$BOS_members = User::whereHas('roles', function ($q) {
			$q->where('slug', 'bos');
		})->whereHas('category', function ($q) use ($category) {
			$q->where('id', $category);
		})->whereHas('userDetail', function ($q) use ($college) {
			$q->where('college_code', '!=', $college);
		})->pluck('id')->toArray();

		$notAssignUsers = User::whereIn('id', $BOS_members)->doesntHave('synopsisTransaction')->first();
		if ($notAssignUsers) {
			return $notAssignUsers;
		}

		$users = \DB::table('synopsis_transactions')
			->select(\DB::raw('count(*) as assing_synopsis, to_user_id'))
			->whereIn('to_user_id', $BOS_members)
			->groupBy('to_user_id')
			->orderBy('assing_synopsis', 'asc')
			->first();
		return $users ? User::find($users->to_user_id) : '';
	}

	public function getAdjudicatorForAssingThesis($category, $college, $isState) {
		$notToAssignAdjudicatorIds = \DB::table('thesis_transactions')->selectRaw("to_user_id, COUNT(to_user_id) as count_thesis")->whereRaw("`accknowledge_email_date` >='2021-04-15' and `deleted_at` is null  GROUP BY `to_user_id` HAVING COUNT(to_user_id) >4")->pluck('to_user_id')->toArray();
		$ADJUDIC_members = User::whereNotIn('id', $notToAssignAdjudicatorIds)->whereHas('roles', function ($q) {
			$q->where('slug', 'adjudicator');
		})->whereHas('category', function ($q) use ($category) {
			$q->where('id', $category);
		})->whereHas('userDetail', function ($q) use ($college, $isState) {
			if ($isState) {
				$q->where('college_code', '!=', $college);
				$q->where('state_id', '=', 28);
			} else {
				$q->where('state_id', '!=', 28);
			}
		})->pluck('id')->toArray();

		//dd($ADJUDIC_members);

		$notAssignUsers = User::whereIn('id', $ADJUDIC_members)->
			whereDoesntHave('thesisTransction', function ($q) {
			$q->whereDate('accknowledge_email_date', '>=', '2021-04-15');
		})->first();

		if ($notAssignUsers) {
			return $notAssignUsers;
		}

		$users = \DB::table('thesis_transactions')
			->select(\DB::raw('count(*) as assing_thesis, to_user_id'))
			->whereIn('to_user_id', $ADJUDIC_members)
			->groupBy('to_user_id')
			->whereDate('accknowledge_email_date', '>=', '2021-04-15')
			->orderBy('assing_thesis', 'asc')
			->first();

		//dd($users);
		return $users ? User::find($users->to_user_id) : '';
	}

	public function sendThesisEmail($uservalue, $thesis) {
		$thesisCount = ThesisTransaction::where('thesis_id', $thesis->id)->get();
		if (count($thesisCount) < 3) {
			$string = 15;
			$result = bin2hex(random_bytes($string));
			$subject = 'Acknowledgement mail for Assign Dissertation ';
			$to = $uservalue->email;
			$confirmationLink = $result . $uservalue->id . date('YmdHis');

			$data = ['type' => 2, 'thesis' => $thesis, 'confirmationLink' => $confirmationLink, 'userId' => $uservalue->id];

			Mail::send('emails.dissertation-assign-confirmation', $data, function ($message) use ($to, $subject) {
				$message->to($to)->subject
					($subject);
				$message->attach(url('public/files/Help_manual_for_Adjudicator_level_review_of_Dissertations.docx'));
				$message->from(config('mail.from.address'), 'WBUHS');
			});

			$thesisTrans = ThesisTransaction::where('to_user_id', $uservalue->id)->where('thesis_id', $thesis->id)->where('is_active', 1)->first();
			if ($thesisTrans) {

				$thesisTrans->accknowledge_email_date = Carbon::now();
				$thesisTrans->accknowledge_link = $confirmationLink;
				$thesisTrans->count_send = $thesisTrans->count_send + 1;
				$thesisTrans->updated_by = 1; //Auth::guard('admin')->user()->id;
				$thesisTrans->save();
			} else {
				$uservalue->count_thesis += 1;
				$uservalue->save();
				ThesisTransaction::insert(['thesis_id' => $thesis->id, 'to_user_id' => $uservalue->id, 'trans_type' => 1, 'send_to' => $uservalue->email, 'accknowledge_link' => $confirmationLink, 'count_send' => 1, 'accknowledge_email_date' => Carbon::now(), 'is_active' => 1, 'created_by' => 1]);
			}
		}

	}

}
