<?php

namespace App\Http\Controllers;
use App\Http\Resources\StudentData as StudentDataResource;
use App\Models\StudentData;
use Illuminate\Http\Request;

class ApiServiceController extends Controller {
	public function addStudentDetail(Request $request) {
		try {
			$studentExist = StudentData::where('registration_no', $request->registration_no)->first();
			if (!$studentExist) {
				$student = StudentData::create(['name' => $request->name, 'registration_no' => $request->registration_no, 'course_id' => $request->course_id, 'institute' => $request->institute, 'phone' => $request->phone, 'email' => $request->email, 'ac_year' => $request->academic_year, 'is_from_api' => 1]);
				return response()->json(['status' => 200, 'data' => ['name' => $student->name, 'email' => $student->email, 'phone' => $student->phone, 'registration_no' => $student->registration_no, 'course_id' => $student->course_id, 'academic_year' => $student->ac_year, 'institute' => $student->institute]]);
			} else {
				return response()->json(['status' => 500, 'error' => 'student is exist']);
			}
		} catch (\Exceptions $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}

	}
	public function validateCompleteDissertation($regis_no, $course_id) {
		$student = StudentData::where('registration_no', $regis_no)->where('course_id', $course_id)->first();
		if ($student) {
			return response()->json(['status' => 200, 'data' => new StudentDataResource($student)]);
		} else {
			return response()->json(['status' => 404, 'error' => 'Student not found']);
		}

	}
}
