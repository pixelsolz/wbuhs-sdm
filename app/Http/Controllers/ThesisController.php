<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Models\Category;
use App\Models\CollegeMaster;
use App\Models\StudentData;
use App\Models\Thesis;
use App\Models\ThesiscycleMaster;
use App\Models\ThesisPdf;
use App\Models\ThesisReviewerResubmitHistories;
use App\Models\ThesisReviewerResult;
use App\Models\ThesisTransaction;
use App\Services\MPdfService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Image;
use Mail;
use Session;
use Validator;
use View;

class ThesisController extends Controller {
	public $mPDF;
	public function __construct(MPdfService $mPDF) {
		$this->mPDF = $mPDF;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Student | Dissertation';
		//$synopsis = Auth::user()->studentSynopsis;
		$thesisCycle = ThesiscycleMaster::all();
		$categories = $this->categoryTree();
		$parentCategory = Category::where('parent', 0)->get();
		$college_masters = CollegeMaster::all();
		return view('frontend.thesis.create', compact('title', 'parentCategory', 'categories', 'thesisCycle', 'college_masters'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$messages = [
			'registration_no.required' => 'Enter Registration No',
			'registration_no.unique' => 'Registration No. Already Used',
		];
		$validator = Validator::make($request->all(), [
			//'category_id' => 'required',
			'name_of_student' => 'required',
			'registration_no' => 'required|unique:thesis',
			'registration_year' => 'required',
			'institute_name' => 'required',
			'mobile_landline' => 'required',
			'guide_name' => 'required',
			'email' => 'email',
			'guide_designation' => 'required',
			'proposed_title_thesis' => 'required',
			'proposed_work_place' => 'required',
			'student_pdf_file' => 'required',
		], $messages);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		if (!Auth::user()->userDetail->registration_no) {
			$studentData = StudentData::where('registration_no', $request->registration_no)->where(DB::raw("TRIM(name)"), $request->name_of_student)->first();

			if (!$studentData) {
				Session::flash('error', 'Registration no. and/or your name mismatch as per records.');
				return redirect()->back()->withInput();
			}
		}

		$checkHasEnabled = StudentData::where('registration_no', $request->registration_no)->latest('id')->first();
		if ($checkHasEnabled && $checkHasEnabled->status == 0) {
			Session::flash('error', 'Submission period has been end');
			return redirect()->back()->withInput();
		}

		if (Thesis::where('registration_no', $request->registration_no)->first()) {
			Session::flash('error', 'This registration no has already used!');
			return redirect()->back()->withInput();
		}

		$mytime = Carbon::now('Asia/Kolkata');
		$currentDate = $mytime->timestamp;
		$lateSubmitDate = Carbon::parse('2022-01-01')->timestamp;
		if ($currentDate >= $lateSubmitDate) {
			Session::flash('error', 'Submission period has been end');
			return redirect()->back()->withInput();
		}

		try {
			$user_id = Auth::user()->id;
			$userDetail = Auth::user()->userDetail;
			$userDetail->current_submission = 'thesis';
			$userDetail->save();
			$synopsis = Auth::user()->studentSynopsis;
			$thesis = Thesis::create($request->only(['name_of_student', 'institute_name', 'registration_no', 'registration_year', 'institute_name', 'mobile_landline', 'email', 'guide_name',
				'guide_designation', 'co_guide_name', 'co_guide_designation', 'proposed_title_thesis', 'proposed_work_place']) + ['user_id' => $user_id, 'category_id' => $request->course_name]);
			//$thesis = Thesis::forceCreate($request->except('student_pdf', '_token', 'signUpload', 'category_id', 'student_pdf_file', 'student_pdf_more', 'sub_category', 'course_name') + ['user_id' => $user_id, 'category_id' => $request->course_name]);
			if (!empty($request->institute_name)) {
				$college = CollegeMaster::where('id', $request->input('institute_name'))->first();
				$thesis->institute_id = $college->id;
				$thesis->institute_name = $college->college_name;
				$thesis->institute_code = $college->college_code;
				$thesis->save();
			}

			if ($request->has('student_signature')) {
				$thesis->student_signature = $request->student_signature;
				$thesis->save();
			}
			if ($request->hasFile('signUpload')) {
				$signImage = $request->file('signUpload');
				$signfilename = time() . $signImage->getClientOriginalName();
				//$signImage->move(public_path('signatures'), $signfilename);
				$image_resize = Image::make($signImage->getRealPath());
				$image_resize->resize(480, 110, function ($constraint) {
					//$constraint->aspectRatio();
				})->save(public_path('signatures/' . $signfilename));
				$thesis->student_signature = $signfilename;
				$thesis->save();
			}

			if ($request->hasFile('student_pdf_more')) {
				$filenames = '';
				foreach ($request->file('student_pdf_more') as $value) {
					$file = $value;
					$other_filename = time() . 'additional_' . $file->getClientOriginalName();
					$filenames .= $other_filename . ',';
					$file->move(public_path('pdfs'), $other_filename);
				}
				$thesis->other_pdfs = rtrim($filenames, ',');
				$thesis->save();
			}

			if ($request->has('student_pdf_file')) {
				$thesis->student_pdf = $request->student_pdf_file;
				$thesis->save();
				ThesisPdf::create(['thesis_id' => $thesis->id, 'thesis_pdf' => $request->student_pdf_file]);
			}

			/*if ($request->hasFile('student_pdf')) {
				$pdfFile = $request->file('student_pdf');
				$filename = time() . $pdfFile->getClientOriginalName();
				$thesis->student_pdf = $filename;
				$thesis->save();
				$pdfFile->move(public_path('pdfs'), $filename);
				ThesisPdf::create(['thesis_id' => $thesis->id, 'thesis_pdf' => $filename]);
			}*/

			$pdfName = time() . 'thesis.pdf';
			$file = View::make('pdf.thesis-pdf', compact('thesis'));
			$this->mPDF->savePDF($file, $pdfName);
			$thesis->form_pdf = $pdfName;
			$thesis->save();

			$sessionArray = ['pay_data' => $thesis, 'pay_type' => 'App\Models\Thesis'];
			session($sessionArray);
			return redirect()->route('thesis.payment.create');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$title = 'Student | Dissertation View';
		$user = Auth::user();
		$categories = $this->categoryTree();
		$thesis = $user->studentThesis;
		$course = Category::where('id', $thesis->category_id)->first();
		if ($user->studentThesis && $user->studentThesis->status <= 4) {
			$thesisCycle = ThesiscycleMaster::whereIn('id', [1, 2, 3, 4])->get();
		} else {
			$thesisCycle = ThesiscycleMaster::whereIn('id', [6, 7, 8, 9])->get();
		}
		return view('frontend.thesis.edit', compact('title', 'categories', 'thesis', 'thesisCycle', 'course'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Student | Dissertation Edit';
		$user = Auth::user();
		$categories = $this->categoryTree();
		$thesis = $user->studentThesis;
		$course = Category::where('id', $thesis->category_id)->first();
		if ($user->studentThesis && $user->studentThesis->status <= 4) {
			$thesisCycle = ThesiscycleMaster::whereIn('id', [1, 2, 3, 4])->get();
		} else {
			$thesisCycle = ThesiscycleMaster::whereIn('id', [6, 7, 8, 9])->get();
		}
		return view('frontend.thesis.edit', compact('title', 'categories', 'thesis', 'thesisCycle', 'course'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		try {
			$thesis = Thesis::find($id);
			if ($request->hasFile('signUpload')) {
				$signImage = $request->file('signUpload');
				$signfilename = time() . $signImage->getClientOriginalName();
				$signImage->move(public_path('signatures'), $signfilename);
				$thesis->student_signature = $signfilename;
				$thesis->save();
			}
			if ($request->has('student_signature')) {
				$thesis->student_signature = $request->student_signature;
				$thesis->save();
			}

			if ($request->hasFile('student_pdf_more')) {
				$filenames = '';
				foreach ($request->file('student_pdf_more') as $value) {
					$file = $value;
					$other_filename = time() . 'additional_' . $file->getClientOriginalName();
					$filenames .= $other_filename . ',';
					$file->move(public_path('pdfs'), $other_filename);
				}
				$thesis->other_pdfs = rtrim($filenames, ',');

				$thesis->save();
			}

			if ($request->has('student_pdf_file')) {
				$thesis->student_pdf = $request->student_pdf_file;
				$thesis->save();
				ThesisPdf::create(['thesis_id' => $thesis->id, 'thesis_pdf' => $request->student_pdf_file]);
			}

			$thesis->status = 6;
			$thesis->is_resubmit = $thesis->is_resubmit + 1;
			$thesis->re_submit = '0';
			$thesis->save();
			$data = ['text' => 'Dissertation Resubmit', 'thesis' => $thesis];
			$view = 'emails.thesis-resubmit';
			$subject = 'Dissertation Resend';
			$reviewResult = $thesis->thesisResult()->where('final_comment', 'D')->get();

			foreach ($reviewResult as $value) {
				Mail::to($value->faculty->email)->send(new SendMail($data, $view, $subject));
				ThesisTransaction::where('thesis_id', $value->thesis_id)->where('to_user_id', $value->faculty_id)->update(['status' => 2, 'last_email_date' => Carbon::now(), 'is_reassigned' => 1]);
				$dataVal = json_decode(json_encode($value), true);
				$reviewdata = collect($dataVal)->except('id');
				ThesisReviewerResubmitHistories::create($reviewdata->all() + ['review_result_id' => $value->id]);
				$value->update(['grade' => '', 'lacunae_knowledge' => '', 'lacunae_knowledge_remarks' => '',
					'present_study' => '', 'present_study_remarks' => '', 'hypothesis' => '', 'hypothesis_remarks' => '', 'eom' => '', 'eom_remarks' => '', 'relevance_remarks' => '', 'clarity_specificity' => '', 'clarity_specificity_remarks' => '', 'relevance' => '',
					'completeness' => '', 'completeness_remarks' => '', 'current_uptodate' => '', 'current_uptodate_remarks' => '',
					'psps' => '', 'psps_remarks' => '', 'type_study' => '', 'type_study_remarks' => '', 'inclution_exclution' => '',
					'inclution_exclution_remarks' => '', 'outcome_parameters' => '', 'outcome_parameters_remarks' => '',
					'study_variables' => '', 'study_variables_remarks' => '', 'sampling_design' => '', 'sampling_design_remarks' => '', 'materials_experimental' => '', 'materials_experimental_remarks' => '', 'data_collection_tools' => '', 'data_collection_tools_remarks' => '', 'statistical_methods' => '', 'statistical_methods_remarks' => '', 'logical_organization' => '', 'logical_organization_remarks' => '', 'correctness_data_analysis' => '', 'correctness_data_analysis_remarks' => '', 'appropriate_tables' => '', 'appropriate_tables_remarks' => '', 'statistical_interpretation' => '', 'interpretation_results_remarks' => '', 'summary_conclusions' => '', 'summary_conclusions_remarks' => '', 'discussion_relevance_remarks' => '', 'interpretation_results' => '', 'limitations' => '', 'limitations_remarks' => '', 'citation_appropriate' => '', 'citation_appropriate_remarks' => '', 'iecc' => '', 'iecc_remarks' => '', 'final_comment' => '', 'reason_reject' => '', 'alternate_reason' => '', 'full_evaluator_signature' => '', 'evaluator_signature' => '', 'suggestion' => '', 'transaction_reassigned_group' => '']);
			}

			/*foreach ($thesis->transaction as $value) {

				Mail::to($value->user->email)->send(new SendMail($data, $view, $subject));

				$value->status = 2;
				$value->last_email_date = Carbon::now();
				$value->count_send = $value->count_send + 1;
				//$value->is_active = 1;
				$value->is_reassigned = 1;
				$value->reassigned_group = $value->reassigned_group + 1;
				$value->save();

				$value->user->notify(new SendNotify('Dissertation Reassigned'));
			}*/

			return redirect()->route('dashboard.index');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	protected function categoryTree($parent = 0, $spacing = '', $user_tree_array = '') {
		if (!is_array($user_tree_array)) {
			$user_tree_array = array();
		}

		$query = DB::select(DB::raw("select * from `wb_categories` WHERE 1 AND `parent` = $parent order by id asc"));

		if (count($query) > 0) {
			foreach ($query as $row) {
				$user_tree_array[] = array("id" => $row->id, "name" => $spacing . $row->name);
				$user_tree_array = $this->categoryTree($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);

			}
		}
		return $user_tree_array;

	}

	public function saveSignature(Request $request) {
		try {
			$image = $request->image;
			$image = str_replace('data:image/jpeg;base64,', '', $image);
			$image = str_replace(' ', '+', $image);
			$imageName = time() . '.' . 'jpeg';
			\File::put(public_path('signatures') . '/' . $imageName, base64_decode($image));
			return response()->json(['status' => 200, 'image' => $imageName]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}

	}

	protected function formValidation($data) {
		$validator = Validator::make($data, [
			'category_id' => 'required',
			'name_of_student' => 'required',
			'registration_no' => 'required',
			'registration_year' => 'required',
			'institute_name' => 'required',
			'mobile_landline' => 'required',
			'guide_name' => 'required',
			'email' => 'email',
			'guide_designation' => 'required',
			'proposed_title_thesis' => 'required',
			'proposed_work_place' => 'required',
			'student_pdf' => 'required | mimes:pdf| max:10000',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
	}

	public function oldThesisPayment($id) {
		//$thesis = Thesis::find($id);
		$thesis = Auth::user()->studentThesis;
		$sessionArray = ['pay_data' => $thesis, 'pay_type' => 'App\Models\Thesis'];
		session($sessionArray);
		return redirect('dissertation/payment/create');
	}

	public function viewPdf(Request $request) {

		ini_set("memory_limit", -1);
		$thesis = Thesis::find($request->id);
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download(public_path('pdfs/' . $thesis->student_pdf), 'dissertation.pdf', $headers);

		/*$thesis = Thesis::find($request->id);
			$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetImportUse();

			$pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $thesis->form_pdf));
			for ($i = 1; $i < $pagecount; $i++) {
				$tplId = $mpdf->ImportPage($i);
				$mpdf->UseTemplate($tplId);
				$mpdf->AddPage();
			}
		*/
	}

	public function viewOherpdf($pdf) {
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download(public_path('pdfs/' . $pdf), 'synopsis.pdf', $headers);

		/*$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetImportUse();
			$pagecount = $mpdf->SetSourceFile(public_path('pdfs/' . $pdf));
			if ($pagecount == 1) {
				$tplId = $mpdf->ImportPage($pagecount);
				$mpdf->UseTemplate($tplId);
			} else {
				for ($i = 1; $i < $pagecount; $i++) {
					$tplId = $mpdf->ImportPage($i);
					$mpdf->UseTemplate($tplId);
					$mpdf->AddPage();
				}
			}

		*/
	}

	public function thesisFileResubmit($id, Request $request) {
		$thesis = Thesis::find($id);
		if (!empty('student_pdf_file')) {
			$thesis->student_pdf = $request->student_pdf_file;
			$thesis->save();
			ThesisPdf::create(['thesis_id' => $thesis->id, 'thesis_pdf' => $request->student_pdf_file]);
		}
		if ($request->hasFile('student_pdf_more')) {
			$filenames = '';
			foreach ($request->file('student_pdf_more') as $value) {
				$file = $value;
				$other_filename = time() . 'additional_' . $file->getClientOriginalName();
				$filenames .= $other_filename . ',';
				$file->move(public_path('pdfs'), $other_filename);
			}
			$thesis->other_pdfs = rtrim($filenames, ',');
			$thesis->save();
		}
		$thesis->re_submit = 0;
		$thesis->save();

		Session::flash('msg', 'Sucessfully updated dissertation');
		return redirect()->route('dashboard.index');
	}

	public function submittedStatus(Request $request) {
		$thesis = Thesis::find($request->thesis_id);
		try {
			$data = [];
			$reason_reject = '';
			$alternate_reason = '';
			if ($thesis->transaction->count()) {

				foreach ($thesis->transaction()->where('status', '!=', 3)->get() as $value) {
					if ($value->is_reassigned == 1) {
						$grade = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->grade_text : 'Revision';
						$reAssigned = $value->thesis->thesisResubmitHistory()->where('faculty_id', $value->to_user_id)->first() ? 1 : 0;
						$reviewId = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->where('transaction_reassigned_group', $value->reassigned_group)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->where('transaction_reassigned_group', $value->reassigned_group)->first()->id : '';
						$reviewUrl = $reviewId ? url('admin/student-result/view', $reviewId) : '';
						$suggestion = '';

					} else {
						$grade = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->grade_text : '';
						$reAssigned = $value->thesis->thesisResubmitHistory()->where('faculty_id', $value->to_user_id)->first() ? 1 : 0;
						$reviewId = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->id : '';
						$reviewUrl = $reviewId ? url('admin/student-result/view', $reviewId) : '';
						$suggestion = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->suggestion : '';

						$reason_reject = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->reason_reject : '';
						$alternate_reason = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->alternate_reason : '';
					}

					$data[] = ['id' => $value->id, 'name' => $value->user->full_name, 'submitted' => ($value->status == 4) ? 'yes' : 'no', 'grade' => $grade, 'reason_reject' => $reason_reject, 'alternate_reason' => $alternate_reason, 'reviewUrl' => $reviewUrl, 'suggestion' => $suggestion, 'reAssigned' => $reAssigned];

				}
			}
			return response()->json(['data' => $data, 'status' => 'success']);

		} catch (\Exception $e) {
			return response()->json(['status' => 'error', 'status_text' => $e->getMessage()]);
		}

	}

	public function viewReasonRejectDownload(Request $request) {

		if ($request->type == 'thesis') {
			$reviewerResult = ThesisReviewerResult::find($request->id);
			return response()->download(public_path('pdfs/' . $reviewerResult->alternate_reason));
		} elseif ($request->type == 'synopsis') {
			$synopsis = Synopsis::find($request->id);
			return response()->download(public_path('pdfs/' . $synopsis->reason_reject_doc));
		}
	}

}
