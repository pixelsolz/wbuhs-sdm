<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\Category;
use App\Models\Cities;
use App\Models\FacultyPayment;
use App\Models\User;
use App\Models\UserDetail;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Image;
use Mail;
use Session;
use Validator;

class AuthController extends Controller {
	public function __construct() {
		$this->middleware('guest:admin')->except('logout');
	}

	public function showRegisterForm() {
		$title = 'Adjudicator | Registration';
		$categories = Category::all();
		$cities = Cities::all();
		return view('admin.register-form', compact('title', 'categories', 'cities'));
	}

	public function storeRegistration(Request $request) {
		$message = [
			'f_name.required' => 'The first name field required.',
			'l_name.required' => 'The last name field required.',
			'recaptcha' => 'Please ensure that you are a human!',
		];
		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			'mobile' => 'required',
			'address' => 'required',
			'gender' => 'required',
			//'account_holder_name' => 'required',
			'category' => 'required',
			//'account_no' => 'required',
			//'bank_name' => 'required',
			//'ifs_code' => 'required',
			'email' => 'required|email',
			'password' => 'required|confirmed|min:6',
			'city_id' => 'required',
			//'g-recaptcha-response' => 'required',
		], $message);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		\DB::beginTransaction();
		try {
			$user = User::create($request->only(['f_name', 'l_name', 'email']) + ['password' => bcrypt($request->password)]);

			$userDetail = UserDetail::create($request->only(['mobile', 'address', 'gender', 'city_id']) + ['user_id' => $user->id]);

			$token = md5($user->email . time());
			$data = ['email' => $user->email, 'token' => $token];
			$view = 'emails.reviewer-registration';
			$subject = 'Reviewer Creation';
			$user->is_admin = 2;
			$user->roles()->attach(2);
			$user->register_token = $token;
			$user->save();

			if ($request->has('account_holder_name')) {
				$array = [];
				$array['account_holder_name'] = $request->account_holder_name;
				$array['account_no'] = $request->account_no;
				$array['bank_name'] = $request->bank_name;
				$array['ifs_code'] = $request->ifs_code;
				FacultyPayment::create(['faculty_id' => $user->id, 'bank_info' => json_encode($array)]);
			}

			if ($request->hasFile('profile_image')) {
				$image = $request->file('profile_image');
				$filename = $image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());
				$image_resize->resize(150, 150, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('images/' . time() . $filename));
				$userDetail->profile_image = 'images/' . time() . $filename;
				$userDetail->save();
			}

			if ($request->has('category')) {
				$user->category()->attach($request->input('category'));
			}

			Mail::to($user->email)->send(new SendMail($data, $view, $subject));

			\DB::commit();
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.messages');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			\DB::rollback();
			return redirect()->back()->withInput();
		}

		//dd($request->all());
	}

	public function registerSuccess() {
		return view('admin.messages');
	}

	public function emailVerifyLogin(Request $request, $token) {

		$user = User::where('register_token', $token)->whereNull('email_verified_at')->first();
		if ($user) {
			$user->email_verified_at = Carbon::now();
			$user->register_token = '';
			$user->save();
			Auth::guard('admin')->login($user);
			return redirect()->route('admin.dashboard');
		} else {
			return view('errors.unauthorize');
		}
	}

	public function showLoginForm() {
		$title = 'Admin | Login';
		return view('admin.login-form', compact('title'));
	}

	public function storeLogin(Request $request) {

		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		if (!Auth::guard('admin')->attempt(['email' => $request->email,
			'password' => $request->password])) {

			Session::flash('msg', ['status' => 'danger', 'msgs' => "Email and password doesn't match!"]);
			return redirect()->back()->withInput();
		}

		Session::flash('msg', 'Welcome to Dashboard ' . Auth::guard('admin')->user()->name);

		return redirect()->route('admin.dashboard');
	}

	public function forgetpassword() {
		$title = 'Forget password';
		return view('admin.forget-password', compact('title'));
	}

	public function forgetPasswordStore(Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		$user = User::where('email', $request->email)->first();
		if ($user) {
			$token = md5($request->email . time());
			DB::table('password_resets')->insert(['email' => $user->email, 'token' => $token]);
			$data = ['text' => 'User Password', 'token' => $token];
			$view = 'emails.admin-forget-password';
			$subject = 'Forgot Password';
			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			Session::flash('msg', ['status' => 'success', 'msgs' => 'Reset password link has been sent to your email']);
			return redirect('admin/login');
		} else {
			Session::flash('error', ['status' => 'danger', 'msgs' => "Email doesn't exist!"]);
			return redirect()->back()->withInput();
		}
	}

	public function showResetForm(Request $request, $token = null) {
		$title = 'Reset Password';
		$exist_token = DB::table('password_resets')->where('token', $token)->first();
		if ($exist_token) {
			return view('admin.reset-password')->with(
				['token' => $token, 'email' => $exist_token->email, 'title' => $title]
			);
		} else {
			return view('errors.unauthorize');
		}

	}

	public function storeResetPassword(Request $request) {
		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:6',
		]);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator);
		}
		$user = User::where('email', $request->email)->first();

		if ($user) {
			$user->password = bcrypt($request->password);
			$user->save();
			DB::table('password_resets')->where('token', $request->token)->update(['token' => NULL]);
			Session::flash('msg', ['status' => 'success', 'msgs' => 'Sucessfully change password']);
			return redirect()->route('admin.login');
		}
	}

	public function logout() {
		Auth::guard('admin')->logout();
		return redirect()->route('admin.login');
	}
}
