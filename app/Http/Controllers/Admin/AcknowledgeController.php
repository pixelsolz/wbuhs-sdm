<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Thesis;
use App\Models\ThesisTransaction;
use App\Models\User;
use Carbon\Carbon;

//use Mail;

class AcknowledgeController extends Controller {
	public function __construct() {
		$this->middleware('guest');
	}

	public function acknowledge($userId, $accknowledge_ment_id) {

		$chk_accknowledge_link = ThesisTransaction::where('to_user_id', $userId)->where('accknowledge_link', $accknowledge_ment_id)->first();
		if ($chk_accknowledge_link) {
			$confirmationLink = $chk_accknowledge_link->accknowledge_link;

			$thesis = Thesis::find($chk_accknowledge_link->thesis_id);
			$userDetails = User::where('id', $userId)->first();

			$chk_accknowledge_link->last_email_date = Carbon::now();
			$chk_accknowledge_link->status = 2;
			$chk_accknowledge_link->save();

			$view = 'emails.dissertation-assign';
			//$attach = public_path('pdfs/synopsis/' . $thesis->form_pdf);
			$subject = 'Dissertation Assign';
			$to = $userDetails->email;
			$data = ['thesis' => $thesis, 'userId' => $userId, 'confirmationLink' => $confirmationLink];

			/*Mail::send('emails.dissertation-assign', $data, function ($message) use ($to, $subject) {
				$message->to($to)->subject
					($subject);
				$message->from('support@wbuhs.ac.in', 'WBUHS');
			});*/

			return view('admin.acknowledge-thank-you', compact('thesis'));
		}
	}

}
