<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Synopsis;
use App\Models\Thesis;
use App\Models\ThesisReviewerResult;
use App\Models\ThesisTransaction;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller {
	public function index() {
		$title = 'Admin | dashboard';
		$user = Auth::guard('admin')->user();
		$dateObj = Carbon::now();
		$dateFrom = $dateObj->subMonths(9)->format('Y-m-d');
		$student = '';
		$pendingThesis = '';
		$completedThesis = '';
		$revissionThesis = '';
		if ($user->is_admin == 1) {
			$synopsis = Synopsis::all();
			$thesis = Thesis::all();
			$student = User::whereHas('roles', function ($q) {
				$q->where('slug', 'student');
			});
			$thesisTranscation = ThesisTransaction::all();
			$thesisCompleteCount = Thesis::where('status', 4)->orWhere('status', 9)->count();
			return view('admin.dashboard', compact('title', 'synopsis', 'thesis', 'student', 'thesisTranscation', 'thesisCompleteCount'));
		} else {
			$synopsis = Synopsis::whereHas('transaction', function ($q) use ($user) {
				$q->where('to_user_id', $user->id);
			});
			$thesis = Thesis::whereHas('transaction', function ($q) use ($user) {
				$q->where('to_user_id', $user->id);
			});

			$pendingThesis = Thesis::where(function ($q) use ($user, $dateFrom) {
				$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
					$q->whereHas('user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});
					$q->where('status', '!=', 4);
					$q->where('accknowledge_email_date', '>=', $dateFrom);
				});
			})->count();
			$completedThesis = Thesis::where(function ($q) use ($user, $dateFrom) {
				$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
					$q->whereHas('user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});
					$q->where('status', 4);
					$q->whereHas('thesis.thesisResult', function ($q) use ($user, $dateFrom) {
						$q->where('faculty_id', $user->id);
						$q->whereIn('final_comment', ['A', 'B', 'C']);
						$q->whereDate('created_at', '>=', $dateFrom);
					});
				});
			})->count();

			$revissionThesis = Thesis::where(function ($q) use ($user, $dateFrom) {
				$q->where('status', '!=', 3);
				$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
					$q->whereHas('user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});
					$q->where('status', 4);
					$q->where(function ($q) use ($user, $dateFrom) {
						$q->whereHas('thesis.thesisResult', function ($q) use ($user, $dateFrom) {
							$q->where('faculty_id', $user->id);
							$q->whereIn('final_comment', ['D', 'E']);
							$q->whereDate('created_at', '>=', $dateFrom);
						});
						$q->orWhere('is_reassigned', 1);
					});
				});
			})->count();

			$submittedCount = ThesisReviewerResult::where('faculty_id', $user->id)->whereNotNull('final_comment')->distinct('thesis_id')->count();

			$thesisCompleteCount = Thesis::whereHas('transaction', function ($q) use ($user) {
				$q->where('to_user_id', $user->id);
			})->where('status', 4)->orWhere('status', 9)->count();
			return view('admin.dashboard', compact('title', 'synopsis', 'thesis', 'student', 'thesisCompleteCount', 'submittedCount', 'pendingThesis', 'completedThesis', 'revissionThesis'));
		}

	}

	public function dashboardData(Request $request) {

		$request->type == 'submitted_reviewer';
		$type = $request->input('type');
		$data = '';
		switch ($type) {
		case 'submitted_reviewer':
			$data = ThesisTransaction::where('status', 4)->get();
			break;
		case 'accepted_reviewer':
			$data = ThesisTransaction::where('status', 2)->get();
			break;
		case 'not_accepted_reviewer':
			$data = ThesisTransaction::where('status', 1)->get();
			break;
		case 'evaluation_pending':
			$data = ThesisTransaction::where('status', 3)->get();
			break;
		default:
			# code...
			break;
		}

		return response()->json(['status' => 200, 'data' => $data]);
	}

}
