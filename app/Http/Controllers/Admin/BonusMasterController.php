<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BonusMaster;
use Illuminate\Http\Request;
use Session;
use Validator;

class BonusMasterController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$title = 'Bonus Master';
		$bonusData = BonusMaster::paginate(10);
		return view('admin.bonus.index', compact('bonusData', 'title'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Bonus Create';
		return view('admin.bonus.add', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		/*$message = [
			'within_days.required' => 'The category field required.',
		];*/
		//dd($request->all());
		$validator = Validator::make($request->all(), [
			'within_days' => 'required',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			BonusMaster::create($request->all());
			Session::flash('success', 'Bonus rule added succesfuly');
			return redirect('admin/bonus');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Bonus Edit';
		$bonus = BonusMaster::find($id);
		return view('admin.bonus.edit', compact('title', 'bonus'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
			'within_days' => 'required',
		]);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$bonus = BonusMaster::find($id);
			$bonus->fill($request->all());
			$bonus->save();
			Session::flash('success', 'Bonus rule edited succesfuly');
			return redirect('admin/bonus');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$bonus = BonusMaster::find($id);
			$bonus->delete();
			return response()->json(['status' => 200, 'status_text' => 'Sucessfuly deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}
}
