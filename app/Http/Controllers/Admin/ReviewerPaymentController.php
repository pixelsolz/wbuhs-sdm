<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PendingReviewerExport;
use App\Exports\ReviewerPendingExport;
use App\Http\Controllers\Controller;
use App\Models\BonusMaster;
use App\Models\FacultyPayment;
use App\Models\FacultyPaymentHistory;
use App\Models\ThesisReviewerResult;
use App\Models\User;
use App\Services\MPdfService;
use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use Session;
use Validator;
use View;

class ReviewerPaymentController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public $mPDF;
	public function __construct(MPdfService $mPDF) {
		$this->mPDF = $mPDF;
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 0);
		ini_set('upload_max_filesize', '1024M');
		ini_set('post_max_size', '1024M');
	}

	public function index(Request $request) {
		$title = 'Reviewer payement List';
		$user = Auth::guard('admin')->user();

		if ($request->has('search')) {

			$facultyPayment = FacultyPayment::whereNotNull('total_amount')->whereHas('faculty', function ($q) use ($request) {
				if ($request->has('input_search') && !empty($request->input_search)) {
					$q->where('f_name', 'like', '%' . $request->input_search . '%');
					$q->orWhere('email', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('userDetail', function ($qe) use ($request) {
						$qe->where('mobile', 'like', '%' . $request->input_search . '%');
					});
				}
			})->orderBy('updated_at', 'asc')->get();
		} else {
			//echo 'Second';

			//$facultyPayment = FacultyPayment::whereNotNull('total_amount')->orderBy('updated_at', 'asc')->get();
			//exit;
			$facultyPayment = FacultyPayment::whereNotNull('total_amount')->orderBy('id', 'asc')->paginate(10)->appends(request()->query());
		}
		return view('admin.reviewer-payment.index', compact('title', 'facultyPayment', 'user', 'request'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Reviewer payment';
		return view('admin.reviewer-payment.create', compact('title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function updatePayment(Request $request) {
		$user = Auth::guard('admin')->user();
		$validator = Validator::make($request->all(), [
			'amount' => 'required|numeric',
		]);
		if ($validator->fails()) {
			return response()->json(array(
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray(),
			), 422);
		}

		try {
			$payment = FacultyPayment::find($request->pay_id);
			//$payment = FacultyPayment::find($request->pay_id);
			if ($request->amount > $payment->total_amount) {
				return response()->json(['status' => 422, 'status_text' => 'paid amount cannot greater than total amount']);
			}
			$payment->total_amount = ((int) $payment->total_amount - (int) $request->amount);
			$payment->save();
			FacultyPaymentHistory::create(['payment_id' => $payment->id, 'paid_amount' => (int) $request->amount, 'faculty_id' => $payment->faculty_id, 'status' => 1, 'created_by' => $user->id]);
			FacultyPaymentHistory::where('faculty_id', $payment->faculty_id)->where('status', 0)->update(['status' => 1]);

			if ((int) $payment->total_amount == 0) {
				$payment->total_amount = NULL;
				$payment->review_amount = NULL;
				$payment->bonus_amount = NULL;
				$payment->is_submitted = NULL;
				$payment->thesis_id = NULL;
				$payment->save();
			}

			return response()->json(['status' => 200, 'status_text' => 'success']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}

	}

	public function thesisRenumeration() {
		$title = 'Renumeration';
		$user = Auth::guard('admin')->user();
		$thesisSql = ThesisReviewerResult::has('thesis')->whereHas('thesis', function ($q) {
			$q->where('status', 4);
		})->where('faculty_id', $user->id)->whereNotNull('final_comment')->orderBy('id', 'desc');
		$bonusAmount = [];

		$thesisreviews = $thesisSql->paginate(10);
		return view('admin.thesis-reviewer.renumeration-list', compact('title', 'user', 'thesisSql', 'thesisreviews'));

	}

	public function renumerationForm(ThesisReviewerResult $th_rr) {
		$title = 'Renumeration Form';
		$thesisreview = $th_rr;
		$user = Auth::guard('admin')->user();
		$tot_bonusAmount = 0;
		if ($thesisreview && $thesisData = $thesisreview->thesis->transaction()->where('to_user_id', $user->id)->first()) {
			$diffDay = Carbon::parse($thesisreview->created_at)->diffInDays(Carbon::parse($thesisData->created_at));

			if ($bonus = BonusMaster::where('within_days', '>=', $diffDay + 1)->first()) {
				$tot_bonusAmount = $bonus->amount;
			}
		}
		$thesisYear = $thesisreview->thesis->registration_year;
		return view('admin.thesis-reviewer.payment-form', compact('title', 'user', 'thesisSql', 'tot_bonusAmount', 'thesisYear', 'thesisreview'));
	}

	public function renumerationStore(Request $request) {
		if (empty($request->examin_year)) {
			Session::flash('year_error', 'Please enter examin Year');
			return redirect()->back();
		}
		if (FacultyPaymentHistory::where('reviewer_result_id', $request->review_id)->first()) {
			Session::flash('year_error', 'already submitted');
			return redirect('admin/renumeration');
		}

		//dd($request->all());
		/*$messsages = array(
				//'examin_year.required' => 'The examin year field is required.',
				'total_amount.required' => 'The total amount field is required.',
				'account_holder_name.required' => 'The account holder name is required.',
				'bank_name.required' => 'The bank name is required.',
				'account_no.required' => 'The account no is required.',
				'ifs_code.required' => 'The IFSC code is required.',
			);

			$validator = Validator::make($request->all(), [
				//'examin_year' => 'required',
				'total_amount' => 'required',
				'account_holder_name' => 'required ',
				'bank_name' => 'required ',
				'account_no' => 'required',
				'ifs_code' => 'required',
			], $messsages);

			if ($validator->fails()) {
				return redirect()->back()
					->withErrors($validator)
					->withInput();
		*/

		$user = Auth::guard('admin')->user();
		$reviewerResult = ThesisReviewerResult::find($request->review_id);
		if ($user->facultyPayment()->count()) {
			$array = [];
			$array['account_holder_name'] = $request->account_holder_name;
			$array['account_no'] = (string) $request->account_no;
			$array['bank_name'] = $request->bank_name;
			$array['ifs_code'] = $request->ifs_code;

			$user->facultyPayment->total_amount = ($user->facultyPayment->total_amount + $request->total_amount);
			$user->facultyPayment->review_amount = $request->review_amount;
			$user->facultyPayment->bonus_amount = $request->bonus_amount;
			$user->facultyPayment->examin_year = $request->examin_year;
			$user->facultyPayment->is_submitted = 1;
			$user->facultyPayment->bank_info = json_encode($array);
			$user->thesis_id = $reviewerResult->thesis_id;
			$user->facultyPayment->save();
			if ($request->has('evaluator_signature')) {
				$user->facultyPayment->evaluator_signature = $request->evaluator_signature;
			} elseif ($request->hasFile('signature_upload1')) {
				$file = $request->file('signature_upload1');
				$signfilename = time() . $file->getClientOriginalName();
				$file->move(public_path('signatures'), $signfilename);
				$user->facultyPayment->upload_sign = $signfilename;
			}
			$user->facultyPayment->save();
		} else {
			$array = [];
			$array['account_holder_name'] = $request->account_holder_name;
			$array['account_no'] = (string) $request->account_no;
			$array['bank_name'] = $request->bank_name;
			$array['ifs_code'] = $request->ifs_code;

			$payment = FacultyPayment::create(['faculty_id' => $user->id, 'total_amount' => $request->total_amount, 'review_amount' => $request->review_amount, 'bonus_amount' => $request->bonus_amount, 'thesis_id' => $reviewerResult->thesis_id, 'bank_info' => json_encode($array), 'is_submitted' => 1]);
			if ($request->has('evaluator_signature')) {
				$payment->evaluator_signature = $request->evaluator_signature;
			} elseif ($request->hasFile('signature_upload1')) {
				$file = $request->file('signature_upload1');
				$signfilename = time() . $file->getClientOriginalName();
				$file->move(public_path('signatures'), $signfilename);
				$payment->upload_sign = $signfilename;
			}
			$payment->save();
		}
		DB::table('faculty_payment_histories')->insert(
			['paid_amount' => $request->review_amount, 'reviewer_result_id' => $request->review_id, 'faculty_id' => $user->id]
		);

		Session::flash('msg', 'Successfully submitted');
		return redirect()->route('admin.dashboard');
	}

	/* CRON Function */
	public function accountsReminder() {
		$namexl = time() . 'account_reminder.xlsx';
		Excel::store(new ReviewerPendingExport, $namexl);
		$data = ['text' => 'Pending Reviewer Payment'];
		$view = 'emails.thesis-status';
		$subject = 'Reminder Thesis';
		$attach = storage_path('app/' . $namexl);
		$accountants = User::whereHas('roles', function ($q) {
			$q->where('slug', 'account');
		})->get();
		foreach ($accountants as $value) {
			Mail::to($value->email)->send(new SendMail($data, $view, $subject, $attach));
		}

	}

	public function updateAllPyments(Request $request) {
		$user = Auth::guard('admin')->user();
		try {
			$values = array_map('intval', explode(',', $request->value_ids));
			foreach ($values as $value) {
				$faculty_pay = FacultyPayment::find($value);
				FacultyPaymentHistory::create(['payment_id' => $faculty_pay->id, 'paid_amount' => (int) $faculty_pay->total_amount, 'faculty_id' => $faculty_pay->faculty_id, 'status' => 1, 'created_by' => $user->id]);
				FacultyPaymentHistory::where('faculty_id', $faculty_pay->faculty_id)->where('status', 0)->update(['status' => 1]);
			}
			FacultyPayment::whereIn('id', $values)->update(['total_amount' => NULL, 'review_amount' => NULL, 'bonus_amount' => NULL, 'is_submitted' => NULL, 'thesis_id' => NULL]);

			return response()->json(['status' => 200, 'status_text' => 'Status changed successfully']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status' => $e->getMessage()]);
		}

	}

	public function exportAccounts(Request $request) {
		$ids = array_map('intval', explode(',', $request->idsValue));
		return Excel::download(new PendingReviewerExport($ids), 'payment.xlsx');
		//return (new PendingReviewerExport($ids))->download('invoices.xlsx');
	}

	public function exportAccountToPDF(Request $request) {
		$ids = array_map('intval', explode(',', $request->idsValuePdf));
		$facultyPayments = FacultyPayment::whereNotNull('total_amount')->whereIn('id', $ids)->get();
		$pdfName = time() . 'accounts.pdf';
		$file = View::make('exports.reviewer-pending', compact('facultyPayments'));
		$this->mPDF->streamPDF($file);
	}
}
