<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminLeave;
use App\Models\Thesis;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Validator;

class AdminLeaveController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$user = Auth::guard('admin')->user();
		$title = 'Availability List';
		if ($request->has('search')) {
			$adminLeaves = AdminLeave::where(function ($q) use ($request) {
				if ($request->has('input_search') && !empty($request->input_search)) {

					$q->whereHas('user', function ($q) use ($request) {
						$q->where('f_name', 'like', '%' . $request->input_search . '%');
						$q->orWhere('email', 'like', '%' . $request->input_search . '%');
						$q->orWhereHas('userDetail', function ($qe) use ($request) {
							$qe->where('mobile', 'like', '%' . $request->input_search . '%');
						});
					});
				}
				if ($request->has('start_date') && !empty($request->start_date)) {
					$st_date = Carbon::parse(str_replace('/', '-', $request->start_date))->format('Y-m-d');
					$q->where('start_date', $st_date);
				}
				if ($request->has('end_date') && !empty($request->end_date)) {
					$en_date = Carbon::parse(str_replace('/', '-', $request->end_date))->format('Y-m-d');
					$q->whereDate('end_date', $en_date);
				}
			})->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
			return view('admin.availability.list', compact('title', 'adminLeaves', 'request'));
		} else {
			$leaves = $user->leaves()->paginate(10);
			if ($user->is_admin == 1) {
				$adminLeaves = AdminLeave::orderBy('id', 'desc')->paginate(10);
				return view('admin.availability.list', compact('title', 'adminLeaves', 'request'));
			} else {
				return view('admin.availability.index', compact('title', 'leaves'));
			}
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$user = Auth::guard('admin')->user();
		$title = 'Availability';
		$leaves = $user->leaves;

		return view('admin.availability.create', compact('title', 'leaves'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$message = [
			'dateValue.required' => 'The Date field required.',
		];
		$validator = Validator::make($request->all(), [
			'dateValue' => 'required',
			'reason' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		//dd($inputDate);
		//dd(Carbon::parse(str_replace('/', '-', $inputDate[0]))->format('Y-m-d'));
		try {

			$user = Auth::guard('admin')->user();
			$inputDate = explode('-', $request->dateValue);
			$leaves = $user->leaves();
			$leaves->create(['user_id' => $user->id, 'start_date' => Carbon::parse(str_replace('/', '-', $inputDate[0]))->format('Y-m-d'), 'end_date' => Carbon::parse(str_replace('/', '-', $inputDate[1]))->format('Y-m-d'), 'reason' => $request->reason]);

			Session::flash('msg', 'Sucessfuly Apply leave');
			return redirect('admin/leave');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$user = Auth::guard('admin')->user();
		$title = 'Availability';
		$leave = AdminLeave::find($id);
		return view('admin.availability.edit', compact('title', 'leave'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		try {
			$adminLeave = AdminLeave::find($id);
			$adminLeave->fill($request->except('dateValue'));
			if ($request->has('dateValue')) {
				$inputDate = explode('-', $request->dateValue);
				$adminLeave->start_date = Carbon::parse(str_replace('/', '-', $inputDate[0]))->format('Y-m-d');
				$adminLeave->end_date = Carbon::parse(str_replace('/', '-', $inputDate[1]))->format('Y-m-d');
			}
			$adminLeave->save();
			Session::flash('msg', 'Sucessfuly update leave');
			return redirect('admin/leave');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function getLeaves() {
		$user = Auth::guard('admin')->user();
		$title = 'Availability';
		$leaves = $user->leaves;

		return response()->json($leaves);
	}

	public function checkAvailability() {
		$thesis = Thesis::find(4);

		$Notavailablefaculties = $thesis->category->user()->whereHas('roles', function ($q) {
			$q->where('slug', 'reviewer');
		})->wherehas('thesisTransction', function ($q) {
			$q->where('status', '>', 1);
		})->pluck('id')->toArray();

		$leavesUser = $thesis->category->user()
			->where(function ($q) use ($Notavailablefaculties) {
				$q->whereHas('roles', function ($q) {
					$q->where('slug', 'reviewer');
				});
				if (count($Notavailablefaculties)) {
					$q->whereNotIn('id', $Notavailablefaculties);
				}
			})->whereHas('leaves', function ($q) {
			$q->whereDate('start_date', '<=', Carbon::now()->format('Y-m-d'));
			$q->whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'));
		})->pluck('id');

		echo $thesis->category->user()
			->where(function ($q) use ($Notavailablefaculties) {
				$q->whereHas('roles', function ($q) {
					$q->where('slug', 'reviewer');
				});
				if (count($Notavailablefaculties)) {
					$q->whereNotIn('id', $Notavailablefaculties);
				}
			})->whereNotIn('id', $leavesUser)->get();
	}

}
