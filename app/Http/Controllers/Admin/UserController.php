<?php

namespace App\Http\Controllers\Admin;

use App\Exports\AssignStudentExport;
use App\Exports\AssignThesisStudentExport;
use App\Exports\NotAssignStudentExport;
use App\Exports\NotAssignThesisStudentExport;
use App\Exports\NotPaidStudentExport;
use App\Exports\PendingThesisExport;
use App\Exports\StatusWiseStudentExport;
use App\Exports\StudentPaymentExport;
use App\Exports\StudentSynopsisExport;
use App\Exports\StudentThesisExport;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Imports\ImportStudent;
use App\Imports\ImportStudentSynopsis;
use App\Mail\SendMail;
use App\Models\AdjudicatorState;
use App\Models\BosMemberPdf;
use App\Models\Category;
use App\Models\Cities;
use App\Models\CollegeMaster;
use App\Models\FacultyPayment;
use App\Models\Payment;
use App\Models\Role;
use App\Models\StateMst;
use App\Models\StudentData;
use App\Models\Support;
use App\Models\Synopsis;
use App\Models\SynopsisStudentData;
use App\Models\SynopsisTitle;
use App\Models\SynopsisTransaction;
use App\Models\TestData;
use App\Models\Thesis;
use App\Models\ThesisCheck;
use App\Models\ThesiscycleMaster;
use App\Models\ThesisPdf;
use App\Models\ThesisReminder;
use App\Models\ThesisReviewerResult;
use App\Models\ThesisTransaction;
use App\Models\User;
use App\Models\UserDetail;
use App\Notifications\SendNotify;
use App\Services\MPdfService;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Image;
use Mail;
use Response;
use Session;
use Validator;

class UserController extends Controller {

	public $mPDF;
	public function __construct(MPdfService $mPDF) {
		$this->mPDF = $mPDF;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	use \App\Traits\CommonTrait;

	public function index(Request $request) {
		$title = 'ADMIN | USERS';
		$categories = $this->categoryTreeDropdown();
		$parent_cats = Category::where('parent', 0)->get();
		if ($request->has('search')) {
			$users = User::where(function ($q) use ($request) {
				if ($request->has('search_by_role') && !empty($request->search_by_role)) {
					$q->whereHas('roles', function ($qu) use ($request) {
						$qu->where('id', $request->search_by_role);
					});
				}
				if ($request->has('input_search')) {
					$q->where(function ($que) use ($request) {
						$que->where('f_name', 'like', '%' . $request->input_search . '%');
						$que->orWhere('email', 'like', '%' . $request->input_search . '%');
						$que->orWhereHas('userDetail', function ($qe) use ($request) {
							$qe->where('mobile', 'like', '%' . $request->input_search . '%');
						});
					});
				}
				if ($request->has('category') && !empty($request->category)) {
					$q->whereHas('category', function ($q) use ($request) {
						if ($request->category == 76) {
							$q->where('parent', $request->category);
							if (!empty($request->child_cat)) {
								$q->where('id', $request->child_cat);
							}
						} else {
							if (!empty($request->sub_cat_ids)) {
								$q->whereIn('parent', explode(',', $request->sub_cat_ids));
							}
							if (!empty($request->child_cat)) {
								$q->where('parent', $request->child_cat);
							}
							if (!empty($request->child_sub_cat)) {
								$q->where('id', $request->child_sub_cat);
							}
						}
					});
				}
			})->withTrashed()->paginate(10)->appends(request()->query());

			/*$users = User::where(function ($q) use ($request) {
				if ($request->has('search_by_role')) {
					//dd($request->all());
					$q->whereHas('roles', function ($q) use ($request) {
						$q->where('id', $request->search_by_role);
					});
				}
				if ($request->has('input_search')) {
					$q->where('f_name', 'like', '%' . $request->input_search . '%')->orWhere('email', 'like', '%' . $request->input_search . '%');
				}

			})->paginate(10)->appends(request()->query());*/
			//echo $users;exit();
			//->paginate(10)->appends(request()->query());
		} else {
			$users = User::orderBy('id', 'desc')->withTrashed()->paginate(10);
		}

		$roles = Role::all();
		return view('admin.user-manage.index', compact('users', 'title', 'roles', 'request', 'parent_cats'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'ADMIN | USER CREATE';
		$roles = Role::all();
		$cities = Cities::all();
		$categories = $this->categoryTreeDropdown();
		$colleges = CollegeMaster::all();
		$states = StateMst::all();
		return view('admin.user-manage.create', compact('title', 'roles', 'categories', 'cities', 'colleges', 'states'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$messsages = array(
			'f_name.required' => 'The Role Name field is required.',
			'l_name.required' => 'The Role Slug field is required.',
			'email.required' => 'The Email field is required.',
			'mobile.required' => 'The Mobile field is required.',
			'address.required' => 'The Mobile field is required.',
			'gender.required' => 'The Mobile field is required.',
			'roles.required' => 'The Role field is required.',
			'state_id.required' => 'The State field is required.',
			'city_id.required' => 'The City field is required.',
			//'account_holder_name' => 'The account holder name is required.',
		);

		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			'email' => 'required|unique:users|email',
			'mobile' => 'required|size:10 |unique:users',
			'address' => 'required',
			'gender' => 'required',
			'roles' => 'required',
			'state_id' => 'required',
			'city_id' => 'required',
			//'account_holder_name' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
			//'account_no' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
			//'bank_name' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
			//'ifs_code' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
		], $messsages);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		\DB::beginTransaction();
		try {

			$generatePassword = substr(md5(mt_rand()), 0, 7);

			$user = User::create($request->only(['f_name', 'l_name', 'email', 'mobile']) + ['password' => bcrypt('user123$')]);

			$userDetail = UserDetail::create($request->only(['mobile', 'address', 'gender', 'state_id', 'city_id']) + ['user_id' => $user->id]);
			if ($request->designation) {
				$userDetail->designation = $request->designation;
				$userDetail->save();
			}
			if (!empty($request->college_name)) {
				$college = CollegeMaster::where('id', $request->input('college_name'))->first();
				$userDetail->college_id = $college->id;
				$userDetail->college_name = $college->college_name;
				$userDetail->college_code = $college->college_code;
				$userDetail->save();
			}
			if (!empty($request->other_college_name)) {

				/*if(empty($request->other_state)){
					Session::flash('error', 'Please select state');
				}*/
				$userDetail->college_name = $request->other_college_name;
				$userDetail->other_state = $request->other_state;
				$userDetail->save();
			}

			$data = ['password' => 'user123$', 'email' => $user->email];
			$view = 'emails.user-create';
			$subject = 'User Creation';

			if ($request->has('roles')) {
				if (in_array(1, $request->input('roles'))) {
					$user->is_admin = 1;
					$user->save();
				} elseif (in_array(2, $request->input('roles'))) {
					$user->is_admin = 2;
					$user->save();
				} elseif (in_array(6, $request->input('roles'))) {
					$user->is_admin = 2;
					$user->save();
				} elseif (in_array(4, $request->input('roles'))) {
					$user->is_admin = 2;
					$user->save();
				} elseif (in_array(5, $request->input('roles'))) {
					$user->is_active = 1;
					$user->save();
				} else {
					$user->is_admin = 2;
					$user->save();
				}
			}

			if ($request->has('account_holder_name')) {
				$array = [];
				$array['account_holder_name'] = $request->account_holder_name;
				$array['account_no'] = $request->account_no;
				$array['bank_name'] = $request->bank_name;
				$array['ifs_code'] = $request->ifs_code;
				FacultyPayment::create(['faculty_id' => $user->id, 'bank_info' => json_encode($array)]);
			}

			if ($request->hasFile('profile_image')) {
				$image = $request->file('profile_image');
				$filename = $image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());
				$image_resize->resize(150, 150, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('images/' . time() . $filename));
				$userDetail->profile_image = 'images/' . time() . $filename;
				$userDetail->save();
			}
			if ($request->has('roles')) {
				$user->roles()->attach($request->input('roles'));
			}

			if ($request->has('category')) {
				$user->category()->attach($request->input('category'));
			}
			$attached_path = '';
			if (in_array(2, $request->input('roles'))) {
				$attached_path = public_path('files/Help_manual_for_Adjudicator_level_review_of_Dissertations.docx');
			}

			Mail::to($user->email)->send(new SendMail($data, $view, $subject, $attached_path));
			$user->email_verified_at = Carbon::now();
			$user->save();

			\DB::commit();
			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('user-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			\DB::rollback();
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'ADMIN | USER SHOW';
		$cities = Cities::all();
		$user = User::withTrashed()->find($id);
		$roles = Role::all();
		$categories = $this->categoryTreeDropdown();
		$userRoles = $user->roles()->pluck('id')->toArray();
		$bank_info = '';
		$bank_info = $user->facultyPayment ? json_decode($user->facultyPayment->bank_info, true) : '';
		$colleges = CollegeMaster::all();
		$states = StateMst::all();
		return view('admin.user-manage.edit', compact('title', 'user', 'roles', 'userRoles', 'categories', 'bank_info', 'cities', 'colleges', 'states'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$messsages = array(
			'f_name.required' => 'The Role Name field is required.',
			'l_name.required' => 'The Role Slug field is required.',
			'email.required' => 'The Email field is required.',
			'mobile.required' => 'The Mobile field is required.',
			'address.required' => 'The Mobile field is required.',
			'gender.required' => 'The Mobile field is required.',
			'roles.required' => 'The Role field is required.',
			//'user_type.required' => 'The User Type field is required.',
			'account_holder_name' => 'The account holder name is required.',
		);

		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			'email' => 'required ',
			'mobile' => 'required ',
			'address' => 'required',
			'gender' => 'required',
			'roles' => 'required',
			//'user_type' => 'required',
			//'account_holder_name' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
			//'account_no' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
			//'bank_name' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
			//'ifs_code' => ($request->has('roles') && in_array('2', $request->input('roles'))) ? 'required' : '',
		], $messsages);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		\DB::beginTransaction();
		try {
			$user = User::withTrashed()->find($id);
			$user->fill($request->only(['f_name', 'l_name', 'email', 'mobile']));
			$user->save();
			if ($user->deleted_at && $request->status == 'Active') {
				$user->restore();
			}
			$userDetail = $user->userDetail->fill($request->only(['mobile', 'address', 'gender', 'state_id', 'city_id']));
			if ($request->designation) {
				$userDetail->designation = $request->designation;
				$userDetail->save();
			}
			$userDetail->save();
			if (!empty($request->college_name)) {
				$college = CollegeMaster::where('id', $request->input('college_name'))->first();
				$userDetail->college_id = $college->id;
				$userDetail->college_name = $college->college_name;
				$userDetail->college_code = $college->college_code;
				$userDetail->save();
			}
			if (!empty($request->other_college_name)) {
				$userDetail->college_name = $request->other_college_name;
				$userDetail->other_state = $request->other_state;
				$userDetail->save();
			}

			$array = [];
			$array['account_holder_name'] = $request->account_holder_name;
			$array['account_no'] = $request->account_no;
			$array['bank_name'] = $request->bank_name;
			$array['ifs_code'] = $request->ifs_code;
			if ($user->facultyPayment) {
				$user->facultyPayment->bank_info = json_encode($array);
				$user->facultyPayment->save();
			} else {
				FacultyPayment::create(['faculty_id' => $user->id, 'bank_info' => json_encode($array)]);
			}

			if ($request->has('user_type')) {
				if ($request->user_type == 1) {
					$user->is_admin = 1;
					$user->save();
				} elseif ($request->user_type == 2) {
					$user->is_admin = 2;
					$user->save();
				}
			}

			if ($request->hasFile('profile_image')) {
				$image = $request->file('profile_image');
				$filename = $image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());
				$image_resize->resize(150, 150, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('images/' . time() . $filename));
				$userDetail->profile_image = 'images/' . time() . $filename;
				$userDetail->save();
			}
			if ($request->has('roles')) {
				$user->roles()->detach();
				$user->roles()->attach($request->input('roles'));
				/*foreach ($request->input('roles') as $key => $value) {
					$user->roles()->attach($value);
				}*/
			}

			if ($request->has('category')) {
				$user->category()->detach();
				$user->category()->attach($request->input('category'));
			}

			\DB::commit();
			Session::flash('msg', 'Sucessfully updated');
			return redirect()->route('user-manage.index');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		try {
			$user = User::find($id);
			$user->delete();
			return response()->json(['status' => 200, 'status_text' => 'Sucessfuly deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function studentSynopsis(Request $request) {
		$title = 'Student Synopsis';
		$user = Auth::guard('admin')->user();
		$synopes = Synopsis::orderBy('id', 'asc')->get();
		/*$categories = Category::where(function ($query) {
			$query->where('has_child', 0);
			$query->orWhereNull('has_child');
		})->orderBy('name')->get();*/
		$categories = $this->categoryTreeDropdown();
		$parent_cats = Category::where('parent', 0)->get();
		if ($request->has('search')) {
			$synopsis = Synopsis::has('payements')->select('synopses.*', 'categories.name as category_name')
				->where(function ($q) use ($request, $user) {
					if ($request->has('search_by_status') && !empty($request->search_by_status)) {
						if ($request->search_by_status == 5) {
							$q->where('synopses.status', 1);
							$q->doesntHave('transaction');
						} else {
							$q->where('synopses.status', $request->search_by_status);
						}
					}
					if ($request->has('input_search') && !empty($request->input_search)) {
						$q->where('synopses.name_of_student', 'like', '%' . $request->input_search . '%');
						$q->orWhere('synopses.registration_no', 'like', '%' . $request->input_search . '%');
						$q->orWhere('synopses.registration_year', 'like', '%' . $request->input_search . '%');
						$q->orWhere('synopses.institute_name', 'like', '%' . $request->input_search . '%');
						$q->orWhere('synopses.mobile_landline', 'like', '%' . $request->input_search . '%');
					}
					if ($user->is_admin != 1 && !in_array('secretarybos', $user->roles()->pluck('slug')->toArray())) {
						$q->whereHas('transaction.user', function ($q) use ($user) {
							$q->where('id', $user->id);
						});
					}
					if ($request->has('category') && !empty($request->category)) {
						$q->whereHas('category', function ($q) use ($request) {
							if ($request->category == 76) {
								$q->where('parent', $request->category);
								if (!empty($request->child_cat)) {
									$q->where('id', $request->child_cat);
								}
							} else {
								if (!empty($request->sub_cat_ids)) {
									$q->whereIn('parent', explode(',', $request->sub_cat_ids));
								}
								if (!empty($request->child_cat)) {
									$q->where('parent', $request->child_cat);
								}
								if (!empty($request->child_sub_cat)) {
									$q->where('id', $request->child_sub_cat);
								}
							}
						});
					}

					if (!empty($request->from_date) && !empty($request->to_date)) {
						$q->whereDate('synopses.created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
						$q->whereDate('synopses.created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
					}

					if (!empty($request->assign_status)) {
						if ($request->assign_status == 1) {
							$q->has('transaction');
						} else {
							$q->doesntHave('transaction');
						}
					}
				})
			/*->when($request->search_by_category, function ($query, $filter) {
					return $query->where('categories.id', $filter);
				})*/
				->leftJoin('categories', 'categories.id', '=', 'synopses.category_id')
				->orderBy('synopses.id', 'desc')
				->paginate(10)
				->appends(request()->query());
		} else {
			$synopsis = Synopsis::has('payements')->select('synopses.*', 'categories.name as category_name')->where(function ($q) use ($user) {
				//&& !in_array('secretarybos', $user->roles()->pluck('slug')->toArray())
				if ($user->is_admin != 1) {
					$q->whereHas('transaction', function ($q) use ($user) {
						$q->where('to_user_id', $user->id);
					});
					/*$q->whereHas('category.user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});*/
				}
			})->leftJoin('categories', 'categories.id', '=', 'synopses.category_id')
				->orderBy('synopses.id', 'desc')->paginate(10)->appends(request()->query());
		}

		/*if ($user->is_admin == 1) {
				$synopsis = Synopsis::orderBy('id', 'desc')->paginate(10);
			} else {
				$synopsis = Synopsis::whereHas('category.user', function ($q) use ($user) {
					$q->where('id', $user->id);
				})->paginate(10);
		*/
		return view('admin.student-synopsis', compact('synopsis', 'title', 'synopes', 'request', 'categories', 'parent_cats'));
	}

	public function withdrawSynopsis(Request $request) {
		SynopsisTransaction::where('synopsis_id', $request->synpId)->where('to_user_id', $request->bosId)->delete();
		return response()->json(['msg' => 'Successfully withdraw', 'status' => 200]);
	}

	public function getSynopTrans(Request $request) {
		$trans = SynopsisTransaction::whereIn('id', explode(',', $request->transaction_ids))->get();
		return response()->json(['data' => $trans, 'status' => 200]);
	}

	public function getSynopCommand($id) {
		$trans = SynopsisTransaction::find($id);
		return response()->json(['data' => $trans->comment, 'status' => 200]);
	}

	/* Assign Synopsis*/

	public function assingSynopsis(Request $request) {
		try {
			$synopsis = Synopsis::find($request->synop_id);
			if ($synopsis->payment_done == 0) {
				return response()->json(['status' => 422, 'status_text' => 'Payment has not made yet']);
			}
			$data = ['type' => 1, 'synopsis' => $synopsis];
			$view = 'emails.synopsis-mail';
			$attach = public_path('pdfs/synopsis/' . $synopsis->form_pdf);
			$subject = 'Student Synopsis';
			$attached_path = public_path('files/Help_manual_for_BOS_review_status.docx');
			if ($request->has('send_to')) {
				$usersId = explode(',', $request->input('send_to'));
				$users = User::whereIn('id', $usersId)->get();
				foreach ($users as $uservalue) {
					Mail::to($uservalue->email)->send(new SendMail($data, $view, $subject, $attached_path));
					$synopTrans = SynopsisTransaction::where('synopsis_id', $synopsis->id)->where('to_user_id', $uservalue->id)->first();
					if ($synopTrans) {
						$synopTrans->updated_by = Auth::guard('admin')->user()->id;
						$synopTrans->save();
					} else {
						SynopsisTransaction::create(['synopsis_id' => $synopsis->id, 'to_user_id' => $uservalue->id, 'trans_type' => 1, 'send_to' => $uservalue->email, 'created_by' => Auth::guard('admin')->user()->id]);
					}

					$uservalue->notify(new SendNotify('Synopsis Resent'));
				}
			}
			return response()->json(['status' => 200, 'status_text' => 'successfully assign']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

	public function sendEmail(Request $request) {
		//dd($request->all());
		try {
			if ($request->type == 'bos') {
				$synopsis = Synopsis::find($request->synop_id);
				$data = ['type' => 1, 'synopsis' => $synopsis];
				$view = 'emails.synopsis-mail';
				$attach = public_path('pdfs/synopsis/' . $synopsis->form_pdf);
				$subject = 'Student Synopsis';
				if ($request->has('send_to')) {
					$usersId = explode(',', $request->input('send_to'));
					$users = User::whereIn('id', $usersId)->get();
					foreach ($users as $uservalue) {
						Mail::to($uservalue->email)->send(new SendMail($data, $view, $subject));
						$synopTrans = SynopsisTransaction::where('synopsis_id', $synopsis->id)->where('to_user_id', $uservalue->id)->first();
						if ($synopTrans) {
							$synopTrans->updated_by = Auth::guard('admin')->user()->id;
							$synopTrans->save();
						} else {
							SynopsisTransaction::create(['synopsis_id' => $synopsis->id, 'to_user_id' => $uservalue->id, 'trans_type' => 1, 'send_to' => $uservalue->email, 'created_by' => Auth::guard('admin')->user()->id]);
						}

						$uservalue->notify(new SendNotify('Synopsis Resent'));
					}
				}
			} else {

				$thesis = Thesis::find($request->synop_id);
				if ($thesis->payment_done == 0) {
					return response()->json(['status' => 422, 'status_text' => 'Payment has not made yet']);
				}

				$view = 'emails.dissertation-assign-confirmation';
				$string = 15;
				$result = bin2hex(random_bytes($string));
				$attach = public_path('pdfs/synopsis/' . $thesis->form_pdf);
				$subject = 'Acknowledgement mail for Assign Dissertation ';
				if ($request->has('send_to')) {
					$usersId = explode(',', $request->input('send_to'));
					$checkThesisSend = $thesis->transaction()->where('status', '!=', 3)->count();
					$sendedUsers = $thesis->transaction()->where('status', '!=', 3)->pluck('to_user_id')->toArray();
					/* If Thesis transaction status 3 then reject */
					if ($thesis->transaction()->where('status', 3)->count()) {
						$thesis->transaction()->where('status', 3)->delete();
					}
					/*$data = ['type' => 2, 'detail' => $transaction->thesis];
						$view = 'emails.withdraw';
						$subject = 'Dissertation Withdraw';
					*/
					/* End */

					$notsendedUsers = array_diff($usersId, $sendedUsers);

					if ((count($notsendedUsers) + $checkThesisSend) > 3) {
						return response()->json(['status' => 422, 'status_text' => 'Already have sent ' . $checkThesisSend . ' reviewers']);
					}

					$users = User::whereIn('id', $notsendedUsers)->get();
					foreach ($users as $uservalue) {
						$to = $uservalue->email;
						$confirmationLink = $result . $uservalue->id . date('YmdHis');
						$data = ['type' => 2, 'thesis' => $thesis, 'confirmationLink' => $confirmationLink, 'userId' => $uservalue->id];

						Mail::send('emails.dissertation-assign-confirmation', $data, function ($message) use ($to, $subject) {
							$message->to($to)->subject($subject);
							$message->attach(url('public/files/Help_manual_for_Adjudicator_level_review_of_Dissertations.docx'));
							$message->from(config('mail.from.address'), 'WBUHS');
						});

						$thesisTrans = ThesisTransaction::where('to_user_id', $uservalue->id)->where('thesis_id', $thesis->id)->where('is_active', 1)->first();
						if ($thesisTrans) {

							$thesisTrans->accknowledge_email_date = Carbon::now();
							$thesisTrans->accknowledge_link = $confirmationLink;
							$thesisTrans->count_send = $thesisTrans->count_send + 1;
							$thesisTrans->updated_by = Auth::guard('admin')->user()->id;
							$thesisTrans->save();
						} else {
							ThesisTransaction::insert(['thesis_id' => $thesis->id, 'to_user_id' => $uservalue->id, 'trans_type' => 1, 'send_to' => $uservalue->email, 'accknowledge_link' => $confirmationLink, 'count_send' => 1, 'accknowledge_email_date' => Carbon::now(), 'is_active' => 1, 'created_by' => Auth::guard('admin')->user()->id]);
						}

						$uservalue->notify(new SendNotify('Dessertation sent'));
					}
					$thesis->status = 2;
					$thesis->save();
				}
			}

			return response()->json(['status' => 200, 'status_text' => 'success']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}
	}

	public function acknowledge($userId, $accknowledge_ment_id) {

		$chk_accknowledge_link = ThesisTransaction::where('to_user_id', $userId)->where('accknowledge_link', $accknowledge_ment_id)->first();
		if ($chk_accknowledge_link) {

			$thesis = Thesis::find($chk_accknowledge_link->thesis_id);
			$userDetails = User::where('id', $userId)->first();

			$chk_accknowledge_link->last_email_date = Carbon::now();
			$chk_accknowledge_link->status = 2;
			$chk_accknowledge_link->save();

			$view = 'emails.dissertation-assign';
			//$attach = public_path('pdfs/synopsis/' . $thesis->form_pdf);
			$subject = 'Dissertation Assign';
			$to = $userDetails->email;
			$data = ['thesis' => $thesis, 'userId' => $userId];

			Mail::send('emails.dissertation-assign', $data, function ($message) use ($to, $subject) {
				$message->to($to)->subject($subject);
				$message->from(config('mail.from.address'), 'WBUHS');
			});

			return Redirect::to('admin/')->with('msg', 'New Dissertation has been assign to you. Please check your registered email for confirmation.');
		}
	}

	public function viewSynpFormPdf($syn_id) {
		$synposis = Synopsis::find($syn_id);
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetImportUse();
		$pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $synposis->form_pdf));
		if ($pagecount == 1) {
			$tplId = $mpdf->ImportPage($pagecount);
			$mpdf->UseTemplate($tplId);
		} else {
			for ($i = 1; $i < $pagecount; $i++) {
				$tplId = $mpdf->ImportPage($i);
				$mpdf->UseTemplate($tplId);
				$mpdf->AddPage();
			}
		}

		$mpdf->Output();
	}

	public function viewPDf($syn_id) {

		ini_set("memory_limit", -1);
		$synposis = Synopsis::find($syn_id);
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download(public_path('pdfs/' . $synposis->student_pdf), 'synopsis.pdf', $headers);

		/*$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetImportUse();

			$pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $synposis->form_pdf));
			for ($i = 1; $i < $pagecount; $i++) {
				$tplId = $mpdf->ImportPage($i);
				$mpdf->UseTemplate($tplId);
				$mpdf->AddPage();
			}
		*/
	}

	public function viewOtherPdf($pdf) {
		ini_set("memory_limit", -1);
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download(public_path('pdfs/' . $pdf), 'other.pdf', $headers);

		/*$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetImportUse();
			$pagecount = $mpdf->SetSourceFile(public_path('pdfs/' . $pdf));
			if ($pagecount == 1) {
				$tplId = $mpdf->ImportPage($pagecount);
				$mpdf->UseTemplate($tplId);
			} else {
				for ($i = 1; $i < $pagecount; $i++) {
					$tplId = $mpdf->ImportPage($i);
					$mpdf->UseTemplate($tplId);
					$mpdf->AddPage();
				}
			}

		*/
	}

	public function viewThesisFormPdf($thesis_id) {
		$thesis = Thesis::find($thesis_id);
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetImportUse();
		$pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $thesis->form_pdf));
		if ($pagecount == 1) {
			$tplId = $mpdf->ImportPage($pagecount);
			$mpdf->UseTemplate($tplId);
		} else {
			for ($i = 1; $i < $pagecount; $i++) {
				$tplId = $mpdf->ImportPage($i);
				$mpdf->UseTemplate($tplId);
				$mpdf->AddPage();
			}
		}

		$mpdf->Output();
	}

	public function viewThesisPdf($thesis_id) {
		$thesis = Thesis::find($thesis_id);
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download(public_path('pdfs/' . $thesis->current_pdf), 'dissertation.pdf', $headers);

		/*$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetImportUse();

			$pagecount = $mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $thesis->current_pdf));
			for ($i = 1; $i < $pagecount; $i++) {
				$tplId = $mpdf->ImportPage($i);
				$mpdf->UseTemplate($tplId);
				$mpdf->AddPage();
			}
		*/
	}

	public function synopsisChangeStatus(Request $request)
    {
//        dd($request);

		try {
			$synposis = Synopsis::find($request->synop_id);
			if ($synposis->transaction()->count() == 0) {
				return response()->json(['status' => 422, 'status_text' => 'please assign synopsis to BOS then change status']);
			}
			$synposis->status = $request->status;
            if ($request->status == 2){
                $synposis->approved_date = Carbon::now()->format('d/m/Y');
            }
			if ($request->status == 3) {
				if ($request->has('reason_reject')) {
					$synposis->reason_reject = $request->reason_reject;
				} elseif ($request->hasFile('reason_reject_doc')) {

					$docFile = $request->file('reason_reject_doc');
					$filename = time() . $docFile->getClientOriginalName();
					$docFile->move(public_path('pdfs'), $filename);
					$synposis->reason_reject_doc = $filename;
					$synposis->save();
				}
			} elseif ($request->status == 4) {

				if ($request->has('reason_reject')) {
					$synposis->reason_reject = $request->reason_reject;
				} elseif ($request->hasFile('reason_reject_doc')) {
					$docFile = $request->file('reason_reject_doc');
					$filename = time() . $docFile->getClientOriginalName();
					$docFile->move(public_path('pdfs'), $filename);
					$synposis->reason_reject_doc = $filename;
					$synposis->save();
				}
			} else {
				$synposis->reason_reject = '';
			}

			/*if ($request->has('reason_reject') && ($request->status == 3 || $request->status == 4)) {
					$synposis->reason_reject = $request->reason_reject;
				} else {
					$synposis->reason_reject = '';
			*/

			$synposis->save();
			$data = ['status' => $request->status, 'reason' => $request->reason_reject ? $request->reason_reject : ''];
			$view = 'emails.student-synopsis-status';
			//$attach = public_path('pdfs/synopsis/' . $synposis->form_pdf);
			if ($request->status == 3) {
				$subject = 'Synopsis Rejected';
			} elseif ($request->status == 4) {
				$subject = 'Synopsis Resubmitted';
			} else {
				$subject = 'Synopsis Approved';
			}

			/*if ($request->hasFile('reason_reject_doc')) {
				$docFile = $request->file('reason_reject_doc');
				$filename = time() . $docFile->getClientOriginalName();
				$docFile->move(public_path('pdfs'), $filename);
				$synopsis->reason_reject_doc = $filename;
				$synopsis->save();
			}*/
			$attach = $request->hasFile('reason_reject_doc') ? $synposis->reason_reject_doc : '';

			Mail::to($synposis->user->email)->send(new SendMail($data, $view, $subject));
			return response()->json(['status' => 200, 'status_text' => 'Successfuly change status']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}
	}

	public function studentThesis(Request $request) {
		$dateObj = Carbon::now();
		$dateFrom = $dateObj->subMonths(9)->format('Y-m-d');
		$title = 'Student Dissertation';
		$categories = $this->categoryTreeDropdown();
		$parent_cats = Category::where('parent', 0)->get();
		$user = Auth::guard('admin')->user();
		$thesisCycle = ThesiscycleMaster::all();
		if ($request->has('search')) {
			$thesisData = Thesis::has('payements')->select('thesis.*', 'categories.name as category_name')
				->where(function ($q) use ($user, $request, $dateFrom) {
					if ($request->has('search_by_status') && !empty($request->search_by_status)) {
						if ($request->search_by_status == 4) {
							$q->whereIn('thesis.status', [4, 9]);
						} else {
							$q->where('thesis.status', $request->search_by_status);
						}
					}
					if ($request->has('input_search') && !empty($request->input_search)) {
						$q->where('thesis.name_of_student', 'like', '%' . $request->input_search . '%');
						$q->orWhere('thesis.registration_no', 'like', '%' . $request->input_search . '%');
						$q->orWhere('thesis.registration_year', 'like', '%' . $request->input_search . '%');
						$q->orWhere('thesis.institute_name', 'like', '%' . $request->input_search . '%');
						$q->orWhere('thesis.mobile_landline', 'like', '%' . $request->input_search . '%');
					}

					if ($request->has('category') && !empty($request->category)) {
						$q->whereHas('category', function ($q) use ($request) {
							if ($request->category == 76) {
								$q->where('parent', $request->category);
								if (!empty($request->child_cat)) {
									$q->where('id', $request->child_cat);
								}
							} else {
								if (!empty($request->sub_cat_ids)) {
									$q->whereIn('parent', explode(',', $request->sub_cat_ids));
								}
								if (!empty($request->child_cat)) {
									$q->where('parent', $request->child_cat);
								}
								if (!empty($request->child_sub_cat)) {
									$q->where('id', $request->child_sub_cat);
								}
							}
						});
					}
					if (!empty($request->from_date) && !empty($request->to_date)) {
						$q->whereDate('thesis.created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
						$q->whereDate('thesis.created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
					}

					if (!empty($request->assign_status)) {
						if ($request->assign_status == 1) {
							$q->has('transaction');
						} else {
							$q->doesntHave('transaction');
						}
					}

					if (!empty($request->paid_status)) {
						if ($request->paid_status == 1) {
							$q->has('payements');
						} else {
							$q->doesntHave('payements');
						}
					}

					if ($user->is_admin != 1) {
						$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
							$q->whereHas('user', function ($q) use ($user) {
								$q->where('id', $user->id);
							});
							$q->where('status', '!=', 4);
							$q->where('accknowledge_email_date', '>=', $dateFrom);
						});
						/*
							$q->whereHas('transaction.user', function ($q) use ($user) {
								$q->where('id', $user->id);
						*/
					}
				})
				->leftJoin('categories', 'categories.id', '=', 'thesis.category_id')
				->orderBy('thesis.id', 'desc')->paginate(10)->appends(request()->query());
		} else {
			$thesisData = Thesis::has('payements')->select('thesis.*', 'categories.name as category_name')
				->where(function ($q) use ($user, $dateFrom) {
					if ($user->is_admin != 1) {
						$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
							$q->whereHas('user', function ($q) use ($user) {
								$q->where('id', $user->id);
							});
							$q->where('status', '!=', 4);
							$q->where('accknowledge_email_date', '>=', $dateFrom);
						});
						/*$q->whereHas('transaction.user', function ($q) use ($user) {
							$q->where('id', $user->id);
						});*/
					}
				})
				->leftJoin('categories', 'categories.id', '=', 'thesis.category_id')
				->orderBy('id', 'desc')->paginate(10)->appends(request()->query());

			/*if ($user->is_admin == 1) {
					$thesisData = Thesis::orderBy('id', 'asc')->paginate(10);
				} else {
					$thesisData = Thesis::whereHas('transaction.user', function ($q) use ($user) {
						$q->where('id', $user->id);
					})->paginate(10);
				}*/
		}

		$thesises = Thesis::all();
		return view('admin.student-thesis', compact('thesisData', 'title', 'user', 'thesisCycle', 'thesises', 'request', 'categories', 'parent_cats'));
	}

	public function goToPreviousState($id) {
		$thesis = Thesis::find($id);
		$thesis->status = 2;
		$thesis->save();
		return redirect()->back();
	}

	public function adjudicatorRevisionThesis(Request $request) {
		$dateObj = Carbon::now();
		$dateFrom = $dateObj->subMonths(9)->format('Y-m-d');
		$title = 'Student Dissertation Pending';
		$user = Auth::guard('admin')->user();
		$thesisData = Thesis::select('thesis.*', 'categories.name as category_name')
			->where(function ($q) use ($user, $dateFrom) {
				$q->where('status', '!=', 3);
				$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
					$q->whereHas('user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});
					$q->where('status', 4);
					$q->where(function ($q) use ($user, $dateFrom) {
						$q->whereHas('thesis.thesisResult', function ($q) use ($user, $dateFrom) {
							$q->where('faculty_id', $user->id);
							$q->whereIn('final_comment', ['D', 'E']);
							$q->whereDate('created_at', '>=', $dateFrom);
						});
						$q->orWhere('is_reassigned', 1);
					});
				});
			})
			->leftJoin('categories', 'categories.id', '=', 'thesis.category_id')
			->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.adjudicator-revision_thesis', compact('thesisData', 'title', 'user'));
	}

	public function adjudicatorCompletedThesis(Request $request) {
		$dateObj = Carbon::now();
		$dateFrom = $dateObj->subMonths(9)->format('Y-m-d');
		$title = 'Student Dissertation Completed';
		$user = Auth::guard('admin')->user();
		$thesisData = Thesis::select('thesis.*', 'categories.name as category_name')
			->where(function ($q) use ($user, $dateFrom) {
				$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
					$q->whereHas('user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});
					$q->where('status', 4);
					$q->whereHas('thesis.thesisResult', function ($q) use ($user, $dateFrom) {
						$q->where('faculty_id', $user->id);
						$q->whereIn('final_comment', ['A', 'B', 'C']);
						$q->whereDate('created_at', '>=', $dateFrom);
					});
				});
			})
			->leftJoin('categories', 'categories.id', '=', 'thesis.category_id')
			->orderBy('id', 'asc')->paginate(10)->appends(request()->query());

		return view('admin.adjudicator-completed_thesis', compact('thesisData', 'title', 'user'));
	}

	public function adjudicatorArchiveThesis(Request $request) {
		$dateObj = Carbon::now();
		$dateFrom = $dateObj->subMonths(9)->format('Y-m-d');
		$title = 'Archive Dissertations';
		$user = Auth::guard('admin')->user();
		$thesisData = Thesis::select('thesis.*', 'categories.name as category_name')
			->where(function ($q) use ($user, $dateFrom) {
				$q->whereHas('transaction', function ($q) use ($user, $dateFrom) {
					$q->whereHas('user', function ($q) use ($user) {
						$q->where('id', $user->id);
					});
					$q->where('accknowledge_email_date', '<', $dateFrom);
				});
			})
			->leftJoin('categories', 'categories.id', '=', 'thesis.category_id')
			->orderBy('id', 'asc')->paginate(10)->appends(request()->query());

		return view('admin.adjudicator-archive-thesis', compact('thesisData', 'title', 'user'));
	}

	public function getChildCategory(Request $request) {
		$categories = Category::where('parent', $request->id)->get();
		return response()->json(['status' => 200, 'data' => $categories]);
	}

	public function markNotificationRead() {
		$user = Auth::guard('admin')->user();
		$user->unreadNotifications->markAsRead();
		return redirect()->back();
	}

	public function profileEdit() {
		$title = 'Profile Edit';
		$user = Auth::guard('admin')->user();
		return view('admin.user-manage.profile-edit', compact('title', 'user'));
	}

	public function profileUpdate(Request $request) {

		$messsages = array(
			'f_name.required' => 'The Role Name field is required.',
			'l_name.required' => 'The Role Slug field is required.',
			'email.required' => 'The Email field is required.',
			'mobile.required' => 'The Mobile field is required.',
			'address.required' => 'The Mobile field is required.',
			'gender.required' => 'The Mobile field is required.',
		);

		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			'email' => 'required ',
			'mobile' => 'required ',
			'address' => 'required',
			'gender' => 'required',
			'profile_image' => 'mimes:jpeg,jpg,png,gif|max:10000',
			'password' => (!empty($request->password)) ? 'min:6' : '',
		], $messsages);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		$auth = Auth::guard('admin')->user();
		$user = User::find($auth->id);
		try {
			$user->fill($request->only('f_name', 'l_name', 'email'));
			$user->save();
			$userDetail = $user->userDetail->fill($request->only('mobile', 'address', 'gender'));
			$userDetail->save();
			if ($request->hasFile('profile_image')) {
				$image = $request->file('profile_image');
				$filename = $image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());
				$image_resize->resize(150, 150, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('images/' . time() . $filename));
				$userDetail->profile_image = 'images/' . time() . $filename;
				$userDetail->save();
			}
			if ($request->has('password') && (!empty($request->password))) {
				$user->password = bcrypt($request->password);
				$user->save();
			}
			Session::flash('success', 'Sucessfuly created');
			return redirect('admin/profile/edit');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	public function submittedStatus(Request $request) {
		$thesis = Thesis::find($request->thesis_id);
		try {
			$data = [];
			$reason_reject = '';
			$alternate_reason = '';
			if ($thesis->transaction->count()) {

				foreach ($thesis->transaction()->where('status', '!=', 3)->get() as $value) {
					if ($value->is_reassigned == 1) {
						$grade = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->grade_text : 'Revision';
						/*if ($value->status == 2) {
								$grade = 'Revision';
							} else {
								$grade = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->grade_text : '';
						*/

						$reAssigned = $value->thesis->thesisResubmitHistory()->where('faculty_id', $value->to_user_id)->first() ? 1 : 0;
						$reviewId = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->where('transaction_reassigned_group', $value->reassigned_group)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->where('transaction_reassigned_group', $value->reassigned_group)->first()->id : '';
						$reviewUrl = $reviewId ? url('admin/student-result/view', $reviewId) : '';
						$suggestion = '';
					} else {
						$grade = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->grade_text : '';
						$reAssigned = $value->thesis->thesisResubmitHistory()->where('faculty_id', $value->to_user_id)->first() ? 1 : 0;
						$reviewId = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->id : '';
						$reviewUrl = $reviewId ? url('admin/student-result/view', $reviewId) : '';
						$suggestion = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->suggestion : '';

						$reason_reject = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->reason_reject : '';
						$alternate_reason = $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first() ? $value->thesis->thesisResult()->where('faculty_id', $value->to_user_id)->first()->alternate_reason : '';
					}

					$data[] = ['id' => $value->id, 'name' => $value->user->full_name, 'submitted' => ($value->status == 4) ? 'yes' : 'no', 'grade' => $grade, 'reason_reject' => $reason_reject, 'alternate_reason' => $alternate_reason, 'reviewUrl' => $reviewUrl, 'suggestion' => $suggestion, 'reAssigned' => $reAssigned];
				}
			}
			return response()->json(['data' => $data, 'status' => 'success']);
		} catch (\Exception $e) {
			return response()->json(['status' => 'error', 'status_text' => $e->getMessage()]);
		}
	}

	public static function thesisList($thesisId) {
		$thesis = Thesis::find($thesisId);
		if ($thesis->status <= 4) {
			return ThesiscycleMaster::whereIn('id', [1, 2, 3, 4])->get();
		} else {
			return ThesiscycleMaster::whereIn('id', [6, 7, 8, 9])->get();
		}
	}

	public function getAvailableReviewer(Request $request) {
		$thesis = Thesis::find($request->thesis_id);
		$thesisAssignedUser = ThesisTransaction::where('thesis_id', $request->thesis_id)->pluck('to_user_id')->toArray();
		$acceptedUser = ThesisTransaction::whereNotIn('to_user_id', $thesisAssignedUser)->whereHas('user.category', function ($q) use ($thesis) {
			$q->where('id', $thesis->category_id);
		})->pluck('to_user_id')->toArray();

		$availableUsers = User::whereHas('category', function ($q) use ($thesis) {
			$q->where('id', $thesis->category_id);
		})->whereHas('roles', function ($q) {
			$q->where('slug', 'adjudicator');
		})

		//->whereNotIn('id', $acceptedUser)
			->get();

		$rejectedUsers = ThesisTransaction::where('thesis_id', $request->thesis_id)->where('status', 3)->pluck('to_user_id')->toArray();
		$sendedUsers = $thesis->transaction ? $thesis->transaction()->whereIn('status', [1, 2, 4])->pluck('to_user_id')->toArray() : '';
		$sendedAcknowledge = $thesis->transaction ? $thesis->transaction()->whereIn('status', [2, 4])->pluck('to_user_id')->toArray() : '';
		$sendedRequest = $thesis->transaction ? $thesis->transaction()->where('status', 1)->pluck('accknowledge_email_date')->toArray() : '';

		$sendTransactionIds = $thesis->transaction ? $thesis->transaction()->whereIn('status', [1, 2, 4])->pluck('id', 'to_user_id')->toArray() : '';

		return response()->json(['status' => 200, 'users' => $availableUsers, 'sendedUser' => $sendedUsers, 'rejectedUser' => $rejectedUsers, 'send_transaction_ids' => $sendTransactionIds, 'sendedAcknowledge' => $sendedAcknowledge, 'sendedRequest' => $sendedRequest]);
	}

	public function DissertationWithdraw(Request $request) {
		$transaction = ThesisTransaction::find($request->trans_id);
		$transaction->delete();
		$data = ['type' => 2, 'detail' => $transaction->thesis];
		$view = 'emails.withdraw';
		$subject = 'Dissertation Withdraw';
		Mail::to($transaction->send_to)->send(new SendMail($data, $view, $subject));
		return response()->json(['status' => 200]);
	}

	public function viewSpeciality($id) {
		$user = User::find($id);
		return response()->json(['status' => 200, 'categories' => $user->category]);
	}

	public function getBOSUsers(Request $request) {
		$synopsis = Synopsis::find($request->synopId);
		$users = $synopsis->category()->user;
		return response()->json(['status' => 200, 'data' => $users]);
	}

	public function changePaymentStatus(Request $request) {
		if ($request->type == 'synopsis') {
			$synopsis = Synopsis::find($request->id);
			$synop_payment = $synopsis->payements()->first();
			$synop_payment->is_paid = $request->status;
			$synop_payment->save();

			$data = ['type' => 1];
			$view = 'emails.payment-done';
			$subject = 'Synopsis payment status';
			Mail::to($synopsis->user->email)->send(new SendMail($data, $view, $subject));
		} else {
			$thesis = Thesis::find($request->id);
			$thesisPayment = $thesis->payements()->first();
			$thesisPayment->is_paid = $request->status;
			$thesisPayment->save();
			$data = ['type' => 2];
			$view = 'emails.payment-done';
			$subject = 'Dissertation payment status';
			Mail::to($thesis->user->email)->send(new SendMail($data, $view, $subject));
		}

		return response()->json(['status' => 200]);
	}

	public function viewReasonRejectDownload(Request $request) {

		if ($request->type == 'thesis') {
			$reviewerResult = ThesisReviewerResult::find($request->id);
			return response()->download(public_path('pdfs/' . $reviewerResult->alternate_reason));
		} elseif ($request->type == 'synopsis') {
			$synopsis = Synopsis::find($request->id);
			return response()->download(public_path('pdfs/' . $synopsis->reason_reject_doc));
		}
	}

	public function bosReviewStatus(Request $request) {

		$transaction = SynopsisTransaction::where('synopsis_id', $request->synop_id)->where('to_user_id', $request->user_id)->first();
		$transaction->review_status = $request->status;
		$transaction->save();

		$data = ['transaction' => $transaction, 'review_status' => $request->status, 'student' => $transaction->synopsis->user];
		//	return $data; exit();
		$view = 'emails.synopsis-review-mail';
		$attach = public_path('pdfs/synopsis/' . $transaction->synopsis->form_pdf);
		$subject = 'Student Synopsis Review Status';
		$mailUser = User::whereHas('roles', function ($q) {
			$q->whereIn('slug', ['superadmin', 'secretarybos']);
		})->get();
		foreach ($mailUser as $user) {
			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
		}

		return response()->json(['status' => 200, 'status_text' => 'Successfully change review status']);
	}

	public function getAssignedUser(Request $request) {
		if ($request->type == 'synopsis') {
			$synopsis = Synopsis::find($request->id);
			$users = $synopsis->transaction()->pluck('to_user_id')->toArray();
			return response()->json(['status' => 200, 'data' => $users]);
		} else {
			$thesis = Thesis::find($request->id);
			$assignedUser = $thesis->transaction()->where('status', 4)->pluck('to_user_id')->toArray();
			$users = User::whereIn('id', $assignedUser)->get();
			return response()->json(['status' => 200, 'data' => $users, 'category' => $thesis->category]);
		}
	}

	public function studentSynopsisReport(Request $request) {
		$title = 'Synopsis Report';
		$courses = Category::where('has_child', 0)->get();

		$synopsises = Synopsis::where(function ($q) use ($request) {
			if ($request->has('search')) {
				if (!empty($request->search_by_course)) {
					$q->where('category_id', $request->search_by_course);
				}
				if (!empty($request->input_search)) {
					$q->where('name_of_student', 'like', '%' . $request->input_search . '%');
					$q->orWhere('registration_year', 'like', '%' . $request->input_search . '%');
					$q->orWhere('institute_name', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('transaction.user', function ($q) use ($request) {
						$q->where(\DB::raw("CONCAT(f_name, ' ', l_name)"), 'like', '%' . $request->input_search . '%');
					});
				}
			}
		})->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.student-synopsis-report', compact('synopsises', 'title', 'request', 'courses'));
	}

	public function synopsisBOSReport($id, Request $request) {
		$title = 'Synopsis BOS Report';
		$user = User::find($id);
		$courses = Category::where('has_child', 0)->get();
		$synopsisTransaction = $user->synopsisTransaction()->paginate(10);
		return view('admin.synopsis-bos-report', compact('title', 'user', 'synopsisTransaction', 'courses', 'request'));
	}

	public function studentDissertationReport(Request $request) {
		$title = 'Dissertation Report';
		$courses = Category::where('has_child', 0)->get();

		$thesises = Thesis::where(function ($q) use ($request) {
			if ($request->has('search')) {
				if (!empty($request->search_by_course)) {
					$q->where('category_id', $request->search_by_course);
				}
				if (!empty($request->input_search)) {
					$q->where('name_of_student', 'like', '%' . $request->input_search . '%');
					$q->orWhere('registration_year', 'like', '%' . $request->input_search . '%');
					$q->orWhere('institute_name', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('transaction.user', function ($q) use ($request) {
						$q->where(\DB::raw("CONCAT(f_name, ' ', l_name)"), 'like', '%' . $request->input_search . '%');
					});
				}
			}
		})->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.student-thesis-report', compact('thesises', 'title', 'request', 'courses'));
	}

	public function synopsisAdjudicatorReport($id, Request $request) {
		$title = 'Dissertation Adjudicator Report';
		$user = User::find($id);
		$courses = Category::where('has_child', 0)->get();
		$thesisTransaction = $user->thesisTransction()->paginate(10);
		return view('admin.thesis-adj-report', compact('title', 'user', 'thesisTransaction', 'courses', 'request'));
	}

	public function getStudentPayments(Request $request) {
		$title = 'Student Payment History';
		$payments = Payment::where(function ($q) use ($request) {
			if (!empty($request->search)) {

				if (!empty($request->pay_for)) {
					if ($request->pay_for == 1) {
						$q->where('pay_type', 'App\Models\Thesis');
					} else {
						$q->where('pay_type', 'App\Models\Synopsis');
					}
				}
				if (!empty($request->from_date) && !empty($request->to_date)) {

					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
				if (!empty($request->by_name)) {
					$q->whereHas('payThesis', function ($q) use ($request) {
						$q->where('name_of_student', 'like', '%' . $request->by_name . '%');
					});
					$q->orWhereHas('paySynopsis', function ($q) use ($request) {
						$q->where('name_of_student', 'like', '%' . $request->by_name . '%');
					});
				}
			}
		})->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.student-payments', compact('title', 'payments', 'request'));
	}

	public function getStudentList(Request $request) {
		$title = 'Student Dessertation List';

		$students = StudentData::where(function ($q) use ($request) {
			if (!empty($request->search)) {

				if (!empty($request->phone)) {

					$q->where('phone', $request->phone);
				}

				if (!empty($request->email)) {

					$q->where('email', $request->email);
				}

				if (!empty($request->by_name)) {
					$q->where('name', 'like', '%' . $request->by_name . '%');
				}
			}
		})->orderBy('id', 'desc')->paginate(50)->appends(request()->query());
		return view('admin.student-dessertation-list', compact('title', 'students', 'request'));
	}

	public function importStudentData(Request $request) {
		if ($request->file('csv_file')) {
			$importFile = $request->file('csv_file');
			$path = $request->file('csv_file')->getRealPath();
			$data = Excel::toArray(new ImportStudent, $importFile);
			$dataArr = [];
			$errorarray = [];
			if (!empty($data) && count($data)) {
				foreach ($data as $row) {
					if (!empty($row)) {
						foreach (array_filter($row) as $key => $value) {
							if ($value[0] == null || $value[1] == null || $value[2] == null || $value[3] == null || $value[4] == null || $value[5] == null || $value[6] == null || $value[7] == null) {
							} else if ($value[0] == 'Sl No' || $value[1] == 'Name of the Candidate' || $value[2] == 'Name of the Subject' || $value[3] == 'Name of the Institute' || $value[4] == 'College Code' || $value[5] == 'WBUHS Registration No.' || $value[6] == 'Phone' || $value[7] == 'Email') {
							} else {
								if (StudentData::where('registration_no', $value[5])->first()) {
									$errorarray[] = ['sl_no' => (int) $value[0], 'name' => $value[1], 'course' => $value[2], 'institute' => $value[3], 'institute_code' => $value[4], 'registration_no' => $value[5], 'phone' => (int) $value[6], 'email' => $value[7]];
								} else {
									StudentData::create(['sl_no' => (int) $value[0], 'name' => $value[1], 'course' => $value[2], 'institute' => $value[3], 'institute_code' => $value[4], 'registration_no' => $value[5], 'phone' => (int) $value[6], 'email' => $value[7]]);
								}
							}
						}
					}
				}
			}
			if (count($errorarray)) {
				$errdata = collect($errorarray)->pluck('registration_no')->toArray();
				Session::flash('err_import', implode(',', $errdata));
			}

			Session::flash('msg', 'Successfully uploaded the file');
			return redirect()->route('student.list');
		}
	}

	public function downloadExcel($type) {
		//$data = StudentData::get()->toArray();
		$data = StudentData::select('sl_no', 'name', 'registration_no', 'course', 'institute', 'phone', 'email')->orderBy('id', 'desc')->take(5)->get();

		return Excel::create('student_dessertation_sample_csv', function ($excel) use ($data) {
			$excel->sheet('mySheet', function ($sheet) use ($data) {
				$sheet->fromArray($data);
			});
		})->download($type);
	}

	public function editDessertationStudent(Request $request) {
		$title = 'Dessertation Student Edit';
		$faculty = Auth::guard('admin')->user();
		$studentDessertationDetails = StudentData::where('id', $request->id)->first();

		return view('admin.student-dessertation-edit', compact('title', 'faculty', 'studentDessertationDetails'));
	}

	public function storeDessertationStudent(Request $request) {

		$user = Auth::guard('admin')->user();
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'registration_no' => 'required',
			'course' => 'required',
			'institute' => 'required',

		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$student = StudentData::find($request->id);
			$student->name = $request->name;
			$student->registration_no = $request->registration_no;
			$student->course = $request->course;
			$student->institute = $request->institute;

			$student->save();

			Session::flash('msg', 'Sucessfuly submit');
			return redirect()->route('student.list');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	public function getSynopsisStudentList(Request $request) {
		$title = 'Student Synopsis List';

		$students = SynopsisStudentData::where(function ($q) use ($request) {
			if (!empty($request->search)) {

				if (!empty($request->phone)) {

					$q->where('phone', $request->phone);
				}

				if (!empty($request->email)) {

					$q->where('email', $request->email);
				}

				if (!empty($request->by_name)) {
					$q->where('name', 'like', '%' . $request->by_name . '%');
				}
			}
		})->orderBy('id', 'desc')->paginate(50)->appends(request()->query());
		return view('admin.student-synopsis-list', compact('title', 'students', 'request'));
	}

	public function studentSynopsisListImport(Request $request) {
		if ($request->file('csv_file')) {
			$importFile = $request->file('csv_file');
			$path = $request->file('csv_file')->getRealPath();
			$data = Excel::toArray(new ImportStudentSynopsis, $importFile);
			$dataArr = [];
			$errorarray = [];
			if (!empty($data) && count($data)) {
				foreach ($data as $row) {
					if (!empty($row)) {
						foreach (array_filter($row) as $key => $value) {
							if ($value[0] == null || $value[1] == null || $value[2] == null || $value[3] == null || $value[4] == null || $value[5] == null || $value[6] == null) {
							} else if ($value[0] == 'Sl No' || $value[1] == 'Name of the Candidate' || $value[2] == 'Name of the Subject' || $value[3] == 'Name of the Institute' || $value[4] == 'WBUHS Registration No.' || $value[5] == 'Phone' || $value[6] == 'Email') {
							} else {
								if (SynopsisStudentData::where('unique_id', $value[5])->first()) {
									$errorarray[] = ['sl_no' => (int) $value[0], 'name' => $value[1], 'course' => $value[2], 'institute' => $value[3], 'unique_id' => $value[4], 'phone' => $value[5], 'email' => (int) $value[6]];
								} else {
									SynopsisStudentData::create(['sl_no' => (int) $value[0], 'name' => $value[1], 'course' => $value[2], 'institute' => $value[3], 'unique_id' => $value[4], 'phone' => (int) $value[5], 'email' => $value[6]]);
								}
							}
						}
					}
				}
			}
			if (count($errorarray)) {
				$errdata = collect($errorarray)->pluck('unique_id')->toArray();
				Session::flash('err_import', implode(',', $errdata));
			}

			Session::flash('msg', 'Successfully uploaded the file');
			return redirect()->route('student-synopsis.list');
		}
	}

	public function downloadSynopsisExcel($type) {
		//$data = StudentData::get()->toArray();
		$data = SynopsisStudentData::select('sl_no', 'name', 'unique_id', 'course', 'institute', 'phone', 'email')->orderBy('id', 'desc')->take(5)->get();

		return Excel::create('student_synopsis_sample_csv', function ($excel) use ($data) {
			$excel->sheet('mySheet', function ($sheet) use ($data) {
				$sheet->fromArray($data);
			});
		})->download($type);
	}

	public function editSynopsisStudent(Request $request) {
		$title = 'Synopsis Student Edit';
		$faculty = Auth::guard('admin')->user();
		$studentSynopsisDetails = SynopsisStudentData::where('id', $request->id)->first();

		return view('admin.student-synopsis-edit', compact('title', 'faculty', 'studentSynopsisDetails'));
	}

	public function storeSynopsisStudent(Request $request) {

		$user = Auth::guard('admin')->user();
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'unique_id' => 'required',
			'course' => 'required',
			'institute' => 'required',

		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$student = SynopsisStudentData::find($request->id);
			$student->name = $request->name;
			$student->unique_id = $request->unique_id;
			$student->course = $request->course;
			$student->institute = $request->institute;

			$student->save();

			Session::flash('msg', 'Sucessfuly submit');
			return redirect()->route('student-synopsis.list');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	public function sendLoginCredentialToAdjudicator() {
		$adjudicators = User::whereHas('roles', function ($q) {
			$q->where('slug', 'adjudicator');
			$q->whereMonth('created_at', '>=', '6');
		})->skip(0)->take(50)->get();
		/*try {
				$view = 'emails.guide-toadjudicator';
				$subject = 'Adjudicator Help Guide';
				$attached_path = public_path('files/Help_manual_for_Adjudicator_level_review_of_Dissertations.docx');
				foreach ($adjudicators as $adjudicator) {
					$data = ['password' => 'user123$', 'email' => $adjudicator->email];
					Mail::to($adjudicator->email)->send(new SendMail($data, $view, $subject, $attached_path));
					echo $adjudicator->email . '<br>';
				}
				return 'mail send done';
			} catch (\Exception $e) {
				return $e->getMessage();
		*/
	}

	public function getStudentSupportList(Request $request) {
		$title = 'Student Support History';
		$students = Support::where(function ($q) use ($request) {

			if (!empty($request->search)) {

				if (!empty($request->by_name)) {
					$q->whereHas('userDetail', function ($q) use ($request) {
						$q->where('f_name', 'like', '%' . $request->by_name . '%');
					});
				}

				if (!empty($request->mobile)) {
					$q->whereHas('userDetail', function ($q) use ($request) {
						$q->where('mobile', $request->mobile);
					});
				}

				if (!empty($request->email)) {
					$q->whereHas('userDetail', function ($q) use ($request) {
						$q->where('email', $request->email);
					});
				}
			}
		})->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.student-support-list', compact('title', 'students', 'request'));
	}

	public function editStudentSupport(Request $request) {
		$title = 'Student Store Edit';
		$faculty = Auth::guard('admin')->user();
		//$studentStoreDetails = SynopsisStudentData::where('id', $request->id)->first();

		$students = Support::where(function ($q) use ($request) {
			$q->whereHas('userDetail', function ($q) use ($request) {
				//$q->where('id', $request->id);
				$q->where('user_id', $request->user_id);
			});
		})->first();

		$studentSupport = Support::find($request->id);

		return view('admin.student-support-edit', compact('title', 'faculty', 'students', 'studentSupport'));
	}

	public function storeStudentSupport(Request $request) {

		$user = Auth::guard('admin')->user();
		$validator = Validator::make($request->all(), [
			'admin_reply' => 'required',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$student = Support::find($request->id);
			$student->admin_reply = $request->admin_reply;
			$student->status = '1';

			$userData = User::find($request->user_id);

			$userName = $userData->f_name . ' ' . $userData->l_name;
			$to = $userData->email;
			$subject = 'Request for "' . $student->subject . '", has been replied by the admin.';
			$data = ['subject' => $student->subject, 'name' => $userName, 'reply' => $request->admin_reply];

			Mail::send('emails.support-reply-confirmation', $data, function ($message) use ($to, $subject) {
				$message->to($to)->subject($subject);
				$message->from(config('mail.from.address'), 'WBUHS');
			});

			$student->save();

			Session::flash('msg', 'Sucessfuly submit');
			return redirect()->route('student-support.list');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	public function replaceThesisPDF(Request $request) {
		$messsages = array();
		$validator = Validator::make($request->all(), [
			'upload_new_pdf' => 'required|mimes:pdf',
		], $messsages);

		if ($validator->fails()) {
			Session::flash('error', 'File type is not PDF');
			return redirect('admin/student-dissertation');
		}

		if ($request->hasFile('upload_new_pdf')) {
			$newPDF = $request->file('upload_new_pdf');
			$newPDFfilename = time() . $newPDF->getClientOriginalName();
			$newPDF->move(public_path('pdfs'), $newPDFfilename);
			$thesis = Thesis::find($request->thesis_id);
			$thesis->student_pdf = $newPDFfilename;
			$thesis->save();
			$thPDF = ThesisPdf::where('thesis_id', $request->thesis_id)->first();
			$thPDF->thesis_pdf = $newPDFfilename;
			$thPDF->save();
			Session::flash('msg', 'Successfully replace file');
			return redirect('admin/student-dissertation');
		}
	}

	public function replaceSynopsisPDF(Request $request) {
		$messsages = array();
		$validator = Validator::make($request->all(), [
			'upload_new_pdf' => 'required|mimes:pdf',
		], $messsages);

		if ($validator->fails()) {
			Session::flash('error', 'File type is not PDF');
			return redirect('admin/student-synopsis');
		}

		if ($request->hasFile('upload_new_pdf')) {
			$newPDF = $request->file('upload_new_pdf');
			$newPDFfilename = time() . $newPDF->getClientOriginalName();
			$newPDF->move(public_path('pdfs'), $newPDFfilename);
			$synopsis = Synopsis::find($request->synopsis_id);
			$synopsis->student_pdf = $newPDFfilename;
			$synopsis->save();
			Session::flash('msg', 'Successfully replace file');
			return redirect('admin/student-synopsis');
		}
	}

	public function userReminder() {
		$now = "'" . Carbon::now()->subDays(2) . "'";
		$nowDate = Carbon::now()->format('Y-m-d');
		$todaysCount = ThesisReminder::whereDate('reminder_date', $nowDate)->count();

		if ($todaysCount < 500) {
			$transactionData = ThesisTransaction::where('accknowledge_email_date', '<>', 0)->where('status', 1)->where('accknowledge_email_date', '<', Carbon::now()->subDays(2))->skip(0)->take(500)->get();

			if (count($transactionData) > 0) {

				$items = array();
				foreach ($transactionData as $value) {

					$reminderData = ThesisReminder::where('thesis_id', $value->thesis_id)->where('user_id', $value->to_user_id)->where('thesis_transactions_id', $value->id)->get();

					$subject = '';
					$subjectAdmin = '';
					//$adminEmail = 'no-reply@gmail.com';
					$adminEmail = config('mail.to.adminAddress');

					$items[] = $value->id;

					if (count($reminderData) == 0) {

						$subject = 'First Reminder for Dissertation Acceptence';
						$subjectAdmin = 'First Reminder has been sent for Dissertation Acceptence';
					} elseif (count($reminderData) == 1) {

						$repetReminderCheck = ThesisReminder::where('reminder_date', '<', Carbon::now()->subDays(2))->where('user_id', $value->to_user_id)->where('thesis_transactions_id', $value->id)->get();

						if (count($repetReminderCheck) > 0) {
							$subject = 'Second Reminder for Dissertation Acceptence';
							$subjectAdmin = 'Second Reminder has been sent for Dissertation Acceptence';
						}
					} elseif (count($reminderData) == 2) {

						$repetReminderCheck = ThesisReminder::where('reminder_date', '<', Carbon::now()->subDays(3))->where('user_id', $value->to_user_id)->where('thesis_transactions_id', $value->id)->get();

						if (count($repetReminderCheck) > 0) {
							$subject = 'Last Reminder for Dissertation Acceptence';
							$subjectAdmin = 'Last Reminder has been sent for Dissertation Acceptence';
						}
					}

					$confirmationLink = ThesisTransaction::where('to_user_id', $value->to_user_id)->first();
					$data = ['text' => 'Not Accepted', 'confirmationLink' => $confirmationLink->accknowledge_link, 'subject' => $subject, 'userId' => $value->to_user_id];
					$view = 'emails.thesis-status';
					$to = $value->send_to;

					if ($subject != '') {

						Mail::send('emails.thesis-reminder', $data, function ($message) use ($to, $subject) {
							$message->to($to)->subject($subject);
							$message->from(config('mail.from.address'), 'WBUHS');
						});

						ThesisReminder::insert(['thesis_transactions_id' => $value->id, 'thesis_id' => $value->thesis_id, 'user_id' => $value->to_user_id]);

						Mail::send('emails.thesis-reminder-admin', $data, function ($message) use ($adminEmail, $subjectAdmin) {
							$message->to($adminEmail)->subject($subjectAdmin);
							$message->from(config('mail.from.address'), 'WBUHS');
						});
					}
				}
			}
		}
	}

	// public function userThesisSubmit() {

	// 	$transactionData = ThesisTransaction::where('accknowledge_email_date', '<>', 0)->where('status', 2)->where('accknowledge_email_date', '<', Carbon::now()->subDays(7))->get();

	// 	if (count($transactionData) > 0) {

	// 		$items = array();
	// 		foreach ($transactionData as $value) {

	// 			$subject = 'Dissertation Not submited!';
	// 			$subjectAdmin = '';
	// 			$adminEmail = config('mail.to.adminAddress');

	// 			$data = ['text' => 'Not Submited'];
	// 			$view = 'emails.thesis-not-submited';
	// 			$to = $value->send_to;

	// 			Mail::send('emails.thesis-submit-reminder', $data, function ($message) use ($to, $subject) {
	// 				$message->to($to)->subject
	// 					($subject);
	// 				$message->from(config('mail.from.address'), 'WBUHS');
	// 			});

	// 			Mail::send('emails.thesis-submit-reminder-admin', $data, function ($message) use ($adminEmail, $subjectAdmin) {
	// 				$message->to($adminEmail)->subject
	// 					($subjectAdmin);
	// 				$message->from(config('mail.from.address'), 'WBUHS');
	// 			});

	// 		}
	// 		//echo $subject;
	// 	}

	// }

	public function userThesisSubmit() {

		$transactionData = ThesisTransaction::where('accknowledge_email_date', '<>', 0)->where('status', 2)->where('last_email_date', '<', Carbon::now()->subDays(14))->get();

		if (count($transactionData) > 0) {

			$items = array();
			foreach ($transactionData as $value) {

				$userData = User::where('id', $value->to_user_id)->first();

				$subject = 'Dissertation Not submited!';
				$subjectAdmin = '';
				$adminEmail = config('mail.to.adminAddress');

				$data = ['text' => 'Not Submited'];
				$view = 'emails.thesis-not-submited';
				$to = $value->send_to;

				Mail::send('emails.thesis-submit-reminder', $data, function ($message) use ($to, $subject) {
					$message->to($to)->subject($subject);
					$message->from(config('mail.from.address'), 'WBUHS');
				});

				Mail::send('emails.thesis-submit-reminder-admin', $data, function ($message) use ($adminEmail, $subjectAdmin) {
					$message->to($adminEmail)->subject($subjectAdmin);
					$message->from(config('mail.from.address'), 'WBUHS');
				});

				$content = 'Gentle reminder that Thesis Adjudication Report is still awaited .
Pl. send it within 07 days .';
				return $this->sendMesageNotify($userData['mobile'], $content);
			}
			//echo $subject;
		}

		$transactionData = ThesisTransaction::where('accknowledge_email_date', '<>', 0)->where('status', 2)->where('last_email_date', '<', Carbon::now()->subDays(21))->get();

		if (count($transactionData) > 0) {

			$items = array();
			foreach ($transactionData as $value) {

				$userData = User::where('id', $value->to_user_id)->first();

				$subject = 'Dissertation Not submited!';
				$subjectAdmin = '';
				$adminEmail = config('mail.to.adminAddress');

				$data = ['text' => 'Not Submited'];
				//$view = 'emails.thesis-not-submited';
				$to = $value->send_to;

				Mail::send('emails.thesis-submit-reminder-last', $data, function ($message) use ($to, $subject) {
					$message->to($to)->subject($subject);
					$message->from(config('mail.from.address'), 'WBUHS');
				});

				Mail::send('emails.thesis-submit-reminder-admin', $data, function ($message) use ($adminEmail, $subjectAdmin) {
					$message->to($adminEmail)->subject($subjectAdmin);
					$message->from(config('mail.from.address'), 'WBUHS');
				});
				$content = 'The report as asked earlier has not been received at this end . Please expedite the matter and send Report within 03 days.';
				return $this->sendMesageNotify($userData['mobile'], $content);
			}
			//echo $subject;
		}
	}

	public function sendMesageNotify($number, $content) {
		$mobile = (int) '91' . $number;
		//$mobile = (int) '919932250076';
		$url = 'http://whitelist.smsapi.org/SendSMS.aspx?UserName=WBUHS&password=6YNOT7wuTT&MobileNo=' . $mobile . '&SenderID=WBUHSR&CDMAHeader=WBUHSR&Message=' . $content;

		$client = new \GuzzleHttp\Client();
		$request = $client->get($url);
	}

	public function userResubmit(Request $request) {
		$user = \Auth::guard('admin')->user();

		$transactionData = ThesisTransaction::where('thesis_id', $request->thesis_id)->first();

		if (!$transactionData) {
			$thesis = Thesis::find($request->thesis_id);
			$thesis->re_submit = $request->status;
			$thesis->save();

			return response()->json(['status' => 200, 'status_text' => 'success']);
		} else {
			return response()->json(['status' => 500, 'error' => 'fail']);
		}
	}

	public function studentPaymentExport(Request $request) {
		$payments = Payment::where(function ($q) use ($request) {
			if (!empty($request->search)) {

				if (!empty($request->pay_for)) {
					if ($request->pay_for == 1) {
						$q->where('pay_type', 'App\Models\Thesis');
					} else {
						$q->where('pay_type', 'App\Models\Synopsis');
					}
				}
				if (!empty($request->from_date) && !empty($request->to_date)) {

					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
				if (!empty($request->by_name)) {
					$q->whereHas('payThesis', function ($q) use ($request) {
						$q->where('name_of_student', 'like', '%' . $request->by_name . '%');
					});
					$q->orWhereHas('paySynopsis', function ($q) use ($request) {
						$q->where('name_of_student', 'like', '%' . $request->by_name . '%');
					});
				}
			}
		})->orderBy('id', 'desc')->pluck('id')->toArray();
		//$ids = array_map('intval', explode(',', $request->idsValue));
		return Excel::download(new StudentPaymentExport($payments), 'student-payment.xlsx');
		//return $payments;
	}

	public function downloadSampleCSV() {
		$file = public_path() . "/manuals/demo-import-student.xlsx";
		$headers = array(
			'Content-Type: application/octet-stream',
		);
		return Response::download($file, 'studentimport-demo.xlsx', $headers);
	}

	public function bosReviewSynopsis(Request $request) {
		$synopTrans = SynopsisTransaction::where('synopsis_id', $request->review_synopsis_id)->where('to_user_id', Auth::guard('admin')->user()->id)->first();
		$synopTrans->review_status = $request->bos_status;
		if (!empty($request->bos_reason_reject)) {
			$synopTrans->comment = $request->bos_reason_reject;
		}
		if ($request->bos_status == 1) {
			$synopTrans->comment = '';
		}
		$synopTrans->save();
		Session::flash('msg', 'Sucessfully updated status');
		return redirect('admin/student-synopsis');
	}

	public function enableResubmitSynopsis($id, Request $request) {
		$synopsis = Synopsis::find($id);
		$synopsis->resubmit_enable = $request->status;
		$synopsis->save();
		return response()->json(['status' => 200]);
	}

	public function sendManuallToBOSMember() {

		$synopsisSendedUsers = SynopsisTransaction::where('review_status', 0)->groupBy('to_user_id')->get();

		$data = ['password' => 'user123$'];
		$view = 'emails.manually.bos-manuall';
		$subject = 'BOS Member user Guide';
		$attached_path = public_path('files/Help_manual_for_BOS_review_status.docx');

		Mail::to('souravg1g2.sp@gmail.com')->send(new SendMail($data, $view, $subject, $attached_path));

		/*foreach ($synopsisSendedUsers as $value) {
			echo $value->user->email . '<br>';
			Mail::to($value->user->email)->send(new SendMail($data, $view, $subject, $attached_path));
		}*/
		return 'ok';
	}

	public function sendAdjudicatorPendingreport() {
		/*$pendingAdjudicators = ThesisTransaction::whereNotNull('accknowledge_link')->whereHas('thesis.category', function ($q) {
				$q->where('parent', 29);
			})->where('status', 2)->skip(0)->take(100)->get();

			$view = 'emails.manually.thesis-reminder';
			$subject = 'Dissertation Reminder';
			foreach ($pendingAdjudicators as $value) {
				$data = ['userId' => $value->to_user_id, 'confirmationLink' => $value->accknowledge_link];
				echo $value->user->email . '<br/>';
				Mail::to($value->user->email)->send(new SendMail($data, $view, $subject));
		*/
	}

	public function sendAdjudicatorReminderAssign() {
		/*$dissertAssignedUsers = ThesisTransaction::whereNotNull('accknowledge_link')->whereHas('thesis.category', function ($q) {
				$q->where('parent', 29);
			})->where('status', 1)->skip(80)->take(25)->get();
			$view = 'emails.dissertation-assign-confirmation';
			$subject = 'Dissertation Assign';
			$attached_path = public_path('files/Help_manual_for_Adjudicator_level_review_of_Dissertations.docx');
			foreach ($dissertAssignedUsers as $value) {
				$data = ['userId' => $value->to_user_id, 'confirmationLink' => $value->accknowledge_link];
				echo $value->user->email . '<br/>';
				Mail::to($value->user->email)->send(new SendMail($data, $view, $subject, $attached_path));
		*/
	}

	public function setUserDefaultPassword($id) {
		$user = User::find($id);
		$user->password = bcrypt('user123$');
		$user->save();
		Session::flash('msg', 'Sucessfuly updated password');
		return redirect()->back();
	}

	public function getUserExcel(Request $request) {
		$users = User::where(function ($q) use ($request) {
			if (!empty($request->type)) {
				$q->whereHas('roles', function ($q) use ($request) {
					$q->where('id', $request->type);
				});
			}
		})->orderBy('id', 'desc')->pluck('id')->toArray();
		return Excel::download(new UsersExport($users), 'user-detail.xlsx');
	}

	public function getPendingAdjudicatorList(Request $request, $id) {
		$pendingThesis = ThesisTransaction::whereIn('status', [1, 2])
			->whereHas('thesis', function ($q) use ($id, $request) {
				$q->where('category_id', $id);
				if (!empty($request->from_date) && !empty($request->to_date)) {
					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
				/*$q->whereHas('thesisResult', function ($q) {
					$q->whereNull('id');
				*/
			})->orderBy('id', 'desc')->pluck('id')->toArray();
		return Excel::download(new PendingThesisExport($pendingThesis), 'pending-thesis.xlsx');
	}
	public function coursewiseStudentList($id) {
		$thesis = Thesis::where('category_id', $id)->orderBy('id', 'desc')->pluck('id')->toArray();
		return Excel::download(new StudentThesisExport($thesis), 'student-thesis.xlsx');
	}

	public function categoryWiseStudentList($id) {
		$thesis = Thesis::whereHas('category', function ($q) use ($id) {
			$q->where('parent', $id);
		})->orderBy('id', 'desc')->pluck('id')->toArray();
		return Excel::download(new StudentThesisExport($thesis), 'student-thesis.xlsx');
	}

	public function coursewiseSynopsisStudentList($id) {
		$synopsis = Synopsis::where('category_id', $id)->orderBy('id', 'desc')->pluck('id')->toArray();
		return Excel::download(new StudentSynopsisExport($synopsis), 'student-synopsis.xlsx');
	}

	public function categoryWiseSynopsisStudentList($id) {
		$synopsis = Synopsis::whereHas('category', function ($q) use ($id) {
			$q->where('parent', $id);
		})->orderBy('id', 'desc')->pluck('id')->toArray();
		return Excel::download(new StudentSynopsisExport($synopsis), 'student-synopsis.xlsx');
	}

	public function getStatusWiseStudentsExport(Request $request) {
		$synopsis = Synopsis::has('transaction')->whereHas('category', function ($q) use ($request) {
			if (!empty($request->child_catId)) {
				$q->where('parent', $request->child_catId);
			}
			if (!empty($request->courseId)) {
				$q->where('id', $request->courseId);
			}
		})->where(function ($q) use ($request) {
			if (!empty($request->from_date) && !empty($request->to_date)) {
				$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
				$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
			}
			if (!empty($request->by_status)) {
				$q->where('status', $request->by_status);
			}
		})->pluck('id')->toArray();
		return Excel::download(new StatusWiseStudentExport($synopsis), 'student-synopsis.xlsx');
	}

	public function getAssignStudentsExport(Request $request) {
		//dd($request->all());
		$synopsis = Synopsis::has('transaction')->whereHas('category', function ($q) use ($request) {
			/*if (!empty($request->category)) {
				$q->whereHas('parentCat', function ($q) use ($request) {
					$q->where('parent', $request->category);
				});
			}*/
			if (!empty($request->child_catId)) {
				$q->where('parent', $request->child_catId);
			}
			if (!empty($request->courseId)) {
				$q->where('id', $request->courseId);
			}
		})->where(function ($q) use ($request) {
			if (!empty($request->from_date) && !empty($request->to_date)) {
				$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
				$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
			}
			if (!empty($request->by_status)) {
				$q->where('status', $request->by_status);
			}
		})->pluck('id')->toArray();
		return Excel::download(new StatusWiseStudentExport($synopsis), 'student-synopsis.xlsx');
	}

	public function getAssignStudentsExport(Request $request) {
		//dd($request->all());
		$synopsis = Synopsis::has('transaction')->whereHas('category', function ($q) use ($request) {
			/*if (!empty($request->category)) {
				$q->whereHas('parentCat', function ($q) use ($request) {
					$q->where('parent', $request->category);
				});
			}*/
			if (!empty($request->child_catId)) {
				$q->where('parent', $request->child_catId);
			}
			if (!empty($request->courseId)) {
				$q->where('id', $request->courseId);
			}

		})->where(function ($q) use ($request) {
			if (!empty($request->from_date) && !empty($request->to_date)) {
				$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
				$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
			}
			if (!empty($request->by_status)) {
				$q->where('status', $request->by_status);
			}
		})->pluck('id')->toArray();
		//dd($synopsis);
		return Excel::download(new AssignStudentExport($synopsis), 'student-synopsis.xlsx');
	}

	public function getNotAssignStudentExport(Request $request) {
		$synopsis = Synopsis::doesntHave('transaction')->whereHas('category', function ($q) use ($request) {
			if (!empty($request->category)) {
				$q->whereHas('parentCat', function ($q) use ($request) {
					$q->where('parent', $request->category);
				});
			}
			if (!empty($request->child_catId)) {
				$q->where('parent', $request->child_catId);
			}
			if (!empty($request->courseId)) {
				$q->where('id', $request->courseId);
			}
		})->where(function ($q) use ($request) {
			if (!empty($request->from_date) && !empty($request->to_date)) {
				$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
				$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
			}
		})->pluck('id')->toArray();
		return Excel::download(new NotAssignStudentExport($synopsis), 'student-synopsis.xlsx');
	}

	public function getAssignThesisStudentsExport(Request $request) {
		if ($request->category == 76) {
			$thesis = Thesis::has('transaction')->whereHas('category', function ($q) use ($request) {
				if (!empty($request->category)) {
					$q->where('parent', $request->category);
				}
				if (!empty($request->child_catId)) {
					$q->where('id', $request->child_catId);
				}
			})->where(function ($q) use ($request) {
				if (!empty($request->from_date) && !empty($request->to_date)) {
					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
			})->pluck('id')->toArray();
		} else {
			$thesis = Thesis::has('transaction')->whereHas('category', function ($q) use ($request) {
				if (!empty($request->category)) {
					$q->whereHas('parentCat', function ($q) use ($request) {
						$q->where('parent', $request->category);
					});
				}
				if (!empty($request->child_catId)) {
					$q->where('parent', $request->child_catId);
				}
				if (!empty($request->courseId)) {
					$q->where('id', $request->courseId);
				}
			})->where(function ($q) use ($request) {
				if (!empty($request->from_date) && !empty($request->to_date)) {
					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
			})->pluck('id')->toArray();
		}

		return Excel::download(new AssignThesisStudentExport($thesis), 'student-thesis.xlsx');
	}

	public function getNotAssignThesisStudentExport(Request $request) {
		if ($request->category == 76) {
			$thesis = Thesis::doesntHave('transaction')->whereHas('category', function ($q) use ($request) {

				if (!empty($request->category)) {
					$q->where('parent', $request->category);
				}
				if (!empty($request->child_catId)) {
					$q->where('id', $request->child_catId);
				}
			})->where(function ($q) use ($request) {
				if (!empty($request->from_date) && !empty($request->to_date)) {
					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
			})->pluck('id')->toArray();
		} else {
			$thesis = Thesis::doesntHave('transaction')->whereHas('category', function ($q) use ($request) {
				if (!empty($request->category)) {
					$q->whereHas('parentCat', function ($q) use ($request) {
						$q->where('parent', $request->category);
					});
				}
				if (!empty($request->child_catId)) {
					$q->where('parent', $request->child_catId);
				}
				if (!empty($request->courseId)) {
					$q->where('id', $request->courseId);
				}
			})->where(function ($q) use ($request) {
				if (!empty($request->from_date) && !empty($request->to_date)) {
					$q->whereDate('created_at', '>=', Carbon::parse(str_replace('/', '-', $request->from_date))->format('Y-m-d'));
					$q->whereDate('created_at', '<=', Carbon::parse(str_replace('/', '-', $request->to_date))->format('Y-m-d'));
				}
			})->pluck('id')->toArray();
		}

		return Excel::download(new NotAssignThesisStudentExport($thesis), 'student-thesis.xlsx');
	}

	public function assingBOSAutomatic() {
		dd('we');
		$unAssignSynopsis = Synopsis::doesntHave('transaction')->whereHas('category', function ($q) {
			$q->where('parent', 60);
		})->has('payements')->whereNull('deleted_at')->skip(0)->take(100)->get();

		$view = 'emails.synopsis-mail';
		$attached_path = public_path('files/Help_manual_for_BOS_review_status.docx');
		$subject = 'Synopsis';

		foreach ($unAssignSynopsis as $value) {
			$getBos = $this->getBosForAssingSynopsis($value->category_id, $value->institute_code);
			$checkExist = SynopsisTransaction::where('synopsis_id', $value->id)->first();

			if ($getBos && !$checkExist) {
				SynopsisTransaction::create(['synopsis_id' => $value->id, 'to_user_id' => $getBos->id, 'trans_type' => 1, 'send_to' => $getBos->email, 'review_status' => 0]);
				Mail::to($getBos->email)->send(new SendMail(['type' => 1, 'synopsis' => $value, 'to_user' => $getBos], $view, $subject, $attached_path));
			}
		}

		return 'ok';
	}

	public function getBosForAssingSynopsis($category, $college) {
		$BOS_members = User::whereHas('roles', function ($q) {
			$q->where('slug', 'bos');
		})->whereHas('category', function ($q) use ($category) {
			$q->where('id', $category);
		})->whereHas('userDetail', function ($q) use ($college) {
			$q->where('college_code', '!=', $college);
		})->pluck('id')->toArray();

		$notAssignUsers = User::whereIn('id', $BOS_members)->doesntHave('synopsisTransaction')->first();
		if ($notAssignUsers) {
			return $notAssignUsers;
		}

		$users = \DB::table('synopsis_transactions')
			->select(\DB::raw('count(*) as assing_synopsis, to_user_id'))
			->whereIn('to_user_id', $BOS_members)
			->groupBy('to_user_id')
			->orderBy('assing_synopsis', 'asc')
			->first();
		return $users ? User::find($users->to_user_id) : '';
	}

	public function viewOtherBosReview() {
		$title = 'Other Bos Report';
		$user = Auth::guard('admin')->user();
		$userCategories = $user->category->pluck('id')->toArray();
		$otherTransactions = SynopsisTransaction::whereHas('synopsis', function ($q) use ($userCategories) {
			$q->whereIn('category_id', $userCategories);
		})->where('to_user_id', '!=', $user->id)->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.synopsis-other-bos', compact('otherTransactions', 'title'));
	}

	public function createAutoBos() {
		dd('test');
		$data = TestData::all();
		foreach ($data as $value) {
			$user = User::create(['f_name' => $value->f_name, 'l_name' => $value->l_name, 'email' => $value->email, 'mobile' => $value->mobile, 'password' => bcrypt('user123$'), 'is_admin' => 2, 'is_active' => 1, 'email_verified_at' => Carbon::now()]);
			$college = CollegeMaster::where('college_code', $value->college_code)->first();
			$userDetail = UserDetail::create(['mobile' => $value->mobile, 'address' => $value->address, 'user_id' => $user->id, 'college_id' => $college->id, 'college_name' => $college->college_name, 'college_code' => $college->college_code]);
			$user->roles()->attach(4);
			$catId = Category::where('name', $value->specialize)->first();
			if ($catId) {
				$user->category()->attach($catId);
			}
			$user->save();
		}
	}

	public function getDuplicateUser() {
		$emails = \DB::table('users')->select('email')->where('is_admin', 2)->whereNull('deleted_at')->groupBy('email')->having(\DB::raw('count(*)'), '>=', 2)->pluck('email')->toArray();
		dd($emails);
		foreach ($emails as $value) {
			$adjudicator = User::find($this->getAdjudicator($value));
			$bos = $this->getBOS($value) ? User::find($this->getBOS($value)) : '';
			if ($bos) {
				$userDetail = $adjudicator->userDetail;
				$userDetail->fill(['college_id' => $bos->userDetail->college_id, 'college_name' => $bos->userDetail->college_name, 'college_code' => $bos->userDetail->college_code]);
				$userDetail->save();
				$adjudicator->roles()->attach(4);
				$adjudicator->save();
				SynopsisTransaction::where('to_user_id', $bos->id)->update(['to_user_id' => $adjudicator->id]);
				$bosUserDet = $bos->userDetail;
				$bosUserDet->forceDelete();
				$bos->forceDelete();
			}
		}
	}

	private function getAdjudicator($email) {
		$user = User::where('email', $email)->whereHas('roles', function ($q) {
			$q->where('slug', 'adjudicator');
		})->first();
		return $user->id;
	}

	private function getBOS($email) {
		$user = User::where('email', $email)->whereHas('roles', function ($q) {
			$q->where('slug', 'bos');
		})->whereDate('created_at', '2020-04-28')->first();
		return $user ? $user->id : '';
	}

	public function getNotPaidThesisStudentExport(Request $request) {
		$thesis = Thesis::doesntHave('payements')->pluck('id')->toArray();
		return Excel::download(new NotPaidStudentExport($thesis), 'student-notpaid.xlsx');
	}

	public function sendEmailForSynopsisSubmit() {
		$students = [
			'dibyayanam.sahu@gmail.com', 'sidhu.d84@gmail.com', 'doctormekhla@gmail.com', 'drsrabanighosh@73gmail.com',
			'aadil151985@gmail.com', 'tanweerqamargaya@gmail.com', 'dayapaswan@gmail.com',
			'aby3246@gmail.com',
			'pujatrigunait1@gmail.com',
			'drabhijit237@gmail.com',
			'dasdebaditya@yahoo.com',
			'dibakarsardar49@gmail.com',
			'dr.pritha.dhara@gmail.com',
			'c.gokulraman@gmail.com',
			'sgshashankgpt5@gmail.com',
			'drjayasri80@gmail.com',
			'ganjshumita@gmail.com',
			'kim_mercy007@yahoo.com',
			'rashmijha612@gmail.com',
			'abhilashchat@gmail.com',
			'anindya2611@gmail.com',
			'nyingpo123@gmail.com',
			'mantughosh732@gmail.com',
			'indraneel2992@gmail.com',
			'rayy.sukanyaa@gmail.com',
			'drmrinmoychakraborty@gmail.com',
			'saddy9592naaz@gmail.com',
			'rabi.murmu07@gmail.com',
			'gettamal94@gmail.com',
			'madhumitasrb@gmail.com',
			'khanalimunzir@gmail.com',
			'roynirupa23@gmail.com',
			'ankurgandhi69@gmail.com',
			'drmdabdullahsaikh@gmail.com',
			'pujatrigunait1@gmail.com',
			'aby3246@gmail.com',
			'sougata.papai28@gmail.com',
			'dr.kunalgoel@gmail.com',
			'saketsoni.amu@gmail.com',
			'rakeshdasagmc@gmail.com',
			'tejasvisharma15@gmail.com',
			'souravshristi@gmail.com',
			'tanweerqamargaya@gmail.com',
			'doctormekhla@gmail.com',
			'riju.datta@gmail.com',
			'sidhu.d84@gmail.com',
			'drrajuagrawal@gmail.com',
			'swetajha_93@yahoo.com',
			'amirmbbs2017@gmail.com',
			'sutapabarik1@gmail.com',
			'bhattacharjee.nilay@gmail.com',
			'rajarshimandal1987@gmail.com',
			'sailensadhukhan10072014@gmail.com',
			'prabirmandi6@gmail.com',
			'aymuos100@gmail.com',
			'pattanayak.jagadish@rediffmail.com',
			'nafisuzma@yahoo.com',
			'druday1987@gmail.com',
			'amustamang@gmail.com',
			'bisuwin914@gmail.com',
			'arindam.naskar.1991@gmail.com',
			'debdaschowdhury12@gmail.com',
			'mazwsc@gmail.com',
			'drsantoshbanik@gmail.com',
			'angukato10@gmail.com',
			'dramardeep16@gmail.com',
			'subs_bhaumik@yahoo.com',
			'preeti1292.gupta@gmail.com',
		];
		foreach ($students as $student) {
			$isSended = Synopsis::where('email', $student)->first();
			$view = 'emails.synopsis-submission-reminder';
			$subject = 'Synopsis Submission Reminder';
			if (!$isSended) {
				Mail::to(strtolower($student))->send(new SendMail(['type' => 1], $view, $subject));
				echo strtolower($student) . '<br/>';
			}
		}
	}

	public function getBosByCategory(Request $request) {
		$synopsis = Synopsis::find($request->id);
		$users = User::whereHas('roles', function ($q) {
			$q->where('slug', 'bos');
		})->whereHas('category', function ($q) use ($synopsis) {
			$q->where('id', $synopsis->category_id);
		})->get();
		return response()->json(['status' => 200, 'data' => $users]);
	}

	public function adjudicatorStateUpdate() {
		$addStates = AdjudicatorState::all();
		foreach ($addStates as $state) {
			if ($state->state_id) {
				$userDetail = $state->userDetail;
				$userDetail->state_id = $state->state_id;
				$userDetail->save();
			}
		}
		echo 'done';
	}

	public function updateAssignThesis() {
		Thesis::has('transaction')->whereHas('category', function ($q) {
			$q->where('parent', 2);
		})->where('status', 1)->update(['status' => 2]);
	}

	public function assingAdjudicatorAutomatic() {

		$unAssignThesis = Thesis::doesntHave('transaction')->whereHas('category', function ($q) {
			$q->where('parent', 2);
		})->has('payements')->whereNull('deleted_at')
			->whereDate('created_at', '>=', Carbon::parse('2020-08-01')->format('Y-m-d'))->skip(0)->take(25)->get();

		foreach ($unAssignThesis as $value) {

			if ($value->transaction()->count() < 3) {
				$getInternalAdjudicator = $this->getAdjudicatorForAssingThesis($value->category_id, $value->institute_code, $isState = true);
				if ($getInternalAdjudicator) {
					$checkExistInternal = ThesisTransaction::where('thesis_id', $value->id)->where('to_user_id', $getInternalAdjudicator->id)->first();

					if ($getInternalAdjudicator && !$checkExistInternal) {
						if ($value->transaction()->count() < 3) {
							$this->sendThesisEmail($getInternalAdjudicator, $value);
						}
					}
				}

				$getExternale1Adjudicator = $this->getAdjudicatorForAssingThesis($value->category_id, $value->institute_code, $isState = false);
				if ($getExternale1Adjudicator) {
					$checkExistExternal1 = ThesisTransaction::where('thesis_id', $value->id)->where('to_user_id', $getExternale1Adjudicator->id)->first();

					if ($getExternale1Adjudicator && !$checkExistExternal1) {
						if ($value->transaction()->count() < 3) {
							$this->sendThesisEmail($getExternale1Adjudicator, $value);
						}
					}
				}

				$getExternal2Adjudicator = $this->getAdjudicatorForAssingThesis($value->category_id, $value->institute_code, $isState = false);
				if ($getExternal2Adjudicator) {
					$checkExistExternal2 = ThesisTransaction::where('thesis_id', $value->id)->where('to_user_id', $getExternal2Adjudicator->id)->first();

					if ($getExternal2Adjudicator && !$checkExistExternal2) {
						if ($value->transaction()->count() < 3) {
							$this->sendThesisEmail($getExternal2Adjudicator, $value);
						}
					}
				}
			}
		}

		return 'ok';
	}

	public function sendThesisEmail($uservalue, $thesis) {
		$thesisCount = ThesisTransaction::where('thesis_id', $thesis->id)->get();
		if (count($thesisCount) < 3) {
			$string = 15;
			$result = bin2hex(random_bytes($string));
			$subject = 'Acknowledgement mail for Assign Dissertation ';
			$to = $uservalue->email;
			$confirmationLink = $result . $uservalue->id . date('YmdHis');

			$data = ['type' => 2, 'thesis' => $thesis, 'confirmationLink' => $confirmationLink, 'userId' => $uservalue->id];

			Mail::send('emails.dissertation-assign-confirmation', $data, function ($message) use ($to, $subject) {
				$message->to($to)->subject($subject);
				$message->attach(url('public/files/Help_manual_for_Adjudicator_level_review_of_Dissertations.docx'));
				$message->from(config('mail.from.address'), 'WBUHS');
			});

			$thesisTrans = ThesisTransaction::where('to_user_id', $uservalue->id)->where('thesis_id', $thesis->id)->where('is_active', 1)->first();
			if ($thesisTrans) {

				$thesisTrans->accknowledge_email_date = Carbon::now();
				$thesisTrans->accknowledge_link = $confirmationLink;
				$thesisTrans->count_send = $thesisTrans->count_send + 1;
				$thesisTrans->updated_by = Auth::guard('admin')->user()->id;
				$thesisTrans->save();
			} else {
				$uservalue->count_thesis += 1;
				$uservalue->save();
				ThesisTransaction::insert(['thesis_id' => $thesis->id, 'to_user_id' => $uservalue->id, 'trans_type' => 1, 'send_to' => $uservalue->email, 'accknowledge_link' => $confirmationLink, 'count_send' => 1, 'accknowledge_email_date' => Carbon::now(), 'is_active' => 1, 'created_by' => 1]);
			}
		}
	}

	public function getAdjudicatorForAssingThesis($category, $college, $isState) {
		$notToAssignAdjudicatorIds = \DB::table('thesis_transactions')->selectRaw("to_user_id, COUNT(to_user_id) as count_thesis")->whereRaw("`accknowledge_email_date` >='2020-12-10' and `deleted_at` is null  GROUP BY `to_user_id` HAVING COUNT(to_user_id) >5")->pluck('to_user_id')->toArray();
		$ADJUDIC_members = User::whereNotIn('id', $notToAssignAdjudicatorIds)->whereHas('roles', function ($q) {
			$q->where('slug', 'adjudicator');
		})->whereHas('category', function ($q) use ($category) {
			$q->where('id', $category);
		})->whereHas('userDetail', function ($q) use ($college, $isState) {
			if ($isState) {
				$q->where('college_code', '!=', $college);
				$q->where('state_id', '=', 28);
			} else {
				$q->where('state_id', '!=', 28);
			}
		})->pluck('id')->toArray();

		$notAssignUsers = User::whereIn('id', $ADJUDIC_members)->doesntHave('thesisTransction')->first();
		if ($notAssignUsers) {
			return $notAssignUsers;
		}

		$users = \DB::table('thesis_transactions')
			->select(\DB::raw('count(*) as assing_thesis, to_user_id'))
			->whereIn('to_user_id', $ADJUDIC_members)
			->groupBy('to_user_id')
			->orderBy('assing_thesis', 'asc')
			->first();
		return $users ? User::find($users->to_user_id) : '';
	}

	public function theisReAssign() {
		$allthesis = ThesisCheck::whereNull('is_done')->skip(0)->take(25)->get();
		foreach ($allthesis as $key => $value) {
			$stateHas = ThesisTransaction::where('thesis_id', $value->thesis_id)->whereHas('user', function ($q) {
				$q->whereHas('userDetail', function ($q) {
					$q->where('state_id', '=', 28);
				});
			})->count();
			if ($stateHas == 0) {
				$getInternalAdjudicator = $this->theisReAssignCheck($value->thesis->category_id, $value->thesis->institute_code, $isState = true);
				if (@$getInternalAdjudicator) {
					$this->sendThesisEmail($getInternalAdjudicator, $value->thesis);
					echo @$getInternalAdjudicator->email . " internal " . $value->thesis_id . "<br/>";
					$value->is_done = 1;
					$value->save();
				}
			} else {
				$getexternalAdjudicator = $this->theisReAssignCheck($value->thesis->category_id, $value->thesis->institute_code, $isState = false);
				if (@$getexternalAdjudicator) {
					$this->sendThesisEmail($getexternalAdjudicator, $value->thesis);
					echo @$getexternalAdjudicator->email . " extranal " . $value->thesis_id . "<br/>";
					$value->is_done = 1;
					$value->save();
				}
			}
		}
	}

	public function theisReAssignCheck($category, $college, $isState) {
		$notToAssignAdjudicatorIds = \DB::table('thesis_transactions')->selectRaw("to_user_id, COUNT(to_user_id) as count_thesis")->whereRaw("`accknowledge_email_date` >='2020-12-10' and `deleted_at` is null  GROUP BY `to_user_id` HAVING COUNT(to_user_id) >7")->pluck('to_user_id')->toArray();

		$ADJUDIC_members = User::whereNotIn('id', $notToAssignAdjudicatorIds)->whereHas('roles', function ($q) {
			$q->where('slug', 'adjudicator');
		})->whereHas('category', function ($q) use ($category) {
			$q->where('id', $category);
		})->whereHas('userDetail', function ($q) use ($college, $isState) {
			if ($isState) {
				$q->where('college_code', '!=', $college);
				$q->where('state_id', '=', 28);
			} else {
				$q->where('state_id', '!=', 28);
			}
		})->pluck('id')->toArray();

		$notAssignUsers = User::whereIn('id', $ADJUDIC_members)->doesntHave('thesisTransction')->first();
		if ($notAssignUsers) {
			return $notAssignUsers;
		}

		$users = \DB::table('thesis_transactions')
			->select(\DB::raw('count(*) as assing_thesis, to_user_id'))
			->whereIn('to_user_id', $ADJUDIC_members)
			->groupBy('to_user_id')
			->orderBy('assing_thesis', 'asc')
			->first();
		return $users ? User::find($users->to_user_id) : '';

		/*$notToAssignAdjudicatorIds = \DB::select("SELECT to_user_id, COUNT(to_user_id) FROM `wb_thesis_transactions` WHERE `accknowledge_email_date` >='2020-12-10' and `deleted_at` is null  GROUP BY `to_user_id` HAVING COUNT(to_user_id) >5");
		dd($notToAssignAdjudicatorIds);*/
	}

	public function uploadBosPdf(Request $request) {
		$title = 'UPLOAD PDF';
		$user = Auth::guard('admin')->user();
		$bosPdfs = BosMemberPdf::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.bos_upload_pdf', compact('title', 'bosPdfs'));
	}

	public function submitUploadBosPdf(Request $request) {
		//dd($request->all());
		ini_set('memory_limit', '25600M');
		ini_set('max_execution_time', 0);
		ini_set('upload_max_filesize', '25600M');
		ini_set('post_max_size', '25600M');
		$validator = Validator::make($request->all(), [
			'pdf_file' => 'required|mimes:pdf',
		], []);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		$user = Auth::guard('admin')->user();

		if ($request->hasFile('pdf_file')) {
			$pdf_file = $request->file('pdf_file');
			$filename = $user->full_name . time() . $pdf_file->getClientOriginalName();
			$pdf_file->move(public_path('bos_pdfs'), $filename);
			BosMemberPdf::create(['user_id' => $user->id, 'pdf_file' => $filename]);
			Session::flash('success', 'Sucessfully uploaded pdf.');
			return redirect()->back();
		}
	}

	public function getBosPdfs(Request $request) {
		$title = 'ADMIN | USERS';
		$bosPdfs = BosMemberPdf::orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.bos_upload_list', compact('bosPdfs', 'title'));
	}

	// Synopsis Title management

	public function studentSynopsisTitle() {
		$title = 'ADMIN | SYNOPSIS TITLE MANAGEMENT';
		$synopsisdetails = SynopsisTitle::orderBy('id', 'asc')->paginate(10);
		return view('admin.synopsis-title.index', compact('title', 'synopsisdetails'));
	}
	public function studentSynopsisTitleCreate() {
		$title = 'ADMIN | CREATE SYNOPSIS TITLE ';
		return view('admin.synopsis-title.create', compact('title'));
	}
	public function studentSynopsisTitleStore(Request $request) {
		// dd($request->all());
		$messsages = array(
			'st_name.required' => 'The name field is required.',
			'st_pdf.required' => 'The file field is required.',
		);

		$validator = Validator::make($request->all(), [
			'st_name' => 'required',
			'st_pdf' => 'required',

		], $messsages);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		try {

			$synopsisTitle = SynopsisTitle::create([

				'st_name' => $request->st_name,

			]);

			if ($request->hasFile('st_pdf')) {

				$pfile = $request->file('st_pdf');
				$pfilename = $pfile->getClientOriginalName();
				$pfile->move(public_path('pdfs'), $pfilename);
				// dd($pfilename);

				$synopsisTitle->st_pdf = $pfilename;
				$synopsisTitle->save();
			}

			Session::flash('msg', 'Sucessfuly created');
			return redirect()->route('admin.student-synopsis-title');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back()->withInput();
		}

	}

	public function uploadBosPdf(Request $request) {
		$title = 'UPLOAD PDF';
		$user = Auth::guard('admin')->user();
		$bosPdfs = BosMemberPdf::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10)->appends(request()->query());
		return view('admin.bos_upload_pdf', compact('title', 'bosPdfs'));

	}

	public function submitUploadBosPdf(Request $request) {
		//dd($request->all());
		ini_set('memory_limit', '25600M');
		ini_set('max_execution_time', 0);
		ini_set('upload_max_filesize', '25600M');
		ini_set('post_max_size', '25600M');
		$validator = Validator::make($request->all(), [
			'pdf_file' => 'required|mimes:pdf',
		], []);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		$user = Auth::guard('admin')->user();

		if ($request->hasFile('pdf_file')) {
			$pdf_file = $request->file('pdf_file');
			$filename = $user->full_name . time() . $pdf_file->getClientOriginalName();
			$pdf_file->move(public_path('bos_pdfs'), $filename);
			BosMemberPdf::create(['user_id' => $user->id, 'pdf_file' => $filename]);
			Session::flash('success', 'Sucessfully uploaded pdf.');
			return redirect()->back();
		}

	}

	public function studentSynopsisTitleDelete($id) {
		try {
			$synopsisDtl = SynopsisTitle::find($id);
			$synopsisDtl->delete();
			return response()->json(['status' => 200, 'status_text' => 'Sucessfuly deleted']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}
	}

    public function thesisEnableDate()
    {
        $students = Synopsis::where('status', 2)->whereNotNull('approved_date')->get();
//        dd($students);
        $thesisEnableDate = Carbon::now()->addDays(365)->format('d/m/Y');
//        dd($thesisEnableDate);
        foreach ($students as $student) {
            if ($student->approved_date >= $thesisEnableDate) {
                StudentData::where('registration_no', $student->registration_no)->update([
                    'thesis_enabble_date' => $thesisEnableDate
                ]);
            }
        }
    }
}
