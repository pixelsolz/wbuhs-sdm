<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Session;
use Validator;

class CategoryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$title = 'Admin | Category';
		$categories = $this->categoryTree();

		//dd($categories);
		return view('admin.category.index', compact('title', 'categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$title = 'Admin | Category create';
		$categories = $this->categoryTreeDropdown();
		return view('admin.category.create', compact('title', 'categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$message = [
			'parent.required' => 'The category field required.',
		];
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'parent' => 'required',
		], $message);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$parent = Category::find($request->parent);
			if ($parent && $parent->has_child != 1) {
				$parent->has_child = 1;
				$parent->save();
			}
			$category = Category::create($request->all());
			Session::flash('success', 'Category created succesfuly');
			return redirect('admin/category');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		try {
			$category = Category::find($id);
			$category->fill($request->all());
			$category->save();
			return redirect('');
		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	protected function categoryTree($parent = 0, $user_tree_array = '') {
		if (!is_array($user_tree_array)) {
			$user_tree_array = array();
		}

		$query = DB::select(DB::raw("select * from `wb_categories` WHERE 1 AND `parent` = $parent order by name asc"));
		//return $query;
		if (count($query) > 0) {
			if ($parent != 0) {
				$user_tree_array[] = "<ul class='nested'>";
			}

			foreach ($query as $row) {
				if ($row->parent == 0) {
					$user_tree_array[] = "<li><span class='boxes'>" . $row->name . "</span>";
				} elseif ($parent == $row->parent) {
					$user_tree_array[] = (($row->has_child == 1) ? "<li><span class='boxes'>" : "<li>") . $row->name . (($row->has_child == 1) ? "</span>" : "</li>");
				} /*else {
					'cdcdc';
					$user_tree_array[] = "</li>";
				}*/
				/*else {
					$user_tree_array[] = "<li>" . $row->name . $parent . "</li>";
				}*/
				//$user_tree_array[] = "<li>" . $row->name . "</li>";
				$user_tree_array = $this->categoryTree($row->id, $user_tree_array);

			}
			if ($parent != 0) {
				$user_tree_array[] = "</ul></li>";
			}
		}
		return $user_tree_array;

	}

	protected function categoryTreeDropdown($parent = 0, $spacing = '-', $user_tree_array = '') {
		if (!is_array($user_tree_array)) {
			$user_tree_array = array();
		}

		$query = DB::select(DB::raw("select * from `wb_categories` WHERE 1 AND `parent` = $parent order by id asc"));
		//return count($query);
		if (count($query) > 0) {
			foreach ($query as $row) {
				$user_tree_array[] = array("id" => $row->id, "name" => $spacing . $row->name);
				$user_tree_array = $this->categoryTreeDropdown($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);

			}
		}
		return $user_tree_array;

	}

	public function categoryWiseUser(Request $request) {
		$title = 'Admin | Category User';
		$categories = Category::all();

		$users = User::where(function ($q) use ($request) {
			if ($request->has('search')) {
				if ($request->has('search_by_category') && !empty($request->search_by_category)) {
					$q->whereHas('category', function ($q) use ($request) {
						$q->where('id', $request->search_by_category);
					});
				}
				if ($request->has('input_search') && !empty($request->input_search)) {
					$q->Where('f_name', 'like', '%' . $request->input_search);
					$q->orWhere('email', 'like', '%' . $request->input_search . '%');
					$q->orWhereHas('userDetail', function ($qe) use ($request) {
						$qe->where('mobile', 'like', '%' . $request->input_search . '%');
					});
				}
			}
		})->whereHas('roles', function ($q) {
			$q->whereIn('slug', ['adjudicator', 'bos']);
		})->orderBy('id', 'desc')->paginate(10)->appends(request()->query());

		/*$users = User::whereHas('roles', function ($q) {
			$q->whereIn('slug', ['reviewer', 'bos']);
		})->orderBy('id', 'desc')->paginate(10);*/
		return view('admin.category.user-wise-cat', compact('title', 'users', 'request', 'categories'));
	}
}
