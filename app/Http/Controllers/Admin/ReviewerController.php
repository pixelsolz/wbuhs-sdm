<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\Thesis;
use App\Models\ThesisReviewerResubmitHistories;
use App\Models\ThesisReviewerResult;
use App\Models\ThesisTransaction;
use App\Models\User;
use App\Notifications\SendNotify;
use Auth;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Image;
use Mail;
use Session;
use Validator;

class ReviewerController extends Controller {
	public function __construct() {

	}

	public function addReviewResult(Request $request, Thesis $thesis) {
		$title = 'Result Add';
		$faculty = Auth::guard('admin')->user();
		$transaction = $thesis->transaction()->where('to_user_id', $faculty->id)->first();
		if ($transaction->is_reassigned == 1) {
			$reviewResult = '';
		} else {
			$reviewResult = ThesisReviewerResult::where('thesis_id', $thesis->id)->where('faculty_id', $faculty->id)->first();
		}

		return view('admin.thesis-reviewer.result-form', compact('title', 'thesis', 'faculty', 'reviewResult'));
	}

	public function editReviewResult(Request $request, ThesisReviewerResult $result) {
		$title = 'Result View';
		$faculty = Auth::guard('admin')->user();
		$reviewResult = $result;
		return view('admin.thesis-reviewer.result-form-view', compact('title', 'reviewResult', 'faculty'));
	}

	public function viewReviewHistory($id) {
		$title = 'Result History View';
		$faculty = Auth::guard('admin')->user();
		$reviewResult = ThesisReviewerResubmitHistories::find($id);
		return view('admin.thesis-reviewer.result-form-view', compact('title', 'reviewResult', 'faculty'));
	}

	public function adminViewResult($user_id, ThesisReviewerResult $result) {
		$title = 'Result View';
		$faculty = User::find($user_id);
		$reviewResult = $result;
		return view('admin.thesis-reviewer.result-form-view', compact('title', 'reviewResult', 'faculty'));
	}

	public function intialResultStore(Request $request, $no) {

		if ($reviewResult = ThesisReviewerResult::where('thesis_id', $request->thesis_id)->where('faculty_id', $request->faculty_id)->first()) {
			switch ($no) {
			case 1:
				$reviewResult->fill($request->only('lacunae_knowledge', 'lacunae_knowledge_remarks', 'present_study', 'present_study_remarks', 'hypothesis', 'hypothesis_remarks'));
				break;
			case 2:
				$reviewResult->fill($request->only('eom', 'eom_remarks', 'clarity_specificity', 'clarity_specificity_remarks'));
				break;
			case 3:
				$reviewResult->fill($request->only('relevance', 'relevance_remarks', 'completeness', 'completeness_remarks', 'current_uptodate', 'current_uptodate_remarks'));
				break;
			case 4:
				$reviewResult->fill($request->only('psps', 'psps_remarks', 'type_study', 'type_study_remarks', 'inclution_exclution', 'inclution_exclution_remarks', 'outcome_parameters', 'outcome_parameters_remarks', 'study_variables', 'study_variables_remarks', 'sampling_design', 'sampling_design_remarks', 'materials_experimental', 'materials_experimental_remarks', 'data_collection_tools', 'data_collection_tools_remarks', 'statistical_methods', 'statistical_methods_remarks'));
				break;
			case 5:
				$reviewResult->fill($request->only('logical_organization', 'logical_organization_remarks', 'correctness_data_analysis', 'correctness_data_analysis_remarks', 'appropriate_tables', 'appropriate_tables_remarks', 'statistical_interpretation', 'statistical_interpretation_remarks'));
				break;
			case 6:
				$reviewResult->fill($request->only('discussion_relevance', 'discussion_relevance_remarks', 'interpretation_results', 'interpretation_results_remarks'));
				break;
			case 7:
				$reviewResult->fill($request->only('summary_conclusions', 'summary_conclusions_remarks'));
				break;
			case 8:
				$reviewResult->fill($request->only('limitations', 'limitations_remarks'));
				break;
			case 9:
				$reviewResult->fill($request->only('citation_appropriate', 'citation_appropriate_remarks'));
				break;
			case 10:
				$reviewResult->fill($request->only('iecc', 'iecc_remarks'));
				break;
			default:
				// code...
				break;
			}
			$reviewResult->save();
		} else {
			ThesisReviewerResult::create($request->only('thesis_id', 'faculty_id', 'thesis_title', 'course_subject', 'lacunae_knowledge', 'lacunae_knowledge_remarks', 'present_study', 'present_study_remarks', 'hypothesis', 'hypothesis_remarks'));
		}
		return response()->json(['status' => 200]);
	}

	public function storeReviewReult(Request $request)
    {
//        dd($request->all());

		$user = Auth::guard('admin')->user();
		$validator = Validator::make($request->all(), [
			'thesis_title' => 'required',
			'course_subject' => 'required',
			'final_comment' => 'required',
			//'alternate_reason' => 'mimes:pdf| max:10000',
		]);


		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		DB::beginTransaction();
		try {
			$thesis = Thesis::find($request->thesis_id);
			$checkReassigned = $thesis->transaction()->where('to_user_id', $user->id)->where('is_active', 1)->first();

			if ($request->hasFile('signature_upload1')) {
				$signImage = $request->file('signature_upload1');
				$signfilename = time() . $signImage->getClientOriginalName();
				$image_resize = Image::make($signImage->getRealPath());
				$signImage->move(public_path('signatures'), $signfilename);
				$image_resize->resize(300, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('signatures/' . $signfilename));
				$request['full_evaluator_signature'] = $signfilename;
			}

			if ($request->hasFile('signature_upload2')) {
				$signImage2 = $request->file('signature_upload2');
				$signfilename2 = time() . $signImage2->getClientOriginalName();
				$signImage2->move(public_path('signatures'), $signfilename2);
				$request['evaluator_signature'] = $signfilename2;
			}

			if (ThesisReviewerResult::where('thesis_id', $request->thesis_id)->where('faculty_id', $user->id)->first()) {
				$result = ThesisReviewerResult::where('thesis_id', $request->thesis_id)->where('faculty_id', $user->id)->first();
				$result->fill($request->except('signature_upload1', 'signature_upload2', 'alternate_reason'));
			} else {
				$result = ThesisReviewerResult::create($request->except('signature_upload1', 'signature_upload2', 'alternate_reason'));
			}

			$thesisTransaction = ThesisTransaction::where('thesis_id', $request->thesis_id)->where('to_user_id', $user->id)->first();
			$thesisTransaction->status = 4;
			$thesisTransaction->save();

			$result->is_active = 1;
			if ($checkReassigned && $checkReassigned->is_reassigned == 1) {
				$result->transaction_reassigned_group = $checkReassigned->reassigned_group;
			}
			if ($request->hasFile('alternate_reason')) {
				$alternateReason = $request->file('alternate_reason');
				$filename = time() . $alternateReason->getClientOriginalName();
				$alternateReason->move(public_path('pdfs'), $filename);
				$result->alternate_reason = $filename;
			}
			$result->save();
			$transCount = $thesis->transaction()->where('status', '!=', 3)->where('is_active', 1)->count();
			$resultCount = $thesis->thesisResult()->where('is_active', 1)->whereNotNull('final_comment')->count();
//    dd($resultCount);
			if ($resultCount == 3) {
				$reviewerComment = $thesis->thesisResult()->where('is_active', 1)->pluck('final_comment')->toArray();
				if (in_array('D', $reviewerComment) || in_array('E', $reviewerComment)) {
					$thesis->status = 4;
					$thesis->re_submit = 1;
					$thesis->save();
					$thesisStatus = 2; // not completed status
				} else {
					$thesis->status = 3;
                    if ($request->final_comment == 'A') {
                        $thesis->approved_date = Carbon::now()->format('d/m/Y');
                        $thesis->save();
                    }
					$thesis->save();
					$thesisStatus = 1; // completed status
				}

				$data = ['status' => $thesisStatus];
				$view = 'emails.thesis-status';
				$subject = 'Dissertation Status';
				$attach = ($result->alternate_reason) ? public_path('pdfs/' . $result->alternate_reason) : '';
				Mail::to($thesis->user->email)->send(new SendMail($data, $view, $subject, $attach));
			}

			$data2 = ['reviewer' => $user];
			$view2 = 'emails.reviewer-submission';
			$subject2 = 'Dissertation submit';
			$superadmins = User::where('is_admin', 1)->get();
			/*foreach ($superadmins as $value) {
				Mail::to($value->email)->send(new SendMail($data2, $view2, $subject2));
			}*/
			DB::commit();
			Session::flash('msg', 'Sucessfuly submit');
			return redirect()->route('admin.student-result');

		} catch (\Exception $e) {
			DB::rollback();
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}

	}

	public function showReviewList() {
		$title = 'Student Result List';
		$faculty = Auth::guard('admin')->user();
		$dateObj = Carbon::now();
		$dateFrom = $dateObj->subMonths(9)->format('Y-m-d');
		/*$thesisData = Thesis::whereHas('category.user', function ($q) use ($faculty) {
				$q->where('id', $faculty->id);
			})->whereHas('transaction', function ($q) use ($faculty) {
				$q->whereIn('status', [4, 9]);
				$q->where('to_user_id', $faculty->id);
		*/
		$thesisData = Thesis::whereHas('transaction', function ($q) use ($faculty, $dateFrom) {
			$q->whereIn('status', [4, 9]);
			$q->where('to_user_id', $faculty->id);
			$q->where('accknowledge_email_date', '>=', $dateFrom);
		})->paginate(5);

		return view('admin.thesis-reviewer.result-list', compact('title', 'thesisData', 'faculty'));

	}

	public function reviewerStatusChange(Request $request) {
		try {
			$transaction = ThesisTransaction::find($request->transaction_id);
			$transaction->status = $request->status;
			$transaction->save();
			$data = ['text' => 'Synopsis'];
			$view = 'emails.thesis-status';
			$subject = 'Synopsis send';
			$superadmins = User::where('is_admin', 1)->get();
			foreach ($superadmins as $user) {
				Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			}

			return response()->json(['status' => 200, 'status_text' => 'success']);
		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
		}

	}

	public function reviewerAccept(Request $request) {
		$user = \Auth::guard('admin')->user();
		$th_transaction = ThesisTransaction::where('thesis_id', $request->thesis_id)->where('to_user_id', $user->id)->first();

		if ($request->status == 2) {
			$th_transaction->status = 2;
			$th_transaction->save();
			$transArray = ThesisTransaction::where('thesis_id', $request->thesis_id)->pluck('status')->toArray();
			/*if (!in_array(1, $transArray)) {
				$thesis = Thesis::find($request->thesis_id);
				$thesis->status = 3;
				$thesis->save();
			}*/
			$data = ['text' => 'Thesis', 'status' => 1, 'user' => Auth::guard('admin')->user()];
			$view = 'emails.thesis-accepted-status';
			$subject = 'Dissertation Accepted';
			$superadmins = User::where('is_admin', 1)->get();
			foreach ($superadmins as $user) {
				Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			}
		} else {
			$th_transaction->status = 3;
			$th_transaction->reason_reject = $request->reason;
			$th_transaction->save();

			$data = ['text' => 'Thesis', 'reason' => $request->reason, 'status' => 2, 'user' => Auth::guard('admin')->user()];
			$view = 'emails.thesis-accepted-status';
			$subject = 'Dissertation Resubmission';
			$superadmins = User::where('is_admin', 1)->get();
			foreach ($superadmins as $user) {
				Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			}
		}
		return response()->json(['status' => 200, 'status_text' => 'success']);

	}

	public function reviewerReminder() {
		$now = Carbon::now()->format('Y-m-d');
		$reminderData = ThesisTransaction::whereRaw('DATE_ADD(`last_email_date`, INTERVAL 4 DAY)= CURDATE()')->where('status', 1)->get();
		if (count($reminderData) > 0) {
			foreach ($reminderData as $value) {
				$result = ThesisReviewerResult::create($request->all());
				$data = ['text' => 'Not Accepted', 'thesises' => $reminderData];
				$view = 'emails.thesis-status';
				$subject = 'Reminder Dissertation';
				//$attach = public_path('pdfs/thesis/' . $value->thesis->form_pdf);
				Mail::to($value->user->email)->send(new SendMail($data, $view, $subject));
				$value->last_email_date = Carbon::now();
				$value->count_send = $value->count_send + 1;
				$value->save();

				$value->user->notify(new SendNotify('Reminder Email Notification'));
			}

			$superadmins = User::where('is_admin', 1)->get();
			foreach ($superadmins as $superadmin) {
				Mail::to($superadmin->user->email)->send(new SendMail($data, $view, $subject));
			}
		}

		/*$DB::select('SELECT * FROM `wb_thesis_transactions` WHERE DATE_ADD(`last_email_date`, INTERVAL 4 DAY)= CURDATE() AND status=1');*/
	}

	public function nofication() {
		$user = Auth::guard('admin')->user();
		$message = 'this is test message';
		$user->notify(new SendNotify($message));
	}

	public function saveSignature(Request $request) {
		try {
			$image = $request->image;
			$image = str_replace('data:image/jpeg;base64,', '', $image);
			$image = str_replace(' ', '+', $image);
			$imageName = time() . '.' . 'jpeg';
			\File::put(public_path('signatures') . '/' . $imageName, base64_decode($image));
			return response()->json(['status' => 200, 'image' => $imageName]);

		} catch (\Exception $e) {
			return response()->json(['status' => 500, 'error' => $e->getMessage()]);
		}

	}

	public function testEmail() {

		/*Mail::send('welcome', ['token' => ''], function($message)  {
			                    $message->from('souravg1g2.sp@gmail.com');
			                    $message->to('souravp@pixelsolutionz.com')
			                            ->subject('Reset Password');
		*/
		$data = ['text' => 'Thesis'];
		$view = 'welcome';
		$subject = 'Dissertation Resubmission';
		Mail::to('souravg1g2.sp@gmail.com')->send(new SendMail($data, $view, $subject));
	}

}
