<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Session;
use Validator;

class ResetPasswordController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Password Reset Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller is responsible for handling password reset requests
		    | and uses a simple trait to include this behavior. You're free to
		    | explore this trait and override any methods you wish to tweak.
		    |
	*/

	use ResetsPasswords;

	/**
	 * Where to redirect users after resetting their password.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	public function showResetForm(Request $request, $token = null) {
		$title = 'Reset Password';
		$exist_token = DB::table('password_resets')->where('token', $token)->first();
		if ($exist_token) {
			return view('auth.reset')->with(
				['token' => $token, 'email' => $exist_token->email, 'title' => $title]
			);
		} else {
			return view('errors.unauthorize');
		}

	}

	public function storeResetPassword(Request $request) {
		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:6',
		]);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator);
		}
		$user = User::where('email', $request->email)->first();
		if ($user) {
			$user->password = bcrypt($request->password);
			$user->save();
			DB::table('password_resets')->where('token', $request->token)->update(['token' => NULL]);
			Session::flash('msg', 'Sucessfully change password');
			return redirect('login');
		}
	}
}
