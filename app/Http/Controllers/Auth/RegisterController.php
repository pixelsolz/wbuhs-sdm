<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\Cities;
use App\Models\Role;
use App\Models\StudentData;
use App\Models\Synopsis;
use App\Models\SynopsisStudentData;
use App\Models\Thesis;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Image;
use Mail;
use Validator;

class RegisterController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Register Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles the registration of new users as well as their
		    | validation and creation. By default this controller uses a trait to
		    | provide this functionality without requiring any additional code.
		    |
	*/

	//use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validation(array $data) {
		$messages = [
			'f_name.required' => 'First Name field required',
			'l_name.required' => 'Last Name filed required',
			'profile_image.mimes' => 'Please choose image',
			'recaptcha' => 'Please ensure that you are a human!',
		];

		$validator = Validator::make($data, [
			'f_name' => 'required|string|max:255',
			'l_name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'mobile' => 'required|digits:10',
			'address' => 'required',
			'gender' => 'required',
			'profile_image' => 'mimes:jpeg,jpg,png,gif',
			'password' => 'required|string|min:6|confirmed',
		], $messages);

		if ($validator->fails()) {
			return response()->json(array(
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray(),

			), 422);
		}

		//return response()->json(['error' => $validator->errors()->all()]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	protected function create(array $data) {

	}

	public function showRegistrationForm() {
		$title = 'Student Registration';
		$cities = Cities::all();
		return view('auth.register-form', compact('title', 'cities'));
	}

	public function register(Request $request) {
		$messages = [
			'f_name.required' => 'First Name field required',
			'l_name.required' => 'Last Name filed required',
			'profile_image.mimes' => 'Please choose image',
			'g-recaptcha-response.required' => 'Please ensure that you are a human!',
		];

		$validator = Validator::make($request->all(), [
			'f_name' => 'required|string|max:255',
			'l_name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'mobile' => 'required|digits:10|unique:users',
			'address' => 'required',
			'gender' => 'required',
			'city_id' => 'required',
			'registration_no' => 'required',
			'profile_image' => 'mimes:jpeg,jpg,png,gif',
			'password' => 'required|string|min:6|confirmed',
			'g-recaptcha-response' => 'required',
		], $messages);

		if ($validator->fails()) {
			return response()->json(array(
				'success' => false,
				'errors' => $validator->getMessageBag()->toArray(),

			), 422);
		}

		$registrationValid = false;
		if (StudentData::where('registration_no', $request->input('registration_no'))->first() || SynopsisStudentData::where('unique_id', $request->input('registration_no'))->first()) {
			$registrationValid = true;
		}

		if ($registrationValid == false) {
			return response()->json(['status' => 422, 'error_type' => 'registration', 'status_text' => 'Registration no. is not mathched@']);
		}

		$data = $request->all();
		DB::beginTransaction();
		try {
			$user = User::create([
				'f_name' => $data['f_name'],
				'l_name' => $data['l_name'],
				'email' => $data['email'],
				'mobile' => $data['mobile'],
				'password' => bcrypt($data['password']),
				//'is_active' => 1,
			]);

			$token = md5($user->email . time());
			$user->register_token = $token;
			//$user->email_verified_at = Carbon::now();
			$user->save();

			$student_role = Role::where('slug', 'student')->first();
			if ($student_role) {
				$user->roles()->attach($student_role->id);
			}

			$userdetail = $user->userDetail()->create(['user_id' => $user->id, 'address' => $data['address'], 'mobile' => $data['mobile'], 'city_id' => $data['city_id'], 'registration_no' => $data['registration_no'], 'gender' => (int) $data['gender']]);

			if (!empty($data['profile_image'])) {
				$image = $data['profile_image'];
				$filename = $image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());
				$image_resize->resize(150, 150, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('images/' . time() . $filename));
				$userDetail->profile_image = 'images/' . time() . $filename;
				$userDetail->save();
			}
			$data = ['text' => 'User Register', 'token' => $token];
			$view = 'emails.mail-verification';
			$subject = 'Registration success';
			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			session(['mobile' => $user->mobile]);
			$otp = $this->otpSend($user->mobile, $user->email);
			$user->otp = $otp;
			$user->save();
			DB::commit();
			return response()->json(['status' => 200, 'data' => $user]);
		} catch (\Exception $e) {

			//Session::flash('error', $e->getMessage());
			DB::rollback();
			return response()->json(['status' => 500, 'status_text' => $e->getMessage()]);
			//return redirect()->back()->withInput();
		}
	}

	public function otpSend($number, $email) {
		$otp = $this->generateOTP();

		$data = ['text' => 'Registration OTP', 'otp' => $otp];
		$view = 'emails.registration-otp';
		$subject = 'Registration OTP';
		Mail::to($email)->send(new SendMail($data, $view, $subject));

		$mobile = (int) '91' . $number;
		$url = 'http://whitelist.smsapi.org/SendSMS.aspx?UserName=WBUHS&password=6YNOT7wuTT&MobileNo=' . $mobile . '&SenderID=WBUHSR&CDMAHeader=WBUHSR&Message=Your OTP for registration of a is ' . $otp . '. Your OTP will expire in 10 min. Please do not share it with anyone';

		$client = new \GuzzleHttp\Client();
		$request = $client->get($url);
		return $otp;

	}

	public function generateOTP() {
		$otp = mt_rand(1000, 9999);
		$checkOtp = User::where('otp', $otp)->first();
		if ($checkOtp) {
			$this->generateOTP();
		} else {
			return $otp;
		}
	}

	public function checkRegistrationNo(Request $request) {
		if (StudentData::where('registration_no', $request->input('registration_no'))->first() || SynopsisStudentData::where('unique_id', $request->input('registration_no'))->first()) {
			if (Synopsis::where('registration_no', $request->input('registration_no'))->first() || Thesis::where('registration_no', $request->input('registration_no'))->first()) {
				return response()->json(['status' => 422, 'status_text' => 'Registration no. already used!']);
			}
			return response()->json(['status' => 200, 'status_text' => 'matched']);
		} else {
			return response()->json(['status' => 422, 'status_text' => 'Registration no. is not matched!']);
		}
	}
}
