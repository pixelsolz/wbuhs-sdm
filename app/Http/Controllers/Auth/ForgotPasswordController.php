<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Mail;
use Session;
use Validator;

class ForgotPasswordController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Password Reset Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller is responsible for handling password reset emails and
		    | includes a trait which assists in sending these notifications from
		    | your application to your users. Feel free to explore this trait.
		    |
	*/

	//use SendsPasswordResetEmails;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	public function showLinkRequestForm() {
		$title = 'Forgot password';
		return view('auth.forgot-form', compact('title'));
	}

	public function sendResetLink(Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
		]);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}

		$user = User::where('email', $request->email)->where('is_active', 1)->first();
		if ($user) {
			$token = md5($request->email . time());
			DB::table('password_resets')->insert(['email' => $user->email, 'token' => $token]);
			$data = ['text' => 'User Register', 'token' => $token];
			$view = 'emails.forgot-password';
			$subject = 'Forgot Password';
			Mail::to($user->email)->send(new SendMail($data, $view, $subject));
			Session::flash('msg', 'Reset password link has been sent to your email');
			return redirect('/login');
		} else {
			Session::flash('email_error', 'Email Id doesnot matched!');
			return redirect()->back();
		}

	}
}
