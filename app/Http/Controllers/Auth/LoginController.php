<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;

class LoginController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Login Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles authenticating users for the application and
		    | redirecting them to your home screen. The controller uses a trait
		    | to conveniently provide its functionality to your applications.
		    |
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/dashboard';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest')->except('logout');
	}

	public function showLoginForm() {
		$title = 'Student Login';
		return view('auth.login-form', compact('title'));

	}

	public function login(Request $request) {

		/*if ($user = User::where('email', $request->email)->first()) {
			if (!$user->mobile_verified_at) {
				session(['mobile' => $user->mobile]);
				$newOtp = $this->sendOtp($user->mobile, $user->email);
				$user->otp= $newOtp;
				$user->save();
				return redirect('otp-form');
			}
		}*/
		//['email' => $request->email,'password' => $request->password, 'is_active' => 1]

		if (!Auth::attempt(['email' => $request->email,
			'password' => $request->password])) {
			Session::flash('error', "Email and password doesn't match!");
			return back();
		}
		Session::flash('msg', "Welcome to Dashboard " . Auth::user()->full_name);
		return redirect('/dashboard');
	}

	public function emailVerifyLogin(Request $request, $token) {

		$user = User::where('register_token', $token)->whereNull('email_verified_at')->first();
		if ($user && $user->mobile_verified_at) {
			$user->email_verified_at = Carbon::now();
			$user->register_token = '';
			//$user->is_active = 1;
			$user->save();
			Auth::login($user);
			return redirect('dashboard');
		} else {
			return view('errors.unauthorize');
		}
	}

	public function showOtpForm() {
		$title = 'OTP Form';
		return view('auth.otp-form', compact('title'));
	}

	public function logout(Request $request) {
		Auth::logout();

		$request->session()->invalidate();

		return redirect('/login');
	}

	public function otpVerification(Request $request) {
		$user = User::where('otp', $request->otp)->first();
		if ($user) {
			$now = Carbon::now();
			$lastUpdated_time = Carbon::parse($user->updated_at);
			$diffInMinutes = $lastUpdated_time->diffInMinutes($now);
			if ($diffInMinutes > 10) {
				Session::flash('error', "OTP is not valid!");
				return redirect()->back();
			} else {
				$user->mobile_verified_at = Carbon::now();
				$user->otp = '';
				$user->is_active = 1;
				$user->save();
				if ($request->session()->has('mobile')) {
					$request->session()->forget('mobile');
				}
				Auth::login($user);
				return redirect('dashboard');
			}
		} else {
			Session::flash('error', "OTP doesnot match!");
			return redirect()->back();
		}
	}

	protected function sendOtp($number, $email) {
		$otp = $this->generateOTP();

		$data = ['text' => 'Registration OTP', 'otp' => $otp];
		$view = 'emails.registration-otp';
		$subject = 'Registration OTP';
		Mail::to($email)->send(new SendMail($data, $view, $subject));

		$mobile = (int) '91' . $number;
		$url = 'http://whitelist.smsapi.org/SendSMS.aspx?UserName=WBUHS&password=6YNOT7wuTT&MobileNo=' . $mobile . '&SenderID=WBUHSR&CDMAHeader=WBUHSR&Message=Your OTP for registration of a is ' . $otp . '. Your OTP will expire in 10 min. Please do not share it with anyone';

		$client = new \GuzzleHttp\Client();
		$request = $client->get($url);
		$response = $request->getBody();
		return $otp;
	}

	public function otpResend(Request $request) {
		$otp = $this->generateOTP();

		$number = $request->mobile;
		$user = User::where('mobile', $request->mobile)->first();

		$data = ['text' => 'Registration OTP', 'otp' => $otp];
		$view = 'emails.registration-otp';
		$subject = 'Registration OTP';
		Mail::to($user->email)->send(new SendMail($data, $view, $subject));

		$mobile = (int) '91' . $number;
		$url = 'http://whitelist.smsapi.org/SendSMS.aspx?UserName=WBUHS&password=6YNOT7wuTT&MobileNo=' . $mobile . '&SenderID=WBUHSR&CDMAHeader=WBUHSR&Message=Your OTP for registration of a is ' . $otp . '. Your OTP will expire in 10 min. Please do not share it with anyone';

		$client = new \GuzzleHttp\Client();
		$request = $client->get($url);
		$response = $request->getBody();
		$user->otp = $otp;
		$user->save();
		return response()->json(['status' => 200]);

	}

	public function generateOTP() {
		$otp = mt_rand(1000, 9999);
		$checkOtp = User::where('otp', $otp)->first();
		if ($checkOtp) {
			$this->generateOTP();
		} else {
			return $otp;
		}
	}
}
