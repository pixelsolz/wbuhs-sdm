<?php

namespace App\Http\Controllers;

use App\Models\Synopsis;
use App\Models\ThesisReviewerResult;
use App\Models\User;
use App\Models\Support;
use Auth;
use Mail;
use Illuminate\Http\Request;
use Image;
use Session;
use Validator;

class StudentProfileController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$title = 'Student Profile';
		$user = Auth::user();
		return view('frontend.student-profile', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//dd($request->all());
		$messsages = array(
			'f_name.required' => 'The Role Name field is required.',
			'l_name.required' => 'The Role Slug field is required.',
			'email.required' => 'The Email field is required.',
			'mobile.required' => 'The Mobile field is required.',
			'address.required' => 'The Mobile field is required.',
			'gender.required' => 'The Mobile field is required.',
		);

		$validator = Validator::make($request->all(), [
			'f_name' => 'required',
			'l_name' => 'required',
			'email' => 'required ',
			'mobile' => 'required ',
			'address' => 'required',
			'gender' => 'required',
			'profile_image'=>'mimes:jpeg,png,jpg'
		], $messsages);

		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator)
				->withInput();
		}
		try {
			$user = User::find($id);
			$user->fill($request->only('f_name', 'l_name', 'email'));
			$user->save();
			$userDetail = $user->userDetail->fill($request->only(['mobile', 'address', 'gender']));
			$userDetail->save();
			if ($request->hasFile('profile_image')) {
				$image = $request->file('profile_image');
				$filename = $image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());
				$image_resize->resize(150, 150, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path('images/' . time() . $filename));
				$userDetail->profile_image = 'images/' . time() . $filename;
				$userDetail->save();
			}
			Session::flash('msg', 'Profile successfully updated');
			return redirect()->route('dashboard.index');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function changePassword() {

		$title = 'Student Password';
		return view('frontend.student-password');
	}

	public function changePasswordStore(Request $request) {
		$messsages = array(
			'password_confirmation.required' => 'The confirm password required.',
		);
		$validator = Validator::make($request->all(), [
			'password' => 'required|confirmed|min:6',
			'password_confirmation' => 'required',
		], $messsages);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator);
		}

		try {
			$user = Auth::user();
			$user->password = bcrypt($request->password);
			$user->save();
			Session::flash('msg', 'Password changed successfully');
			return redirect()->route('dashboard.index');

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}

	
	public function support() {

		$title = 'Student Support';
		return view('frontend.student-support');
	}

	
	public function supportStore(Request $request) {
		$messsages = array(
			'subject.required' => 'The Subject is required.',
			'content.required' => 'The Content is required.',
		);
		$validator = Validator::make($request->all(), [
			'subject' => 'required|min:6',
			'content' => 'required',
		], $messsages);
		if ($validator->fails()) {
			return redirect()->back()
				->withErrors($validator);
		}

		$userData =  Auth::user();

		
		try {
			
			$user = Support::create(['user_id' => Auth::id(), 'subject' => $request->subject, 'content' => $request->content]);

			$to = $userData->email;
			$subject = 'Request for "'.$request->subject.'", has been submited successfully.';
			$data = ['subject' => $request->subject, 'name' => $userData->full_name];

			Mail::send('emails.support-confirmation', $data, function($message) use ($to, $subject) {
			         $message->to($to)->subject
			            ($subject);
			         $message->from(config('mail.from.address'),'WBUHS');
			      });

			$toAdmin = config('mail.to.adminAddress');
			$subjectAdmin = 'A new request for support from "'.$userData->full_name.'", has been submited.';
			$dataAdmin = ['subject' => $request->subject, 'content' => $request->content, 'name' => $userData->full_name, 'userEmail' =>$to];
			Mail::send('emails.support-confirmation-admin', $dataAdmin, function($message) use ($toAdmin, $subjectAdmin) {
			         $message->to($toAdmin)->subject
			            ($subjectAdmin);
			         $message->from(config('mail.from.address'),'WBUHS');
			      });

			Session::flash('msg', 'Messsages has been sent successfully to the admin');
			return redirect()->back();

		} catch (\Exception $e) {
			Session::flash('error', $e->getMessage());
			return redirect()->back();
		}
	}


	public function rejectionList(Request $request) {
		$title = 'Rejected List';
		$user = Auth::user();
		$rejectedData = '';
		$type = '';
		$rejectedType = '';
		$rejectedSynopsis = $user->studentSynopsis()->where('status', 3)->orwhere('status', 4)->first();
		$rejectedThesis = $user->studentThesis()->where('status', 4)->first();
		if ($rejectedSynopsis) {
			$rejectedData = $rejectedSynopsis;
			$rejectedType = ($rejectedSynopsis->status == 3) ? 'Rejection' : 'Resubmission';
			$type = 'Synopsis';
		} elseif ($rejectedThesis) {
			$rejectedData = $rejectedThesis;
			$type = 'Dissertation';
			$rejectedType = 'Resubmission';
		}
		//dd($rejectedData);exit();
		return view('frontend.rejected', compact('title', 'rejectedData', 'type', 'rejectedType'));
	}

	public function downloadReason(Request $request) {
		if ($request->type == 'thesis') {
			$reviewerResult = ThesisReviewerResult::find($request->id);
			return response()->download(public_path('pdfs/' . $reviewerResult->alternate_reason));
		} elseif ($request->type == 'synopsis') {
			$synopsis = Synopsis::find($request->id);
			return response()->download(public_path('pdfs/' . $synopsis->reason_reject_doc));
		}
	}
}
