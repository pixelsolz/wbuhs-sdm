<?php

namespace App\Console\Commands;
use App\Http\Controllers\paymentController;
use Illuminate\Console\Command;

class PaymentTransaction extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'payment:transaction';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(paymentController $paymentCtrl) {
		parent::__construct();
		$this->paymentCtrl = $paymentCtrl;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->paymentCtrl->sendPaymentReport();
	}
}
