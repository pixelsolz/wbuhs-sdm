<?php

namespace App\Console\Commands;
use App\Http\Controllers\Admin\ReviewerController;
use Illuminate\Console\Command;

class ReviewerReminder extends Command
{
    public $reviewerCtrl;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reviewer:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ReviewerController $reviewerCtrl) {
		parent::__construct();
		$this->reviewerCtrl = $reviewerCtrl;
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->reviewerCtrl->reviewerReminder();
    }
}
