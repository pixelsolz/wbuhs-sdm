<?php

namespace App\Console\Commands;
use App\Http\Controllers\Admin\ReviewerPaymentController;
use Illuminate\Console\Command;

class AccountsReminder extends Command
{
    public $paymentCtrl;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ReviewerPaymentController $paymentCtrl) {
		parent::__construct();
		$this->paymentCtrl = $paymentCtrl;
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->paymentCtrl->accountsReminder();
    }
}
