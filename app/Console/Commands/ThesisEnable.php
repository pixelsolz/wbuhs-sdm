<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\UserController;
use Illuminate\Console\Command;

class ThesisEnable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thesis:enable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enable thesis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserController $thesisEnableControl)
    {
        parent::__construct();
        $this->thesisEnableControl = $thesisEnableControl;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->thesisEnableControl->thesisEnableDate();
    }
}
