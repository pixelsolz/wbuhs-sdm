<?php

namespace App\Console\Commands;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Console\Command;
use Mail;

class UserReminder extends Command
{
    public $userCtrl;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserController $userCtrl) {
		parent::__construct();
		$this->userCtrl = $userCtrl;
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->userCtrl->userReminder();
       // $a = 'pixelsamiran@gmail.com';
       // Mail::raw("User Reminder Minute Update", function($message) use ($a)
       //    {
       //        $message->from('wbuhs@pxlsysdev.in');
       //        $message->to($a)->subject('Hourly Update');
       //    });
           
           


    }
}
