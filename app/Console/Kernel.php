<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		//
		Commands\HourlyUpdate::class,
		Commands\UserReminder::class,
		Commands\ThesisSubmit::class,
        Commands\ThesisEnable::class

	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule) {
		$schedule->command('payment:transaction')->dailyAt('04:00');
		$schedule->command('reviewer:reminder')->dailyAt('05:00');
		$schedule->command('user:reminder')->dailyAt('06:00');
		$schedule->command('thesis:enable')->dailyAt('06:00');
		//$schedule->command('thesis:submit')->everyThirtyMinutes();

		//$schedule->command('accounts:reminder')->weeklyOn(5, '8:00');

		//$schedule->command('hour:update')->everyMinute();
		// $schedule->command('inspire')
		//          ->hourly();
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
