<?php

namespace App\Services;
use File;
use View;

/**
 *
 */
class MPdfService {
	public $mpdf;

	public $config = [
	];

	function __construct() {
		/*ini_set('memory_limit', '1024M');
			ini_set('max_execution_time', 0);
			ini_set('upload_max_filesize', '1024M');
		*/
		$this->mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4',
			'author' => '',
			'subject' => '',
			'keywords' => '',
			'creator' => 'Laravel Pdf',
			'display_mode' => 'fullpage',
			'tempDir' => base_path('../temp/'),
		]);
		$this->mpdf->SetTitle('Document');
		$this->config = [
			'mode' => 'utf-8',
			'format' => 'A4',
			'author' => '',
			'subject' => '',
			'keywords' => '',
			'creator' => 'Laravel Pdf',
			'display_mode' => 'fullpage',
			//'tempDir' => base_path('../temp/')
		];
	}

	public function streamPDF($file) {
		$this->mpdf->WriteHTML($file);
		return $this->mpdf->Output();
	}

	public function savePDF($file, $filename) {
		$this->mpdf->WriteHTML($file);
		return $this->mpdf->Output('public/pdfs/synopsis/' . $filename, 'F');
	}

	public function mergePDF($savefile, $st_file, $synop_file) {
		$this->mpdf->SetImportUse();

		$pagecount1 = $this->mpdf->SetSourceFile(public_path('pdfs/synopsis/' . $synop_file));
		$tpl1Id = $this->mpdf->ImportPage($pagecount1);
		$this->mpdf->UseTemplate($tpl1Id);

		$this->mpdf->AddPage();

		$pagecount = $this->mpdf->SetSourceFile(public_path('pdfs/' . $st_file));
		for ($i = 1; $i <= $pagecount; $i++) {
			$tplId = $this->mpdf->ImportPage($i);
			$this->mpdf->UseTemplate($tplId);
			$this->mpdf->AddPage();
		}

		//$this->mpdf->WriteHTML($synop_file);
		/*$pagecount1 = $this->mpdf->SetSourceFile(public_path('pdfs/synopsis/1540641636synopsis.pdf'));
			$tpl1Id = $this->mpdf->ImportPage($pagecount1);
		*/

		return $this->mpdf->Output('public/pdfs/synopsis/' . $savefile, 'F');
	}

	public function downlodPDF($filename = 'document.pdf') {
		$this->mpdf->WriteHTML('Hello World');
		return $this->mpdf->Output($filename, 'D');
	}

	public function setProtection($permisson, $userPassword = '', $ownerPassword = '') {
		if (func_get_args()[2] === NULL) {
			$ownerPassword = bin2hex(openssl_random_pseudo_bytes(8));
		};
		return $this->mpdf->SetProtection($permisson, $userPassword, $ownerPassword);
	}

	public function loadView($view, $data = []) {
		return new Pdf(View::make($view, $data, $mergeData)->render(), $this->config);
	}

	public function loadFile($file) {
		return new Pdf(File::get($file), $this->config);
	}

}
