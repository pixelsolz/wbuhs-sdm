<?php

namespace App\Imports;

use App\Models\SynopsisStudentData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class ImportStudentSynopsis implements ToModel, WithBatchInserts {

	public function model(array $row) {
		return new SynopsisStudentData([
			'sl_no' => $row[0],
			'name' => $row[1],
			'course' => $row[2],
			'institute' => $row[3],
			'unique_id' => $row[4],
			'phone' => $row[5],
			'email' => $row[6],
		]);
	}

	public function batchSize(): int {
		return 1000;
	}

}