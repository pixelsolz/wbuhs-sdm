<?php

namespace App\Imports;

use App\Models\StudentData;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class ImportStudent implements ToModel, WithBatchInserts {

	public function model(array $row) {
		return new StudentData([
			'sl_no' => $row[0],
			'name' => $row[1],
			'course' => $row[2],
			'institute' => $row[3],
			'institute_code' => $row[4],
			'registration_no' => $row[5],
			'phone' => $row[6],
			'email' => $row[7],
		]);
	}

	public function batchSize(): int {
		return 1000;
	}

}