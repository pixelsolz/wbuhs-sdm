<?php

namespace App\Exports;

use App\Models\ThesisTransaction;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PendingThesisExport implements FromView {
	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.thesis-pending', [
			'users' => ThesisTransaction::whereIn('id', $this->ids)->get(),
		]);
	}
}
