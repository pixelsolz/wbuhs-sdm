<?php

namespace App\Exports;

use App\Models\Synopsis;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentSynopsisExport implements FromView {
	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.student-synopsis', [
			'synopsis_list' => Synopsis::whereIn('id', $this->ids)->get(),
		]);
	}
}
