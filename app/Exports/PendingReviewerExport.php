<?php

namespace App\Exports;

use App\Models\FacultyPayment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PendingReviewerExport implements FromView {
	/**
	 * @return \Illuminate\Support\Collection
	 */

	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.reviewer-pending', [
			'facultyPayments' => FacultyPayment::whereNotNull('total_amount')->whereIn('id', $this->ids)->get(),
		]);
	}
}
