<?php

namespace App\Exports;

use App\Models\Payment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class StudentPaymentExport implements FromView {
	/**
	 * @return \Illuminate\Support\Collection
	 */

	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.student-payment-export', [
			'studentPayments' => Payment::whereIn('id', $this->ids)->get(),
		]);
	}
}
