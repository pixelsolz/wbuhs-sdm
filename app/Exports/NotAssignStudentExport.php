<?php

namespace App\Exports;

use App\Models\Synopsis;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NotAssignStudentExport implements FromView {
	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.not-assign-list', [
			'synopsis_list' => Synopsis::whereIn('id', $this->ids)->orderBy('created_at', 'desc')->get(),
		]);
	}
}
