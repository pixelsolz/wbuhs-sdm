<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AdjudicatorExport implements FromView {
	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.adjudicator-list', [
			'adjudicator_list' => User::whereIn('id', $this->ids)->orderBy('created_at', 'desc')->get(),
		]);
	}
}
