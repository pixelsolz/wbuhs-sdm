<?php

namespace App\Exports;

use App\Models\FacultyPayment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReviewerPendingExport implements FromView {
	/**
	 * @return \Illuminate\Support\Collection
	 */

	public function view(): View {
		return view('exports.reviewer-pending', [
			'facultyPayments' => FacultyPayment::whereNotNull('total_amount')->get(),
		]);
	}
}
