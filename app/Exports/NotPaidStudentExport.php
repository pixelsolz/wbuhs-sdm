<?php

namespace App\Exports;

use App\Models\Thesis;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NotPaidStudentExport implements FromView {
	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.not-paid-list', [
			'student_list' => Thesis::whereIn('id', $this->ids)->orderBy('created_at', 'desc')->get(),
		]);
	}
}