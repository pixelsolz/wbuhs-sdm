<?php

namespace App\Exports;

use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView {
	public function __construct(array $ids) {
		$this->ids = $ids;
	}

	public function view(): View {
		return view('exports.user-exports', [
			'users' => User::whereIn('id', $this->ids)->get(),
		]);
	}
}
