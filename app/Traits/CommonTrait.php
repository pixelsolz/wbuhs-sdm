<?php

namespace App\Traits;
use DB;

trait CommonTrait {

	public function categoryTreeDropdown($parent = 0, $spacing = '', $user_tree_array = '') {
		if (!is_array($user_tree_array)) {
			$user_tree_array = array();
		}

		$query = DB::select(DB::raw("select * from `wb_categories` WHERE 1 AND `parent` = $parent order by id asc"));
		//return count($query);
		if (count($query) > 0) {
			foreach ($query as $row) {
				$user_tree_array[] = array("id" => $row->id, "name" => $spacing . $row->name);
				$user_tree_array = $this->categoryTreeDropdown($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);

			}
		}
		return $user_tree_array;

	}
}
