<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusMaster extends Model {
	protected $table = 'bonus_masters';

	protected $fillable = ['within_days', 'amount'];

}
