<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThesisTransaction extends Model {
	use SoftDeletes;
	protected $table = 'thesis_transactions';

	protected $fillable = ['thesis_id', 'to_user_id', 'trans_type', 'accknowledge_link', 'send_to', 'status', 'reason_reject', 'count_send', 'last_email_date', 'accknowledge_email_date', 'is_active', 'is_reassigned', 'reassigned_group', 'created_by', 'updated_by'];

	protected $appends = ['status_text', 'trans_type_text', 'user_name', 'last_em_date'];

	public function thesis() {
		return $this->belongsTo(Thesis::class, 'thesis_id', 'id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'to_user_id', 'id');
	}

	public function getUserNameAttribute() {
		return $this->user->full_name;
	}

	public function getStatusTextAttribute() {
		$value = '';
		switch ($this->status) {
		case 1:
			$value = 'On Processed';
			break;
		case 2:
			$value = 'Accepted';
			break;
		case 3:
			$value = 'Pending Evaluation';
			break;
		case 4:
			$value = 'Submitted';
			break;
		default:
			# code...
			break;
		}

		return $value;
	}

	public function getTransTypeTextAttribute() {
		$value = '';
		switch ($this->trans_type) {
		case 1:
			$value = 'Email';
			break;
		case 2:
			$value = 'SMS';
			break;
		case 3:
			$value = 'Email & SMS';
			break;
		default:
			# code...
			break;
		}
		return $value;
	}

	public function scopeGetEmailid($query) {
		//$array = $query->pluck('send_to')->toArray();
		$userId = $query->pluck('to_user_id')->toArray();

		$users = User::whereIn('id', $userId)->pluck('f_name')->toArray();
		return !empty($users) ? implode(',', $users) : '';
	}

	public function scopeGetFaculty($query, $type) {
		if ($type == 2) {
			$userId = $query->whereIn('status', [2, 4])->pluck('to_user_id')->toArray();
		} else {
			$userId = $query->where('status', 1)->pluck('to_user_id')->toArray();
		}

		$users = User::whereIn('id', $userId)->get();
		$names = '';
		if (count($users)) {
			foreach ($users as $value) {
				$names .= $value->full_name . '<br/>(' . $value->email . '),<br />';
			}
		}
		return rtrim($names, ',<br />');
		//return !empty($users) ? implode(',', $users) : '';
	}

	public function scopeFacultyData($query, $userId) {
		return $query->where('to_user_id', $userId)->first() ? $query->where('to_user_id', $userId)->first() : '';
	}

	public function getLastEmDateAttribute() {
		return $this->last_email_date ? Carbon::parse($this->last_email_date)->format('Y-m-d') : '';
	}

	public function getDate($userId) {

		$accknowledge_email_date = ThesisTransaction::where('id', $userId)->pluck('accknowledge_email_date')->first();
		return $accknowledge_email_date;
	}
}
