<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateMst extends Model {

	protected $table = 'state_mst';
	public $timestamps = false;

	protected $fillable = ['state_code', 'state_name'];
}
