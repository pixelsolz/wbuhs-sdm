<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Support extends Authenticatable {
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $table = 'support';
	protected $fillable = [
		'user_id', 'subject', 'content', 'status', 'admin_reply'	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	
	public function userDetail() {
		return $this->belongsTo(User::class, 'user_id','id');
	}

	public function roles() {
		return $this->belongsToMany(Role::class, 'role_users');
	}

	/**
	 * Checks if User has access to $permissions.
	 */
	public function hasAccess(array $permissions): bool {
		// check if the permission is available in any role
		foreach ($this->roles as $role) {
			if ($role->hasAccess($permissions)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the user belongs to role.
	 */
	public function inRole(string $roleSlug) {
		return $this->roles()->where('slug', $roleSlug)->count() == 1;
	}

	public function getFullNameAttribute() {
		return $this->f_name . ' ' . $this->l_name;
	}

	

	

}
