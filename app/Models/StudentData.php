<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentData extends Model {
	protected $table = 'student_data';

	protected $fillable = ['name', 'registration_no', 'course', 'institute', 'institute_code', 'phone', 'email', 'thesis_enabble_date', 'status'];

}
