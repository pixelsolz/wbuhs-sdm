<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	protected $table = 'categories';

	protected $fillable = ['name', 'slug', 'parent', 'has_child'];

	public function synopsis() {
		return $this->hasMany(Synopsis::class, 'category_id', 'id');
	}

	public function user() {
		return $this->belongsToMany(User::class, 'category_user_map');
	}
}
