<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacultyPaymentHistory extends Model {
	protected $table = 'faculty_payment_histories';

	protected $fillable = ['paid_amount', 'payment_id', 'reviewer_result_id', 'faculty_id', 'status', 'created_by'];

	protected $appends = ['status_text'];

	public function reviewResult() {
		return $this->belongsTo(ThesisReviewerResult::class, 'reviewer_result_id', 'id');
	}

	public function getStatusTextAttribute() {
		return $this->status == 1 ? 'Paid' : 'Unpaid';
	}

	public function faculty() {
		return $this->belongsTo(User::class, 'faculty_id', 'id');
	}

	public function payments() {
		return $this->belongsTo(FacultyPayment::class, 'payment_id', 'id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'created_by', 'id');
	}
}
