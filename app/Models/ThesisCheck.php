<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThesisCheck extends Model {
	protected $table = 'thesis_check';
	protected $fillable = ['thesis_id', 'is_done'];
	public $timestamps = false;

	public function thesis() {
		return $this->belongsTo(Thesis::class, 'thesis_id', 'id');
	}
}
