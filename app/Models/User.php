<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
	use Notifiable;
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'f_name', 'l_name', 'email', 'password', 'is_admin', 'register_token', 'mobile', 'is_active', 'count_thesis', 'mobile_verified_at', 'otp',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	protected $appends = ['full_name', 'speciality', 'status', 'leave_list', 'synopsis_count'];

	public function userDetail() {
		return $this->hasOne(UserDetail::class, 'user_id', 'id');
	}

	public function roles() {
		return $this->belongsToMany(Role::class, 'role_users');
	}

	/**
	 * Checks if User has access to $permissions.
	 */
	public function hasAccess(array $permissions): bool {
		// check if the permission is available in any role
		foreach ($this->roles as $role) {
			if ($role->hasAccess($permissions)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the user belongs to role.
	 */
	public function inRole(string $roleSlug) {
		return $this->roles()->where('slug', $roleSlug)->count() == 1;
	}

	public function getFullNameAttribute() {
		if ($this->userDetail && $this->userDetail->designation) {
			return '(' . $this->userDetail->designation . ')' . ' ' . $this->f_name . ' ' . $this->l_name;
		} else {
			return $this->f_name . ' ' . $this->l_name;
		}
	}

	public function category() {
		return $this->belongsToMany(Category::class, 'category_user_map');
	}

	public function studentSynopsis() {
		return $this->hasOne(Synopsis::class, 'user_id', 'id');
	}

	public function studentThesis() {
		return $this->hasOne(Thesis::class, 'user_id', 'id');
	}

	public function synopsisTransaction() {
		return $this->hasMany(SynopsisTransaction::class, 'to_user_id', 'id');
	}

	public function thesisTransction() {
		return $this->hasMany(ThesisTransaction::class, 'to_user_id', 'id');
	}

	public function facultyPayment() {
		return $this->hasOne(FacultyPayment::class, 'faculty_id', 'id');
	}

	public function leaves() {
		return $this->hasMany(AdminLeave::class, 'user_id', 'id');
	}

	public function scopeRoleType($query, $type) {
		return $query->whereHas('roles', function ($q) use ($type) {
			$q->where('slug', $type);
		});
	}

	public function reviewerResult() {
		return $this->hasMany(ThesisReviewerResult::class, 'faculty_id', 'id');
	}

	public function getSpecialityAttribute() {
		return $this->category ? implode(', ', $this->category()->pluck('name')->toArray()) : '';
	}

	public function getStatusAttribute() {
		return $this->deleted_at ? 'Inactive' : 'Active';
	}

	public function scopeCheckStdSubmission($query, $userId) {
		//return $query->where('id', $userId)->first()->studentThesis;
		$data = '';
		if (!empty($query->where('id', $userId)->first()->studentSynopsis) && !empty($query->where('id', $userId)->first()->studentThesis)) {
			$data = "(submitted:'S','D')";
		} elseif (!empty($query->where('id', $userId)->first()->studentSynopsis)) {
			$data = "(submitted:'S')";
		} elseif (!empty($query->where('id', $userId)->first()->studentThesis)) {
			$data = "(submitted:'D')";
		}
		return $data;
	}

	public function getLeaveListAttribute() {
		$now = Carbon::now()->format('Y-m-d');
		$leaves = $this->leaves()->whereDate('start_date', '>=', $now)->get();
		if (count($leaves)) {
			$leaveDates = '';
			foreach ($leaves as $value) {
				$leaveDates .= 'From ' . $value->start_date . ' End ' . $value->end_date . ',';
			}
			return rtrim($leaveDates, ',');
		} else {
			return '';
		}

	}

	public function getSynopsisCountAttribute() {
		return count($this->synopsisTransaction()->get());

	}

}
