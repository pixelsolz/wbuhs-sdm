<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThesisPdf extends Model {
	use SoftDeletes;
	protected $table = 'thesis_pdfs';

	protected $fillable = ['thesis_id', 'thesis_pdf'];

	public function thesis() {
		return $this->belongsTo(Thesis::class, 'thesis_id', 'id');
	}

}
