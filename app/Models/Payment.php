<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model {
	use SoftDeletes;
	protected $table = 'payments';

	protected $fillable = ['pay_id', 'pay_type', 'pay_amount', 'transaction_no', 'application_id', 'transaction_date', 'bill_desk_response', 'type', 'is_paid', 'status'];

	protected $appends = ['pay_for'];

	public function pay() {
		return $this->morphTo();
	}

	public function getPayForAttribute() {
		return $this->pay_type == 'App\Models\Thesis' ? 'Dissertation' : 'Synopsis';
	}

	public function payThesis() {
		return $this->belongsTo(Thesis::class, 'pay_id')
			->where('payments.pay_type', Thesis::class);
	}

	public function paySynopsis() {
		return $this->belongsTo(Synopsis::class, 'pay_id')
			->where('payments.pay_type', Synopsis::class);
	}
}
