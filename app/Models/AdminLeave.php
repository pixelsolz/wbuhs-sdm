<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminLeave extends Model {
	protected $table = 'admin_leaves';

	protected $fillable = ['user_id', 'start_date', 'end_date', 'reason'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}
