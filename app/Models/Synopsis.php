<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Synopsis extends Model {
	use SoftDeletes;
	protected $table = 'synopses';

	protected $fillable = ['category_id', 'user_id', 'name_of_student', 'registration_no',
		'registration_year', 'institute_name', 'institute_code', 'institute_id', 'mobile_landline', 'email', 'guide_name',
		'guide_designation', 'co_guide_name', 'co_guide_designation', 'proposed_title_thesis',
		'proposed_work_place', 'student_signature', 'student_pdf', 'form_pdf', 'more_pdfs', 'status', 'approved_date', 'is_resubmitted', 'resubmit_enable', 'reason_reject', 'reason_reject_doc'];

	protected $appends = ['is_paid', 'status_text', 'payment_done'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function category() {
		return $this->belongsTo(Category::class, 'category_id', 'id');
	}

	public function payements() {
		return $this->morphMany('App\Models\Payment', 'pay');
	}

	public function getIsPaidAttribute() {
		return $this->payements->count() ? 1 : 0;
	}

	public function getPaymentDoneAttribute() {
		return $this->payements()->first() ? $this->payements()->first()->is_paid : '';
	}

	public function transaction() {
		return $this->hasMany(SynopsisTransaction::class, 'synopsis_id', 'id');
	}

	public function getStatusTextAttribute() {
		$value = '';
		switch ($this->status) {
		case 1:
			$value = ($this->is_resubmitted) ? 'Resubmitted' : 'Submitted';
			break;
		case 2:
			$value = 'Approved';
			break;
		case 3:
			$value = 'Rejected';
			break;
		case 4:
			$value = 'Resubmission';
			break;
		default:
			$value = '';
			break;
		}
		return $value;
	}

	public function scopeGetBosUser($query, $synopId) {
		$synopsis = $query->where('id', $synopId)->first();
		if ($synopsis) {
			return $synopsis->category->user()->whereHas('roles', function ($q) {
				$q->where('slug', 'bos');
			})->get();
		}
	}

	public function scopeAssignedBOS($query, $synopId) {
		$synopsis = $query->where('id', $synopId)->first();
		$link = '';
		foreach ($synopsis->transaction as $value) {
			$link .= '<a href="' . route('admin.synopsis-bos-report', ['id' => $value->user->id]) . '">' . $value->user->full_name . '</a>,';
		}
		return rtrim($link, ',');

	}

	public function scopeGetOtherPdf($query, $synopId) {
		$synopsis = $query->where('id', $synopId)->first();
		if ($synopsis && !empty($synopsis->more_pdfs)) {
			$otherPdfs = explode(',', $synopsis->more_pdfs);
			$link = '';
			foreach ($otherPdfs as $key => $value) {
				$link .= '<a href="' . route('student-other-pdf', ['pdf' => $value]) . '" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" data-tog="tooltip" title="other file"></i></a>&nbsp; ';
			}
			return ' || ' . $link;
		} else {
			return '';
		}
	}

	public function synopsisStudentData() {
		return $this->hasOne(SynopsisStudentData::class, 'unique_id', 'registration_no');
	}

}
