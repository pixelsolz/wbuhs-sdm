<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdjudicatorState extends Model {
	protected $table = 'adjudicator_state';

	protected $fillable = ['name', 'email', 'phone', 'user_id', 'user_detail_id', 'state_id'];

	public function userDetail() {
		return $this->belongsTo(UserDetail::class, 'user_detail_id', 'id');
	}

}
