<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	protected $table = 'categories';

	protected $fillable = ['name', 'slug', 'parent', 'has_child'];

	public function synopsis() {
		return $this->hasMany(Synopsis::class, 'category_id', 'id');
	}

	public function user() {
		return $this->belongsToMany(User::class, 'category_user_map');
	}

	public function parentCat() {
		return $this->belongsTo(Category::class, 'parent');
	}

	public function scopeGetParent($query, $parentid) {
		return 'test';
		//return $parentId = $query->where('id', $id)->first()->parent;

		return $query->where('id', $parentid)->first() ? $query->where('id', $parentid)->first() : '';

	}

	public function scopeGetRoot($query, $parentid) {
		$qu = $query->where('id', $parentid)->first();
		if ($qu && $qu->parent == 0) {
			return $qu;
		} else {
			return Category::find($qu->parent);
		}

	}
}
