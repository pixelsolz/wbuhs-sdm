<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Thesis extends Model {
	use SoftDeletes;
	protected $table = 'thesis';

	protected $fillable = [
		'category_id', 'user_id', 'name_of_student', 'registration_no', 'registration_year', 'institute_id', 'institute_code', 'institute_name',
		'mobile_landline', 'email', 'guide_name', 'guide_designation', 'co_guide_name', 'co_guide_designation',
		'proposed_title_thesis', 'proposed_work_place', 'student_signature', 'student_pdf', 'form_pdf', 'other_pdfs', 'status', 'approved_date', 're_submit', 'is_resubmit',
	];

	protected $appends = ['is_paid', 'status_text', 'current_pdf', 'payment_done'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function category() {
		return $this->belongsTo(Category::class, 'category_id', 'id');
	}

	public function payements() {
		return $this->morphMany('App\Models\Payment', 'pay');
	}

	public function getIsPaidAttribute() {
		return $this->payements->count() ? 1 : 0;
	}

	public function getPaymentDoneAttribute() {
		return $this->payements()->first() ? $this->payements()->first()->is_paid : '';
	}

	public function thesisPdf() {
		return $this->hasMany(ThesisPdf::class, 'thesis_id', 'id');
	}

	public function getCurrentPdfAttribute() {
		return $this->thesisPdf()->count() ? $this->thesisPdf()->orderBy('id', 'desc')->first()->thesis_pdf : '';
	}

	public function getStatusTextAttribute() {
		$value = '';
		switch ($this->status) {
		case 1:
			$value = 'Submitted';
			break;
		case 2:
			$value = 'Assigned';
			break;
		case 3:
			$value = 'Accepted';
			break;
		case 4:
			$value = 'Send for Revision';
			break;
		case 5:
			$value = 'Resubmitted';
			break;
		case 6:
			$value = 'ReAssigned';
			break;
		default:
			$value = '';
			break;
		}
		return $value;
	}

	public function transaction() {
		return $this->hasMany(ThesisTransaction::class, 'thesis_id', 'id');
	}

	public function thesisResult() {
		return $this->hasMany(ThesisReviewerResult::class, 'thesis_id', 'id');
	}

	public function thesisResubmitHistory() {
		return $this->hasMany(ThesisReviewerResubmitHistories::class, 'thesis_id', 'id');
	}

	public function scopeGetReviewers($query, $category) {
		return $query->where('category_id', $category)->first()->category
			->user()->whereHas('roles', function ($q) {
			$q->where('slug', 'bos');
		})->get();
	}

	public function scopeReasonRejected($query, $id) {
		$thesis = $query->where('id', $id)->first();
		$thesisRejected = $thesis->thesisResult()->where('final_comment', 'D')->first();
		if ($thesisRejected) {
			return $thesisRejected;
		}
	}

	public function scopeGetOtherPdf($query, $thesisId) {
		$thesis = $query->where('id', $thesisId)->first();
		if ($thesis && !empty($thesis->other_pdfs)) {
			$otherPdfs = explode(',', $thesis->other_pdfs);
			$link = '';
			foreach ($otherPdfs as $key => $value) {

				$link .= '<a href="' . route('student-other-pdf', ['pdf' => $value]) . '" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" data-tog="tooltip" title="other file"></i></a>&nbsp;';
			}
			return ' || ' . $link;
		} else {
			return '';
		}
	}

	public function scopeAdjudicatorAssigned($query, $thesisId) {
		$thesis = $query->where('id', $thesisId)->first();
		$link = '';
		foreach ($thesis->transaction()->where('status', '!=', 3)->get() as $value) {
			$link .= '<a>' . $value->user->full_name . '</a>,';
		}
		return rtrim($link, ',');
	}

}
