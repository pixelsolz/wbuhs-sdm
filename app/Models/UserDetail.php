<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetail extends Model {
	use SoftDeletes;
	protected $table = 'user_details';

	protected $fillable = ['user_id', 'address', 'mobile', 'registration_no', 'gender', 'state_id', 'city_id', 'profile_image', 'designation', 'college_id', 'college_name', 'college_code', 'other_state', 'current_submission'];

	protected $appends = ['gender_text', 'city_text'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function getGenderTextAttribute() {
		return $this->gender == 1 ? 'Male' : 'Female';
	}

	public function city() {
		return '';
		//return $this->belongsTo(Cities::class, 'city_id', 'id');
	}

	public function getCityTextAttribute() {
		return $this->city_id;
	}

}
