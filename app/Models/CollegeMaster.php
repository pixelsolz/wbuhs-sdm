<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollegeMaster extends Model {
	protected $table = 'college_masters';

	protected $fillable = ['college_name', 'college_code'];
}
