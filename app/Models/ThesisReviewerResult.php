<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThesisReviewerResult extends Model {
	use SoftDeletes;
	protected $table = 'thesis_reviewer_results';

	protected $fillable = ['thesis_id', 'faculty_id', 'grade', 'remarks', 'thesis_title', 'course_subject', 'lacunae_knowledge', 'lacunae_knowledge_remarks', 'present_study', 'present_study_remarks', 'hypothesis', 'hypothesis_remarks', 'eom', 'eom_remarks', 'clarity_specificity', 'clarity_specificity_remarks', 'relevance', 'relevance_remarks', 'completeness', 'completeness_remarks', 'current_uptodate', 'current_uptodate_remarks', 'psps', 'psps_remarks', 'type_study', 'type_study_remarks', 'inclution_exclution', 'inclution_exclution_remarks', 'outcome_parameters', 'outcome_parameters_remarks', 'study_variables', 'study_variables_remarks', 'sampling_design', 'sampling_design_remarks', 'materials_experimental', 'materials_experimental_remarks', 'data_collection_tools', 'data_collection_tools_remarks', 'statistical_methods', 'statistical_methods_remarks', 'logical_organization', 'logical_organization_remarks', 'correctness_data_analysis', 'correctness_data_analysis_remarks', 'appropriate_tables', 'appropriate_tables_remarks', 'statistical_interpretation', 'statistical_interpretation_remarks', 'discussion_relevance', 'discussion_relevance_remarks', 'interpretation_results', 'interpretation_results_remarks', 'summary_conclusions', 'summary_conclusions_remarks', 'limitations', 'limitations_remarks', 'citation_appropriate', 'citation_appropriate_remarks', 'iecc', 'iecc_remarks', 'final_comment', 'reason_reject', 'alternate_reason', 'full_evaluator_signature', 'evaluator_signature', 'suggestion', 'transaction_reassigned_group', 'is_active'];

	protected $appends = ['grade_text'];

	public function thesis() {
		return $this->belongsTo(Thesis::class, 'thesis_id', 'id');
	}

	public function faculty() {
		return $this->belongsTo(User::class, 'faculty_id', 'id');
	}

	public function scopeFacultyWiseResult($query, $user_id, $field) {
		$data = $query->where('faculty_id', $user_id)->orderBy('id', 'desc')->first();
		if ($data) {
			if ($field == 'created_at') {
				return Carbon::parse($data->$field)->format('d/m/Y');
			} elseif ($field == 'final_comment') {
				if ($data->$field == 'A' || $data->$field == 'B' || $data->$field == 'C') {
					return 'Accepted';
				} else {
					return 'Send for Revision';
				}

			}
			return $data->$field ? $data->$field : '';
		} else {
			return '';
		}
	}

	public function getGradeTextAttribute() {
		if ($this->final_comment == 'A' || $this->final_comment == 'B' || $this->final_comment == 'C') {
			return 'Accepted';
		} elseif ($this->final_comment == 'D' || $this->final_comment == 'E') {
			return 'Send for Revision';
		}
	}

	public function paymentHistory() {

		return $this->hasOne(FacultyPaymentHistory::class, 'reviewer_result_id', 'id');
	}
}
