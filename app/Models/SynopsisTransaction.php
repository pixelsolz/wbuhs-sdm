<?php

namespace App\Models;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SynopsisTransaction extends Model {
	use SoftDeletes;
	protected $table = 'synopsis_transactions';

	protected $fillable = ['synopsis_id', 'to_user_id', 'trans_type', 'send_to', 'review_status', 'comment', 'created_by', 'updated_by'];

	protected $appends = ['user_name', 'review_status_text'];

	public function synopsis() {
		return $this->belongsTo(Synopsis::class, 'synopsis_id', 'id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'to_user_id', 'id')->withTrashed();
	}

	public function getUserNameAttribute() {
		return $this->user->full_name;
	}

	public function scopeGetEmailid($query) {
		$usersId = $query->pluck('to_user_id')->toArray();
		$users = User::whereIn('id', $usersId)->get();
		$names = '';
		if (count($users)) {
			foreach ($users as $value) {
				$names .= $value->full_name . ',<br />';
			}
		}

		return rtrim($names, ',<br />');
		/*$array = $query->pluck('user_name')->toArray();
		return !empty($array) ? implode(',', $array) : '';*/
	}

	public function scopeGetReviewStatus($query, $synopsis_id, $toUser) {

		$data = $query->where('synopsis_id', $synopsis_id)->where('to_user_id', $toUser)->first();
		if ($data) {
			return $data->review_status ? $data->review_status : '';
		} else {
			return '';
		}
	}

	public function getReviewStatusTextAttribute() {
		$value = '';

		if ($this->review_status == 1) {
			$value = 'Approved';
		} elseif ($this->review_status == 2) {
			$value = 'Rejected';
		} elseif ($this->review_status == 3) {
			$value = 'Resubmission';
		} else {
			$value = 'Pending';
		}
		return $value;
	}
}
