<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentResponse extends Model {
	protected $table = 'payment_response';

	protected $fillable = ['bill_desk_response'];

}
