<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SynopsisStudentData extends Model {
	protected $table = 'synopsis_student_data';

	protected $fillable = ['sl_no', 'name', 'unique_id', 'course', 'institute', 'phone', 'email', 'status'];

}
