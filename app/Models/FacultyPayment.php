<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacultyPayment extends Model {
	protected $table = 'faculty_payments';

	protected $fillable = ['faculty_id', 'review_amount', 'bonus_amount', 'total_amount', 'bank_info', 'is_submitted', 'examin_year', 'evaluator_signature', 'upload_sign', 'thesis_id'];

	public $appends = ['bank_detail'];

	public function paymentHistory() {
		if (FacultyPaymentHistory::where('faculty_id', $this->faculty_id)->count()) {
			return FacultyPaymentHistory::where('faculty_id', $this->faculty_id);
		}
		//return $this->hasMany(FacultyPaymentHistory::class, 'faculty_pay_id', 'id');
	}

	public function faculty() {
		return $this->belongsTo(User::class, 'faculty_id', 'id');
	}

	public function getBankDetailAttribute() {
		return $this->bank_info ? json_decode($this->bank_info, true) : '';
	}

	public function thesis() {
		if ($this->thesis_id) {
			return $this->belongsTo(Thesis::class, 'thesis_id', 'id');
		}
	}

	public function scopeCheckRenumeration($q, $user) {

		$reviewRes = $user->reviewerResult()->count() ? $user->reviewerResult()->orderBy('id', 'desc')->first() : '';
		$facultyPay = $q->where('faculty_id', $user->id)->first();
		if ($facultyPay && $reviewRes) {
			$history = $facultyPay->paymentHistory()->where('reviewer_result_id', $reviewRes->id)->count();
			if ($history > 0) {
				return false;
			} elseif ($facultyPay->thesis_id && $facultyPay->thesis_id == $reviewRes->thesis->id) {
				return false;
			} else {
				return true;
			}
		} elseif ($reviewRes && !$facultyPay) {
			return true;
		}
		return false;
	}

	public function facultyPaymentHisory() {
		return $this->hasMany(FacultyPaymentHistory::class, 'payment_id', 'id');
	}

}
