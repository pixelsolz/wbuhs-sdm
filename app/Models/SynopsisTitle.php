<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SynopsisTitle extends Model
{
    use Notifiable;
    protected $table = 'synopsis_title';

    protected $fillable = ['id', 'st_name', 'st_pdf'];
}
