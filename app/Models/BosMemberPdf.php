<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BosMemberPdf extends Model {
	use SoftDeletes;
	protected $table = 'bos_member_pdfs';

	protected $fillable = ['user_id', 'pdf_file'];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}
