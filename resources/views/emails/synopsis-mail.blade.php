

<!DOCTYPE html>

<html>
<head>
    <title>WBUHS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px 0; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="">

        <tr>
          <td align="center" valign="middle" style="position: relative;">
            <h3 style="margin: 0; color: #2b2b2b; font-size: 28px; line-height: 30px; font-weight: bold; padding: 20px 0 10px;">{{$type==1 ? 'Synopsis':'Dissertation'}} @if($type==1) Assign @endif</h3>
          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">
            @if($type==1)
            <p style="margin-bottom: 20px;">Dear {{@$to_user->full_name}}, <br/> New Synopsis is assign to you. Student Name: {{$synopsis->user->full_name}}.</p>
            <p>Please login with your email and password : 'user123$'. And follow the attached user guide, if you are facing any difficulty. </p>
            @else
            <p style="margin-bottom: 20px;">New Dissertation has been submitted.<br>
                Student Name: {{$synopsis->user->full_name}}
            </p>
            @endif
          </td>
        </tr>
        @if($type==1)
        <tr>
          <td align="center" valign="middle" style="position: relative; padding: 10px 0; ">
            <a href="{{route('admin.student-synopsis')}}" style="display: inline-block; border-radius: 8px; border: 1px solid #249d40;
            background: #249d40; color: #fff; font-weight: 600; font-size: 17px; line-height: 18px;
            text-decoration: none; padding: 15px 25px; margin-bottom: 15px; text-transform: capitalize;">click here go to detail </a>
          </td>
        </tr>
        @endif
      </td>
    </tr>

    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; ">
          Copyright © 2018 WBUHS Synopsis & Dissertation management system
      </td>
    </tr>
</table>
</body>
</html>



