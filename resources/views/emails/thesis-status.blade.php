

<!DOCTYPE html>

<html>
<head>
    <title>WBUHS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px 0; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="">

        <tr>
          <td align="center" valign="middle" style="position: relative;">
            <h3 style="margin: 0; color: #2b2b2b; font-size: 28px; line-height: 30px; font-weight: bold; padding: 20px 0 10px;">Dissertation Status</h3>
          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">
          	@if($status==1)
            <p style="margin-bottom: 20px;">You have Successfully completed Dissertation.</p>
            @else
            <p style="margin-bottom: 20px;">Your Dissertation has not completed, submit again.</p>
            @endif
          </td>
        </tr>

      </td>
    </tr>

    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; ">
          Copyright © 2018 WBUHS
      </td>
    </tr>
</table>
</body>
</html>



