 <!DOCTYPE html>

<html>
<head>
    <title>WBUHS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="position: relative; padding: 10px 0;">
        <a href="#" style="display: inline-block;"><img src="{{url('wbuhs-sdm/public/images/img_logo.png')}}" title="WBUHS" alt="WBUHS"/></a>
      </td>
    </tr>
    <tr>
      <td align="center" valign="middle" style="position: relative;">
        <h3 style="margin: 0; color: #2b2b2b; font-size: 28px; line-height: 30px; font-weight: bold; padding: 20px 0 30px;">Sub:Adjudication of thesis M.D. /M.S. /MDS/MPH</h3>
      </td>
    </tr>
    <tr>
      <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">
        <p style="margin:0 0 20px;">Dear Sir/Madam, </p>
        <p style="margin:0 0 20px;">I request you to kindly examine the thesis assigned by the BOS and send approval,comments and modifications suggested if any, at your earliest convenience but not later than 2 weeks. </p>
        <p style="margin:0 0 20px;">Please confirm your consent / acceptance  in reply E-mail.</p>
        <p style="margin:0 0 20px;">Based on your recommendation/approval of thesis, candidate will be considered eligible for PG examination to be conducted by the University.</p>
        <p style="margin:0 0 20px;">If you suggest any modification/correction in thesis, we will be asking the candidate to resubmit the thesis with necessary modifications which will be re-sent to you for re-examination, if you agree </p>
        <p>Your report of resubmitted thesis and approval thereof must be sent to O/o Dean within 7 working days.</p>
        <!-- <p>Please log into your profile and then click on the below link to provide your acknowledgement.</p> -->
        <!-- <a href="{{url('admin/acknowledge', ['userId'=>$userId, 'confirmationLink'=>$confirmationLink])}}" style="display: inline-block; border-radius: 8px; border: 1px solid #249d40;
            background: #249d40; color: #fff; font-weight: 600; font-size: 17px; line-height: 18px;
            text-decoration: none; padding: 15px 25px; margin-bottom: 15px; text-transform: capitalize;">click here</a> -->

      </td>
    </tr>
    <tr>
      <td align="right" valign="middle" style="position: relative; padding: 0 20px;">
        <p style="margin:0 0 10px; font-weight: 600;">Thanking You </p>
        <p style="margin:0 0 5px;">O/o Dean, Medicine</p>
        <p style="margin:0 0 5px;">WBUHS</p> 
      </td>
    </tr>  
    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; "> 
          Copyright © 2018 wbuhs.ac.in
      </td>
    </tr>
</table>
</body>
</html>
