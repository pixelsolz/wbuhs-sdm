<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>THE WEST BENGAL UNIVERSITY OF HEALTH SCIENCES</title>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body style="font-family: serif; font-size: 10pt;">
        <main class="site-main">
            <section class="synopsis-frm-sec" style="padding:40px 0;">
                <div class="container">

                    <div class="table-responsive">
                        <table class="table" style="">
                        <tr>
                            <td style="width:250px; border: none;">
                               <div class="logo-hldr" style="margin: 0 auto;border: 0px solid #000; padding: 30px; width: 130px;">
                               	<img src="{{url('public/img/img_logo.png')}}">
                               </div>
                            </td>
                            <td style="text-align: right;border: none; width: 600px;">
                                <h3 style="font-weight:600; margin: 0;">THE WEST BENGAL UNIVERSITY OF HEALTH SCIENCES</h3>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:250px;border: none; padding-bottom: 30px;"></td>
                            <td style="text-align: right;border: none;padding-bottom: 30px;">
                                <p>DD–36, Salt Lake, Sector–1, Kolkata, W.B, PIN – 700 064<br>Website: http://www.wbuhs.ac.in<br> EPBX: (033) 2321 – 3461, (033) 2334 – 6602, Fax: (033) 2358 - 0100</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border: none;padding-bottom: 30px;">
                                <h4 style="font-size: 18px;line-height: 26px;color: #126fd2; font-weight: 500;">Form for submitting ‘RESEARCH PROPOSAL’ (Thesis) on the thesis for MD / MS / MDS / M. Sc (Nursing) / MPT / MD (Hom.) / MD (Ayur.) / MASLP etc. (tick ) </h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border: none;">
                                <p style="font-size: 20px; ">in the subject {{$thesis->proposed_title_thesis}}</p>
                            </td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">1.Course Name:</h3>  </td>
                            <td style="border: none;">{{$thesis->category->name}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">2. Name of the Student (Block Letters) :</h3>  </td>
                            <td style="border: none;">{{$thesis->name_of_student}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">3. WBUHS Reg. No. & Year (Mandatory)  :</h3>  </td>
                            <td style="border: none;">{{$thesis->registration_no.' & '. $thesis->registration_year}}</td>
                        </tr>

                         <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">4. Name of the Institution  :</h3>  </td>
                            <td style="border: none;">{{$thesis->institute_name}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">5. Cell Phone / E-mail / Land line No.   :</h3>  </td>
                            <td style="border: none;">{{$thesis->mobile_landline}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">6. Name of the Guide with     Proper designation   :</h3>  </td>
                            <td style="border: none;">{{$thesis->guide_name .' '. $thesis->guide_designation}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">7. Name of the Co-Guides (if any) with proper designation :</h3>  </td>
                            <td style="border: none;">{{$thesis->co_guide_name. ' '.$thesis->co_guide_designation}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">8. Proposed Title of the Thesis :</h3>  </td>
                            <td style="border: none;">{{$thesis->proposed_title_thesis}}</td>
                        </tr>

                        <tr>
                            <td style="width:350px; border: none; vertical-align: middle;"><h3 style="font-size: 18px; color: #000; margin: 0; ">9. Proposed place of work :</h3>  </td>
                            <td style="border: none;">{{$thesis->proposed_work_place}}</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right; border: none;padding-top:50px;">
                            <img src="{{url('public/signatures/'.$thesis->student_signature)}}" style="width: 150px;">
                            <h5 style="font-size: 16px; color:#000; margin: 0; padding-bottom: 10px;text-decoration: underline;">Signature of the candidate</h5><span style="color:#000;">Forwarded</span>
                            </td>

                        </tr>




                        <tr>
                            <td style="border: none; padding-top: 60px">
                                <span style="display: block; color: #000; font-size:16px;">Forwarded </span>
                                <span style="display: block; color: #000; font-size:16px; text-decoration: underline;">Signature of Guide with Official Seal & date  </span>

                                <span style="display: block; color: #000; font-size:16px;">Countersigned</span>



                            </td>
                            <td style="border: none; text-align: right; padding-top: 60px;">

                                    <span style="display: block; color: #000; font-size:16px; text-decoration: underline;">Signature of Guide with Official Seal & date  </span>

                                <span style="display: block; color: #000; font-size:16px;">Countersigned</span>
                            </td>
                        </tr>

                        <tr>
                            <td style="border: none; padding-top: 60px">

                                <span style="display: block; color: #000; font-size:16px; text-decoration: underline;">Signature of the HOD </span>

                                <span style="display: block; color: #000; font-size:16px;">with official seal & date </span>



                            </td>
                            <td style="border: none; text-align: right; padding-top: 60px;">

                                    <span style="display: block; color: #000; font-size:16px; text-decoration: underline;">Signature of the Head of the Institute</span>

                                <span style="display: block; color: #000; font-size:16px;">with official seal & date</span>
                            </td>
                        </tr>
                    </table>
                    </div>

                </div>
            </section>
        </main>
    </body>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>





















