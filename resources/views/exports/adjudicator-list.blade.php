<table>
	<thead>
		    <tr>
		        <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>User Id</th>
                <th>User Detail Id</th>
                <th>address</th>
                <th>city</th>
                <th>State Id</th>
		    </tr>
	    </thead>
	    	    <tbody>
	    	@foreach($adjudicator_list as $adjudicator)
	        <tr>
	           <td>{{@$adjudicator->full_name}}</td>
	           <td>{{@$adjudicator->email}}</td>
	           <td>{{@$adjudicator->mobile}}</td>
	           <td>{{@$adjudicator->id}}</td>
	           <td>{{@$adjudicator->userDetail->id}}</td>
	           <td>{{@$adjudicator->userDetail->address}}</td>
	           <td>{{@$adjudicator->userDetail->city_id}}</td>
	           <td>{{@$adjudicator->userDetail->state_id}}</td>
	        </tr>
	        @endforeach
	    </tbody>
</table>