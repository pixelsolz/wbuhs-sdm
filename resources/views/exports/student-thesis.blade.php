<table>
    <thead>
    <tr>
        <th><b>Name of Student</b></th>
        <th><b>Reg. No.</b></th>
        <th><b>Reg. Year</b></th>
        <th><b>Institute name</b></th>
        <th><b>Email</b></th>
        <th><b>Phone</b></th>
        <th><b>Subject</b></th>
        <th><b>Payment Status</b></th>
        <th><b>Thesis Status</b></th>

    </tr>
    </thead>
    <tbody>
    @foreach($thesis_list as $thesis)
        <tr>
            <td>{{@$thesis->name_of_student}}</td>
            <td>{{@$thesis->registration_no}}</td>
            <td>{{@$thesis->registration_year}}</td>
            <td>{{@$thesis->institute_name}}</td>
            <td>{{@$thesis->email}}</td>
            <td>{{@$thesis->mobile_landline}}</td>
            <td>{{@$thesis->category->name}}</td>
            <td>{{@$thesis->is_paid ==1 ? 'paid':'Not Paid'}}</td>
            <td>{{@$thesis->status_text}}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>

