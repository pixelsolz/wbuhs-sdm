<table>
    <thead>
    <tr>
        <th><b>Name of Student</b></th>
        <th><b>Reg. No.</b></th>
        <th><b>Reg. Year</b></th>
        <th><b>Institute name</b></th>
        <th><b>Email</b></th>
        <th><b>Phone</b></th>
        <th><b>Subject</b></th>
        <th><b>Payment Status</b></th>
        <th><b>Synopsis Status</b></th>

    </tr>
    </thead>
    <tbody>
    @foreach($synopsis_list as $synopsis)
        <tr>
            <td>{{@$synopsis->name_of_student}}</td>
            <td>{{@$synopsis->registration_no}}</td>
            <td>{{@$synopsis->registration_year}}</td>
            <td>{{@$synopsis->institute_name}}</td>
            <td>{{@$synopsis->email}}</td>
            <td>{{@$synopsis->mobile_landline}}</td>
            <td>{{@$synopsis->category->name}}</td>
            <td>{{@$synopsis->is_paid ==1 ? 'paid':'Not Paid'}}</td>
            <td>{{@$synopsis->status_text}}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>

