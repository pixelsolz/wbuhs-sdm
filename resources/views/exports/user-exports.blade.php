<table>
    <thead>
    <tr>
        <th><b>Name</b></th>
        <th><b>Email</b></th>
        <th><b>Mobile</b></th>
        <th><b>City</b></th>
        <th><b>Speciality</b></th>
        <th><b>College Name</b></th>
        <th><b>Status</b></th>

    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{@$user->full_name}}</td>
            <td>{{@$user->email}}</td>
            <td>{{@$user->mobile}}</td>
            <td>{{@$user->userDetail->city_text}}</td>
            <td>{{@$user->speciality}}</td>
            @if(@$user->roles()->first()->slug =='student')
                @if(@$user->studentSynopsis)
                <td>{{@$user->studentSynopsis->institute_name}}</td>
                @elseif(@$user->studentThesis)
                <td>{{@$user->studentThesis->institute_name}}</td>
                @else
                <td></td>
                @endif
            @else
            <td>{{@$user->userDetail->college_name}}</td>
            @endif
            <td>{{@$user->status}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

