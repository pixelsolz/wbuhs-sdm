<table>
	<thead>
		    <tr>
		        <th>Name of Student</th>
                <th>Reg. No.</th>
                <th>Reg. Year</th>
                <th>Institute name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Course</th>
                <th>Subject</th>
                <th>Thesis Status</th>
                <th>Thesis Submitted Date</th>
		    </tr>
	    </thead>
	    	    <tbody>
	    	@forelse($thesis_list as $thesis)
	        <tr>
	           <td>{{@$thesis->name_of_student}}</td>
	            <td>{{@$thesis->registration_no}}</td>
	            <td>{{@$thesis->registration_year}}</td>
	            <td>{{@$thesis->institute_name}}</td>
	            <td>{{@$thesis->email}}</td>
	            <td>{{@$thesis->mobile_landline}}</td>
	            <td>{{@$thesis->category->parentCat->name}}</td>
	            <td>{{@$thesis->category->name}}</td>
	            <td>{{@$thesis->status_text}}</td>
	            <td>{{\Carbon\Carbon::parse(@$thesis->created_at)->format('d-m-Y')}}</td>

	        </tr>
	    	@empty
	    	<tr>
	            <td></td>
	        </tr>
	    	@endforelse
	    </tbody>
</table>