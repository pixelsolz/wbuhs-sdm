<style type="text/css">

table {
 font-family: sans-serif;
 border: 7mm solid aqua;
 border-collapse: collapse;
}
table.table2 {
 border: 2mm solid aqua;
 border-collapse: collapse;
}
table.layout {
 border: 0mm solid black;
 border-collapse: collapse;
}
td.layout {
 text-align: center;
 border: 0mm solid black;
}
td {
 padding: 3mm;
 border: 2mm solid blue;
 vertical-align: middle;
}
td.redcell {
 border: 3mm solid red;
}
td.redcell2 {
 border: 2mm solid red;
}
</style>

<table class="layout">
    <thead>
    <tr style="border:1px solid black;">
        <th style="border:1px solid black;">CREDIT A/c. NO.</th>
        <th style="border:1px solid black;">Amount</th>
        <th style="border:1px solid black;">IFSC CODE</th>
        <th style="border:1px solid black;">TYPE OF TRANSACTION</th>
        <th style="border:1px solid black;">BENEFICIARY A/C. NO.</th>
        <th style="border:1px solid black;">BENEFICIARY NAME</th>
        <th style="border:1px solid black;">BENEFICIARY BANK NAME</th>
        <th style="border:1px solid black;">PARTICULARS</th>
        <th style="border:1px solid black;">MOBILE NO.</th>
        <th style="border:1px solid black;">Bank confirmation</th>

    </tr>
    </thead>
    <tbody>
    @foreach($facultyPayments as $payment)
        <tr style="border:1px solid black;">
            <td style="border:1px solid black;">460901110050006</td>
            <td style="border:1px solid black;">{{ $payment->total_amount }}</td>
            <td style="border:1px solid black;">{{$payment->bank_detail['ifs_code']}}</td>
            <td style="border:1px solid black;">TR</td>
            <td style="border:1px solid black;">{{$payment->bank_detail['account_no']}}</td>
            <td style="border:1px solid black;">{{$payment->bank_detail['account_holder_name']}}</td>
            <td style="border:1px solid black;">{{$payment->bank_detail['bank_name']}}</td>
            <td style="border:1px solid black;">REMUNERATION</td>
            <td style="border:1px solid black;">{{ $payment->faculty->userDetail->mobile}}</td>
            <td style="border:1px solid black;"></td>

        </tr>
    @endforeach
    </tbody>
</table>