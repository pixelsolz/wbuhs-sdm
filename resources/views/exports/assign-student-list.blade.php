<table>
	<thead>
		    <tr>
		        <th>Name of Student</th>
                <th>Reg. No.</th>
                <th>Reg. Year</th>
                <th>Institute name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Course</th>
                <th>Subject</th>
                <th>Synopsis Status</th>
                <th>Synopsis Submitted Date</th>
                <th>BOS</th>
                <th>BOS phone</th>
                <th>BOS Email</th>
                <th>BOS College</th>
                <th>BOS Status</th>
		    </tr>
	    </thead>
	    	    <tbody>
	    	@forelse($synopsis_list as $synopsis)
	        <tr>
	           <td>{{@$synopsis->name_of_student}}</td>
	            <td>{{@$synopsis->registration_no}}</td>
	            <td>{{@$synopsis->registration_year}}</td>
	            <td>{{@$synopsis->institute_name}}</td>
	            <td>{{@$synopsis->email}}</td>
	            <td>{{@$synopsis->mobile_landline}}</td>
	            <td>{{@$synopsis->category->parentCat->name}}</td>
	            <td>{{@$synopsis->category->name}}</td>
	            <td>{{@$synopsis->status_text}}</td>
	            <td>{{\Carbon\Carbon::parse(@$synopsis->created_at)->format('d-m-Y')}}</td>
                <td>
	        		<table>
	        		<tr>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		</tr>
	        		@foreach(@$synopsis->transaction as $transaction)
	        		<tr>
	        		<td>{{@$transaction->user->full_name}}</td>
	        		<td>{{@$transaction->user->mobile}}</td>
	        		<td>{{@$transaction->user->email}}</td>
	        		<td>{{@$transaction->user->userDetail->college_name}}</td>
	        		<td>{{@$transaction->review_status_text}}</td>
	        		</tr>
	        		@endforeach
	        		</table>
	        	</td>

	        </tr>
	    	@empty
	    	<tr>
	            <td></td>
	        </tr>
	    	@endforelse
	    </tbody>
</table>