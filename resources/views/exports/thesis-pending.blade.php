<table>
    <thead>
    <tr>
        <th><b>Adjudicator Name</b></th>
        <th><b>Email</b></th>
        <th><b>Contact No.</b></th>
        <th><b>College Name</b></th>
        <th><b>Course</b></th>
        <th><b>Student Name</b></th>
        <th><b>Student Email</b></th>
        <th><b>Student Phone</b></th>

    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{@$user->user->full_name}}</td>
            <td>{{@$user->user->email}}</td>
            <td>{{@$user->user->mobile}}</td>
            <td>{{@$user->user->userDetail->college_name}}</td>
            <td>{{@$user->thesis->category->name}}</td>
            <td>{{@$user->thesis->name_of_student}}</td>
            <td>{{@$user->thesis->email}}</td>
            <td>{{@$user->thesis->mobile_landline}}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>

