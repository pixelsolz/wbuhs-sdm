<table>
	<thead>
		    <tr>
		        <th>Name of Student</th>
                <th>Reg. No.</th>
                <th>Reg. Year</th>
                <th>Institute name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Subject</th>
                <th>payment Status</th>
                <th>Submitted Date</th>
		    </tr>
	    </thead>
	    	    <tbody>
	    	@forelse($student_list as $student)
	        <tr>
	           <td>{{@$student->name_of_student}}</td>
	            <td>{{@$student->registration_no}}</td>
	            <td>{{@$student->registration_year}}</td>
	            <td>{{@$student->institute_name}}</td>
	            <td>{{@$student->email}}</td>
	            <td>{{@$student->mobile_landline}}</td>
	            <td>{{@$student->category->name}}</td>
	            <td>Not paid</td>
	            <td>{{\Carbon\Carbon::parse(@$student->created_at)->format('d-m-Y')}}</td>

	        </tr>
	    	@empty
	    	<tr>
	            <td></td>
	        </tr>
	    	@endforelse
	    </tbody>
</table>