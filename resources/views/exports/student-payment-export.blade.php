<table>
    <thead>
    <tr>
        <th><b>Student Name</b></th>
        <th><b>Registration No</b></th>
        <th><b>College Name</b></th>
        <th><b>Course</b></th>
        <th><b>Email</b></th>
        <th><b>Contact No.</b></th>
        <th><b>Amount</b></th>
        <th><b>Transaction No.</b></th>
        <th><b>Date Of Thesis Submission</b></th>

    </tr>
    </thead>
    <tbody>
    @foreach($studentPayments as $payment)
        <tr>
            <td>{{@$payment->pay->name_of_student}}</td>
            <td>{{@$payment->pay->registration_no}}</td>
            <td>{{@$payment->pay->institute_name}}</td>
            <td>{{@$payment->pay->category->name}}</td>
            <td>{{@$payment->pay->email}}</td>
            <td>{{@$payment->pay->mobile_landline}}</td>
            <td>{{@$payment->pay_amount}}</td>
            <td>{{@$payment->transaction_no}}</td>
            <td>{{\Carbon\Carbon::parse(@$payment->created_at)->format('d-m-Y')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

