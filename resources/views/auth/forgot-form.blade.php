@extends('frontend.layout')

@section('content')
 <section class="student-thsis">
               <div class="container">
                   <div class="thsis-content">
                        <h3>Forgot Password</h3>
                            <p>If you forget your password, while logging in, you may have to reset your password. Follow the instructions below to change/reset your password. Do not share your login/password with anyone.  </p>
                   </div>
               </div>
           </section>

           <section class="thsis-frm-rgtr-sec">
               <div class="container">

                <div class="ths-frm-wrp">


                    <div class="frm-wrpd">

                         <form method="post" action="{{url('forgot-password')}}">
                             @csrf
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control user" type="email" name="email" value="{{ old('email') }}"  placeholder="Email" required />
                        </div>

                          @if(session()->has('email_error'))
                            <span class="error">{{session('email_error')}}</span>
                          @endif
                        <div class="form-group">
                        <button class="btn btn-primary login" type="submit">Send</button>
                        <a href="{{url('login')}}" class="frgt-pass">Login</a>
                        </div>
                    </form>
                    </div>

                </div>

               </div>
           </section>


@endsection