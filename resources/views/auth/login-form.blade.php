@extends('frontend.layout')

@section('content')
 <section class="student-thsis">
               <div class="container">
                   <div class="thsis-content">
                        <h3>Synopsis & Dissertation</h3>
                            <p>Students can submit their dissertation or presentation by logging in with their user ID and password. While submitting a dissertation, you need to pay the fees of Rs. 2000 in online payment mode. Only after successful payment, your dissertation will be accepted. </p>
                	   <p>Download Synopsis Submission Userguide <a href="{{url('synopsis-manual')}}" title="Synopsis Submission Userguide"><i class="fa fa-download"></i></a></p>   
		</div>
               </div>
           </section>

           <section class="thsis-frm-rgtr-sec">
               <div class="container">

                <div class="ths-frm-wrp">
                    @if(Session::has('msg'))
                      <div class="alert alert-success">
                         {{ session('msg') }}
                      </div>
                    @endif
                    <div class="anchr-wrp">
                        <a href="{{url('login')}}" class="btn btn-primary login active">Login</a>
                        <a href="{{url('registration')}}" class="btn btn-primary rgtr">Register</a>

                    </div>
                    <div class="frm-wrpd">

                        <form method="post" action="{{url('login')}}">
                             @csrf
                          @if(Session::has('error'))
                          <span class="error">{{ Session('error')}}</span>
                          @endif
                        {{--@if($errors->any())
                         <span class="error">{{$errors->first()}}</span>
                        @endif--}}
                        <div class="form-group">
                            <label>User Email</label>
                            <input class="form-control" type="email" ype="email" name="email" value="{{ old('email') }}" placeholder="Enter your Email" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input class="form-control" type="password" name="password" placeholder="Enter your Password" required>
                        </div>

                        <div class="form-group">
                        <button class="btn btn-primary login" type="submit">Login</button>
                           <!-- <input class="form-control" type="submit" value="login" /> -->
                            <a href="{{url('forgot-password')}}" class="frgt-pass">Forgot Password?</a>
                        </div>
                    </form>
                    </div>

                </div>

               </div>
           </section>


@endsection