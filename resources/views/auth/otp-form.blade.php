@extends('frontend.layout')

@section('content')
 <section class="student-thsis">
               <div class="container">
                   <div class="thsis-content">
                        <h3>OTP</h3>
                            <p>You will recieve an OTP shortly. It might take a minute or two. If you do not get the OTP within 180 seconds, request for a new OTP.</p>
                   </div>
               </div>
           </section>

           <section class="thsis-frm-rgtr-sec">
               <div class="container">

                <div class="ths-frm-wrp">


                    <div class="frm-wrpd">

                         <form method="post" action="{{url('otp-verification')}}">
                             @csrf
                          <input type="hidden" name="otp_mobile" value="{{Session::get('mobile')}}">
                        <div class="form-group">
                            <label>OTP</label>
                            <input class="form-control user" type="text" name="otp" value=""  placeholder="OTP"  />
                        </div>

                          @if(session()->has('error'))
                            <span class="error">{{session('error')}}</span>
                          @endif
                        <div class="form-group">
                        <button class="btn btn-primary login" type="submit">Submit</button>
                        @php $otpUrl =route('otp-resend'); @endphp
                        <button class="btn btn-primary btn-disable"  onclick="otpResend('{{$otpUrl}}', event)" style="float:right">Send new OTP</button>
                        </div>
                    </form>
                    </div>

                </div>

               </div>
           </section>


@endsection
