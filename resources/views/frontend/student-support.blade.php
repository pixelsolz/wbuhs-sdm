@extends('frontend.layout')
@section('content')

<style type="text/css">
   .errors{
      color: red;
      font-size: 11px;
      margin-left:305px;
   }
</style>

<section class="desh-bord">
   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      
      
      @elseif(Session::has('msg'))
      <div class="alert alert-success">
         <strong>{{Session('msg')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Support</h3>
            <div class="right-pnl">

               <form action="{{url('support')}}" method="post">
                  @csrf

                  <div class="form-group @if($errors->first('subject')) has-error @endif">
                     <label for="f_name">Subject<span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="subject" placeholder="Subject" name="subject" value="{{old('subject')}}">
                     @if($errors->first('subject'))
                     <span class="errors">{{$errors->first('subject')}}</span>
                     @endif
                  </div>
                  <div class="form-group @if($errors->first('content')) has-error @endif">
                     <label for="l_name">Content<span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="content" id="content">{{old('content')}}</textarea>
                     @if($errors->first('content'))
                     <span class="errors">{{$errors->first('content')}}</span>
                     @endif
                  </div>


                    <div class="submitOuter">
                  <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>


@endsection