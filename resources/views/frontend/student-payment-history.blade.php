@extends('frontend.layout')
@section('content')

<section class="desh-bord">
   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Payment History</h3>
            <div class="right-pnl">
               <table class="table">
    <thead>
      <tr>
        <th>Payment For</th>
        <th>Payment Amount</th>
        <th>Payment Date</th>
        <th>Payment Type</th>
      </tr>
    </thead>
    <tbody>
        @if(!empty($synopsisPayment))
        <tr>
            <td>{{@$synopsisPayment->pay_for}}</td>
            <td>{{$synopsisPayment->pay_amount}}</td>
            <td>{{\Carbon\Carbon::parse($synopsisPayment->created_at)->format('d-m-Y')}}</td>
            <td>{{$synopsisPayment->type}}</td>
        </tr>
        @endif
        @if(!empty($dissertationPayment))
        <tr>
            <td>{{@$dissertationPayment->pay_for}}</td>
            <td>{{$dissertationPayment->pay_amount}}</td>
            <td>{{\Carbon\Carbon::parse($dissertationPayment->created_at)->format('d-m-Y')}}</td>
            <td>{{$dissertationPayment->type}}</td>
        </tr>
        @endif
    </tbody>
  </table>

            </div>
         </div>
      </div>
   </div>
</section>
</div>

@endsection