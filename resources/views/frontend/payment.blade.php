@extends('frontend.layout')

@section('content')
	<section class="desh-bord">
	<div class="container">
			@if(Session::has('error'))
			<div class="alert alert-danger">
			  <strong>{{Session('error')}}</strong>
			</div>
			@endif

	<div class="row">
		@include('frontend.includes.sidemenu')
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
		<h3 class="dashboard_title">Payment Form</h3>
		<div class="right-pnl">
			{{--<form method='POST' action='https://pgi.billdesk.com/pgidsk/PGIMerchantPayment'>--}}
			<form method='get' action='{{url("/test/payment-check")}}'>
				{{--@csrf--}}
				<input type='hidden' name='msg' id='msg' value="<?php echo ($newstr); ?>">
				<div style="text-align: center;">
					 {{-- <button type="submit" class="btn btn-primary" disabled>Offline Pay</button> --}}
				 <button type="submit"  type="submit" class="btn btn-primary">Online Pay</button>
			 	</div>
			</form>


		</div>
		</div>
	</div>



	</div>
	</section>

@endsection
