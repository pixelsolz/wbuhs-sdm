@extends('frontend.layout')
@section('content')
@php
$currentRoute = Route::currentRouteName();
@endphp
<style>
   .kbw-signature { width: 400px; height: 100px; }
   #append_more_pdf{float: right;width: 62%;margin: 10px 0 10px 10px}
   #append_more_pdf .form-group{width: 100%; margin: 0;}
   #append_more_pdf .form-group > input{width: 100%;}
</style>
<section class="desh-bord">

   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
         <h3 class="dashboard_title">Dissertation Form</h3>

            <div class="time-line-div">
                                <h3>Status</h3>
                                <div class="time-line-div-inner">
                                <div class="legend">
                                  <ul>
                                    <li><span class="notcomplete"></span> Not Complete</li>
                                    <li><span class="complete"></span> Complete</li>
                                  </ul>
                                </div>
                                <ul class="time-line-ul">
                              @if($thesis->is_resubmit)
                                <li class="time-line-li @if($thesis->status >=5 || ($thesis->is_resubmit && $thesis->status ==3)) active-li @endif">
                                  <span class="time-line-span">Dissertation Resubmitted</span><span class="circle" data-toggle="tooltip" title="{!! \Carbon\Carbon::parse($thesis->created_at)->format('d-m-Y') !!}"></span>
                                </li>
                                <li class="time-line-li @if($thesis->status >=6 || ($thesis->is_resubmit && $thesis->status ==3)) active-li @endif">
                                  <span class="time-line-span">Reassigend to Adjudicator</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li @if($thesis->status ==3) active-li @endif">
                                  <span class="time-line-span">Accepted</span><span class="circle"></span>
                                </li>
                                @else
                                <li class="time-line-li @if($thesis->status >=1) active-li @endif">
                                  <span class="time-line-span">Dissertation Submitted</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li @if($thesis->status >=2) active-li @endif">
                                  <span class="time-line-span">Assigend to Adjudicator</span><span class="circle"></span>
                                </li>
                                @if($thesis->status !=4)
                                 <li class="time-line-li @if($thesis->status ==3) active-li @endif">
                                  <span class="time-line-span">Accepted</span><span class="circle"></span>
                                </li>
                                @endif
                                @if($thesis->status ==4)
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Send for Revision</span><span class="circle"></span>
                                </li>
                                @endif
                                @endif

                              </ul>
                              {{--<ul class="time-line-ul">

                                 @foreach($thesisCycle as $cycle)
                                <li class="time-line-li @if($cycle->id <= $thesis->status) active-li @endif">
                                  <span class="time-line-span">{{$cycle->label}}</span><span class="circle"></span>
                                </li>
                                @endforeach

                              </ul>--}}
                              </div>
                              @php $subUrl=url('get/thesis-status') @endphp
                              <a href="#" data-toggle="modal" data-target="#grade-modal" data-tog="tooltip" title="See detail" onclick="getFacultyStatus('{{$subUrl}}','{{$thesis->id}}')">Evaluate Dissertation Detail</a>
                              </div>




            <div class="right-pnl">

               @if($currentRoute =='dissertation.show' && \Auth::user()->studentThesis->status ==5)
               <div class="edit-outer"><a href="{{route('dissertation.edit',['id'=>$thesis->id])}}">Edit <i class="fa fa-pencil-square-o"></i></a></div>@endif

               <form action="{{route('dissertation.update',['id'=>$thesis->id])}}" method="post" enctype="multipart/form-data"  onsubmit="return studentform(event)" id="updateThesisFile">
                  @csrf
                  @method('PUT')

                   <div class="form-group @if($errors->first('category_id')) has-error @endif">

                     <div class="category-label">
                        <label for="category_id">Category<span class="require-asterik">*</span>:</label>
                     </div>
                     <input class="form-control" type="text" value="{{$course->getRoot($course->parent)->name}}" readonly>

                  </div>

                  @if($course->getParent($course->parent) && $course->getParent($course->parent)->parent !=0)
                  <div class="form-group @if($errors->first('category_id')) has-error @endif">

                     <div class="category-label">
                        <label for="category_id">Sub category<span class="require-asterik">*</span>:</label>
                     </div>
                     <input class="form-control" type="text" value="{{$course->getParent($course->parent)->name}}" readonly>

                  </div>
                  @endif

                    <div class="form-group @if($errors->first('category_id')) has-error @endif">

                     <div class="category-label">
                        <label for="category_id">Course<span class="require-asterik">*</span>:</label>
                     </div>
                     <input class="form-control" type="text" value="{{$course->name}}" readonly>

                  </div>




                <!--   <div class="form-group">
                     <label for="category_id">Category <span class="require-asterik">*</span>: &nbsp; @if($currentRoute !='thesis.show')
                     If Category not matched <a href="" data-toggle="modal" data-target="#thesisEditModal"><i class="fa fa-plus"></i></a>@endif
                     </label>
                     <select class="form-control" name="category_id" id="category_id" @if($currentRoute=='thesis.show') disabled @endif>
                     <option value="">Select Category</option>
                     @foreach( $categories as $category)
                     <option value="{{$category['id']}}" @if(old('category_id')? old('category_id') == $category['id'] :($thesis->category_id ==$category['id'] )) selected @endif>{!! $category['name'] !!}</option>
                     @endforeach
                     </select>
                     @if($errors->first('category_id'))
                     <span class="error">{{$errors->first('category_id')}}</span>
                     @endif
                  </div> -->
                  <div class="form-group @if($errors->first('name_of_student')) has-error @endif">
                     <label for="name_of_student">Name of the Student <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="name_of_student" placeholder="Enter Student Name" name="name_of_student" value="{{old('name_of_student') ?old('name_of_student'):$thesis->name_of_student }}" readonly>

                  </div>
                  <div class="form-group  @if($errors->first('registration_no')) has-error @endif">
                     <label for="registration_no">WBUHS Reg. No. <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="registration_no" placeholder="Enter Registration No" name="registration_no" value="{{old('registration_no')?old('registration_no'):$thesis->registration_no }}" readonly>

                  </div>
                  <div class="form-group @if($errors->first('registration_year')) has-error @endif">
                     <label for="registration_year">WBUHS Reg. Year <span class="require-asterik">*</span>:</label>
                     <input type="number" class="form-control" id="registration_year" placeholder="Enter Registration Year" name="registration_year" value="{{old('registration_year')? old('registration_year'):$thesis->registration_year }}" readonly >

                  </div>
                  <div class="form-group @if($errors->first('institute_name')) has-error @endif">
                     <label for="institute_name">Name of the Institution <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="institute_name" placeholder="Enter Institute Name" name="institute_name" value="{{old('institute_name')? old('institute_name'):$thesis->institute_name }}" readonly>

                  </div>
                  <div class="form-group @if($errors->first('mobile_landline')) has-error @endif">
                     <label for="mobile_landline">Cell Phone <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="mobile_landline" placeholder="Enter Mobile / Land No" name="mobile_landline" value="{{old('mobile_landline')? old('mobile_landline'):$thesis->mobile_landline }}" readonly>

                  </div>
                  <div class="form-group @if($errors->first('email')) has-error @endif">
                     <label for="email">E-mail <span class="require-asterik">*</span>:</label>
                     <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" value="{{old('email')? old('email'):$thesis->email }}"  readonly>

                  </div>
                  <div class="form-group @if($errors->first('guide_name')) has-error @endif">
                     <label for="guide_name">Guide Name <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="guide_name" placeholder="Enter Guide Name" name="guide_name" value="{{old('guide_name')? old('guide_name'):$thesis->guide_name }}" @if($currentRoute=='dissertation.show') readonly @endif>

                  </div>
                  <div class="form-group @if($errors->first('guide_designation')) has-error @endif">
                     <label for="guide_designation">Guide Designation <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="guide_designation" placeholder="Enter Guide Designation" name="guide_designation" value="{{old('guide_designation') ? old('guide_designation'):$thesis->guide_designation }}" @if($currentRoute=='dissertation.show') readonly @endif>

                  </div>
                  <div class="form-group">
                     <label for="co_guide_name">Co-Guides Name:</label>
                     <input type="text" class="form-control" id="co_guide_name" placeholder="Enter Co Guide Name" name="co_guide_name" value="{{old('co_guide_name')? old('co_guide_name'):$thesis->co_guide_name }}" @if($currentRoute=='dissertation.show') readonly @endif>
                  </div>
                  <div class="form-group">
                     <label for="co_guide_designation">Co-Guides Designation:</label>
                     <input type="text" class="form-control" id="co_guide_designation" placeholder="Enter Co Guide Designation" name="co_guide_designation" value="{{old('co_guide_designation')?old('co_guide_designation'):$thesis->co_guide_designation }}" @if($currentRoute=='dissertation.show') readonly @endif>
                  </div>
                  <div class="form-group @if($errors->first('proposed_title_thesis')) has-error @endif">
                     <label for="proposed_title_thesis">Proposed Title of the Dissertation <span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="proposed_title_thesis" id="proposed_title_thesis" @if($currentRoute=='dissertation.show') readonly @endif>{{old('proposed_title_thesis')?old('proposed_title_thesis'):$thesis->proposed_title_thesis }}</textarea>

                  </div>
                  <div class="form-group @if($errors->first('proposed_work_place')) has-error @endif">
                     <label for="proposed_work_place">Proposed place of work <span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="proposed_work_place" id="proposed_work_place" @if($currentRoute=='dissertation.show') readonly @endif>{{old('proposed_work_place') ? old('proposed_work_place'):$thesis->proposed_work_place }}</textarea>

                  </div>
                  {{--@if($currentRoute !='dissertation.show')--}}
                  <div class="form-group @if($errors->first('student_pdf')) has-error @endif">
                     <label for="proposed_work_place">Upload PDF <span class="require-asterik">*</span>:

                      @if($thesis->re_submit ==0)
                      <a href="{{route('thesis.view.pdf',['id'=>$thesis->id])}}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px"></i></a> @else <span>(Add more PDF)<a href="javascript:void(0)" onclick="addMorePdf()"> <i class="fa fa-plus" style="font-size:18px"></i></a></span> @endif</label>
                     <input class="form-control" type="file" name="student_pdf" data-url="{{url('synopsis/file-upload')}}">

                     <div id="append_more_pdf">
                     </div>
                  </div>
                  @if($currentRoute=='dissertation.show' && $thesis->other_pdfs)
                  <div class="form-group">
                      <label for="proposed_work_place">Other PDF:</label>
                      @foreach(explode(',',$thesis->other_pdfs) as $other_pdf)
                      <a href="{{url('dissertation/other-pdf',['pdf'=>$other_pdf])}}" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px"></i></a> &nbsp;
                      @endforeach
                  </div>
                  @endif

                  {{--@endif--}}
                  <input type="hidden" name="student_signature" id="student_signature">
                  <div>
                     @if($currentRoute!='dissertation.show')
                     <label for="sig" class="signature-field">Signature <span class="require-asterik">*</span>: (or Upload Signature)<a href="javascript:void(0)" onclick="showSignUpload('signature_off')"> click</a></label>
                     <label for="sig" style="display: none;" class="signature-upload">Upload Signature <span class="require-asterik">*</span>: (or Signature)<a href="javascript:void(0)" onclick="showSignUpload('signature_on')"> click</a></label>
                     <div class="form-group signature-field">
                        <div id="sig"></div>
                     </div>

                     <p style="clear: both;" class="signature-field">
                        <a href="#" id="saveImage" data-url="{{url('dissertation/save-signature')}}" onclick="signatureUpload('{{csrf_token()}}', event)">Save Signature</a>
                        <a href="#" id="clear">Clear</a>
                        <!-- <button class="btn btn-secondary" id="clear">Clear</button>
                        <button class="btn btn-secondary" id="saveImage" data-url="{{url('thesis/save-signature')}}" onclick="signatureUpload('{{csrf_token()}}', event)">Save</button> -->
                     </p>
                     @endif
                     @if($currentRoute =='dissertation.show')
                     <label class="signature-field">Signature:</label>
                     <span class="view-signature">
                     <img src="{{url('public/signatures/'.$thesis->student_signature)}}" id="showsign" class="signature-field">
                     </span>
                     @else
                     {{--<img src="{{url('public/signatures/'.$thesis->student_signature)}}" id="showsign" class="signature-field">--}}
                     @endif
                     <div class="form-group signature-upload" id="signature-image" style="display: none;">
                        <input type="file" name="" class="form-control">
                     </div>
                  </div>
                  <!-- <div id="sig"></div> -->
                  <div class="submitOuter">

                     @if($thesis->re_submit ==1)
                     @php $thesisFileUpdateUrl = route('thesis-file.resubmit', $thesis->id) @endphp
                     <button type="submit" class="btn btn-primary submit-student" onclick="updateThesisFile('{{$thesisFileUpdateUrl}}', event)">Edit Dissertation</button>
                     @else
                     @if($currentRoute!='dissertation.show')
                     <button type="submit" class="btn btn-primary submit-student" disabled="true">Edit Dissertation</button>
                     @endif
                     @endif
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- The Modal -->
<div class="modal fade" id="thesisEditModal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <h4 class="modal-title">Add Category</h4>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <form action="{{route('category.student-add')}}" id="category_modal" data-reload-url="{{route('dissertation.index')}}">
               @csrf
               <div class="form-group">
                  <label for="email">Category:</label>
                  <select class="form-control" name="parent">
                     <option value="0">Root Category</option>
                     @foreach( $categories as $category)
                     <option value="{{$category['id']}}">{!! $category['name'] !!}</option>
                     @endforeach
                  </select>
               </div>
               <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control"  name="name">
                  <span class="error" id="name"></span>
               </div>
            </form>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="modalFormSubmit('category_modal')">Add &nbsp;<i class="fa fa-spinner fa-spin" style="font-size:20px; display: none;" id="spinner"></i></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

        <div class="modal fade design-modal" id="grade-modal">
          <div class="modal-dialog modal-md">
            <div class="modal-content modal-default">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Evaluate Dissertation Detail</h4>
              </div>
              <div class="modal-body">
              <table class="table">
              <thead>
                <tr>
                  <th>Faculty</th>
                  <th>Submitted</th>
                  <th>Status</th>
                  <th>Comment</th>
                </tr>
              </thead>
              <tbody id="gradeModalTbody">

              </tbody>
            </table>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade design-modal" id="reason-rejected-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Reason for Revision</h4>
                </div>
                <div class="modal-body">
                <p id="reason_content"></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

@endsection
