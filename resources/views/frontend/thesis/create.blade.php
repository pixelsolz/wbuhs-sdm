@extends('frontend.layout')
@section('content')
@php
$currentRoute = Route::currentRouteName();
@endphp
<style>
   .kbw-signature { width: 400px; height: 100px; }
   #append_more_pdf{float: right;width: 62%;margin: 10px 0 10px 10px}
   #append_more_pdf .form-group{width: 100%; margin: 0;}
   #append_more_pdf .form-group > input{width: 100%;}
</style>
<section class="desh-bord">
   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Dissertation Form Submission</h3>
            @if($errors->has('registration_no'))
            <span class="dfgdfg" style="display: block; color: #f00; text-align: center;
                        font-size: 15px;">{{$errors->first('registration_no')}}</span>
            @endif
            <div class="right-pnl">

               <form action="{{route('dissertation.store')}}" method="post" enctype="multipart/form-data" onsubmit="return studentform(event)">
                  @csrf

                  <div class="form-group @if($errors->first('category_id')) has-error @endif">

                     <div class="category-label">
                        <label for="category_id">Category<span class="require-asterik">*</span>:</label>
                     </div>
                     @php $catUrl =url('get-subcategory'); @endphp
                     <select class="form-control" name="category_id" id="category_id" onchange="getSubCategory(this, '{{$catUrl}}')">
                        <option value="">Select Category</option>
                        @foreach( $parentCategory as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                     </select>

                  </div>

                  <div class="form-group" id="subcat_div">
                     <div class="category-label">
                        <label for="category_id">Sub Category <span class="require-asterik">*</span>:</label>
                     </div>
                     <select class="form-control" name="sub_category" id="add_sub_cat" onchange="getSubCategory(this, '{{$catUrl}}')">
                        <option value="">Select Sub Category</option>
                     </select>

                  </div>

                  <div class="form-group">
                     <div class="category-label">
                        <label for="add_course">Category Name <span class="require-asterik">*</span>:</label>
                     </div>
                     <select class="form-control" name="course_name" id="add_course">
                        <option value="">Select Course</option>
                     </select>

                  </div>

                   <!--<div id="add_sub_cat"></div>
                  <div id="add_course"></div>-->



                  <div class="form-group @if($errors->first('name_of_student')) has-error @endif">
                     <label for="name_of_student">Name of the Student <span class="require-asterik">*</span>:</label>
                      @php $checknameUrl =url('set-registration-no'); @endphp
                     <input type="text" class="form-control" id="name_of_student" placeholder="Enter Student Name" name="name_of_student" value="{{(old('name_of_student'))?old('name_of_student'):\Auth::user()->full_name}}">
                     @if($errors->first('name_of_student'))
                     <span class="error">{{$errors->first('name_of_student')}}</span>
                     @endif
                  </div>
                  <div class="form-group @if($errors->first('registration_no')) has-error @endif">
                      @php $checknameUrl =url('set-registration-no'); @endphp
                     <label for="registration_no">WBUHS Reg. No. <span class="require-asterik">*</span>:&nbsp;@if(!@\Auth::user()->userDetail->registration_no)<a href="javascript:void(0)" onclick="checkRegistration( '{{$checknameUrl}}')">Check your Reg. No.</a>@endif</label>
                     @php $checkregistrationUrl =url('check-registration-no'); @endphp
                     <input type="text" class="form-control" id="registration_no" placeholder="Enter Registration No" name="registration_no" value="{{@\Auth::user()->userDetail->registration_no}}" readonly>

                  </div>
                  <div class="form-group @if($errors->first('registration_year')) has-error @endif">
                     <label for="registration_year">WBUHS Reg. Year <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="registration_year" placeholder="Enter Registration Year" name="registration_year" value="" >

                  </div>
                  <div class="form-group @if($errors->first('institute_name')) has-error @endif">
                     <label for="institute_name">Name of the Institution <span class="require-asterik">*</span>:</label>
                     <select class="form-control" name="institute_name">
                              <option value="">Select College Name</option>
                              @foreach($college_masters as $college)
                              <option value="{{$college['id']}}">{!! $college['college_name']!!}</option>
                              @endforeach
                           </select>
                     {{--<input type="text" class="form-control" id="institute_name" placeholder="Enter Institute Name" name="institute_name" value="" >--}}

                  </div>
                  <div class="form-group @if($errors->first('mobile_landline')) has-error @endif">
                     <label for="mobile_landline">Cell Phone <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="mobile_landline" placeholder="Enter Mobile / Land No" name="mobile_landline" value="{{(old('mobile_landline'))?old('mobile_landline'):\Auth::user()->userDetail->mobile}}" readonly>

                  </div>
                  <div class="form-group @if($errors->first('email')) has-error @endif">
                     <label for="email">E-mail <span class="require-asterik">*</span>:</label>
                     <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" value="{{(old('email'))?old('email'):\Auth::user()->email}}" readonly>

                  </div>
                  <div class="form-group @if($errors->first('guide_name')) has-error @endif">
                     <label for="guide_name">Guide Name <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="guide_name" placeholder="Enter Guide Name" name="guide_name" value="{{old('guide_name')}}">

                  </div>
                  <div class="form-group @if($errors->first('guide_designation')) has-error @endif">
                     <label for="guide_designation">Guide Designation <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="guide_designation" placeholder="Enter Guide Designation" name="guide_designation" value="{{old('guide_designation')}}">

                  </div>
                  <div class="form-group">
                     <label for="co_guide_name">Co-Guides Name:</label>
                     <input type="text" class="form-control" id="co_guide_name" placeholder="Enter Co Guide Name" name="co_guide_name" value="{{old('co_guide_name')}}">
                  </div>
                  <div class="form-group">
                     <label for="co_guide_designation">Co-Guides Designation:</label>
                     <input type="text" class="form-control" id="co_guide_designation" placeholder="Enter Co Guide Designation" name="co_guide_designation" value="{{old('co_guide_designation')}}">
                  </div>
                  <div class="form-group @if($errors->first('proposed_title_thesis')) has-error @endif">
                     <label for="proposed_title_thesis">Proposed Title <span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="proposed_title_thesis" id="proposed_title_thesis">{{old('proposed_title_thesis')}}</textarea>

                  </div>
                  <div class="form-group @if($errors->first('proposed_work_place')) has-error @endif">
                     <label for="proposed_work_place">Proposed place of work <span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="proposed_work_place" id="proposed_work_place">{{old('proposed_work_place')}}</textarea>

                  </div>
                  <div class="form-group @if($errors->first('student_pdf')) has-error @endif">
                     <label for="proposed_work_place">Upload PDF <span class="require-asterik">*</span>: (Add more PDF)<a href="javascript:void(0)" onclick="addMorePdf()"> <i class="fa fa-plus" style="font-size:18px"></i></a></a></label>
                     <input class="form-control" type="file" name="student_pdf" id="upload_file" data-url="{{url('synopsis/file-upload')}}">
                     <div class="file-size">Max file size : 25 MB</div>
                     <div id="append_more_pdf">
                     </div>

                  </div>
                  <input type="hidden" name="student_signature" id="student_signature">
                  <div>
                     <label for="sig" class="signature-field">Signature <span class="require-asterik">*</span>: (or Upload Signature)<a href="javascript:void(0)" onclick="showSignUpload('signature_off')"> click</a></label>
                     <label for="sig" style="display: none;" class="signature-upload">Upload Signature <span class="require-asterik">*</span>: (or Signature)<a href="javascript:void(0)" onclick="showSignUpload('signature_on')"> click</a></label>
                     <div class="form-group signature-field">
                        <div id="sig"></div>
                     </div>
                     <p style="clear: both;" class="signature-field">
                        <a href="#" id="saveImage" data-url="{{url('dissertation/save-signature')}}" onclick="signatureUpload('{{csrf_token()}}', event)">Save Signature</a>
                        <a href="#" id="clear">Clear</a>
                     </p>
                     <!-- <img src="" id="showsign" class="signature-field"> -->
                     <div class="form-group signature-upload" id="signature-image" style="display: none;">
                        <input type="file" name="signUpload" class="form-control">
                     </div>
                  </div>
                  <!-- <div id="sig"></div> -->
                  <div class="submitOuter">
                  <button type="submit" class="btn btn-primary submit-student" disabled="true">Submit Dissertation</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- The Modal -->
<div class="modal fade" id="thesisModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <h4 class="modal-title">Add Category</h4>
         </div>
         <div class="modal-body">
            <form action="{{route('category.student-add')}}" id="category_modal" data-reload-url="{{route('synopsis.index')}}">
               @csrf
               <div class="form-group">
                  <label for="email">Category:</label>
                  <select class="form-control" name="parent">
                     <option value="0">Root Category</option>
                     @foreach( $categories as $category)
                     <option value="{{$category['id']}}">{!! $category['name'] !!}</option>
                     @endforeach
                  </select>
               </div>
               <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control"  name="name">
                  <span class="error" id="name"></span>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="modalFormSubmit('category_modal')">Add &nbsp;<i class="fa fa-spinner fa-spin" style="font-size:20px; display: none;" id="spinner"></i></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
@endsection
