@extends('frontend.layout')

@section('content')
    <!-- Main content -->

    <section class="content">
        <div class="container">
            <div class="thsis-content">
                <h2 class="inner-title">Synopsis Title</h2>
            </div>
    
            <div class="table-responsive">
                <table id="example123" class="table table-bordered table-hover phd-pdf-table">
                    <thead>
                        <tr>
                            <th >Sl No.</th>
                            <th>Title</th>
                            <th>Download</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $a=1; @endphp
                        @forelse($synopsistitles as $synopsistitle)
                            <tr>
                                <td>{{ $a }}</td>
                                <td>{{ $synopsistitle->st_name }}</td>
    
                                <td>
                                    <a href="{{ asset('public/pdfs/' . $synopsistitle->st_pdf) }}" target="_blank">
                                        <img src="{{ asset('public/images/pdf.png') }}" /></a>
                                </td>
    
                            </tr>
                            @php $a++; @endphp
                        @empty
                            <tr>
                                <td colspan="6" style="text-align: center;">No data available</td>
                            </tr>
                        @endforelse
                    </tbody>
    
                </table>
            </div>
            <div>{{ $synopsistitles->links() }}</div>
        </div>


     
    </section>
    <!-- /.content -->

@endsection
