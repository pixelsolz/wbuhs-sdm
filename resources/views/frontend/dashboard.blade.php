@extends('frontend.layout')
@section('content')
<section class="desh-bord">
   <div class="container">
      @if(Session::has('msg'))
      <div class="alert alert-success">
         <strong>{{Session('msg')}}</strong>
      </div>
      @endif
      @if(Session::has('status'))
      <div class="alert alert-success" role="alert">
         {{ session('status') }}
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Dashboard</h3>
            @if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->status == 3)
            <div class="alert alert-danger">
              <strong>Synopsis Rejected!</strong> Your Synopsis has been Rejected.
            </div>
            @elseif(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->status == 4)
            <div class="alert alert-danger">
              <strong>Synopsis Send for revision!</strong> Your Synopsis has not been accepted please Resubmit again.
            </div>
            @endif
            @if(Auth::user()->studentThesis && \Auth::user()->studentThesis->status ==4)
            <div class="alert alert-danger">
              <strong>Dissertation Send for revision!</strong> Your Dissertation has not been accepted please Resubmit again.
            </div>
            @endif
            <div class="right-pnl">
               <ul>
               <!-- check user not submit thesis -->
               @if(!@Auth::user()->studentThesis && !@Auth::user()->studentThesis->is_paid)
                  <li>
                     <div class="lft">
                        <h4>{{\Auth::user()->studentSynopsis()->count()}}</h4>
                        @if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid)
                        @if(\Auth::user()->studentSynopsis->status ==4)
                        <a href="{{route('synopsis.edit',['id'=>\Auth::user()->studentSynopsis->id])}}" class="{{(Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
                        @else
                        <span><a href="{{url('synopsis',[\Auth::user()->studentSynopsis])}}">Synopsis</a></span>
                        @endif
                        @elseif(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid ==0)
                        <span><a href="{{url('synopsis/old-data',['id'=>\Auth::user()->studentSynopsis->id])}}">Synopsis</a></span>
                        @else
                        <span><a href="{{url('synopsis/create')}}">Synopsis</a></span>
                        @endif
                     </div>
                     <div class="right">
                        <span class="icon"><i class="fa fa-book" aria-hidden="true"></i></span>
                     </div>
                  </li>

                  @endif
                  <!-- END -->

                  {{--@if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->status ==2)--}}
                  <!-- check user has thesis-->
                 {{-- @if(@Auth::user()->studentThesis && @Auth::user()->studentThesis->is_paid) --}}
                  <li>
                     <div class="lft">
                        <h4>{{\Auth::user()->studentThesis()->count()}}</h4>
                        @if(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid)
                        <span><a href="{{\Auth::user()->studentThesis->status ==4? route('dissertation.edit',['id'=>\Auth::user()->studentThesis]): url('dissertation',[\Auth::user()->studentThesis])}}">Dissertation</a> </span>
                        @elseif(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid ==0)
                        <span><a href="{{url('dissertation/old-data',['id'=>\Auth::user()->studentThesis->id])}}">Dissertation</a></span>
                        @else
                        <span><a href="{{url('dissertation/create')}}">Dissertation</a> </span>
                        @endif
                     </div>
                     <div class="right">
                        <span class="icon"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                     </div>
                  </li>

                 {{--  @endif --}}
                  <!-- END -->

                  {{--@endif--}}
                  <!-- <li>
                     <div class="lft">
                        <h4>14</h4>
                        <span>VAT Check Done</span>
                     </div>
                     <div class="right">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                     </div>
                  </li> -->
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
