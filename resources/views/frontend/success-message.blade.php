@extends('frontend.layout')
@section('content')


<section class="desh-bord">
   <div class="container">
      <div class="row">
         {{--@include('frontend.includes.sidemenu')--}}
         <div class="col-md-12 col-sm-12 col-xs-12">
         	<div class="thankyouOuter">
         		<img src="{{url('public/img/thankcheck.png')}}" alt="thankcheck">
               @if($type ==1)
         		<p>Your Synopsis has been submitted succesfully.</p>
               @else
               <p>Your Dissertation has been submitted succesfully.</p>
               @endif

               @if($paid_type =='offline')
                  <p>You have paid by offline. So after paying fees your {{($type ==1? 'Synopsis':'Dissertation')}} will be procced.</p>
               @endif
         		<a href="{{route('dashboard.index')}}">Go To dashboard</a>
         	</div>
         </div>
      </div>
   </div>
</section>

@endsection