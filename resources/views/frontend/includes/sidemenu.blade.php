<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
   <div class="lft-pnl">
      <a href="{{url('dashboard')}}" class="{{Request::is('dashboard')? 'active': ''}}"><i class="fa fa-dashboard" aria-hidden="true"></i> Dashboard</a>
      @if(!@Auth::user()->studentThesis && !@Auth::user()->studentThesis->is_paid)

      @if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid)
         @if(\Auth::user()->studentSynopsis->status ==4)
         <a href="{{route('synopsis.edit',['id'=>\Auth::user()->studentSynopsis->id])}}" class="{{(Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
         @else
         <a href="{{url('synopsis',[\Auth::user()->studentSynopsis])}}" class="{{(Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
         @endif
      @elseif(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid ==0)
         <a href="{{url('synopsis/old-data',['id'=>\Auth::user()->studentSynopsis->id])}}" class="{{(Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
      @else
      <a href="{{url('synopsis/create')}}" class="{{(Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
      @endif

      @endif
     {{--@if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->status ==2)--}}

     <!-- check user has thesis-->
      {{--@if(@Auth::user()->studentThesis && @Auth::user()->studentThesis->is_paid)--}}

      @if(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid)

      <a href="{{\Auth::user()->studentThesis->status==4 ? route('dissertation.edit',['id'=>\Auth::user()->studentThesis]): url('dissertation',[\Auth::user()->studentThesis])}}" class="{{(Request::is('dissertation')||Request::is('dissertation/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i>Dissertation Submission</a>

       @elseif(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid ==0)
         <a href="{{url('dissertation/old-data',['id'=>\Auth::user()->studentThesis->id])}}" class="{{(Request::is('dissertation')||Request::is('dissertation/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i>Dissertation Submission</a>
      @else
      <a href="{{url('dissertation/create')}}" class="{{(Request::is('dissertation')||Request::is('dissertation/*'))? 'active': ''}}"><i class="fa fa-files-o" aria-hidden="true"></i>Dissertation Submission</a>
      @endif
      {{--@endif--}}
      @if((@\Auth::user()->studentSynopsis || @\Auth::user()->studentThesis) &&  (@\Auth::user()->studentSynopsis->status ==3 || @\Auth::user()->studentSynopsis->status == 4 || (@\Auth::user()->studentThesis && @\Auth::user()->studentThesis->status ==4)))
      <a href="{{route('rejection-list')}}" class="{{(Request::is('rejection-list')||Request::is('rejection-list/*'))? 'active': ''}}"> <i class="fa fa-ban" aria-hidden="true"></i>Rejection or Resubmission List</a>
      @endif


      {{--@endif--}}
      <!-- END-->
      @if((!empty(\Auth::user()->studentSynopsis->payements) && count(\Auth::user()->studentSynopsis->payements))|| (!empty(\Auth::user()->studentThesis->payements) && count(\Auth::user()->studentThesis->payements)) )
      <a href="{{url('payment/history')}}" class="{{Request::is('payment/history')? 'active': ''}}"> <i class="fa fa-money" aria-hidden="true"></i> Payment History</a>
     @endif

      <a href="{{route('profile.edit',['profile'=>\Auth::user()])}}" class="{{(Request::is('profile')|| Request::is('profile/*'))? 'active': ''}}"> <i class="fa fa-user" aria-hidden="true"></i> Profile</a>
      <a href="{{url('change-password')}}" class="{{Request::is('change-password')? 'active': ''}}"> <i class="fa fa-key" aria-hidden="true"></i> Change Password</a> <a href="{{url('support')}}" class="{{Request::is('support')? 'active': ''}}"> <i class="fa fa-support" aria-hidden="true"></i> Support</a>
   </div>
</div>
