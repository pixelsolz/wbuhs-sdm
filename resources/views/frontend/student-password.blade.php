@extends('frontend.layout')
@section('content')

<style type="text/css">
   .errors{
      color: red;
      font-size: 11px;
      margin-left:305px;
   }
</style>

<section class="desh-bord">
   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Password Change</h3>
            <div class="right-pnl">

               <form action="{{url('change-password')}}" method="post">
                  @csrf

                  <div class="form-group @if($errors->first('password')) has-error @endif">
                     <label for="f_name">Password<span class="require-asterik">*</span>:</label>
                     <input type="password" class="form-control" id="password" placeholder="Password" name="password" value="{{old('password')}}">
                     @if($errors->first('password'))
                     <span class="errors">{{$errors->first('password')}}</span>
                     @endif
                  </div>
                  <div class="form-group @if($errors->first('password_confirmation')) has-error @endif">
                     <label for="l_name">Confirm Password<span class="require-asterik">*</span>:</label>
                     <input type="password" class="form-control" id="password_confirmation" placeholder="Confirm password" name="password_confirmation" value="{{old('password_confirmation')}}">
                     @if($errors->first('password_confirmation'))
                     <span class="errors">{{$errors->first('password_confirmation')}}</span>
                     @endif
                  </div>


                    <div class="submitOuter">
                  <button type="submit" class="btn btn-primary">Change</button>
                    </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>


@endsection