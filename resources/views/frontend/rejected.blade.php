@extends('frontend.layout')
@section('content')

<section class="desh-bord">
   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Rejected Or Resubmission</h3>
            <div class="right-pnl">
               <table class="table">
    <thead>
      <tr>
        <th>Type</th>
        <th>Date</th>
        <th>Status</th>
        <th>Reason</th>
      </tr>
    </thead>
    <tbody>
      @if($type=='Synopsis')
      <tr>
        <td>{{$type}}</td>
        <td>{{\Carbon\Carbon::parse($rejectedData->created_at)->format('d/m/Y')}}</td>
        <td>{{$rejectedData->status_text}}</td>
        <td>
        @if($rejectedData->reason_reject)
        <a href="javascript:void(0)" onclick="reasonRejectModal('{{$rejectedType}}','{{$rejectedData->reason_reject}}')">{{strlen($rejectedData->reason_reject) >15 ? substr($rejectedData->reason_reject,0,15).'...' : $rejectedData->reason_reject}}</a>
        @else
          <form action="{{url('download-reason/doc')}}" method="get">
            <input type="hidden" name="id" value="{{$rejectedData->id}}">
            <input type="hidden" name="type" value="synopsis">
            <button class="fa fa-download" aria-hidden="true" type="submit"></button>
          </form>
          <!-- <a href="" onclick="" ><i class="fa fa-download" aria-hidden="true" data-tog="tooltip" title="Reason"></i></a> -->
        @endif
        </td>
      </tr>
      @else
        <tr>
          <td>{{$type}}</td>
          <td>{{\Carbon\Carbon::parse($rejectedData->created_at)->format('d/m/Y')}}</td>
          <td>{{$rejectedData->status_text}}</td>
          @php
            $rejectedReason = $rejectedData->reasonRejected($rejectedData->id);
          @endphp
          @if($rejectedReason->reason_reject)
          <td><a href="javascript:void(0)" onclick="reasonRejectModal('{{$rejectedType}}','{{$rejectedReason->reason_reject}}')">{{strlen($rejectedReason->reason_reject) >15 ? substr($rejectedReason->reason_reject,0,15).'...' : $rejectedReason->reason_reject}}</a></td>
          @else
            <form action="{{url('download-reason/doc')}}" method="get">
            <input type="hidden" name="id" value="{{$rejectedReason->id}}">
            <input type="hidden" name="type" value="thesis">
            <button class="fa fa-download" aria-hidden="true" type="submit"></button>
          </form>
          @endif
        </tr>

      @endif

    </tbody>
  </table>

            </div>
         </div>
      </div>
   </div>
</section>
 <div class="modal fade" id="1myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reason of <span id="title"></span></h4>
        </div>
        <div class="modal-body">
          {{--$rejectedData->reason_reject--}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@endsection