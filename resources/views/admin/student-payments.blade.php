@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Student Payments
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>

                <div class="form-group row">
                    <form action="{{route('student.payment-history')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-3">
                   <input class="form-control datepicker" type="text" name="from_date" placeholder="From Date" autocomplete="off" @if(!empty($request->from_date)) value="{{$request->from_date}}" @endif>
                  </div>

                  <div class="col-xs-3">
                   <input class="form-control datepicker" type="text" name="to_date" placeholder="To Date" autocomplete="off" @if(!empty($request->to_date)) value="{{$request->to_date}}" @endif>
                  </div>

		              <div class="col-xs-3">
                      <select class="form-control" name="pay_for">
                          <option value="">Select payment for</option>
                          <option value="1" @if($request->pay_for ==1) selected @endif>Dissertation</option>
                          <option value="2" @if($request->pay_for ==2) selected @endif>Synopsis</option>
                      </select>
                  </div>

                  <div class="col-xs-2">
                    <a href="javascript:void(0)"class="btn btn-default" onclick="printData()"><i class="fa fa-print"></i> Print</a>
                  </div>
                  <div class="col-xs-3"><br/>
                     <input class="form-control" type="text" name="by_name" placeholder="search By Student Name" autocomplete="off" @if(!empty($request->by_name)) value="{{$request->by_name}}" @endif>
                  </div>
                  <div class="col-xs-1"><br/>
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>
                </form>
              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body" id="print_area">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover" width="100%">
                <thead>
                <tr>
                  <th width='5%'>Sl No.</th>
                  <th width='20%'>Name of Student</th>
                  <th width='15%'>Course Name</th>
                  <th width='10%'>Payment</th>
                  <th width='10%'>Type</th>
                  <th width='10%'>Payment for</th>
                  <th width='10%'>Payment Date</th>
                  <th width='15%'>Transaction No.</th>
                  <th width='5%'>Status</th>
                </tr>
                </thead>
                <tbody>
                @php  $count=1; @endphp
                @forelse($payments as $payment)
                  <tr>
                    <td width='5%'>{{$count}}</td>
                    <td width='20%'>{{$payment->pay->name_of_student}}</td>
                    <td width='15%'>{{$payment->pay->category->name}}</td>
                    <td width='10%'>{{$payment->pay_amount}}</td>
                    <td width='10%'>{{$payment->type}}</td>
                    <td width='10%'>{{$payment->pay_for}}</td>
                    <td width='10%'>{{\Carbon\Carbon::parse($payment->created_at)->format('d-m-Y')}}</td>
                    <td width='15%'>{{@$payment->transaction_no}}</td>
                    <td width='5%'>{{$payment->is_paid ==1? 'paid':'not paid'}}</td>
                  </tr>
                   @php $count++; @endphp
                @empty
                  <tr><td colspan="9" style="text-align: center;">No Data Availbale</td></tr>
                @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $payments->links() }}</div>

            </div>


          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>

  @endsection
