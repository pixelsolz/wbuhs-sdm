@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Synopsis Title
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> User List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit</li>
      </ol>--}}
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Sysnopsis Edit Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{route('admin.student-synopsis-title-update',['id'=>$synopsisDetail->id])}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('st_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="st_name">Name<span class="required_field">*</span></label>
                           <input type="text" name="st_name" class="form-control" value="{{old('st_name') ?old('st_name'):$synopsisDetail->st_name }}">
                           @if($errors->has('st_name'))
                           <span class="error">{{$errors->first('st_name')}}</span>
                           @endif
                        </div>
                     </div>
                  
                     <div class="form-group">
                        <label for="exampleInputFile">File</label>
                        <input type="file" id="exampleInputFile" name="st_pdf">
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection
