@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Speciality List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>

              <div class="form-group row">
                <form action="{{url('admin/category/user')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-4">
                    <select class="form-control" name="search_by_category">
                      <option value="">All</option>
                      @foreach($categories as $category)
                        <option value="{{$category->id}}" @if($request->search_by_category && $request->search_by_category==$category->id) selected @endif>{{$category->name}}</option>
                      @endforeach

                    </select>
                  </div>
                  <div class="col-xs-4">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, email or mobile" value="{{($request->input_search? $request->input_search:'' )}}">
                  </div>

                  <div class="col-xs-4">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

                </form>
              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile no.</th>
                  <th>Speciality</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($users as $user)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$user->full_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->userDetail->mobile }}</td>
                    <td>{{implode(',',$user->category()->pluck('name')->toArray())}}</td>

                  </tr>
                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="5" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $users->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


  </div>

  @endsection
