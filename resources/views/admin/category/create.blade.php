@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create Speciality
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> User List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create</li>
      </ol>--}}
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Speciality Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/category')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">

                     <div class="form-group row @if($errors->has('parent'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="name">Speciality<span class="required_field">*</span></label>
                           <select class="form-control" name="parent">
                              <option value="">select</option>
                              <option value="0">Root</option>
                              @foreach($categories as $category)
                              <option value="{{$category['id']}}">{!! $category['name'] !!}</option>
                              @endforeach
                           </select>
                           @if($errors->has('parent'))
                           <span class="error">{{$errors->first('parent')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="name">Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{old('name')}}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection