@extends('admin.admin-layout')
@section('title', $title)
@section('content')
@inject('usrCont', 'App\Http\Controllers\Admin\UserController')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Thesis List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

            </div>
            <!-- /.box-header -->

            <div class="box-body">
               <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Reg. No.</th>
                  <th>Reg. Year</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <!--<th>Email</th>
                   <th>Guide Name</th>
                  <th>Guide Desgination</th> -->
                  @if(\Auth::guard('admin')->user()->is_admin ==1)
                  <th>Action</th>
                  @endif
                  <th>Document</th>
                  <th>Status</th>
                   @if(\Auth::guard('admin')->user()->is_admin ==1)
                  <th>Aceepted</th>
                  <th>Grade</th>
                  @endif
                  @if(\Auth::guard('admin')->user()->is_admin !=1)
                  <th>Change Status</th>
                  @endif
                  <th>Created At</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($thesisData as $thesis)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$thesis->name_of_student}}</td>
                    <td>{{$thesis->registration_no}}</td>
                    <td>{{$thesis->registration_year }}</td>
                    <td>{{$thesis->institute_name}}</td>
                    <td>{{$thesis->mobile_landline}}</td>
                    @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <td>
                      dcdcc
                      {{$thesis->getReviewers.'cvdcd'}}
                     <span class="badge bg-green" data-tog="tooltip" title="{{$thesis->transaction()->getEmailid()}}">{{count($thesis->transaction)}}</span>&nbsp;
                     <a href="#" data-toggle="modal" data-target="#modal-default" data-tog="tooltip" title="Send email" id="synopsis-modal" onclick="setSynopsiId('{{$thesis->id}}','{{$thesis->transaction}}','{{$thesis->category->name}}')" data-synopsis="{{$thesis->id}}"><i class="fa fa-send-o"></i></a>
                    </td>
                    @endif
                    <td><a href="{{url('admin/student-synopsis/view',['syn_id'=>$thesis->id])}}" target="_blank" data-tog="tooltip" title="View Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
                    <td>{{$thesis->status_text}} &nbsp;
                   {{--  @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <a href="#" data-toggle="modal" data-target="#status-modal" data-tog="tooltip" title="Change Status" onclick="setSynopsisStatus('{{$thesis->id}}','{{$thesis->status}}','{{$thesis->reason_reject}}')"><i class="fa fa-hand-pointer-o"></i></a>

                    @endif--}}
                    &nbsp;
                    <a href="#" class="showFase" data-tog="tooltip" title="Life cycle" data-id="{{$thesis->id}}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>

                    <a href="#" data-id="{{$thesis->id}}" class="hideFase" title="Hide" style="display: none;"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>

                    </td>
                    @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <td><span class="badge bg-green" data-tog="tooltip" title="{{$thesis->transaction()->getFaculty(2)}}">{{$thesis->transaction()->where('status',2)->count()}}</span>&nbsp; <span class="badge bg-red" data-tog="tooltip" title="{{$thesis->transaction()->getFaculty(1)}}">{{$thesis->transaction()->where('status',1)->count()}}</span></td>
                    <td>
                    @php $subUrl=url('admin/faculty/submitted-status') @endphp
                    <a href="#" onclick="getFacultyStatus('{{$subUrl}}', '{{$thesis->id}}')" data-toggle="modal" data-target="#grade-modal" data-tog="tooltip" title="See detail"><i class="fa fa-hand-pointer-o"></i></a></td>
                    @endif
                    @if(\Auth::guard('admin')->user()->is_admin !=1)
                    <td>
                    @php
                      $transdata = $thesis->transaction()->where('to_user_id', \Auth::guard('admin')->user()->id)->first();
                    @endphp
                   <input id="toggle-one" type="checkbox" @if($transdata->status != 1)checked @endif data-url="{{route('admin.reviewer-accept')}}" data-thesis_id="{{$thesis->id}}">
                    @if($transdata->status == 2)

                    <a href="{{url('admin/student-result/add',$thesis->id)}}" class="btn btn-primary btn-xs">set grade</a>
                    @endif</td>
                    @endif
                    <td>{{\Carbon\Carbon::parse($thesis->created_at)->format('d/m/Y')}}</td>
                  </tr>

                  <tr class="show-tr"  data-id="{{$thesis->id}}" style="display: none;">

                    <td colspan="{{\Auth::guard('admin')->user()->is_admin ==1 ? 12: 11}}">
                    <div class="time-line-div admin-timeline">
                              <ul class="time-line-ul">
                              @foreach($usrCont::thesisList($thesis->id) as $cycle)
                                <li class="time-line-li @if($cycle->id <= $thesis->status) active-li @endif">
                                  <span class="time-line-span">{{$cycle->label}}</span><span class="circle"></span>
                                </li>
                              @endforeach
                                 <!-- <li class="time-line-li active-li">
                                  <span class="time-line-span">Assigend to Reviewer</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li">
                                  <span class="time-line-span">Reviewer Accept</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li">
                                  <span class="time-line-span">Reviewer Submit</span><span class="circle"></span>
                                </li>
                                 <li class="time-line-li">
                                  <span class="time-line-span">Complete Or Resubmit</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li">
                                  <span class="time-line-span">Reassigned</span><span class="circle"></span>
                                </li>
                                 <li class="time-line-li">
                                  <span class="time-line-span">jnjf</span><span class="circle"></span>
                                </li> -->
                              </ul>
                              </div>
                            </td>

                  </tr>

                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="{{\Auth::guard('admin')->user()->is_admin ==1 ? 12: 11}}" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $thesisData->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

      <!-- Email send Modal -->
      <div class="modal fade" id="modal-default">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Email</h4>
              </div>
              <div class="modal-body">

                <table class="table">
                    <tbody id="send_email_tbody">
                    {{--@if(count($thesises))
                       @foreach(@$thesis->category->user as $user)
                         <tr>
                           <td>{{@$user->full_name}}</td>
                           <td><input type="checkbox" name="send_email[]" id="send_email" value="{{$user->id}}"></td>

                         </tr>

                       @endforeach
                       @endif--}}
                       <input type="hidden" name="synopsis_id">
                    </tbody>
                 </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                @php $route= url('admin/send-email') @endphp
                <button type="button" class="btn btn-primary" onclick="sendEmail('{{$route}}','{{csrf_token()}}','reviewer')">send</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- Status Change Modal-->

        <div class="modal fade" id="status-modal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Set status</h4>
              </div>
              <div class="modal-body">
              <form action="{{route('student.synopsis.status-change')}}" id="synopsis_status">
              @csrf
                <input type="hidden" name="synop_id" id="synop_id">
               <div class="form-group radio">
                  <label><input type="radio" value="1" name="status" >Processed</label>
                </div>
                <div class="form-group radio">
                  <label><input type="radio" value="2" name="status" >Accept</label>
                </div>
                <div class="form-group radio">
                  <label><input type="radio" value="3" name="status">Not accept</label>
                </div>
                <div class="form-group" id="not-accept-reason" style="display: none;">
                <label >Reason:</label>
                  <textarea name="reason_reject" class="form-control"></textarea>
                </div>
              </form>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>



        <!-- Grade detail Modal-->

        <div class="modal fade" id="grade-modal">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Grade Detail</h4>
              </div>
              <div class="modal-body">
              <table class="table">
              <thead>
                <tr>
                  <th>Faculty name</th>
                  <th>Submitted</th>
                  <th>Grade</th>
                </tr>
              </thead>
              <tbody id="gradeModalTbody">

              </tbody>
            </table>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

  </div>

  @endsection
