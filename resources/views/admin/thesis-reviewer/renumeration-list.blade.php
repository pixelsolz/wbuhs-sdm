@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Renumeration List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>


            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Institute name</th>
                  <th>Regitration Year</th>
                  <th>Course Name</th>
                  <th>Payment Status</th>
                  <th>Action</th>
                  <th>Submit Date</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($thesisreviews as $result)
                  <tr>

                    <td>{{@$count}}</td>
                    <td>{{@$result->thesis->name_of_student}}</td>
                    <td>{{@$result->thesis->institute_name}}</td>
                    <td>{{@$result->thesis->registration_year}}</td>
                    <td>{{@$result->thesis->category->name}}</td>
                    <td>{{@$result->paymentHistory ? $result->paymentHistory->status_text:''}}</td>
                    <td>
                    @if(!@$result->paymentHistory)<a href="{{route('renumeration.show-form',$result->id)}}" data-tog="tooltip" data-original-title="Submit renumeration"><i class="fa fa-hand-pointer-o"></i></a>@endif</td>
                    <td>{{(@$result->paymentHistory && @$result->paymentHistory->created_at)?\Carbon\Carbon::parse($result->paymentHistory->created_at)->format('d-m-Y'):''}}</td>
                  </tr>
                   @php $count++; @endphp
                @empty
                  <tr><td colspan="8" style="text-align: center;">No Data Availbale</td></tr>
                @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $thesisreviews->links() }}</div>

            </div>


          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>

  @endsection
