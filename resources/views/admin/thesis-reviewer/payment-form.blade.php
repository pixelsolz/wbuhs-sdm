@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<style type="text/css">
.bankDetails h3{
   color: #1073bf;
    margin: 0 0 20px 0;
    font-size: 25px;
    line-height: 32px;
    font-weight: 600;
    letter-spacing: 0.68px;
}
.bankDetails label{
   font-size: 18px;
    line-height: 24px;
    margin: 0 0 5px 0;
    color: #107a7d;
    font-weight: 600;
    width: 18%;
    vertical-align: top;
}
.bankDetails input{
   width: 40%;
    padding: 10px;
    height: 40px;
    border: none;
    border-bottom: 1px solid #107a7d;
    display: inline-block;
}
.paymentInnerSec{
   padding: 40px 0 0 0;
}
.paymentInner h3{
   background: #1073bf;
    color: #fff;
    margin: 0 0 20px 0;
    padding: 10px;
    font-size: 25px;
    line-height: 32px;
    font-weight: 600;
    letter-spacing: 0.68px;
}
.paymentInner input{
   width: 35%;
    padding: 10px;
    height: 40px;
    border: none;
    border-bottom: 1px solid #107a7d;
    display: inline-block;
}
.paymentInner p{
   font-size: 18px;
   line-height: 26px;
   color: #1b1b1b;
   margin: 0 0 0 0;
   vertical-align: top;
}
.paymentInner input#paydate{
   width: 10%;
}
.rateTable{
   padding: 40px 0 40px 0;
}
.rateTable h5{
   font-size: 20px;
    line-height: 26px;
    color: #1b1b1b;
    margin: 0 0 20px 0;
    font-weight: 600;
}
.rateTable table{
   margin-bottom: 40px;
}
.rateTable table tr td{
   border: 1px solid #0a5a5d;
   font-size: 18px;
   line-height: 24px;
}
.rateTable table tr th{
   background: #088b90;
   border: 1px solid #0a5a5d;
   font-size: 18px;
   line-height: 24px;
   color: #fff;
   font-weight: 400;
}
.rateTable p{
   font-size: 18px;
    line-height: 24px;
    color: #1b1b1b;
    margin: 0 0 10px 0;
}
.rateTable input{
    padding: 10px;
    height: 40px;
    border: none;
    border-bottom: 1px solid #107a7d;
    display: inline-block;
    width: 96%;
}
.rateTable h6{
   font-size: 18px;
    line-height: 24px;
    color: #1b1b1b;
    margin: 0 0 20px 0;
    text-align: center;
}
.kbw-signature { width: 400px; height: 100px; }
</style>


<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Renumeration Form
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ route('admin.student-result') }}"><i class="fa fa-dashboard"></i> Renumeration Form</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Renumeration Form</li>
      </ol>--}}
   </section>
   <!-- Main content -->
   <section class="content">

     <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="box box-primary">
               <div class="box-header">
                @if(Session::has('same_year'))
                <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('same_year')}}
                </div>
              @elseif(Session::has('notexist_year'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('notexist_year')}}
              </div>
              @elseif(Session::has('year_error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('year_error')}}
              </div>
              @endif
              </div>
               <form action="{{url('admin/renumeration/store')}}" id="renumation_form" enctype="multipart/form-data" method="post">
               @csrf
               <input type="hidden" name="review_id" value="{{$thesisreview->id}}">
                <div class="box-body">

               <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="msFormSection">
                  <div class="paymentInner">
                     <p>Prof. (Dr.) Sri./ Smt./ Ms <input type="text" name="" value="{{$user->full_name}}"> My remuneration as Paper-Setter/Moderator/ Scrutiner/ Head Examiner/ Tabulator/ Observer/ Adjudicator in connection with the <input type="text" name="paper_setter"> Examination,<input type="text" value="{{--$thesisYear--}}{{'2017-2020'}}" name="examin_year" id="paydate" readonly></p>
                  </div>
                  <div class="rateTable">
                     <h5>As per remuneration statement attached here in below</h5>
                     <div class="table-responsive">
                        <table class="table table-bordered">
                           <tr>
                              <th>Rate per Unit Rs.</th>
                             <!--  <th>Bonus Amount</th> -->
                              <th>No .of Units</th>
                              <th>Review Amount</th>
                           </tr>
                           <tr>
                              <td>Rs. 1000.00</td>
                              {{--<td>{{sprintf('%0.2f', ($tot_bonusAmount))}}</td>--}}
                              <td>{{1}}</td>
                              <td>Rs. {{sprintf('%0.2f', (1000.00 *1)) }}</td>
                           </tr>
                        </table>
                        @php
                          $total_amount = ((1000.00 *1))
                        @endphp
                     </div>
                     <input type="hidden" name="review_amount" value="{{1000.00 }}">
                     <input type="hidden" name="bonus_amount" value="{{$tot_bonusAmount}}">
                     <p>(Rs <input type="text" name="total_amount" value="{{sprintf('%0.2f', $total_amount)}}"> )</p>
                     {{--@if($errors->has('total_amount'))
                           <span class="error">{{$errors->first('total_amount')}}</span>
                      @endif--}}
                     <h6>(as per University's rules)</h6>
                  </div>
                  <div class="evaluatorInfo">
                     <div class="formgroup">
                        <label>Name (in Block Letters)</label>
                        <input type="text" name="" value="{{$user->full_name}}">
                     </div>
                     <!-- <div class="formgroup">
                        <label>Signature in full</label>
                        <div class="signatureInner"></div>
                     </div> -->
                     <div class="formgroup">
                        <label>Official Designation</label>
                        <input type="text" name="">
                     </div>
                     <div class="formgroup">
                        <label>Address</label>
                        <textarea rows="4">{{@$user->userDetail->address}}</textarea>
                     </div>
                  </div>
                  <div class="bankDetails">
                     <h3>Bank'Details: (in Block Letters)</h3>
                     <div class="formgroup">
                        <label>1. Name of the Account holder</label>
                        <input type="text" name="account_holder_name" value="{{@$user->facultyPayment->bank_detail['account_holder_name']}}">
                        {{--@if($errors->has('account_holder_name'))
                           <span class="error">{{$errors->first('account_holder_name')}}</span>
                        @endif--}}
                     </div>
                     <div class="formgroup">
                        <label>2. Name of the bank &amp; Branch</label>
                        <input type="text" name="bank_name" value="{{@$user->facultyPayment->bank_detail['bank_name']}}">
                        {{--@if($errors->has('bank_name'))
                           <span class="error">{{$errors->first('bank_name')}}</span>
                        @endif--}}
                     </div>
                     <div class="formgroup">
                        <label>3. Account Number</label>
                        <input type="text" name="account_no" value="{{@$user->facultyPayment->bank_detail['account_no']}}">
                        {{--@if($errors->has('account_no'))
                           <span class="error">{{$errors->first('account_no')}}</span>
                        @endif--}}
                     </div>
                     <div class="formgroup">
                        <label>IFSCCode</label>
                        <input type="text" name="ifs_code" value="{{@$user->facultyPayment->bank_detail['ifs_code']}}">
                          <span class="error" id="ifsc_error"></span>
                         {{--@if($errors->has('ifs_code'))
                           <span class="error">{{$errors->first('ifs_code')}}</span>
                         @endif--}}
                     </div>
                  </div>
                  <div class="signatureOuter">
                   <h5 class="evaultor-sign"><a href="javascript:void(0)" onclick="uploadSignature('upload_signature', 'evaultor-sign')">(click to upload)</a> Signature of the evaluator</h5>
                    <h5 class="upload_signature" style="display: none;"><a href="javascript:void(0)" onclick="uploadSignature('evaultor-sign', 'upload_signature')">(click to Signature)</a> Upload Signature</h5>

                    <div id="evaultor-sign" class="evaultor-sign">
                     <div class="signatureInner" id="sig">

                     </div>
                     <p style="clear: both;" class="signature-field">

                     <a href="#" class="btn btn-secondary saveImage-signature" id="saveImage" data-showid="showsign" data-signfield="sig" data-url="{{url('admin/signature-save')}}" onclick="signatureUpload('{{csrf_token()}}', event, 'evaluator_signature','sig')">Save Signature</a>

                      <a href="#" class="btn btn-secondary clear-signature" id="clear" data-showid="showsign" data-signfield="sig">Clear</a>

                   </p>
                   </div>

                   <div id="upload_signature" style="display: none;" class="upload_signature">

                     <input type="file" name="signature_upload1">


                   </div>
                   <!-- <img src="" id="showsign" class="signature-field"> -->
                  </div>
                   <input type="hidden" name="evaluator_signature" id="evaluator_signature">

                 <!--  <div class="paymentInner paymentInnerSec">
                     <h3>FOR USE IN THE AUDIT AND ACCOUNTS OFFICE</h3>
                     <p>Checked and passed for Rupees (Rs <input type="text" name=""> ) Rupees <input type="text" name=""> and <input type="text" name=""> P.only.</p>
                  </div>
                  <div class="signatureOuter">
                     <div class="signatureInner"></div>
                     <h5>Finance Officer</h5>
                  </div> -->
               </div>
                </div>
                  </div>
                  <div class="box-footer">

                   {{-- @php $checkSubmit = \Auth::guard('admin')->user()->facultyPayment()->where('is_submitted',1)->first();
                                        $checkpaid = \Auth::guard('admin')->user()->facultyPayment->paymentHistory()->whereYear('created_at', \Carbon\Carbon::now()->format('Y'))->count();
                                         @endphp--}}

                                         <button type="submit" class="btn btn-primary submit-result-btn"  disabled onclick="renumerationFormSubmit(event)">Submit</button>

                  </div>
                  </form>
               </div>

               </div>
            </div>
   </section>

</div>
@endsection
