@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Student Dessertation Edit
      </h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Student Dessertation Edit</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/student-dessertation-store/'.$studentDessertationDetails->id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
               
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="name">Student Name<span class="required_field">*</span></label>
                           <input type="text" name="name" class="form-control" value="{{old('name') ?old('name'):$studentDessertationDetails->name }}">
                           @if($errors->has('name'))
                           <span class="error">{{$errors->first('name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('registration_no'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="registration_no">Unique Id<span class="required_field">*</span></label>
                           <input type="text" name="registration_no" class="form-control" value="{{old('registration_no') ? old('registration_no'): $studentDessertationDetails->registration_no}}">
                           @if($errors->has('registration_no'))
                           <span class="error">{{$errors->first('registration_no')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('course'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="course">Course<span class="required_field">*</span></label>
                           <input type="text" name="course" class="form-control" value="{{old('course') ? old('course'): $studentDessertationDetails->course}}">
                           @if($errors->has('course'))
                           <span class="error">{{$errors->first('course')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('institute'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="institute">Institute<span class="required_field">*</span></label>
                           <input type="text" name="institute" class="form-control" value="{{old('institute') ? old('institute'): $studentDessertationDetails->institute}}">
                           @if($errors->has('institute'))
                           <span class="error">{{$errors->first('institute')}}</span>
                           @endif
                        </div>
                     </div>

                      <div class="form-group row @if($errors->has('phone'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="phone">Phone<span class="required_field">*</span></label>
                           <input type="number" readonly="" name="phone" class="form-control" value="{{old('phone') ? old('phone'): $studentDessertationDetails->phone}}">
                           @if($errors->has('phone'))
                           <span class="error">{{$errors->first('phone')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" readonly="" name="email" class="form-control" value="{{old('email') ? old('email'): $studentDessertationDetails->email}}">
                           @if($errors->has('email'))
                           <span class="error">{{$errors->first('email')}}</span>
                           @endif
                        </div>
                     </div>
                    
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection
