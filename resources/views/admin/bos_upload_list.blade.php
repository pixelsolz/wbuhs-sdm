@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BOS Members Uploaded PDF List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>BOS. Name</th>
                  <th>Pdf</th>
                  <th>Created date</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($bosPdfs as $pdf)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$pdf->user->full_name}}</td>
                    <td><a href="{{url('public/bos_pdfs/'. $pdf->pdf_file)}}" download>{{$pdf->pdf_file}}</a></td>
                    <td>{{\Carbon\Carbon::parse($pdf->created_at)->format('Y-m-d') }}</td>
                  </tr>
                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="4" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
              <div>{{ $bosPdfs->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
