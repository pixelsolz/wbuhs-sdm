<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$title}}</title>



    <!-- <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"> -->

   <link rel="stylesheet" href="{{asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link href="{{ asset('public/css/login-style.css')}}" rel="stylesheet">

    <style type="text/css">
      .error{
          color: red;
          font-size: 14px;
      }
    </style>

  </head>

  <body>

    <div class="body-wrapper">

        <div class="login-sec">

            <div class="logo-sec">

                <a class="logo" href="#" title="STS"><img src="{{ asset('public/img/img_logo.png')}}" alt="STS"></a>

            </div>

            <div class="form-sec">

              <div>

                <h2 class="title1">Reset Password</h2>
                @if(session()->has('msg'))
                    <div class="alert alert-{{ session('msg')['status'] }}">
                    {!! session('msg')['msgs'] !!}
                    </div>
                @endif

                <form method="post" action="{{url('admin/reset-password')}}">
                       @csrf
                       <input type="hidden" name="email" value="{{$email}}">
                       <input type="hidden" name="token" value="{{$token}}">

                    <div class="form-group">

                      <input class="form-control pass" type="password" name="password" value="" placeholder="Password" required>
                        @if($errors->first('password'))
                        <span class="error">{{$errors->first('password')}}</span>
                        @endif
                    </div>

                    <div class="form-group">

                      <input class="form-control pass" type="password" name="password_confirmation" value="" placeholder="" required>
                        @if($errors->first('password_confirmation'))
                        <span class="error">{{$errors->first('password_confirmation')}}</span>
                        @endif
                    </div>

                    <div class="form-group">

                      <input class="form-control" type="submit" value="Submit" />

                    </div>

                </form>

              </div>

            </div>

        </div>

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="{{asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>



  </body>

</html>