@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Upload Pdf
      </h1>
      <ol class="breadcrumb">
         <!-- <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> User List</a></li> -->
         <!--  <li><a href="#">Forms</a></li> -->
         <!-- <li class="active"> profile update</li> -->
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Upload Pdf</h3>
                   @if(Session::has('success'))
                  <div class="alert alert-success alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     {{ Session('success')}}
                  </div>
                  @endif
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/bos-pdf/submit')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">

                     <div class="form-group">
                        <label for="exampleInputFile">Upload Pdf</label>
                        <input type="file" id="exampleInputFile" name="pdf_file">
                         @if($errors->has('pdf_file'))
                           <span class="error">{{$errors->first('pdf_file')}}</span>
                         @endif
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>

              <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Pdf</th>
                  <th>Created date</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($bosPdfs as $pdf)
                  <tr>
                    <td>{{$count}}</td>
                    <td><a href="{{url('public/bos_pdfs/'. $pdf->pdf_file)}}" download>{{$pdf->pdf_file}}</a></td>
                    <td>{{\Carbon\Carbon::parse($pdf->created_at)->format('Y-m-d') }}</td>
                  </tr>
                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="4" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
              <div>{{$bosPdfs->links()}}</div>

            </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection