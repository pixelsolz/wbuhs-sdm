<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$title}}</title>



    <!-- <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"> -->

   <link rel="stylesheet" href="{{asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link href="{{ asset('public/css/login-style.css')}}" rel="stylesheet">



  </head>

  <body>

    <div class="body-wrapper">

        <div class="login-sec">

            <div class="logo-sec">

                <a class="logo" href="#" title="STS"><img src="{{ asset('public/img/img_logo.png')}}" alt="STS"></a>

            </div>

            <div class="form-sec">

              <div>

                <h2 class="title1">Forgot Password</h2>
                @if(session()->has('msg'))
                    <div class="alert alert-{{ session('msg')['status'] }}">
                    {!! session('msg')['msgs'] !!}
                    </div>
                @endif
                @if(session()->has('error'))
                  <div class="alert alert-danger">
                    {!! session('error')['msgs'] !!}
                    </div>
                @endif

                <form method="post" action="{{url('admin/forget-password')}}">
                       @csrf
                    <div class="form-group">

                      <input class="form-control user" type="email" name="email" value="{{ old('email') }}"  placeholder="Email" required />

                    </div>


                    <div class="form-group">

                      <input class="form-control" type="submit" value="Submit" />

                    </div>

                    <a class="btn btn-link" href="{{url('admin/login')}}">Login</a>

                </form>

              </div>

            </div>

        </div>

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="{{asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>



  </body>

</html>