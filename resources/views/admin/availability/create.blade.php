@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
        Update Availability
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ url('admin/leave') }}"><i class="fa fa-dashboard"></i>Availability</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active">Update Availability</li>
      </ol>--}}
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Update Availability</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/leave')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">

                     <div class="form-group row @if($errors->has('dateValue'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="reservationtime">Date<span class="required_field">*</span></label>
                           <input type="text" name="dateValue" class="form-control" value="{{old('dateValue')}}" id="reservationtime">
                           @if($errors->has('dateValue'))
                           <span class="error">{{$errors->first('dateValue')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('reason'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="reason">Reason<span class="required_field">*</span></label>
                           <textarea class="form-control" name="reason">{{old('reason')}}</textarea>
                           @if($errors->has('reason'))
                           <span class="error">{{$errors->first('reason')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection