@extends('admin.admin-layout')
@section('title', $title)

@section('style')
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection
@section('content')



<div class="content-wrapper">
	<section class="content-header">
      <h1>
         Availability
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> Availability</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Calendar</li>
      </ol>--}}
   </section>

    <section class="content">

    	<div class="row">
           <div class="col-md-3">
            <div class="box">
              <div class="box-body">
                 <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Not Available Date</th>
                      <th>Action</th>
                  </tr>
                  </thead>

                  <tbody>
                    @forelse($leaves as $leave)
                     <tr>
                      <td>{{$leave->start_date. ' to '.$leave->end_date}}</td>
                      <td>
                      @if(\Carbon\Carbon::parse($leave->start_date)->format('Y-m-d') >= \Carbon\Carbon::now()->format('Y-m-d'))
                       <a href="{{route('leave.edit',['id'=>$leave->id])}}"><i class="fa fa-pencil-square-o"></i></a>
                      @endif
                      </td>
                    </tr>
                    @empty
                    <tr><td colspan="2"></td></tr>
                    @endforelse
                  </tbody>

                 </table>
                 <div>{{ $leaves->links() }}</div>
              </div>
            </div>
           </div>


    		 <div class="col-md-9">
         <div class="box box-primary">

            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              <a href="{{url('admin/leave/create')}}" class="btn btn-primary">Update Availability</a>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>

            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
    		 </div>
    	</div>
    </section>

</div>


@endsection

