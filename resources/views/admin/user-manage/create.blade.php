@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create User
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> User List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Create</li>
      </ol>--}}
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/user-manage')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('f_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="f_name">First Name<span class="required_field">*</span></label>
                           <input type="text" name="f_name" class="form-control" value="{{old('f_name')}}">
                           @if($errors->has('f_name'))
                           <span class="error">{{$errors->first('f_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('l_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="l_name">Last Name<span class="required_field">*</span></label>
                           <input type="text" name="l_name" class="form-control" value="{{old('l_name')}}">
                           @if($errors->has('l_name'))
                           <span class="error">{{$errors->first('l_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" name="email" class="form-control" value="{{old('email')}}">
                           @if($errors->has('email'))
                           <span class="error">{{$errors->first('email')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('mobile'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="mobile">Mobile<span class="required_field">*</span></label>
                           <input type="number" name="mobile" class="form-control" value="{{old('mobile')}}">
                           @if($errors->has('mobile'))
                           <span class="error">{{$errors->first('mobile')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('address'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="address">Address<span class="required_field">*</span></label>
                           <textarea name="address" class="form-control">{{old('address')}}</textarea>
                           @if($errors->has('address'))
                           <span class="error">{{$errors->first('address')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('state_id'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="state_id">State<span class="required_field">*</span></label>
                           <select class="form-control" name="state_id">
                              <option value="">select state</option>
                              @foreach($states as $state)
                              <option value="{{$state->id}}" @if(old('state_id')==$state->id) selected @endif>{{$state->state_name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('state_id'))
                           <span class="error">{{$errors->first('state_id')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('city_id'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="city_id">City<span class="required_field">*</span></label>
                           <input type="text"  class="form-control" name="city_id" value="{{old('city_id')}}">

                           @if($errors->has('city_id'))
                           <span class="error">{{$errors->first('city_id')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('gender'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="gender">Gender<span class="required_field">*</span></label>
                           <select class="form-control" name="gender">
                              <option value="">select</option>
                              <option value="1" @if(old('gender')==1) selected @endif>Male</option>
                              <option value="2" @if(old('gender')==2) selected @endif>Female</option>
                           </select>
                           @if($errors->has('gender'))
                           <span class="error">{{$errors->first('gender')}}</span>
                           @endif
                        </div>
                     </div>

                     <!-- <div class="form-group row @if($errors->has('user_type'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="user_type">User Type<span class="required_field">*</span></label>
                           <select class="form-control" name="user_type">
                              <option value="">select</option>
                              <option value="1" @if(old('user_type')==1) selected @endif>Super Admin</option>
                              <option value="2" @if(old('user_type')==2) selected @endif>Admin</option>
                              <option value="3" @if(old('user_type')==3) selected @endif>Student</option>
                           </select>
                           @if($errors->has('user_type'))
                           <span class="error">{{$errors->first('user_type')}}</span>
                           @endif
                        </div>
                     </div> -->

                     <div class="form-group row">
                        <div class="col-lg-9 col-xs-12">
                           <label>Role<span class="required_field">*</span></label>
                            @if($errors->has('roles'))
                            <br>
                           <span class="error">{{$errors->first('roles')}}</span>
                           @endif

                           <div class="form-group">
                              @foreach($roles as $role)
                              @if($role->slug !='superadmin')
                              <input type="checkbox" class="minimal" name="roles[]" id="roles" value="{{$role->id}}" onclick="showCategory('{{$role->slug}}')" @if(old('roles') && in_array($role->id, old('roles'))) checked @endif>&nbsp;
                              <label>{{$role->name}}</label>&nbsp;
                              @endif
                              @endforeach
                           </div>

                        </div>
                     </div>
                     <div class="form-group row forAdjudicator" @if(old('roles') && in_array(2, old('roles'))) @else style="display: none;" @endif >
                        <div class="col-lg-9 col-xs-12">
                           <label for="designation">Designation<span class="required_field">*</span></label>
                           <select class="form-control" name="designation">
                              <option value="">select</option>
                              <option value="professor">Professor</option>
                              <option value="associate-professor">Associate Professor</option>
                              <option value="assistant-professor">Assistant Professor</option>
                           </select>
                        </div>
                     </div>

                     <div class="form-group row forAdjudicator" @if(old('roles') && in_array(2, old('roles'))) @else style="display: none;" @endif >
                        <div class="col-lg-9 col-xs-12">
                           <label for="designation">Designation<span class="required_field">*</span></label>
                           <select class="form-control" name="designation">
                              <option value="">select</option>
                              <option value="professor">Professor</option>
                              <option value="associate-professor">Associate Professor</option>
                              <option value="assistant-professor">Assistant Professor</option>
                           </select>
                        </div>
                     </div>

                     <div class="form-group row"  id="category-div" @if(old('roles') && in_array(2, old('roles'))) @else style="display: none;" @endif >
                        <div class="col-lg-9 col-xs-12">
                           <label for="user_type">Category<span class="required_field">*</span></label>
                           <select class="form-control" name="category[]" multiple="true">
                              @foreach($categories as $category)
                              <option value="{{$category['id']}}">{!! $category['name']!!}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('college_name'))has-error @endif" style="display: none;" id="user_college_name">
                        <div class="col-lg-9 col-xs-12">
                           <label for="college_name">College Name<span class="required_field">*</span> <a href="javascript:void(0)" id="show_other_college">Show other</a> <a href="javascript:void(0)" style="display: none;" id="hide_other_college">Hide other</a></label>
                            <select class="form-control" name="college_name" id="college_name">
                              <option value="">Select College Name</option>
                              @foreach($colleges as $college)
                              <option value="{{$college['id']}}">{!! $college['college_name']!!}</option>
                              @endforeach
                           </select>
                           <textarea name="other_college_name" id="other_college_name" class="form-control" style="display: none;">{{old('other_college_name')}}</textarea>
                           @if($errors->has('other_college_name'))
                           <span class="error">{{$errors->first('college_name')}}</span>
                           @endif
                        </div>

                     </div>
                     <div class="form-group row" style="display: none;" id="select_state_div">
                        <div class="col-lg-9 col-xs-12">
                           <label for="college_name">Select State<span class="required_field">*</span></label>
                            <select class="form-control" name="other_state" id="other_state">
                              <option value="">Select State Name</option>
                              @foreach($states as $state)
                              <option value="{{$state['id']}}">{!! $state['state_name']!!}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>

                    {{-- <div id="bankinformation" @if(old('roles') && in_array(2, old('roles'))) @else style="display: none;" @endif >
                        <div class="form-group row @if($errors->has('account_holder_name'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="account_holder_name">Account Holder name<span class="required_field">*</span></label>
                              <input type="text" name="account_holder_name" class="form-control" value="{{old('account_holder_name')}}">
                              @if($errors->has('account_holder_name'))
                              <span class="error">{{$errors->first('account_holder_name')}}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row @if($errors->has('account_no'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="account_no">Account No.<span class="required_field">*</span></label>
                              <input type="number" name="account_no" class="form-control" value="{{old('account_no')}}">
                              @if($errors->has('account_no'))
                              <span class="error">{{$errors->first('account_no')}}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row @if($errors->has('bank_name'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="bank_name">Bank Name<span class="required_field">*</span></label>
                              <input type="text" name="bank_name" class="form-control" value="{{old('bank_name')}}">
                              @if($errors->has('bank_name'))
                              <span class="error">{{$errors->first('bank_name')}}</span>
                              @endif
                           </div>
                        </div>

                         <div class="form-group row @if($errors->has('ifs_code'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="ifs_code">IFSCCode<span class="required_field">*</span></label>
                              <input type="text" name="ifs_code" class="form-control" value="{{old('ifs_code')}}">
                              @if($errors->has('ifs_code'))
                              <span class="error">{{$errors->first('ifs_code')}}</span>
                              @endif
                           </div>
                        </div>

                     </div>--}}

                    <!--  <div class="form-group row @if($errors->has('role'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="role">Role<span class="required_field">*</span></label>
                           <select class="form-control" name="role">
                              <option value="">select</option>
                              @foreach($roles as $role)
                              <option value="{{$role->id}}" @if(old('role')==$role->id) selected @endif>{{$role->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('role'))
                           <span class="error">{{$errors->first('role')}}</span>
                           @endif
                        </div>
                     </div> -->
                     <div class="form-group">
                        <label for="exampleInputFile">Profile Image</label>
                        <input type="file" id="exampleInputFile" name="profile_image">
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection