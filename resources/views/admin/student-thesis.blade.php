@extends('admin.admin-layout')
@section('title', $title)
@section('content')
@inject('usrCont', 'App\Http\Controllers\Admin\UserController')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dissertation
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>
              @if(@\Auth::guard('admin')->user()->is_admin ==1)
              <form action="{{route('admin.student-thesis')}}" method="get">
              <div class="form-group row">

                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-2">
                    <select class="form-control" name="search_by_status">
                      <option value="">All</option>
                      <option value="1" @if($request->search_by_status && $request->search_by_status ==1) selected @endif>Dissertation Submit</option>
                      <option value="2" @if($request->search_by_status && $request->search_by_status ==2) selected @endif>Assigend to Adjudicator</option>
                      <option value="3" @if($request->search_by_status && $request->search_by_status ==3) selected @endif>Accepted</option>
                      <option value="4" @if($request->search_by_status && $request->search_by_status ==4) selected @endif>Send for Revision</option>
                      <option value="5" @if($request->search_by_status && $request->search_by_status ==5) selected @endif>Resubmitted</option>
                      <option value="6" @if($request->search_by_status && $request->search_by_status ==6) selected @endif>Reasigned to Adjudicator</option>
                    </select>
                  </div>
                  @php $catUrl = url('admin/get-child/category'); @endphp
                  <div class="col-xs-2" id="parent_div_cat">
                    <select class="form-control" name="category" onchange="getCategories(this.value, '{{$catUrl}}')">
                        <option value="">All</option>
                        @foreach($parent_cats as $parent)
                          <option value="{{$parent->id}}" @if($request->category &&  $request->category==$parent->id) selected @endif >{{$parent->name}}</option>
                        @endforeach
                      {{--@foreach($categories as $category)
                        <option value="{{$category['id']}}" @if($request->category==$category['id'])? selected @endif >{!! $category['name']!!}</option>
                      @endforeach--}}
                    </select>
                  </div>
                  <div class="col-xs-4">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name Reg no. year or mobile" value="{{($request->input_search? $request->input_search:'' )}}">
                  </div>


              </div>

              <div class="form-group row">
                 <div class="col-xs-2">
                   <input class="form-control datepicker" type="text" name="from_date" placeholder="From Date" autocomplete="off" @if(!empty($request->from_date)) value="{{@$request->from_date}}" @endif>
                  </div>

                  <div class="col-xs-2">
                   <input class="form-control datepicker" type="text" name="to_date" placeholder="To Date" autocomplete="off" @if(!empty($request->to_date)) value="{{@$request->to_date}}" @endif>
                  </div>
                  <div class="col-xs-2">
                    <select class="form-control" name="assign_status">
                      <option value="">select status</option>
                      <option value="1" @if($request->assign_status &&  $request->assign_status==1) selected @endif>Assign</option>
                      <option value="2" @if($request->assign_status &&  $request->assign_status==2) selected @endif>Notassign</option>
                    </select>
                  </div>
                  <div class="col-xs-2">
                    <select class="form-control" name="paid_status">
                      <option value="">select payment status</option>
                      <option value="1" @if($request->paid_status &&  $request->paid_status==1) selected @endif>Paid</option>
                      <option value="2" @if($request->paid_status &&  $request->paid_status==2) selected @endif>Not paid</option>
                    </select>
                  </div>
                <div class="col-xs-1">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

              </div>
            </form>
            <div class="form-group row">
               @php $courseId = @$request->child_sub_cat;
               $child_catId = @$request->child_cat;
               $category =@$request->category;
               $from_date =  @$request->from_date;
               $to_date = @$request->to_date;
               @endphp
               @if($child_catId && !$courseId)
               <div class="col-xs-2">
                <a href="{{url('admin/thesis/cat/student-list',$child_catId)}}" class="btn btn-info">Student List</a>
              </div>
              @endif
                @if($courseId)
                <form method="get" action="{{url('admin/pending/adjudicator-list',$courseId)}}">
                  <input type="hidden" name="from_date" value="{{$from_date}}">
                  <input type="hidden" name="to_date" value="{{$to_date}}">
              <div class="col-xs-2">
                <button class="btn btn-info" type="submit">Pending Reviewer</button>
                {{--<a href="{{url('admin/pending/adjudicator-list',$courseId)}}" class="btn btn-info">Pending Reviewer</a>--}}
                  </div>
                </form>
              <div class="col-xs-2">
                <a href="{{url('admin/thesis/student-list',$courseId)}}" class="btn btn-info">Student List</a>
              </div>
               @endif
                <div class="col-xs-2">
                      <a href="{{url('admin/thesis/assign-student/export?child_catId='.$child_catId.'&courseId='.$courseId.'&category='.$category.'&from_date='.$from_date.'&to_date='.$to_date)}}" class="btn btn-info">Assign Students Export</a>
                    </div>
                    <div class="col-xs-3">
                      <a href="{{url('admin/thesis/not-assign-student/export?child_catId='.$child_catId.'&courseId='.$courseId.'&category='.$category.'&from_date='.$from_date.'&to_date='.$to_date)}}" class="btn btn-info">Notassign Students Export</a>
                    </div>
                  <div class="col-xs-2">
                     <a href="{{url('admin/thesis/not-paid-student/export')}}" class="btn btn-info">Not paid Export</a>
                  </div>
            </div>
            @endif

            </div>
            <!-- /.box-header -->

            <div class="box-body">
               <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Reg. No.</th>
                  <th>Reg. Year</th>
                  <th>Proposed Title</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <!--<th>Email</th>
                   <th>Guide Name</th>
                  <th>Guide Desgination</th> -->
                  @if(\Auth::guard('admin')->user()->is_admin ==1)
                  <th>Assign</th>
                  @endif
                  <th>Document</th>
                  @if(\Auth::guard('admin')->user()->is_admin ==1)
                  <th>Re-Submit</th>
                   @endif

                   @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <th>Status</th>
                   <th>Payment Status</th>
                  <th>Accepted / Pending Accepted</th>
                  <th>Evaluate Detail</th>
                  @endif

                  @if(\Auth::guard('admin')->user()->is_admin !=1)
                  <th>Adjudicator Status</th>
                  <th>Assign Date</th>
                  @endif
                  @if(\Auth::guard('admin')->user()->is_admin ==1)
                  <th>Created At</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @php $count=1;$page = !empty($_GET['page'])?$_GET['page']-1:0; @endphp
                @forelse($thesisData as $thesis)
                  <tr>
                    <td>{{$count +($page*10)}}</td>
                    <td>
                      <div>{{$thesis->name_of_student}}</div>
                      <div class="text-info"><small>{{$thesis->category_name}}</small></div>
                    </td>
                    <td>{{$thesis->registration_no}}</td>
                    <td>{{$thesis->registration_year }}</td>
                    <td>{{$thesis->proposed_title_thesis }}</td>
                    <td>{{$thesis->institute_name}}</td>
                    <td>{{$thesis->mobile_landline}}</td>

                    @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <td>

                     {{--<span class="badge bg-green" data-tog="tooltip" title="{{$thesis->transaction()->getEmailid()}}">{{count($thesis->transaction)}}</span>&nbsp;--}}
                      @php $getReviewUrl = url('admin/available-reviewers') @endphp
                     @if($thesis->status < 3)
                     <a href="#" data-toggle="modal" data-target="#modal-default" data-tog="tooltip" title="Assigend to Adjudicator" id="synopsis-modal" onclick="getAvailableReviewer('{{$thesis->id}}','{{$getReviewUrl}}','{{$thesis->category->name}}','{{$thesis->status}}')" data-synopsis="{{$thesis->id}}"><i class="fa fa-send-o"></i></a>
                     @else
                      @php $getAssignedUrl = url('admin/get-assigned-user') @endphp
                     <a href="#" data-toggle="modal" data-target="#modal-default" data-tog="tooltip" title="Assigend to Adjudicator" id="synopsis-modal" onclick="getAssignedUser('thesis','{{$thesis->id}}','{{$getAssignedUrl}}')"><i class="fa fa-send-o"></i></a>
                     @php $getPreviousStateUrl = url('admin/thesis-previous-state',$thesis->id) @endphp
                     &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" title="Go to Assign stage" onclick="setToPreviusState('{{$getPreviousStateUrl}}')"><i class="fa fa-hand-pointer-o"></i></a>
                    @endif

                    </td>
                    @endif

                    <td>
                      <a href="{{url('admin/student-dissertation/viewform',['thesis_id'=>$thesis->id])}}" target="_blank" data-tog="tooltip" title="Dissertation Form Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> ||
                      <a href="{{url('admin/student-dissertation/view',['thesis_id'=>$thesis->id])}}" target="_blank" data-tog="tooltip" title="Dissertation Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                      @if(\Auth::guard('admin')->user()->is_admin ==1)
		                   &nbsp;<a href="javascript:void(0)" data-tog="tooltip"  title="Replace Pdf" onclick="replacePDF('{{$thesis->id}}')"><i class="fa fa-hand-pointer-o"></i></a>
                       @endif
                      {!! $thesis->getOtherPdf($thesis->id) !!}
                    </td>
                    @if(\Auth::guard('admin')->user()->is_admin ==1)
                    @php
                      $statusSetUrl = route('admin.user-resubmit');
                    @endphp
                    <td><input data-id="{{$thesis->id}}" id="reSubmit_{{$thesis->id}}" class="toggle-class" type="checkbox"  onchange="reSubmitPermission('{{$statusSetUrl}}','{{$thesis->id}}')" data-off="No" {{ $thesis->re_submit ? 'checked' : '' }}></td>
                    @endif

                    @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <td>{{$thesis->status_text}} &nbsp;
                   {{--  @if(\Auth::guard('admin')->user()->is_admin ==1)
                    <a href="#" data-toggle="modal" data-target="#status-modal" data-tog="tooltip" title="Change Status" onclick="setSynopsisStatus('{{$thesis->id}}','{{$thesis->status}}','{{$thesis->reason_reject}}')"><i class="fa fa-hand-pointer-o"></i></a>

                    @endif--}}

                    &nbsp;
                    <a href="#" class="showFase" data-tog="tooltip" title="Life cycle" data-id="{{$thesis->id}}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>

                    <a href="#" data-id="{{$thesis->id}}" class="hideFase" title="Hide" style="display: none;"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                    @if($thesis->status ==4)
                      <a href="javascript:void(0)" data-tog="tooltip" title="Reason"><i class="fa fa-hand-pointer-o"></i></a>
                    @endif

                    </td>

                   <td><input class="paid-status" data-id="{{$thesis->id}}" data-on="Paid" data-off="Not Paid" data-url="{{route('admin.payment.status-change')}}" data-type="thesis" type="checkbox" @if($thesis->payment_done ==1) checked disabled @endif  data-toggle="toggle" data-size="small"></td>



                    <td><span class="badge bg-green"  data-toggle-pop="popover" data-placement="top" data-content="{!! $thesis->transaction()->getFaculty(2) !!}" data-html="true"  title="Accepted List">{{$thesis->transaction()->whereIn('status',[2,4])->count()}}</span>&nbsp;
                    <span class="badge bg-red" data-toggle-pop="popover" data-placement="top" data-content="{!! $thesis->transaction()->getFaculty(1) !!}" data-html="true" title="Pending Accepted list">{{$thesis->transaction()->where('status',1)->count()}}</span>
                    </td>
                    <td>
                    @php $subUrl=url('admin/faculty/submitted-status') @endphp
                    <a href="#" onclick="getFacultyStatus('{{$subUrl}}', '{{$thesis->id}}')" data-toggle="modal" data-target="#grade-modal" data-tog="tooltip" title="See detail"><i class="fa fa-hand-pointer-o"></i></a></td>
                    @endif

                    @if(\Auth::guard('admin')->user()->is_admin !=1)
                    <td>
                    @php
                      $transdata = $thesis->transaction()->where('to_user_id', \Auth::guard('admin')->user()->id)->first();
                      $statusSetUrl = route('admin.reviewer-accept');
                    @endphp
                    <select class="form-control" onchange="reviewerAcceptStatus(this.value, '{{$statusSetUrl}}','{{$thesis->id}}')" @if($transdata->status ==2 || $transdata->status ==4) disabled @endif>
                      <option value="1" @if($transdata->status ==1) selected @endif>On Processed</option>
                      <option value="2" @if($transdata->status ==2) selected @endif>Accepted</option>
                      <option value="3" @if($transdata->status ==3) selected @endif>Not Accepted</option>
                      @if($transdata->status ==4)
                      <option value="4" @if($transdata->status ==4) selected @endif>Accepted</option>
                      @endif
                    </select>

                   {{--<input id="toggle-one" type="checkbox" @if($transdata->status != 1)checked @endif data-url="{{route('admin.reviewer-accept')}}" data-thesis_id="{{$thesis->id}}"> --}}

                    @if($transdata->status == 2)

                    <a href="{{url('admin/student-result/add',$thesis->id)}}" class="btn btn-primary btn-xs">Evaluate</a>
                    <!-- Set if have previous command -->
                    @php $history =@$thesis->thesisResubmitHistory->count() ? @$thesis->thesisResubmitHistory()->where('faculty_id', \Auth::guard('admin')->user()->id)->orderBy('id',desc)->first():''; @endphp
                    @if(@$history)

                      &nbsp; <a href="{{url('admin/review-history',@$history->id)}}" class="btn btn-primary btn-xs">Previous command</a>
                    @endif
                    @endif</td>
                    @endif
                    @if(\Auth::guard('admin')->user()->is_admin !=1)
                    <td>{{\Carbon\Carbon::parse(@$transdata->accknowledge_email_date)->format('d/m/Y')}}</td>
                    @else
                    <td>{{\Carbon\Carbon::parse($thesis->created_at)->format('d/m/Y')}}</td>
                    @endif
                  </tr>

                  <tr class="show-tr"  data-id="{{$thesis->id}}" style="display: none;">

                    <td colspan="{{\Auth::guard('admin')->user()->is_admin ==1 ? 13: 12}}">
                    <div class="time-line-div admin-timeline">
                              <div class="legend">
                                  <ul>
                                    <li><span class="notcomplete"></span> Not Complete</li>
                                    <li><span class="complete"></span> Complete</li>
                                  </ul>
                                </div>
                              <ul class="time-line-ul">
                              @if($thesis->is_resubmit)
                                <li class="time-line-li @if($thesis->status >=5 || ($thesis->is_resubmit && $thesis->status ==3)) active-li @endif">
                                  <span class="time-line-span">Dissertation Resubmitted</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li @if($thesis->status >=6 || ($thesis->is_resubmit && $thesis->status ==3)) active-li @endif">
                                  <span class="time-line-span">Reassigend to Adjudicator</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li @if($thesis->status ==3) active-li @endif">
                                  <span class="time-line-span">Accepted</span><span class="circle"></span>
                                </li>
                                @else
                                <li class="time-line-li @if($thesis->status >=1) active-li @endif">
                                  <span class="time-line-span">Dissertation Submitted</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li @if($thesis->status >=2) active-li @endif">
                                  <span class="time-line-span">Assigend to Adjudicator</span><span class="circle"></span>
                                </li>
                                @if($thesis->status !=4)
                                 <li class="time-line-li @if($thesis->status ==3) active-li @endif">
                                  <span class="time-line-span">Accepted</span><span class="circle"></span>
                                </li>
                                @endif
                                @if($thesis->status ==4)
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Send for Revision</span><span class="circle"></span>
                                </li>
                                @endif
                                @endif

                              </ul>
                              </div>
                            </td>

                  </tr>

                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="{{\Auth::guard('admin')->user()->is_admin ==1 ? 12: 11}}" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $thesisData->links() }}</div>

            </div>
            <!-- /.box-body -->

            <div class="overlay" style="display: none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

      <!-- Email send Modal -->
      <div class="modal fade design-modal" id="modal-default">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assign Dissertation to Adjudicator <span id="assign_thesis_title"></span></h4>
                <span id="category"></span>
              </div>
              <div class="modal-body">

                <table class="table">
                    <tbody id="send_email_tbody">

                    </tbody>
                 </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                @php $route= url('admin/send-email') @endphp
                <button type="button" id="reviewer_assign_btn" class="btn btn-primary btn-disable" onclick="sendEmail('{{$route}}','{{csrf_token()}}','reviewer','Dissertation')">Assign &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- Status Change Modal-->

        <div class="modal fade design-modal" id="status-modal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Set status</h4>
              </div>
              <div class="modal-body">
              <form action="{{route('student.synopsis.status-change')}}" id="synopsis_status">
              @csrf
                <input type="hidden" name="synop_id" id="synop_id">
               <div class="form-group radio">
                  <label><input type="radio" value="1" name="status" >Processed</label>
                </div>
                <div class="form-group radio">
                  <label><input type="radio" value="2" name="status" >Accepted</label>
                </div>
                <div class="form-group radio">
                  <label><input type="radio" value="3" name="status">Not Accepted</label>
                </div>
                <div class="form-group" id="not-accept-reason" style="display: none;">
                <label >Reason:</label>
                  <textarea name="reason_reject" class="form-control"></textarea>
                </div>
              </form>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>



        <!-- Grade detail Modal-->

        <div class="modal fade design-modal" id="grade-modal">
          <div class="modal-dialog modal-md">
            <div class="modal-content modal-default">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Evaluate Detail</h4>
              </div>
              <div class="modal-body">
              <table class="table">
              <thead>
                <tr>
                  <th>Faculty name</th>
                  <th>Submitted</th>
                  <th>Status</th>
                  <th>Comment</th>
                  <th>View</th>
                </tr>
              </thead>
              <tbody id="gradeModalTbody">

              </tbody>
            </table>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade design-modal" id="reason-rejected-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Reason for Revision</h4>
                </div>
                <div class="modal-body">
                <p id="reason_content"></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

<!-- Replace pdf-->
        <div class="modal fade design-modal" id="replacePdf-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                <form action="{{url('admin/replace-new/pdf')}}" method="post" enctype='multipart/form-data'>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Upload New PDF</h4>
                </div>
                <div class="modal-body">
                @csrf
                <input type="hidden" name="thesis_id" id="replace_thesis_id">
                <input type="file" name="upload_new_pdf">
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Replace" style="float:left;">

                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
              </div>
            </div>
          </div>

  </div>

  @endsection
