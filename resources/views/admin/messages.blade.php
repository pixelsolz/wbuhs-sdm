<!DOCTYPE html>
<html>
<head>
	<title>Messages</title>
</head>
<body>
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
         	<div class="thankyouOuter">
         		<img src="{{url('public/img/thankcheck.png')}}" alt="thankcheck">
         			<p>You have successfully registered as Adjudicator.<br>Please check your email for login.</p>
         		<a href="{{route('admin.login')}}">Go To Login</a>
         	</div>
         </div>
      </div>
   </div>
</body>
</html>