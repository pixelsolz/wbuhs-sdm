<!DOCTYPE html>
<html>

@include('admin.includes.header')

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
@include('admin.includes.navigation')
@include('admin.includes.sidenav')
@yield('content')
@include('admin.includes.footer')
<div class="control-sidebar-bg"></div>
</div>
@include('admin.includes.footer-js')
</div>
</body>
</html>