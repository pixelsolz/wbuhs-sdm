@extends('admin.admin-layout')
@section('title', $title)
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control Panel</small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> -->
    </section>

    @php
      $dashdataUrl = url('admin/dashboard-data');
    @endphp
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
       <div class="row">
         <div class="col-xs-12">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('msg') !!}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! Session::get('error') !!}
              </div>
               @endif

         </div>
       </div>

      <div class="row">


        @if(Helper::sidenavAccess('student-synopsis'))
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$synopsis->count()}}</h3>

              <p>Total number of Synopses</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-synopsis')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

        <!-- ./col -->
         @if(Helper::sidenavAccess('student-synopsis'))
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$synopsis->where('status',2)->count()}}</h3>

              <p>Completed Synopses</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-synopsis?search=search&search_by_status=2')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

        @if(Helper::sidenavAccess('student-synopsis'))
       <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$synopsis->where('status',3)->count()}}</h3>

              <p>Rejected Synopses</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-synopsis?search=search&search_by_status=3')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

      @if(Helper::sidenavAccess('student-synopsis'))
       <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$synopsis->where('status',4)->count()}}</h3>

              <p>Synopses under review</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-synopsis?search=search&search_by_status=4')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

        @if(Helper::sidenavAccess('student-synopsis'))
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$synopsis->where('status',1)->count()}}</h3>

              <p>Synopses in process</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-synopsis?search=search&search_by_status=1')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

         @if(Helper::sidenavAccess('student-thesis') && \Auth::guard('admin')->user()->is_admin ==1)
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$thesis->count()}}</h3>

              <p>Total number of Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-dissertation')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif


      @if(Helper::sidenavAccess('student-thesis') && \Auth::guard('admin')->user()->is_admin ==2)
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{@$pendingThesis}}</h3>

              <p>Pending Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-dissertation')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

        @if(\Auth::guard('admin')->user()->is_admin ==1)
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{!empty($thesis->count()) ?$thesis->where('status',3)->count():0 }}</h3>

              <p>Completed Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-dissertation?search=search&search_by_status=3')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

      @if(Helper::sidenavAccess('student-thesis') && \Auth::guard('admin')->user()->is_admin ==2)
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{@$completedThesis}}</h3>

              <p>Completed Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-dissertation/completed')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

      @if(Helper::sidenavAccess('student-thesis') && \Auth::guard('admin')->user()->is_admin ==2)
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{@$revissionThesis}}</h3>

              <p>Revision Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-dissertation/revision')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        @endif

        <!-- ./col -->
      @if(\Auth::guard('admin')->user()->is_admin ==1)

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ !empty($thesis->count()) ?$thesis->where('status',4)->count():0}}</h3>

              <p>Dissertations under review</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="javascript:void(0)" onclick="getDashboardData('evaluation_pending','{{$dashdataUrl}}')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        {{--<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ !empty($thesis->count()) ?$thesis->where('status',1)->count():0}}</h3>

              <p>Dissertations procces</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="javascript:void(0)" onclick="getDashboardData('evaluation_pending','{{$dashdataUrl}}')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>--}}


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{@$student->count()}}</h3>

              <p>Total Registered Student</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{url('admin/user-manage?search=search&search_by_role=5')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         @endif

        <!-- ./col -->




        {{--<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$thesis->count() ? $thesis->where('status',1)->count(): 0}}</h3>

              <p>Pending Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('/admin/student-thesis?search=search&search_by_status=1')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>--}}
        @if(\Auth::guard('admin')->user()->is_admin ==1)
        {{--<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{!empty($thesisTranscation->count())?$thesisTranscation->where('status',4)->count():0}}</h3>

              <p>Submitted Dissertations By Adjudicator</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="javascript:void(0)" onclick="getDashboardData('submitted_reviewer','{{$dashdataUrl}}')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>--}}

        {{--<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ !empty($thesisTranscation->where('status',2)->count())? $thesisTranscation->where('status',2)->count():0}}</h3>

              <p>Accepted Adjudicators</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="javascript:void(0)" onclick="getDashboardData('accepted_reviewer','{{$dashdataUrl}}')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>--}}

        {{--<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ !empty($thesisTranscation->where('status',1)->count()) ?$thesisTranscation->where('status',1)->count():0}}</h3>

              <p>Not Accepted Adjudicators</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="javascript:void(0)" onclick="getDashboardData('not_accepted_reviewer','{{$dashdataUrl}}')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> --}}

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ !empty($thesisTranscation->where('status',3)->count()) ?$thesisTranscation->where('status',3)->count():0}}</h3>

              <p>Evaluation Pending - Dissertations</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="javascript:void(0)" onclick="getDashboardData('evaluation_pending','{{$dashdataUrl}}')" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ !empty($thesisTranscation->count()) ?$thesisTranscation->count():0}}</h3>

              <p>Total no of dissertation assign to Adjudicator</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="{{url('admin/student-dissertation')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


        @endif

        <!-- ./col -->
        <!--<div class="col-lg-3 col-xs-6">
          small box
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8">
           <div class="box box-info box-show" style="display: none;">
            <div class="box-header with-border">
              <h3 class="box-title set-heading"></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool hide-div"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Sl No.</th>
                    <th>Reviewer Name</th>
                    <th>Status</th>
                    <th>Assigned Date</th>
                  </tr>
                  </thead>
                  <tbody id="dashboard_tbody">
                  <tr>
                    <td><a href="pages/examples/invoice.html">OR9842</a></td>
                    <td>Call of Duty IV</td>
                    <td><span class="label label-success">Shipped</span></td>
                    <td>
                      <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                    </td>
                  </tr>

                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
          </div>
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

  @endsection
