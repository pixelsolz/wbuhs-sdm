@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Student Synopsis List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              @if(Session::has('err_import'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Same registration no exist - {{session('err_import')}}
              </div>
              @endif

              <div class="clearfix"></div>

                <div class="form-group row">
                  <form action="{{ route('student-synopsis.list') }}" method="get">
                    <input type="hidden" name="search" value="search">

                    <div class="col-xs-3"><br/>
                       <input class="form-control" type="text" name="by_name" placeholder="search By Student Name" autocomplete="off" @if(!empty($request->by_name)) value="{{$request->by_name}}" @endif>
                    </div>
                    <div class="col-xs-3"><br/>
                       <input class="form-control" type="text" name="phone" placeholder="search By Phone No."  @if(!empty($request->phone)) value="{{$request->phone}}" @endif>
                    </div>
                    <div class="col-xs-3"><br/>
                       <input class="form-control" type="text" name="email" placeholder="search By Email Id"  @if(!empty($request->email)) value="{{$request->email}}" @endif>
                    </div>
                    <div class="col-xs-1"><br/>
                      <button type="submit" class="btn btn-info">Search</button>
                    </div>
                  </form>
                </div>

                <div class="row">

                  <div class="col-xs-3">
                       <a href="{{url('public/manuals/demo-import-synopsis-students.xlsx')}}" class="btn btn-success" download>Download Sample CSV</a>
                  </div>
                <form class="form-inline" method="POST" action="{{ url('admin/student-synopsis-list/import')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                <div class="form-group">
                   <label for="csv_file">CSV file to import:</label>
                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                     <button class="btn btn-info" type="submit">submit</button>
                </div>
                </form>

              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body" id="print_area">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover" width="100%">
                <thead>
                <tr>
                  <th width='5%'>Sl No.</th>
                  <th width='20%'>Name of Student</th>
                  <th width='20%'>Unique Id</th>
                  <th width='10%'>Course Name</th>
                  <!--<th width='20%'>Institute</th>-->
                  <th width='10%'>Phone</th>
                  <th width='10%'>Email</th>
                  <th width='5%'>Edit</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($students as $student)
                  <tr>
                    <td width='5%'>{{$count}}</td>
                    <td width='20%'>{{$student->name}}</td>
                    <td width='20%'>{{$student->unique_id}}</td>
                    <td width='20%'>{{$student->course}}</td>
                    <!-- <td width='20%'>{{$student->institute}}</td>-->
                    <td width='10%'>{{$student->phone}}</td>
                    <td width='10%'>{{$student->email}}</td>
                    <td width="5%">

                        <a href="{{url('admin/student-synopsis/edit',['id'=>$student->id])}}" class="glyphicon glyphicon-pencil" data-tog="tooltip" title="Edit"></a>
                    </td>

                  </tr>
                   @php $count++; @endphp
                @empty
                  <tr><td colspan="9" style="text-align: center;">No Data Availbale</td></tr>
                @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $students->links() }}</div>

            </div>


          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>

  @endsection
