

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Thank You for your acknowledgement!</h1>
      <h3>New Dissertation has been assign to you. Please click on the below mentioned link and check the Dissertation.</h3>
      <h2> Student Name:  {{@$thesis->name_of_student}}</h2>
      <h2> Student Email:  {{@$thesis->email}}</h2>
    </section>
     <a href="{{url('admin/student-dissertation')}}" style="display: inline-block; border-radius: 8px; border: 1px solid #249d40;
            background: #249d40; color: #fff; font-weight: 600; font-size: 17px; line-height: 18px;
            text-decoration: none; padding: 15px 25px; margin-bottom: 15px; text-transform: capitalize;">click here</a>
  </div>



