@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Edit Bonus Rule
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('bonus.index') }}"><i class="fa fa-dashboard"></i>Bonus Master</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Bonus Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/bonus/'.$bonus->id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">

                     <div class="form-group row @if($errors->has('within_days'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="within_days">Within Days<span class="required_field">*</span></label>
                           <input type="number" name="within_days" class="form-control" value="{{old('within_days')? old('within_days'):$bonus->within_days}}">
                           @if($errors->has('within_days'))
                           <span class="error">{{$errors->first('within_days')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('amount'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="within_days">Amount<span class="required_field">*</span></label>
                           <input type="number" name="amount" class="form-control" value="{{old('amount')? old('amount'):$bonus->amount}}">
                           @if($errors->has('amount'))
                           <span class="error">{{$errors->first('amount')}}</span>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection