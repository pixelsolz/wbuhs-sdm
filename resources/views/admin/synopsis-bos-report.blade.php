@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Synopsis Report for BOS {{$user->full_name}}
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>

                            <div class="form-group row">
                <form action="{{route('admin.student-synopsis-report')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-7">
                    <select class="form-control" name="search_by_course">
                      <option value="">All Courses</option>
                      @foreach($courses as $course)
                        <option value="{{$course->id}}" @if($request->search_by_course && $request->search_by_course == $course->id) selected @endif>{{$course->name}} @if(@$course->GetParent(@$course->parent)->parent !=0)- {{@$course->GetParent(@$course->parent)->name}}@endif - {{@$course->GetRoot(@$course->parent)->name}}</option>
                      @endforeach

                    </select>
                  </div>
                  <div class="col-xs-3">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, Reg no., year, institute name or mobile" value="{{($request->input_search? $request->input_search:'' )}}">
                  </div>

                  <div class="col-xs-2">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

                </form>
              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Institute name</th>
                  <th>Regitration Year</th>
                  <th>Course Name</th>
                  <th>Assigned Date</th>
                  <th>Synossis Status</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($synopsisTransaction as $transaction)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$transaction->synopsis->name_of_student}}</td>
                    <td>{{$transaction->synopsis->institute_name}}</td>
                    <td>{{$transaction->synopsis->registration_year}}</td>
                    <td>{{$transaction->synopsis->category->name}}</td>
                    <td>{{\Carbon\Carbon::parse($transaction->created_at)->format('d-m-Y')}}</td>
                    <td>{{$transaction->synopsis->status_text}}</td>
                  </tr>
                   @php $count++; @endphp
                @empty
                  <tr><td colspan="6" style="text-align: center;">No Data Availbale</td></tr>
                @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $synopsisTransaction->links() }}</div>

            </div>


          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>

  @endsection
