@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Student Synopsis Edit
      </h1>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Student Synopsis Edit</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/student-support-store/'.$studentSupport->id.'/'.$studentSupport->user_id)}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
               
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="name">Student Name<span class="required_field">*</span></label>
                           <input type="text" readonly="" name="name" class="form-control" value="{{old('name') ?old('name'):$students->userDetail->f_name .' '.$students->userDetail->l_name }}">
                        </div>
                     </div>
                    
                      <div class="form-group row @if($errors->has('mobile'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="mobile">Phone<span class="required_field">*</span></label>
                           <input type="number" readonly="" name="mobile" class="form-control" value="{{old('mobile') ? old('mobile'): $students->userDetail->mobile}}">
                          
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" readonly="" name="email" class="form-control" value="{{old('email') ? old('email'): $students->userDetail->email}}">
                           
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('subject'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="subject">Subject<span class="required_field">*</span></label>
                           <input type="subject" readonly="" name="subject" class="form-control" value="{{old('subject') ? old('subject'): $students->subject}}">
                           
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('content'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="content">Content<span class="required_field">*</span></label>
                           <textarea readonly="" name="content" class="form-control"> {{old('content') ? old('content'): $studentSupport->content}}</textarea>
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="admin_reply">Reply<span class="required_field">*</span></label>

                           <textarea @if ($studentSupport->status==1) readonly="" @endif name="admin_reply" class="form-control"> {{old('admin_reply') ? old('admin_reply'): $studentSupport->admin_reply}} </textarea>
                         
                           @if($errors->has('admin_reply'))
                           <span class="error">{{$errors->first('admin_reply')}}</span>
                           @endif
                        </div>
                     </div>
                    
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection
