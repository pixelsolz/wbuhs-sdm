<table>
    <thead>
    <tr>
        <th>CREDIT A/c. NO.</th>
        <th>Amount</th>
        <th>IFSC CODE</th>
        <th>TYPE OF TRANSACTION</th>
        <th>BENEFICIARY A/C. NO.</th>
        <th>BENEFICIARY NAME</th>
        <th>BENEFICIARY BANK NAME</th>
        <th>PARTICULARS</th>
        <th>MOBILE NO.</th>
        <th>Bank confirmation</th>

    </tr>
    </thead>
    <tbody>
        @php $crdNo = '46090111005000'; @endphp
    @foreach($facultyPayments as $payment)
        <tr>
            <td>{{$crdNo + '6'}}</td>
            <td>{{ $payment->total_amount }}</td>
            <td>{{$payment->bank_detail['ifs_code']}}</td>
            <td>TR</td>
            <td>{{$payment->bank_detail['account_no']}}</td>
            <td>{{$payment->bank_detail['account_holder_name']}}</td>
            <td>{{$payment->bank_detail['bank_name']}}</td>
            <td>REMUNERATION</td>
            <td>{{ $payment->faculty->userDetail->mobile}}</td>
            <td></td>

        </tr>
    @endforeach
    </tbody>
</table>