<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('frontend.includes.header')

        <main class="site-main">
        <!-- <main class="py-4"> -->
            @yield('content')
        </main>
@include('frontend.includes.footer')
</html>
