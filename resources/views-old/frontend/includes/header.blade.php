<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>WBUHS | Synopsis&Thesis{{-- config('app.name', 'Laravel') --}}</title>
     <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet">
     <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">
    <link href="{{asset('public/css/jquery.signature.css')}}" rel="stylesheet">
     <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('public/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/easy-responsive-tabs.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/styles-larabl.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/bower_components/bootstrap-sweetalert/dist/sweetalert.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
</head>
 <body>
	 <div class="ajax-loader" style="display:none;">
		 <div class="append-loader"><img src="{{url('public/img/ajax-loader.gif')}}"></div>
	 </div>
 <div class="wrapper">
 	        <header class="site-header">
            <div class="header-top clear">
                <div class="container">
                        <div class="header-top-left">

                        </div>
                        <div class="header-top-right">


                        @if(\Auth::check())
                           <a href="#" class="usernm"><i class="fa fa-user" aria-hidden="true"></i> {{Auth::user()->full_name}}</a>
                            <a href="{{url('logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                        @else
                            <span>&nbsp;</span>
                        @endif


                        </div>
                </div>
            </div>
            <div class="header-bottom clear">
                <div class="container">
                        <div class="header-bottom-left">
                            <a class="logo" href="https://wbuhs.ac.in/" title="THE WEST BENGAL UNIVERSITY OF HEALTH SCIENCES">
                                <img src="{{url('public/images/logo.png')}}" alt="logo"/>
                            </a>
                        </div>
                        <div class="header-bottom-right">
                            <a class="archive" href="#"><img src="{{url('public/images/archive.png')}}"/>Archive</a>
                            <a class="blog" href="#"><img src="{{url('public/images/blog.png')}}"/>blog</a>
                            <a class="faq" href="#"><img src="{{url('public/images/faq.png')}}"/>FAQ</a>
                        </div>
                </div>
            </div>

        </header>

        <div class="banner">

            <img src="{{url('public/images/thesis.png')}}" alt="student-registration">

        </div>

        <!-- Modal -->

<div id="myModal" class="modal fade admin-mdl" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">

        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

             <!--  <div class="modal-body">

                                    <div class="admin-profile">
                                        <div class="pfile-hdr">
                                            <div class="img-wrp"> <img src="{{url('public/img/avatar2.png')}}"></div>

                                            <span>Sourav paul</span>
                                        </div>
                                        <div class="pfile-bdy clear">
                                            <div class="pull-left">
                                                <a href="#" class="btn btn-default btn-deflat">Profile</a>
                                            </div>

                                                <div class="pull-right">
                                                    <a href="#" class="btn btn-default btn-deflat">Sign out</a>
                                                    </div>
                                        </div>

                                    </div>


              </div> -->

    </div>

  </div>
</div>
