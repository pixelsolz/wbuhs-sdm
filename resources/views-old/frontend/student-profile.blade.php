@extends('frontend.layout')
@section('content')

<section class="desh-bord">
   <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger">
         <strong>{{Session('error')}}</strong>
      </div>
      @endif
      <div class="row">
         @include('frontend.includes.sidemenu')
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Profile</h3>
            <div class="right-pnl">

               <form action="{{route('profile.update',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
                  @csrf
                  @method('put')

                  <div class="form-group @if($errors->first('f_name')) has-error @endif">
                     <label for="f_name">First Name <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="f_name" placeholder="Enter first name" name="f_name" value="{{old('f_name')? old('f_name'): $user->f_name}}">
                     {{--@if($errors->first('f_name'))
                     <span class="error">{{$errors->first('f_name')}}</span>
                     @endif--}}
                  </div>
                  <div class="form-group @if($errors->first('l_name')) has-error @endif">
                     <label for="l_name">Last Name<span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="l_name" placeholder="Enter last name" name="l_name" value="{{old('l_name')? old('l_name'): $user->l_name}}">
                     {{--@if($errors->first('l_name'))
                     <span class="error">{{$errors->first('l_name')}}</span>
                     @endif --}}
                  </div>
                  <div class="form-group @if($errors->first('email')) has-error @endif">
                     <label for="email">Email <span class="require-asterik">*</span>:</label>
                     <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" value="{{old('email')? old('email'): $user->email}}" readonly="true">
                     {{--@if($errors->first('email'))
                     <span class="error">{{$errors->first('email')}}</span>
                     @endif--}}
                  </div>
                  <div class="form-group @if($errors->first('mobile')) has-error @endif">
                     <label for="mobile">Mobile <span class="require-asterik">*</span>:</label>
                     <input type="number" class="form-control" id="mobile" placeholder="Enter mobile no." name="mobile" value="{{old('mobile')? old('mobile'): $user->userDetail->mobile}}">
                     {{--@if($errors->first('mobile'))
                     <span class="error">{{$errors->first('mobile')}}</span>
                     @endif--}}
                  </div>


                  <div class="form-group @if($errors->first('address')) has-error @endif">
                     <label for="address">Address<span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="address" id="address">{{old('address')? old('address'): $user->userDetail->address}}</textarea>
                     {{--@if($errors->first('address'))
                     <span class="error">{{$errors->first('address')}}</span>
                     @endif--}}
                  </div>
                  <div class="form-group @if($errors->first('gender')) has-error @endif">
                     <label for="gender">Gender<span class="require-asterik">*</span>:</label>
                     <select name="gender" class="form-control">
                        <option value="">select</option>
                        <option value="1" @if(old('gender')?old('gender')==1: ($user->userDetail->gender ? $user->userDetail->gender==1: '' ) ) selected @endif>Male</option>
                        <option value="2" @if(old('gender')?old('gender')==2: ($user->userDetail->gender ? $user->userDetail->gender==2: '' ) ) selected @endif>Female</option>
                     </select>
                     {{--@if($errors->first('gender'))
                     <span class="error">{{$errors->first('gender')}}</span>
                     @endif--}}
                  </div>
                  <div class="form-group @if($errors->first('profile_image')) has-error @endif">
                     <label for="profile_image">Image Upload:</label>
                     <input class="form-control" type="file" name="profile_image">
                     {{--@if($errors->first('profile_image'))
                     <span class="error">{{$errors->first('profile_image')}}</span>
                     @endif--}}
                  </div>

                    <div class="submitOuter">
                  <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>


@endsection