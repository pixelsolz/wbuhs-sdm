@extends('frontend.layout')

@section('content')
	<section class="desh-bord">
	<div class="container">
			@if(Session::has('error'))
			<div class="alert alert-danger">
			  <strong>{{Session('error')}}</strong>
			</div>
			@endif

	<div class="row">
		@include('frontend.includes.sidemenu')
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
		<h3 class="dashboard_title">Payment Form</h3>
		<div class="right-pnl">
			<form action="{{url('payment')}}" method="post" enctype="multipart/form-data" id="payment_form">
				@csrf
				<input type="hidden" name="pay_type">
				{{--<div class="form-group">
			      <label for="name_of_student">payment Amount<span class="require-asterik">*</span>:</label>
			       <input type="text" class="form-control" id="pay_amount" placeholder="Enter Payment Amount" name="pay_amount" value="400" readonly>
			        @if($errors->first('name_of_student'))
						<span class="error">{{$errors->first('name_of_student')}}</span>
					@endif
			    </div>--}}
			    <div style="text-align: center;">
			      <button type="submit" class="btn btn-primary" onclick="paymentCheck(event, 'payment_form','offline')">Offline Pay</button>
			      <button type="submit" class="btn btn-primary" onclick="paymentCheck(event, 'payment_form','online')" disabled="true">Online Pay</button>
			     </div>
			</form>

		</div>
		</div>
	</div>



	</div>
	</section>

@endsection