@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Synopsis
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>

                            <div class="form-group row">
                <form action="{{route('admin.student-synopsis')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-4">
                    <select class="form-control" name="search_by_status">
                      <option value="">All</option>
                      <option value="1" @if($request->search_by_status && $request->search_by_status ==1) selected @endif>Proccesed</option>
                      <option value="2" @if($request->search_by_status && $request->search_by_status ==2) selected @endif>Approved</option>
                      <option value="3" @if($request->search_by_status && $request->search_by_status ==3) selected @endif>Rejected</option>
                      <option value="4" @if($request->search_by_status && $request->search_by_status ==4) selected @endif>Under Revision</option>

                    </select>
                  </div>
                  <div class="col-xs-4">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, Reg no., year, institute name or mobile" value="{{($request->input_search? $request->input_search:'' )}}">
                  </div>

                  <div class="col-xs-4">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

                </form>
              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Registration No.</th>
                  <th>Registration Year</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <!--<th>Email</th>
                   <th>Guide Name</th>
                  <th>Guide Desgination</th> -->
                  @if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray()))
                  <th>Assign</th>
                  @endif
                  <th>Document</th>

                  @if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray()))
                  <th>Payment Status</th>
                  @endif

                  <th>BOS review status</th>

                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($synopsis as $synop)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$synop->name_of_student}}</td>
                    <td>{{$synop->registration_no}}</td>
                    <td>{{$synop->registration_year }}</td>
                    <td>{{$synop->institute_name}}</td>
                    <td>{{$synop->mobile_landline}}</td>
                    <!--<td>{{$synop->email}}</td>
                     <td>{{$synop->guide_name}}</td>
                    <td>{{$synop->guide_designation}}</td> -->
                    @if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray()))
                    <td>
                     <span class="badge bg-green" data-toggle-pop="popover" data-content="{!! $synop->transaction()->getEmailid() !!}" data-html="true" title="Sent List">{{count($synop->transaction)}}</span>&nbsp;
                     @if($synop->status ==1)
                     @php
                     $assignedUser = $synop->transaction->count() ? implode($synop->transaction()->pluck('to_user_id')->toArray(),','):'';
                     @endphp
                     <a href="#" data-toggle="modal" data-target="#modal-default" data-tog="tooltip" title="Assign Synopsis to BOS" id="synopsis-modal" onclick="setSynopsiId('{{$synop->id}}','{{$synop->getBosUser($synop->id)}}', '{{$synop->category->name}}', '{{$assignedUser}}')" data-synopsis="{{$synop->id}}"><i class="fa fa-send-o"></i></a>
                     @endif
                    </td>
                    @endif

                    <td>
                    <a href="{{url('admin/student-synopsis/viewform',['syn_id'=>$synop->id])}}" target="_blank" data-tog="tooltip" title="Synopsis Form Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> ||
                      <a href="{{url('admin/student-synopsis/view',['syn_id'=>$synop->id])}}" target="_blank" data-tog="tooltip" title="Synopsis Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                      {!!$synop->getOtherPdf($synop->id)!!}
                    </td>

                     @if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray()))
                    <td>
                    <input class="paid-status" data-id="{{$synop->id}}" data-on="Paid" data-off="Not Paid" data-url="{{route('admin.payment.status-change')}}" data-type="synopsis" type="checkbox" @if($synop->payment_done ==1) checked disabled @endif  data-toggle="toggle" data-size="small">
                    </td>
                    @endif


                    <td>
                      @if(in_array('bos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray()))

                  @php $statusUrl = route('synopsis.review-status'); $bosReviewStatus = $synop->transaction()->getReviewStatus($synop->id, \Auth::guard('admin')->user()->id) @endphp

                      <select name="review_status" class="form-control" onchange="synopsisReviewStatus(this.value, '{{$synop->id}}','{{\Auth::guard('admin')->user()->id}}', '{{$statusUrl}}')" @if($bosReviewStatus !=0) disabled @endif>
                            <option value="">select</option>
                            <option value="1" @if($bosReviewStatus== 1) selected @endif>Approved</option>
                            <option value="2" @if($bosReviewStatus== 2) selected @endif>Rejected</option>
                            <option value="3" @if($bosReviewStatus== 3) selected @endif>Resubmission</option>
                      </select>
                      @else
                      <span><a href="javascript:void(0)" onclick="getBOSReviewStatus('{{$synop->transaction}}')">view status</a></span>

                      @endif
                    </td>


                    <td>{{$synop->status_text}} &nbsp;
                    @if($synop->status ==3 || $synop->status ==4)
                      @if($synop->reason_reject)
                      <a href="javascript:void(0)" data-tog="tooltip" title="Reason" onclick="reasonRejected('{{$synop->reason_reject}}')"><i class="fa fa-hand-pointer-o"></i></a>
                      @elseif($synop->reason_reject_doc)
                        <a href="javascript:void(0)" onclick="downloadReasonDoc('{{$synop->id}}','synopsis')" data-tog="tooltip" title="Reason"><i class="fa fa-download"></i></a>
                      @endif

                    @endif

                    @if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray()))
                    @if($synop->status ==1)
                    <a href="#" data-toggle="modal" data-target="#status-modal" data-tog="tooltip" title="Change Status" onclick="setSynopsisStatus('{{$synop->id}}','{{$synop->status}}','{{$synop->reason_reject}}')"><i class="fa fa-hand-pointer-o"></i></a>
                    @endif
                    @endif
                     &nbsp; <a href="#" class="showFase" data-tog="tooltip" title="Life cycle" data-id="{{$synop->id}}"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>

                    <a href="#" data-id="{{$synop->id}}" class="hideFase" title="Hide" style="display: none;"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                    </td>
                  </tr>
                  <tr class="show-tr"  data-id="{{$synop->id}}" style="display: none;">
                    <td colspan="11">
                      <div class="time-line-div admin-timeline">
                        <div class="legend">
                                  <ul>
                                    <li><span class="notcomplete"></span> Not Complete</li>
                                    <li><span class="complete"></span> Complete</li>
                                  </ul>
                                </div>
                        <ul class="time-line-ul">
                          <li class="time-line-li active-li">
                                  <span class="time-line-span">{{$synop->is_resubmitted?'Resubmitted':'Submitted'}}</span><span class="circle"></span>
                                </li>
                                @if($synop->status !=4 && $synop->status !=3)
                                <li class="time-line-li @if($synop->status==2) active-li @endif">
                                  <span class="time-line-span">Approved</span><span class="circle"></span>
                                </li>
                                @endif
                                @if($synop->status ==4)
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Send for Revision</span><span class="circle"></span>
                                </li>
                                @endif
                                @if($synop->status ==3)
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Rejected</span><span class="circle"></span>
                                </li>
                                @endif
                        </ul>
                      </div>
                    </td>
                  </tr>
                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="11" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $synopsis->links() }}</div>

            </div>
            <!-- /.box-body -->

            <div class="overlay" style="display: none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>

          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

      <!-- Email send Modal -->
      <div class="modal fade design-modal" id="modal-default">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assign Synopsis To BOS <span id="assign_title"></span></h4>
              </div>
              <div class="modal-body">

                <table class="table">
                    <tbody id="send_email_tbody">
                    {{--@if(count($synopes))
                       @foreach($synop->category->user as $user)
                         <tr>
                           <td>{{$user->full_name}}</td>
                           <td><input type="checkbox" name="send_email[]" id="send_email" value="{{$user->id}}"></td>

                         </tr>
                       @endforeach
                       @endif--}}
                       <!-- <input type="hidden" name="synopsis_id"> -->
                    </tbody>
                 </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                @php $route= url('admin/assign-synopsis') @endphp
                <button type="button" class="btn btn-primary btn-disable" onclick="sendEmail('{{$route}}','{{csrf_token()}}','bios', 'Synopsis')">send &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- Status Change Modal-->

        <div class="modal fade design-modal" id="status-modal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Set status</h4>
              </div>
              <div class="modal-body">
              <form action="{{route('student.synopsis.status-change')}}" id="synopsis_status" enctype="multipart/form-data">
              @csrf
                <input type="hidden" name="synop_id" id="synop_id">
               <div class="form-group radio">
                  <input type="radio" value="2" name="status" class="minimal"><label>Approved</label>
                </div>
                <div class="form-group radio">
                  <input type="radio" value="3" name="status" class="minimal"><label>Rejected</label>
                </div>
                <div class="form-group radio">
                  <input type="radio" value="4" name="status" class="minimal"><label>Resubmitted</label>
                </div>
                <div class="form-group" id="not-accept-reason" style="display: none;">
                <label >Reason / Upload doc file <a href="javascript:void(0)" onclick="showDocs('upload_docs','not-accept-reason')">(click)</a>:</label>
                  <textarea name="reason_reject" class="form-control"></textarea>
                </div>

                <div style="display: none;" id="upload_docs">
                  <label >Upload doc file / Reason <a href="javascript:void(0)" onclick="showDocs('not-accept-reason','upload_docs')">(click)</a>:</label>
                  <input type="file" name="reason_reject_doc">
                </div>
              </form>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus(event)">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- Reson Rejected -->

         <div class="modal fade design-modal" id="reason-rejected-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Reason for Rejection</h4>
                </div>
                <div class="modal-body">
                <p id="reason_content"></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <!-- View review status -->

        <div class="modal fade design-modal" id="view-review-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">BOS Review Status</h4>
                </div>
                <div class="modal-body">
               <table class="table">
                  <thead>
                    <th>BOS name</th>
                    <th>Status</th>
                  </thead>
                   <tbody id="review_status_tbody">

                   </tbody>
               </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

  </div>

  @endsection
