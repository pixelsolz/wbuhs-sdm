@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h3>
        Adjudicator Not Available List
      </h3>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif
              {{--<a href="{{route('leave.index')}}" class="btn btn-primary">Creat New</a>--}}
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
               <div class="clearfix"></div>

              <div class="form-group row">
                <form action="{{route('leave.index')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-3">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, email or mobile" value="{{($request->input_search? $request->input_search:'' )}}">
                  </div>
                  <div class="col-xs-3">
                  <input type="text" class="form-control datepicker" name="start_date" placeholder="Start date" value="{{($request->start_date? $request->start_date:'' )}}">
                  </div>
                  <div class="col-xs-3">
                  <input type="text" class="form-control datepicker" name="end_date" placeholder="End date" value="{{($request->end_date? $request->end_date:'' )}}">
                  </div>

                  <div class="col-xs-3">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

                </form>
              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Leave Start Date</th>
                  <th>Leave End Date</th>
                  <th>Reason</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($adminLeaves as $leave)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$leave->user->full_name}}</td>
                    <td>{{$leave->user->email}}</td>
                    <td>{{$leave->user->userDetail->mobile}}</td>
                    <td>{{\Carbon\Carbon::parse($leave->start_date)->format('d/m/Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($leave->end_date)->format('d/m/Y')}}</td>
                    <td>{{$leave->reason}}</td>
                  </tr>
                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="7" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
              <div>{{ $adminLeaves->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  @endsection
