  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{(\Auth::guard('admin')->user()->userDetail && \Auth::guard('admin')->user()->userDetail->profile_image) ? url('public/'.\Auth::guard('admin')->user()->userDetail->profile_image):url('public/img/img_logo.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{\Auth::guard('admin')->user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     <!--  <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       <!--  <li class="header">MAIN NAVIGATION</li> -->

        <li class="{{Request::is('admin')? 'active': ''}}">
          <a href="{{url('admin')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        @if(Helper::sidenavAccess('role-list'))
        <li class="{{(Request::is('admin/manage-role') || Request::is('admin/manage-role/*'))? 'active': ''}}">
          <a href="{{url('admin/manage-role')}}">
            <i class="fa fa-cogs"></i> <span>Manage Role</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        @if(Helper::sidenavAccess('user-list'))
         <li class="{{(Request::is('admin/user-manage') || Request::is('admin/user-manage/*'))? 'active': ''}}">
          <a href="{{url('admin/user-manage')}}">
            <i class="fa fa-group"></i> <span>Manage User</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        @if(Helper::sidenavAccess('category-list'))
        <li class="treeview {{(Request::is('admin/category') || Request::is('admin/category/*'))? 'active': ''}}">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Speciality</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{(Request::is('admin/category'))? 'active': ''}}"><a href="{{url('admin/category')}}"><i class="fa fa-circle-o"></i>List</a></li>
            <li class="{{(Request::is('admin/category/create'))? 'active': ''}}"><a href="{{url('admin/category/create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
            <li class="{{(Request::is('admin/category/user'))? 'active': ''}}"><a href="{{url('admin/category/user')}}"><i class="fa fa-circle-o"></i>User Speciality List</a></li>
          </ul>
        </li>



         {{--<li class="{{(Request::is('admin/category') || Request::is('admin/category/*'))? 'active': ''}}">
          <a href="{{url('admin/category')}}">
            <i class="fa fa-book"></i> <span>Category</span>
          </a>
        </li>--}}
        @endif

        @if(Helper::sidenavAccess('student-synopsis'))
         <li class="{{(Request::is('admin/student-synopsis') || Request::is('admin/student-synopsis/*'))? 'active': ''}}">
          <a href="{{url('admin/student-synopsis')}}">
            <i class="fa fa-files-o"></i> <span>Synopsis</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        @if(Helper::sidenavAccess('student-thesis'))
         <li class="{{(Request::is('admin/student-thesis') || Request::is('admin/student-thesis/*'))? 'active': ''}}">
          <a href="{{url('admin/student-thesis')}}">
            <i class="fa fa-files-o"></i> <span>Dissertation</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        @if(\Auth::guard('admin')->user()->is_admin !=1 && Helper::sidenavAccess('student-result-list'))
        <li class="{{(Request::is('admin/student-result') || Request::is('admin/student-result/*'))? 'active': ''}}">
          <a href="{{url('admin/student-result')}}">
            <i class="fa fa-files-o"></i> <span>Dissertation Review</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        @if(Helper::sidenavAccess('leave-list'))
         <li class="{{(Request::is('admin/leave') || Request::is('admin/leave/*'))? 'active': ''}}">
          <a href="{{url('admin/leave')}}">
            <i class="fa fa-hand-paper-o"></i> <span>{{(\Auth::guard('admin')->user()->is_admin ==1)?'Adjudicator':'My'}} Availability</span>
          </a>
        </li>
        @endif
        {{Helper::checkIsReviewer()}}
        @if(Helper::checkIsReviewer())
        <li class="{{(Request::is('admin/renumeration') || Request::is('admin/renumeration/*'))? 'active': ''}}">
          <a href="{{url('admin/renumeration')}}">
            <i class="fa fa-money"></i> <span>Renumeration</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        @if(Helper::sidenavAccess('role-list'))
        <li class="{{(Request::is('admin/reviewerPayment') || Request::is('admin/reviewerPayment/*'))? 'active': ''}}">
          <a href="{{url('admin/reviewerPayment')}}">
            <i class="fa fa-money"></i> <span>Adjudicator payment</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        {{-- @if(Helper::sidenavAccess('bonus-master-list'))
      <li class="{{(Request::is('admin/bonus') || Request::is('admin/bonus/*'))? 'active': ''}}">
          <a href="{{url('admin/bonus')}}">
            <i class="fa fa-money"></i> <span>Bonus Master</span>
          </a>
        </li>
        @endif --}}

        @if(\Auth::guard('admin')->user()->is_admin !=1)
        <li class="{{(Request::is('admin/profile/edit') || Request::is('admin/profile/edit/*'))? 'active': ''}}">
          <a href="{{url('admin/profile/edit')}}">
            <i class="fa fa-user"></i> <span>Profile Update</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        @endif

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li> -->






       <!--  <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
