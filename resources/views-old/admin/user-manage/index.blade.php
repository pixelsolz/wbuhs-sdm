@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<style type="text/css">
  .inactive_menu {
  background-color: #F8F8F8;
}
</style>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User List

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <a href="{{route('user-manage.create')}}" class="btn btn-primary newline-class">Create New User</a>
               <div class="clearfix"></div>
              <div class="form-group row">
              <form action="{{route('user-manage.index')}}" method="get">
                <input type="hidden" name="search" value="search">
              <div class="col-xs-4">
                  <select class="form-control" name="search_by_role">
                    <option value="">Select Role</option>
                    @foreach($roles as $role)
                    <option value="{{$role->id}}" @if(!empty($request->search_by_role) && $request->search_by_role == $role->id) selected @endif >{{$role->name}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-xs-4">

                    <input type="text" class="form-control" name="input_search" placeholder="Search by name email or mobile" value="{{!empty($request->input_search)?$request->input_search: '' }}">

                </div>
                <div class="col-xs-4">
                <button type="submit" class="btn btn-info">Search</button>
                </div>
                </form>
                </div>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="5%">Sl No.</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>City</th>
                  <th>Status</th>
                  <th>Roles</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; @endphp
                @forelse($users as $user)
                <tr class="@if($user->status=='Inactive') inactive_menu @endif">
                  <td>{{$a}}</td>
                  <td>{{$user->full_name}}</td>
                  <td><a href="mailto:{{$user->email}}" >{{$user->email}}</a></td>
                  <td>{{$user->userDetail? $user->userDetail->mobile: ''}}</td>
                  <td><a href="#" data-toggle-pop="popover" data-content="{!! @$user->userDetail->address !!}" data-html="true" title="Address">{{$user->userDetail? $user->userDetail->city_text: ''}}</a></td>
                  <td>{{$user->status}}</td>
                  <td>{{$user->roles ? implode(', ',$user->roles->pluck('name')->toArray()): ''}}
                  @if(in_array('student',$user->roles->pluck('slug')->toArray()))
                    {{$user->checkStdSubmission($user->id)}}
                  @endif
                  @if(in_array('adjudicator',$user->roles->pluck('slug')->toArray()) || in_array('bos',$user->roles->pluck('slug')->toArray()))
                  @php $specializationUrl = url('admin/view-category',[$user->id]); @endphp
                  <a href="#" data-toggle="modal" data-target="#view-speciality" data-tog="tooltip" title="View Speciality" onclick="viewSpeciality('{{$specializationUrl}}')"><i class="fa fa-hand-pointer-o"></i></a>
                  @endif
                  </td>
                  <td>
                  @if($user->is_admin!= 1)
                   <a href="{{route('user-manage.edit', ['id'=>$user->id])}}" class="glyphicon glyphicon-pencil" data-tog="tooltip" title="Edit"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                @php
                  $deleteroute = route('user-manage.destroy', ['id'=>$user->id]);
                @endphp
                @if($user->status =='Active')
                    <a href="javascript:void(0)" onclick="deleteData('{{$deleteroute}}','{{csrf_token()}}')" class="glyphicon glyphicon-remove" data-tog="tooltip" title="Delete"></a>
                  @endif
                   @endif
                  </td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>
            </div>
              <div>{{ $users->links() }}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         <div class="modal fade" id="view-speciality">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Speciality</h4>
              </div>
              <div class="modal-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Sl No.</th>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody id="specialze-tbody">

                  </tbody>
                </table>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

  @endsection
