@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create User
      </h1>
      {{--<ol class="breadcrumb">
         <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> User List</a></li>
         <!--  <li><a href="#">Forms</a></li> -->
         <li class="active"> Edit</li>
      </ol>--}}
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User Form</h3>
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/user-manage',['id'=>$user->id])}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('f_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="f_name">First Name<span class="required_field">*</span></label>
                           <input type="text" name="f_name" class="form-control" value="{{old('f_name') ?old('f_name'):$user->f_name }}">
                           @if($errors->has('f_name'))
                           <span class="error">{{$errors->first('f_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('l_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="l_name">Last Name<span class="required_field">*</span></label>
                           <input type="text" name="l_name" class="form-control" value="{{old('l_name') ? old('l_name'): $user->l_name}}">
                           @if($errors->has('l_name'))
                           <span class="error">{{$errors->first('l_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" name="email" class="form-control" value="{{old('email') ? old('email'): $user->email}}">
                           @if($errors->has('email'))
                           <span class="error">{{$errors->first('email')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('mobile'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="mobile">Mobile<span class="required_field">*</span></label>
                           <input type="number" name="mobile" class="form-control" value="{{old('mobile') ? old('mobile'): $user->userDetail->mobile}}">
                           @if($errors->has('mobile'))
                           <span class="error">{{$errors->first('mobile')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('address'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="address">Address<span class="required_field">*</span></label>
                           <textarea name="address" class="form-control">{{old('address') ? old('address'): $user->userDetail->address}}</textarea>
                           @if($errors->has('address'))
                           <span class="error">{{$errors->first('address')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('city_id'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="city_id">City<span class="required_field">*</span></label>
                           <select name="city_id" class="form-control">
                              <option value="">select</option>
                              @foreach($cities as $city)
                              <option value="{{$city->id}}" @if($user->userDetail->city_id == $city->id) selected @endif>{{$city->name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('city_id'))
                           <span class="error">{{$errors->first('city_id')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('gender'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="gender">Gender<span class="required_field">*</span></label>
                           <select class="form-control" name="gender">
                              <option value="">select</option>
                              <option value="1" @if($user->userDetail->gender==1) selected @endif>Male</option>
                              <option value="2" @if($user->userDetail->gender==2) selected @endif>Female</option>
                           </select>
                           @if($errors->has('gender'))
                           <span class="error">{{$errors->first('gender')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('status'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="status">Status<span class="required_field">*</span></label>
                           <select class="form-control" name="status">
                              <option value="">select</option>
                              <option value="Active" @if($user->status =='Active') selected @endif>Active</option>
                              <option value="Inactive" @if($user->status =='Inactive') selected @endif>Inactive</option>
                           </select>
                           @if($errors->has('status'))
                           <span class="error">{{$errors->first('status')}}</span>
                           @endif
                        </div>
                     </div>

                     {{--<div class="form-group row @if($errors->has('user_type'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="user_type">User Type<span class="required_field">*</span></label>
                           <select class="form-control" name="user_type">
                              <option value="">select</option>
                              <option value="1" @if(old('user_type') && old('user_type')==1? 1: $user->is_admin) selected @endif>Super Admin</option>
                              <option value="2" @if(old('user_type') && old('user_type')==2? 2: $user->is_admin) selected @endif>Admin</option>
                              <option value="3" @if(old('user_type')==3) selected @endif>Student</option>
                           </select>
                           @if($errors->has('user_type'))
                           <span class="error">{{$errors->first('user_type')}}</span>
                           @endif
                        </div>
                     </div>--}}

                     <div class="form-group row">
                        <div class="col-lg-9 col-xs-12">
                           <label>Role<span class="required_field">*</span></label>
                            @if($errors->has('roles'))
                            <br>
                           <span class="error">{{$errors->first('roles')}}</span>
                           @endif

                           <div class="form-group">
                              @foreach($roles as $role)
                              @if($role->slug !='superadmin')
                              <input type="checkbox" class="minimal" name="roles[]" id="roles" value="{{$role->id}}" onclick="showCategory('{{$role->slug}}')" @if(in_array($role->id, $userRoles)) checked @endif>&nbsp;
                               <label>{{$role->name}}</label>&nbsp;
                               @endif
                              @endforeach
                           </div>
                        </div>
                     </div>

                     <div class="form-group row"  id="category-div" @if((old('roles') && in_array(2, old('roles'))) || in_array(2, $userRoles) || in_array(4, $userRoles)  ) @else style="display: none;" @endif >
                        <div class="col-xs-9">
                           <label for="user_type">Category<span class="required_field">*</span></label>
                           <select class="form-control" name="category[]" multiple="true">
                              @foreach($categories as $category)
                              <option value="{{$category['id']}}" @if(in_array($category['id'] ,$user->category()->pluck('id')->toArray()) ) selected @endif>{!! $category['name']!!}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>

                     <div id="bankinformation" @if( (old('roles') && in_array(2, old('roles'))) || in_array(2,$userRoles) ) @else style="display: none;" @endif >
                        <div class="form-group row @if($errors->has('account_holder_name'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="account_holder_name">Account Holder name<span class="required_field">*</span></label>
                              <input type="text" name="account_holder_name" class="form-control" value="{{old('account_holder_name')? old('account_holder_name'): (!empty($bank_info['account_holder_name']) ? $bank_info['account_holder_name']:'') }}">
                              @if($errors->has('account_holder_name'))
                              <span class="error">{{$errors->first('account_holder_name')}}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row @if($errors->has('account_no'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="account_no">Account No.<span class="required_field">*</span></label>
                              <input type="number" name="account_no" class="form-control" value="{{old('account_no') ?old('account_no'):(!empty($bank_info['account_no'])?$bank_info['account_no']:'')  }}">
                              @if($errors->has('account_no'))
                              <span class="error">{{$errors->first('account_no')}}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group row @if($errors->has('bank_name'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="bank_name">Bank Name<span class="required_field">*</span></label>
                              <input type="text" name="bank_name" class="form-control" value="{{old('bank_name') ?old('bank_name'): (!empty($bank_info['bank_name'])?$bank_info['bank_name']:'')}}">
                              @if($errors->has('bank_name'))
                              <span class="error">{{$errors->first('bank_name')}}</span>
                              @endif
                           </div>
                        </div>
                        {{--<div class="form-group row @if($errors->has('bank_address'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="Bank Address">Bank Address<span class="required_field">*</span></label>
                              <textarea class="form-control" name="bank_address">{{old('bank_address')? old('bank_address'): !empty($bank_info['bank_address'])?$bank_info['bank_address']:''}}</textarea>
                              @if($errors->has('bank_address'))
                              <span class="error">{{$errors->first('bank_address')}}</span>
                              @endif
                           </div>
                        </div>--}}

                        <div class="form-group row @if($errors->has('ifs_code'))has-error @endif">
                           <div class="col-lg-9 col-xs-12">
                              <label for="ifs_code">IFSCCode<span class="required_field">*</span></label>
                              <input type="text" name="ifs_code" class="form-control" value="{{old('ifs_code') ?old('ifs_code'): (!empty($bank_info['ifs_code'])?$bank_info['ifs_code']:'')}}">
                              @if($errors->has('ifs_code'))
                              <span class="error">{{$errors->first('ifs_code')}}</span>
                              @endif
                           </div>
                        </div>

                     </div>


                     <div class="form-group">
                        <label for="exampleInputFile">Profile Image</label>
                        <input type="file" id="exampleInputFile" name="profile_image">
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection
