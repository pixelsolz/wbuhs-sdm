@extends('admin.admin-layout')
@section('title', $title)
@section('content')
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Update Profile
      </h1>
      <ol class="breadcrumb">
         <!-- <li><a href="{{ route('user-manage.index') }}"><i class="fa fa-dashboard"></i> User List</a></li> -->
         <!--  <li><a href="#">Forms</a></li> -->
         <!-- <li class="active"> profile update</li> -->
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User Form</h3>
                   @if(Session::has('success'))
                  <div class="alert alert-success">
                     {{ Session('success')}}
                  </div>
                  @endif
                  @if(Session::has('error'))
                  <div class="alert alert-danger">
                     {{ Session('error')}}
                  </div>
                  @endif
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="{{url('admin/profile/update')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <div class="box-body">
                     <div class="form-group row @if($errors->has('f_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="f_name">First Name<span class="required_field">*</span></label>
                           <input type="text" name="f_name" class="form-control" value="{{old('f_name') ?old('f_name'):$user->f_name }}">
                           @if($errors->has('f_name'))
                           <span class="error">{{$errors->first('f_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('l_name'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="l_name">Last Name<span class="required_field">*</span></label>
                           <input type="text" name="l_name" class="form-control" value="{{old('l_name') ? old('l_name'): $user->l_name}}">
                           @if($errors->has('l_name'))
                           <span class="error">{{$errors->first('l_name')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('email'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" name="email" class="form-control" value="{{old('email') ? old('email'): $user->email}}">
                           @if($errors->has('email'))
                           <span class="error">{{$errors->first('email')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('mobile'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="mobile">Mobile<span class="required_field">*</span></label>
                           <input type="number" name="mobile" class="form-control" value="{{old('mobile') ? old('mobile'): $user->userDetail->mobile}}">
                           @if($errors->has('mobile'))
                           <span class="error">{{$errors->first('mobile')}}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row @if($errors->has('address'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="address">Address<span class="required_field">*</span></label>
                           <textarea name="address" class="form-control">{{old('address') ? old('address'): $user->userDetail->address}}</textarea>
                           @if($errors->has('address'))
                           <span class="error">{{$errors->first('address')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('gender'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="gender">Gender<span class="required_field">*</span></label>
                           <select class="form-control" name="gender">
                              <option value="">select</option>
                              <option value="1" @if($user->userDetail->gender==1) selected @endif>Male</option>
                              <option value="2" @if($user->userDetail->gender==2) selected @endif>Female</option>
                           </select>
                           @if($errors->has('gender'))
                           <span class="error">{{$errors->first('gender')}}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row @if($errors->has('password'))has-error @endif">
                        <div class="col-lg-9 col-xs-12">
                           <label for="password">Password<span class="required_field">*</span></label>
                           <input type="password" name="password" class="form-control" value="">
                           @if($errors->has('password'))
                           <span class="error">{{$errors->first('password')}}</span>
                           @endif
                        </div>
                     </div>

                     <!-- <div class="form-group row @if($errors->has('user_type'))has-error @endif">
                        <div class="col-xs-9">
                           <label for="user_type">User Type<span class="required_field">*</span></label>
                           <select class="form-control" name="user_type">
                              <option value="">select</option>
                              <option value="1" @if(old('user_type') && old('user_type')==1? 1: $user->is_admin) selected @endif>Super Admin</option>
                              <option value="2" @if(old('user_type') && old('user_type')==2? 2: $user->is_admin) selected @endif>Admin</option>
                              <option value="3" @if(old('user_type')==3) selected @endif>Student</option>
                           </select>
                           @if($errors->has('user_type'))
                           <span class="error">{{$errors->first('user_type')}}</span>
                           @endif
                        </div>
                     </div> -->

                     <div class="form-group">
                        <label for="exampleInputFile">Profile Image</label>
                        <input type="file" id="exampleInputFile" name="profile_image">
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection