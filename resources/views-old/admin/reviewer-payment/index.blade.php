@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Adjudicator Payment List

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

              <div class="clearfix"></div>

              <div class="form-group row">
                <form action="{{route('reviewerPayment.index')}}" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-4">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, email or mobile" value="{{($request->input_search? $request->input_search:'' )}}">
                  </div>

                  <div class="col-xs-4">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

                </form>
              </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Evaluate Amount</th>
                  <th>Total Amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php $a=1; @endphp
                @forelse($facultyPayment as $facultypay)
                <tr>
                  <td>{{$a}}</td>
                  <td>{{$facultypay->faculty->full_name}}</td>
                  <td>{{$facultypay->faculty->email}}</td>
                  <td>{{$facultypay->faculty->userDetail? $facultypay->faculty->userDetail->mobile: ''}}</td>
                  <td>{{$facultypay->review_amount }}</td>
                  <td>{{$facultypay->total_amount}}</td>
                  @php $url=url('admin/reviewerpayment/update'); @endphp
                  <td><a href="#" data-tog="tooltip" title="Pay & update" onclick="updatepayment('{{$url}}', '{{$facultypay->id}}')"><i class="fa fa-hand-pointer-o"></i></a></td>
                  </tr>
                   @php $a++; @endphp
               @empty
                <tr>
                  <td colspan="8" style="text-align: center;">No data available</td>
                </tr>
               @endforelse
                </tbody>

              </table>
              <div>{{-- $users->links() --}}</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<!-- Modal -->
  <div class="modal fade" id="payModal" role="dialog">
    <div class="modal-dialog modal-sm">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  @endsection
