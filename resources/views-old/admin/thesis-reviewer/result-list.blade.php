@extends('admin.admin-layout')
@section('title', $title)
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dessertation Review
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @if(Session::has('msg'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('msg')}}
              </div>
              @elseif(Session::has('error'))
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('error')}}
              </div>

              @endif

            </div>
            <!-- /.box-header -->

            <div class="box-body">
               <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Registration No. & Year</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <th>Action</th>
                  <th>Status</th>
                  <th>Created At</th>
                  <th>Final Comment</th>
                 <!--  <th>Remarks</th> -->
                  <th>Submitted Date</th>
                </tr>
                </thead>
                <tbody>
                @php $count=1; @endphp
                @forelse($thesisData as $thesis)
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$thesis->name_of_student}}</td>
                    <td>{{$thesis->registration_no. ' '.$thesis->registration_year}}</td>
                    <td>{{$thesis->institute_name}}</td>
                    <td>{{$thesis->mobile_landline}}</td>
                    <td>

                    @if($thesis->thesisResult()->facultyWiseResult($faculty->id, 'grade'))
                     <a href="{{url('admin/student-result/edit',['thesis'=>$thesis->id])}}" data-tog="tooltip" title="Edit Garde"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     @else
                     @php
                      $transaction = $thesis->transaction()->where('to_user_id',$faculty->id)->first();

                      $resultData ='';
                      if($transaction && $transaction->is_reassigned ==1){
                        $resultData = $thesis->thesisResult()->where('faculty_id',$faculty->id)->where('transaction_reassigned_group',$transaction->reassigned_group)->first();
                      }elseif($transaction && $transaction->status ==4){
                       $resultData = $thesis->thesisResult()->where('faculty_id',$faculty->id)->first();
                      }
                     @endphp
                     <a href="{{($resultData)?url('admin/student-result/view',['result'=>$resultData->id]) :url('admin/student-result/add',['thesis'=>$thesis->id])}}" data-tog="tooltip" title="See Evaluation"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></a>
                     @endif
                      &nbsp;&nbsp;<a href="{{url('admin/student-synopsis/view',['thesis_id'=>$thesis->id])}}" target="_blank" data-tog="tooltip" title="View Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                    </td>
                    <td>{{$thesis->transaction()->facultyData($faculty->id)->status_text}}
                    {{-- <a href="#" data-tog="tooltip" title="Change status" data-toggle="modal" id="status_modal_open" data-target="#status_modal" data-transId="{{$thesis->transaction()->facultyData($faculty->id)->id}}" data-trans-status="{{$thesis->transaction()->facultyData($faculty->id)->status}}"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></a> --}}
                    </td>
                    <td>{{\Carbon\Carbon::parse($thesis->created_at)->format('d/m/Y')}}</td>
                    <td>{{$thesis->thesisResult()->facultyWiseResult($faculty->id, 'final_comment')}}</td>
                    {{--<td>{{$thesis->thesisResult()->facultyWiseResult($faculty->id, 'remarks')}}</td>--}}
                    <td>{{$thesis->thesisResult()->facultyWiseResult($faculty->id, 'created_at')}}</td>
                  </tr>
                  @php $count++; @endphp
               @empty
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               @endforelse


                </tbody>

              </table>
            </div>
              <div>{{ $thesisData->links() }}</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>

        <div class="modal fade" id="status_modal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Status</h4>
              </div>
              <div class="modal-body">
              <form action="{{url('admin/student-result/status-change')}}" id="status-change-form">
                  @csrf
                  <div class="form-group">
                    <label>Status change:</label>
                    <select name="status" class="form-control">
                        <option value="">select</option>
                        <option value="1">On Process</option>
                        <option value="2">Access</option>
                        <option value="3">Submit</option>
                    </select>
                  </div>
                  <input type="hidden" name="transaction_id">
              </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-disable" onclick="changeThesisStatus()">Submit &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

  @endsection
