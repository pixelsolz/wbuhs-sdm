<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$title}}</title>



    <!-- <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"> -->

   <link rel="stylesheet" href="{{asset('public/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <link href="{{ asset('public/css/login-style.css')}}" rel="stylesheet">

    <style type="text/css">
      .error{
        color: red;
        font-size: 11px;
      }
    </style>

  </head>

  <body>

    <div class="body-wrapper">

        <div class="login-sec">

            <div class="logo-sec">

                <a class="logo" href="#" title="STS"><img src="{{ asset('public/img/img_logo.png')}}" alt="STS"></a>

            </div>

            <div class="form-sec">

              <div>

                <h2 class="title1">Adjudicator Registration</h2>
                @if(session()->has('error'))
                    <div class="alert alert-{{ session('error')}}">
                    {!! session('error') !!}
                    </div>
                @endif

                <form method="post" action="{{route('admin.register.store')}}" enctype="multipart/form-data">
                       @csrf
                    <div class="form-group">

                      <input class="form-control user" type="text" name="f_name" value="{{ old('f_name') }}"  placeholder="First name"  />
                        @if($errors->has('f_name'))
                           <span class="error">{{$errors->first('f_name')}}</span>
                        @endif
                    </div>

                    <div class="form-group">

                      <input class="form-control user" type="text" name="l_name" value="{{ old('l_name') }}"  placeholder="Last name"  />
                        @if($errors->has('l_name'))
                           <span class="error">{{$errors->first('l_name')}}</span>
                        @endif
                    </div>


                    <div class="form-group">

                      <input class="form-control user" type="email" name="email" value="{{ old('email') }}"  placeholder="Email"  />
                        @if($errors->has('email'))
                           <span class="error">{{$errors->first('email')}}</span>
                        @endif
                    </div>

                    <div class="form-group">

                      <input class="form-control user" type="text" name="mobile" value="{{ old('mobile') }}"  placeholder="Mobile"  />
                        @if($errors->has('mobile'))
                           <span class="error">{{$errors->first('mobile')}}</span>
                        @endif
                    </div>

                    <div class="form-group">

                      <textarea class="form-control user" name="address" placeholder="Address">{{old('address')?old('address'):''}}</textarea>
                        @if($errors->has('address'))
                           <span class="error">{{$errors->first('address')}}</span>
                        @endif
                    </div>

                    <div class="form-group">
                      <select class="form-control user" name="city_id">
                        <option value="">select city</option>
                        @foreach($cities as $city)
                        <option value="{{$city->id}}" @if(old('city_id') && old('city_id') ==$city->id) selected @endif>{{$city->name}}</option>
                        @endforeach
                      </select>
                        @if($errors->has('gender'))
                           <span class="error">{{$errors->first('gender')}}</span>
                        @endif
                    </div>

                    <div class="form-group">
                      <select class="form-control user" name="gender">
                        <option value="">select gender</option>
                        <option value="1" @if(old('gender') && old('gender') =='1') selected @endif>Male</option>
                        <option value="2" @if(old('gender') && old('gender') =='2') selected @endif>Female</option>
                      </select>
                        @if($errors->has('gender'))
                           <span class="error">{{$errors->first('gender')}}</span>
                        @endif
                    </div>

                    <div class="form-group">
                      <select class="form-control user" name="category[]" multiple="true">
                        <option value="">select speciality</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                      </select>
                        @if($errors->has('category'))
                           <span class="error">{{$errors->first('category')}}</span>
                        @endif
                    </div>

                    <div class="form-group">
                    <input class="form-control user" type="file" name="profile_image">

                    </div>

                    <div class="form-group">

                      <input class="form-control pass" type="password" name="password"  placeholder="Password" >
                        @if($errors->has('password'))
                           <span class="error">{{$errors->first('password')}}</span>
                        @endif
                    </div>

                    <div class="form-group">

                      <input class="form-control pass" type="password" name="password_confirmation"placeholder="Confirm password" >
                        @if($errors->has('password_confirmation'))
                           <span class="error">{{$errors->first('password_confirmation')}}</span>
                        @endif
                    </div>

                    <div class="form-group">
                    <input class="form-control user" type="text" name="account_holder_name" placeholder="Account holder name">
                      @if($errors->has('account_holder_name'))
                        <span class="error">{{$errors->first('account_holder_name')}}</span>
                      @endif
                    </div>

                    <div class="form-group">
                    <input class="form-control user" type="text" name="account_no" placeholder="Account No.">
                      @if($errors->has('account_no'))
                        <span class="error">{{$errors->first('account_no')}}</span>
                      @endif
                    </div>

                    <div class="form-group">
                    <input class="form-control user" type="text" name="bank_name" placeholder="Bank name">
                       @if($errors->has('bank_name'))
                          <span class="error">{{$errors->first('bank_name')}}</span>
                        @endif
                    </div>

                    <div class="form-group">
                    <textarea class="form-control user" name="bank_address" placeholder="Bank address"></textarea>
                        @if($errors->has('bank_address'))
                          <span class="error">{{$errors->first('bank_address')}}</span>
                        @endif
                    </div>

                   <div class="form-group">
                    <input class="form-control user" type="text" name="ifs_code" placeholder="IFS code">
                      @if($errors->has('ifs_code'))
                          <span class="error">{{$errors->first('ifs_code')}}</span>
                      @endif
                    </div>

                     <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="{{config('app.google_recaptcha_key')}}"></div>
                        <span class="error" id="grecaptcha_response"></span>
                      </div>


                    <div class="form-group">

                      <input class="form-control" type="submit" value="Register" />

                    </div>

                </form>

              </div>

            </div>

        </div>

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="{{asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>



  </body>

</html>