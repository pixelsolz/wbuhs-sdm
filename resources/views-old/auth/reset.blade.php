@extends('frontend.layout')

@section('content')
 <section class="student-thsis">
               <div class="container">
                   <div class="thsis-content">
                        <h3>Reset Password</h3>
                   </div>
               </div>
           </section>

           <section class="thsis-frm-rgtr-sec">
               <div class="container">

                <div class="ths-frm-wrp">


                    <div class="frm-wrpd">

                       <form method="post" action="{{url('reset-password')}}">
                       @csrf
                       <input type="hidden" name="email" value="{{$email}}">
                       <input type="hidden" name="token" value="{{$token}}">
                    <div class="form-group @if($errors->first('password')) has-error  @endif">
                       <label>Password</label>
                      <input class="form-control pass" type="password" name="password" value="" placeholder="Password" required>

                     {{--  @if($errors->first('password'))
                        <span class="error">{{$errors->first('password')}}</span>
                      @endif--}}
                    </div>

                    <div class="form-group @if($errors->first('password_confirmation')) has-error  @endif">
                       <label>Confirm Password</label>
                      <input class="form-control pass" type="password" name="password_confirmation" value="" placeholder="Confirm Password" required>
                       {{--@if($errors->first('password_confirmation'))
                        <span class="error">{{$errors->first('password_confirmation')}}</span>
                      @endif --}}
                    </div>

                    <div class="form-group">

                      <input class="form-control" type="submit" value="Reset Password" />

                    </div>
                </form>

                    </div>

                </div>

               </div>
           </section>


@endsection












