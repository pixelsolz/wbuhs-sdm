@extends('frontend.layout')
@section('content')

<style type="text/css">
  .reg-error{
    color: #c11a05;
    font-size: 13px;
    line-height: 18px;
    display: block;
    margin: 0 0 20px 0;
  }

</style>
 <section class="student-thsis">
               <div class="container">
                    <div class="thsis-content">
                        <h3>Synopsis & Dissertation</h3>
                            <p>Students can register on the WBUHS portal by logging in with their user ID and password. This registration number will be required for viewing exam results or submitting a thesis for assessment. </p>
                   </div>
               </div>
           </section>

           <section class="thsis-frm-rgtr-sec">
               <div class="container">

                <div class="ths-frm-wrp">

                     <div class="anchr-wrp">
                        <a href="{{url('login')}}" class="btn btn-primary login">Login</a>
                        <a href="{{url('registration')}}" class="btn btn-primary rgtr active">Register</a>

                    </div>


                     <div class="frm-wrpd-rgstr">

                       <form method="post" action="{{url('registration')}}" enctype="multipart/form-data" id="ajax-form" data-redirectUrl="{{url('/login')}}">
                        @csrf
                        <div class="form-group f_name">
                            <label>First Name <em>*</em></label>
                            <input type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" placeholder="Enter First Name">
                             <span class="error" id="f_name"></span>
                        </div>
                        <div class="form-group l_name">
                            <label>Last Name <em>*</em></label>
                            <input type="text" class="form-control "  name="l_name" value="{{ old('l_name') }}" placeholder="Enter Last Name">
                             <span class="error" id="l_name"></span>
                        </div>

                        <div class="form-group email">
                            <label>Email <em>*</em></label>
                            <input type="Email" class="form-control " name="email" value="{{ old('email') }}" placeholder="Enter Email">
                            <span class="error" id="email"></span>
                        </div>
                        <div class="form-group mobile">
                            <label>Mobile No. <em>*</em></label>
                            <input type="tel" class="form-control " name="mobile" value="{{ old('mobile') }}" placeholder="Enter Mobile">
                            <span class="error" id="mobile"></span>
                        </div>

                        <div class="form-group address">
                        <label>Address <em>*</em></label>
                      <textarea name="address" class="form-control " placeholder="Enter Address">{{old('address')}}</textarea>

                        <span class="error" id="address"></span>

                       </div>

                       <div class="form-group city_id">
                        <label>City <em>*</em></label>
                        <select class="form-control" name="city_id">
                          <option value="">Select</option>
                          @foreach($cities as $city)
                          <option value="{{$city->id}}">{{$city->name}}</option>
                          @endforeach
                        </select>

                        <span class="error" id="city_id"></span>

                       </div>

                      <div class="form-group gender">
                      <label>Gender <em>*</em></label>
                        <select class="form-control " name="gender">
                          <option value="">Select</option>
                          <option value="1">Male</option>
                          <option value="2">Female</option>
                        </select>

                        <span class="error" id="gender"></span>

                      </div>


                        <div class="form-group password">
                            <label>Password <em>*</em></label>
                            <input type="password" class="form-control " name="password" placeholder="Enter Password">
                             <span class="error" id="password"></span>
                        </div>

                        <div class="form-group">
                            <label>Confirm Password <em>*</em></label>
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Enter Password confirmation">
                            <span class="error" id="password_confirmation"></span>
                        </div>
                        <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="{{config('app.google_recaptcha_key')}}"></div>
                        <span class="error" id="grecaptcha_response"></span>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-primary btn-disable" type="submit"> Submit &nbsp;<i class="fa fa-spinner fa-spin" id="submit-loader" style="font-size:24px; display: none;"></i></button>
                        </div>
                    </form>
                    </div>


                </div>

               </div>
           </section>


@endsection