

<!DOCTYPE html>

<html>
<head>
    <title>WBUHS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px 0; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="">
         {{--<tr>
          <td align="center" valign="middle" style="position: relative; padding: 10px 0;">
            <a href="https://urentme.com" style="display: inline-block;"><img src="{{url('public/img/img_logo.png')}}" title="URENT_ME" alt="urent_me"/></a>
          </td>
        </tr>--}}
        <tr>
          <td align="center" valign="middle" style="position: relative;">
            <h3 style="margin: 0; color: #2b2b2b; font-size: 28px; line-height: 30px; font-weight: bold; padding: 20px 0 10px;">
              @if($status==2)
                {{'Synopsis Approved'}}
              @elseif($status==3)
                {{'Synopsis Rejected'}}
              @else
                {{'Synopsis Resubmitted'}}
              @endif
            </h3>
          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">
          	@if($status==2)
            <p style="margin-bottom: 20px;">Your Synopsis has been approved. You can submit Thesis.</p>
            @elseif($status==3)
            <p style="margin-bottom: 20px;">Your Synopsis has been rejected.<br> Reason: {{$reason}}</p>
            @else
            <p style="margin-bottom: 20px;">Your Synopsis has been resubmitted.<br> Reason: {{$reason}}</p>
            @endif
          </td>
        </tr>

      </td>
    </tr>

    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; ">
          Copyright © 2018 WBUHS
      </td>
    </tr>
</table>
</body>
</html>



