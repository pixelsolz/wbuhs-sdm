<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToSynopsesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('synopses', function (Blueprint $table) {
			$table->tinyInteger('status')->nullable()->comment('1=process,2=accept,3=not accept')->after('student_pdf');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('synopses', function (Blueprint $table) {
			$table->tinyInteger('status')->nullable()->comment('1=process,2=accept,3=not accept')->after('student_pdf');
		});
	}
}
