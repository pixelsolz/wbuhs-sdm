<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSynopsesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('synopses', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('category_id');
			$table->unsignedInteger('user_id');
			$table->string('name_of_student');
			$table->string('registration_no');
			$table->string('registration_year');
			$table->string('institute_name');
			$table->string('mobile_landline')->nullable();
			$table->string('email')->nullable();
			$table->string('guide_name')->nullable();
			$table->string('guide_designation')->nullable();
			$table->string('co_guide_name')->nullable();
			$table->string('co_guide_designation')->nullable();
			$table->string('proposed_title_thesis')->nullable();
			$table->string('proposed_work_place')->nullable();
			$table->string('student_signature')->nullable();
			$table->timestamps();

			$table->foreign('category_id')->references('id')->on('categories');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('synopses');
	}
}
