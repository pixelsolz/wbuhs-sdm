<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSynopsisTransactionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('synopsis_transactions', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('synopsis_id');
			$table->string('to_user_id')->nullable();
			$table->tinyInteger('trans_type')->nullable()->comment('1=email,2=sms');
			$table->string('send_to')->nullable();
			$table->timestamps();

			$table->foreign('synopsis_id')->references('id')->on('synopses');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('synopsis_transactions');
	}
}
