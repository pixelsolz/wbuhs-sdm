<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSignatureUploadToSynopsesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('synopses', function (Blueprint $table) {
			$table->string('student_pdf')->after('student_signature')->nullable();
			$table->string('form_pdf')->after('student_pdf')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('synopses', function (Blueprint $table) {
			$table->string('student_pdf')->after('student_signature');
			$table->string('form_pdf')->after('student_pdf');
		});
	}
}
