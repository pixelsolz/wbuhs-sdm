<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreatedByToSynopsisTransactionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('synopsis_transactions', function (Blueprint $table) {
			$table->integer('created_by')->after('send_to');
			$table->integer('updated_by')->nullable()->after('created_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('synopsis_transactions', function (Blueprint $table) {
			$table->integer('created_by')->after('send_to');
			$table->integer('updated_by')->after('created_by');
		});
	}
}
