<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('user_details', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->text('address')->nullable();
			$table->string('mobile')->nullable();
			$table->tinyInteger('gender')->default(1)->comment('1=male,2=female');
			$table->string('profile_image')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('user_details');
	}
}
