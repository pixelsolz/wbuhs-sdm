<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccknowledgeLinkToThesisTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('thesis_transactions', function (Blueprint $table) {
            $table->string('accknowledge_link')->nullable()->after('status');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('thesis_transactions', function (Blueprint $table) {
            $table->string('accknowledge_link')->nullable()->after('status');
        });
    }
}
