<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$user = User::create(['f_name' => 'superadmin', 'l_name' => '', 'email' => 'superadmin@gmail.com','email_verified_at' => Carbon::now(), 'password' => bcrypt('superadmin@123#'), 'is_admin' => 1]);
		//$role = Role::find(1);
		$user->roles()->attach(1);
	}
}
