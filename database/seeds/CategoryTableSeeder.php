<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder {

	public $array = [
		['name' => 'Modern Medicine', 'slug' => 'modern_medicine', 'parent' => 0, 'has_child' => 1],
		['name' => 'Masters Degree', 'slug' => 'masters_degree', 'parent' => 1, 'has_child' => 1],
		['name' => 'Anaesthesiology', 'slug' => 'anaesthesiology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Anatomy', 'slug' => 'anatomy', 'parent' => 2, 'has_child' => 0],
		['name' => 'Biochemistry', 'slug' => 'biochemistry', 'parent' => 2, 'has_child' => 0],
		['name' => 'Community Medicine', 'slug' => 'community_medicine', 'parent' => 2, 'has_child' => 0],
		['name' => 'Dermatology', 'slug' => 'dermatology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Forensic and State Medicine', 'slug' => 'forensic_and_state_medicine', 'parent' => 2, 'has_child' => 0],
		['name' => 'General Medicine', 'slug' => 'general_medicine', 'parent' => 2, 'has_child' => 0],
		['name' => 'General Surgery', 'slug' => 'general_surgery', 'parent' => 2, 'has_child' => 0],
		['name' => 'Gynaecology and Obstetrics', 'slug' => 'gynaecology_and_obstetrics', 'parent' => 2, 'has_child' => 0],
		['name' => 'Microbiology', 'slug' => 'microbiology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Ophthalmology', 'slug' => 'ophthalmology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Orthopaedics', 'slug' => 'orthopaedics', 'parent' => 2, 'has_child' => 0],
		['name' => 'Otolaryngology', 'slug' => 'otolaryngology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Paediatrics', 'slug' => 'paediatrics', 'parent' => 2, 'has_child' => 0],
		['name' => 'Pathology', 'slug' => 'pathology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Pharmacology', 'slug' => 'Pharmacology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Physical Medicine and', 'slug' => 'physical_medicine_and', 'parent' => 2, 'has_child' => 0],
		['name' => 'Physical Medicine and Rehabilitation', 'slug' => 'physical_medicine_and_rehabilitation', 'parent' => 2, 'has_child' => 0],
		['name' => 'Physiology', 'slug' => 'physiology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Pulmonology', 'slug' => 'pulmonology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Psychiatry', 'slug' => 'psychiatry', 'parent' => 2, 'has_child' => 0],
		['name' => 'Radiodiagnosis', 'slug' => 'radiodiagnosis', 'parent' => 2, 'has_child' => 0],
		['name' => 'Radiotherapy', 'slug' => 'radiotherapy', 'parent' => 2, 'has_child' => 0],
		['name' => 'Transfusion Medicine and Immunohaematology', 'slug' => 'transfusion_medicine_and_immunohaematology', 'parent' => 2, 'has_child' => 0],
		['name' => 'Tropical Medicine', 'slug' => 'tropical_medicine', 'parent' => 2, 'has_child' => 0],
		['name' => 'Public Health', 'slug' => 'public_health', 'parent' => 2, 'has_child' => 0],

		['name' => 'Post Doctoral Degree', 'slug' => 'post_doctoral_degree', 'parent' => 1, 'has_child' => 1],
		['name' => 'Cardiac Anaesthesia', 'slug' => 'cardiac_anaesthesia', 'parent' => 29, 'has_child' => 0],
		['name' => 'Cardio-thoracic Surgery', 'slug' => 'cardiac_anaesthesia', 'parent' => 29, 'has_child' => 0],
		['name' => 'Cardiology', 'slug' => 'cardiology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Clinical Haematology', 'slug' => 'clinical_haematology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Clinical Pharmacology', 'slug' => 'clinical_pharmacology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Gastroenterology', 'slug' => 'gastroenterology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Endocrinology and Metabolism', 'slug' => 'endocrinology_and_metabolism', 'parent' => 29, 'has_child' => 1],
		['name' => 'Neonatology', 'slug' => 'neonatology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Nephrology', 'slug' => 'nephrology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Neurology', 'slug' => 'neurology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Rheumatology', 'slug' => 'rheumatology', 'parent' => 29, 'has_child' => 0],
		['name' => 'Neurosurgery', 'slug' => 'neurosurgery', 'parent' => 29, 'has_child' => 0],
		['name' => 'Paediatric Surgery', 'slug' => 'paediatric_surgery', 'parent' => 29, 'has_child' => 0],
		['name' => 'Plastic Surgery', 'slug' => 'plastic_surgery', 'parent' => 29, 'has_child' => 0],
		['name' => 'Urosurgery', 'slug' => 'urosurgery', 'parent' => 29, 'has_child' => 0],

		['name' => 'Ayush', 'slug' => 'ayush', 'parent' => 0, 'has_child' => 1],
		['name' => 'Ayurved', 'slug' => 'ayurved', 'parent' => 45, 'has_child' => 1],
		['name' => 'Ayurved Sanghita and Siddhanta', 'slug' => 'ayurved_sanghita_and_siddhanta', 'parent' => 46, 'has_child' => 0],
		['name' => 'Dravyaguna', 'slug' => 'dravyaguna', 'parent' => 46, 'has_child' => 0],
		['name' => 'Kayachikitsa', 'slug' => 'kayachikitsa', 'parent' => 46, 'has_child' => 0],
		['name' => 'Panchakarma', 'slug' => 'panchakarma', 'parent' => 46, 'has_child' => 0],
		['name' => 'Rog Nidhan', 'slug' => 'rog_nidhan', 'parent' => 46, 'has_child' => 0],

		['name' => 'Homeopathy', 'slug' => 'homeopathy', 'parent' => 45, 'has_child' => 0],
		['name' => 'Case Taking and Repertory', 'slug' => 'case_taking_and_repertory', 'parent' => 52, 'has_child' => 0],
		['name' => 'Homeopathic Materia Medica', 'slug' => 'homeopathic_materia_medica', 'parent' => 52, 'has_child' => 0],
		['name' => 'Homeopathic Pharmacy', 'slug' => 'homeopathic_pharmacy', 'parent' => 52, 'has_child' => 0],
		['name' => 'Organon of Medicine and Homeopathic Philosophy', 'slug' => 'organon_of_medicine_and_homeopathic_philosophy', 'parent' => 52, 'has_child' => 0],
		['name' => 'Paediatrics', 'slug' => 'paediatrics', 'parent' => 52, 'has_child' => 0],
		['name' => 'Practice of Medicine', 'slug' => 'practice_of_medicine', 'parent' => 52, 'has_child' => 0],

		['name' => 'Dental', 'slug' => 'dental', 'parent' => 0, 'has_child' => 1],
		['name' => 'MDS', 'slug' => 'MDS', 'parent' => 59, 'has_child' => 1],
		['name' => 'Conservative Dentistry', 'slug' => 'conservative_dentistry', 'parent' => 60, 'has_child' => 0],
		['name' => 'Oral Medicine and Radiology', 'slug' => 'oral_medicine_and_radiology', 'parent' => 60, 'has_child' => 0],
		['name' => 'Oral and Maxillofacial Surgery', 'slug' => 'oral_and_maxillofacial_surgery', 'parent' => 60, 'has_child' => 0],
		['name' => 'Oral Pathology and Microbiology', 'slug' => 'oral_pathology_and_microbiology', 'parent' => 60, 'has_child' => 0],
		['name' => 'Orthodontics', 'slug' => 'orthodontics', 'parent' => 60, 'has_child' => 0],
		['name' => 'Paedodontics', 'slug' => 'paedodontics', 'parent' => 60, 'has_child' => 0],
		['name' => 'Periodontics', 'slug' => 'periodontics', 'parent' => 60, 'has_child' => 0],
		['name' => 'Prosthetic Dentistry', 'slug' => 'prosthetic_dentistry', 'parent' => 60, 'has_child' => 0],
		['name' => 'Public Health Dentistry', 'slug' => 'public_health_dentistry', 'parent' => 60, 'has_child' => 0],

		['name' => 'Nursing', 'slug' => 'nursing', 'parent' => 0, 'has_child' => 1],
		['name' => 'M Sc (Nursing)', 'slug' => 'msc_nursing', 'parent' => 70, 'has_child' => 1],

		['name' => 'Community Health Nursing', 'slug' => 'community_health_nursing', 'parent' => 71, 'has_child' => 0],
		['name' => 'Medical Surgical Nursing', 'slug' => 'medical_surgical_nursing', 'parent' => 71, 'has_child' => 0],
		['name' => 'Paediatric Nursing', 'slug' => 'paediatric_nursing', 'parent' => 71, 'has_child' => 0],
		['name' => 'Psychiatric Nursing', 'slug' => 'psychiatric_nursing', 'parent' => 71, 'has_child' => 0],

		['name' => 'Paramedical and Allied', 'slug' => 'paramedical_and_allied', 'parent' => 0, 'has_child' => 0],
		['name' => 'Master of Prosthetics and Orthotics', 'slug' => 'master_of_prosthetics_and_orthotics', 'parent' => 76, 'has_child' => 0],
		['name' => 'Master of Orthotic Therapy', 'slug' => 'master_of_orthotic_therapy', 'parent' => 76, 'has_child' => 0],
		['name' => 'Master of Physical Therapy', 'slug' => 'master_of_physical_therapy', 'parent' => 76, 'has_child' => 0],
		['name' => 'Master of Audiology and Speech Language Pathology', 'slug' => 'master_of_audiology_and_speech_language_pathology', 'parent' => 76, 'has_child' => 0],
	];
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		foreach ($this->array as $value) {
			Category::create($value);
		}
	}
}
