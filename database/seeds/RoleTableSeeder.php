<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Role::create(['name' => 'Super Admin', 'slug' => 'superadmin', 'permissions' => json_encode(['all' => 'true'])]);
	}
}
