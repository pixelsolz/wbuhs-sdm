<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public $permissionArray = [
		['name' => 'user-list', 'label' => 'User List', 'model_name' => 'users'],
		['name' => 'user-create', 'label' => 'User Create', 'model_name' => 'users'],
		['name' => 'user-edit', 'label' => 'User Edit', 'model_name' => 'users'],
		['name' => 'user-delete', 'label' => 'User Delete', 'model_name' => 'users'],
		['name' => 'role-list', 'label' => 'Role List', 'model_name' => 'roles'],
		['name' => 'role-create', 'label' => 'Role Create', 'model_name' => 'roles'],
		['name' => 'role-edit', 'label' => 'Role Edit', 'model_name' => 'roles'],
		['name' => 'role-delete', 'label' => 'Role Delete', 'model_name' => 'roles'],
		['name' => 'category-list', 'label' => 'Category List', 'model_name' => 'category'],
		['name' => 'category-create', 'label' => 'Category Create', 'model_name' => 'category'],
		['name' => 'category-edit', 'label' => 'Category Edit', 'model_name' => 'category'],
		['name' => 'category-delete', 'label' => 'Category delete', 'model_name' => 'category'],
		['name' => 'student-synopsis', 'label' => 'Student Synopsis', 'model_name' => 'student-synopsis'],
	];

	public function run() {
		foreach ($this->permissionArray as $key => $value) {
			Permission::create($value);
		}

	}
}
