<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
  .inactive_menu {
  background-color: #F8F8F8;
}
</style>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User List

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>

              <a href="<?php echo e(route('user-manage.create')); ?>" class="btn btn-primary newline-class">Create New User</a>
               <div class="clearfix"></div>
              <div class="form-group row">
              <form action="<?php echo e(route('user-manage.index')); ?>" method="get">
                <input type="hidden" name="search" value="search">
              <div class="col-xs-2">
                  <select class="form-control" name="search_by_role">
                    <option value="">Select Role</option>
                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($role->id); ?>" <?php if(!empty($request->search_by_role) && $request->search_by_role == $role->id): ?> selected <?php endif; ?> ><?php echo e($role->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>

                <div class="col-xs-3">

                    <input type="text" class="form-control" name="input_search" placeholder="Search by name email or mobile" value="<?php echo e(!empty($request->input_search)?$request->input_search: ''); ?>">

                </div>
                  <?php $catUrl = url('admin/get-child/category'); ?>
                  <div class="col-xs-2" id="parent_div_cat">
                    <select class="form-control" name="category" onchange="getCategories(this.value, '<?php echo e($catUrl); ?>')">
                        <option value="">All</option>
                        <?php $__currentLoopData = $parent_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option value="<?php echo e($parent->id); ?>" <?php if($request->category &&  $request->category==$parent->id): ?> selected <?php endif; ?> ><?php echo e($parent->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      
                    </select>
                  </div>
                <div class="col-xs-1">
                <button type="submit" class="btn btn-info">Search</button>
                </div>
                </form>
                </div>
                <div class="form-group row">
                  <div class="col-xs-3">
                    <?php
                    $type = $request->input('search_by_role')?$request->input('search_by_role'):'';
                    ?>
                <a href="<?php echo e(url('admin/export/user-list?type='.$type)); ?>" class="btn btn-info">Export User</a>
                </div>
                </div>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example123" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="5%">Sl No.</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>City</th>
                  <th>Status</th>
                  <th>Roles</th>
                  <th>Set Default Password</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $a=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr class="<?php if($user->status=='Inactive'): ?> inactive_menu <?php endif; ?>">
                  <td><?php echo e($a); ?></td>
                  <td><?php echo e($user->full_name); ?></td>
                  <td><a href="mailto:<?php echo e($user->email); ?>" ><?php echo e($user->email); ?></a></td>
                  <td><?php echo e($user->userDetail? $user->userDetail->mobile: ''); ?></td>
                  <td><a href="#" data-toggle-pop="popover" data-content="<?php echo @$user->userDetail->address; ?>" data-html="true" title="Address"><?php echo e($user->userDetail? $user->userDetail->city_text: ''); ?></a></td>
                  <td><?php echo e($user->status); ?></td>
                  <td><?php echo e($user->roles ? implode(', ',$user->roles->pluck('name')->toArray()): ''); ?>

                  <?php if(in_array('student',$user->roles->pluck('slug')->toArray())): ?>
                    <?php echo e($user->checkStdSubmission($user->id)); ?>

                  <?php endif; ?>
                  <?php if(in_array('adjudicator',$user->roles->pluck('slug')->toArray()) || in_array('bos',$user->roles->pluck('slug')->toArray())): ?>
                  <?php $specializationUrl = url('admin/view-category',[$user->id]); ?>
                  <a href="#" data-toggle="modal" data-target="#view-speciality" data-tog="tooltip" title="View Speciality" onclick="viewSpeciality('<?php echo e($specializationUrl); ?>')"><i class="fa fa-hand-pointer-o"></i></a>
                  <?php endif; ?>
                  </td>
                  <td><a href="<?php echo e(route('user-manage.set_dafault.password',$user->id)); ?>">click</a></td>
                  <td>
                  <?php if($user->is_admin!= 1): ?>
                   <a href="<?php echo e(route('user-manage.edit', ['id'=>$user->id])); ?>" class="glyphicon glyphicon-pencil" data-tog="tooltip" title="Edit"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <?php
                  $deleteroute = route('user-manage.destroy', ['id'=>$user->id]);
                ?>
                <?php if($user->status =='Active'): ?>
                    <a href="javascript:void(0)" onclick="deleteData('<?php echo e($deleteroute); ?>','<?php echo e(csrf_token()); ?>')" class="glyphicon glyphicon-remove" data-tog="tooltip" title="Delete"></a>
                  <?php endif; ?>
                   <?php endif; ?>
                  </td>
                  </tr>
                   <?php $a++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>
                </tbody>

              </table>
            </div>
              <div><?php echo e($users->links()); ?></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

         <div class="modal fade" id="view-speciality">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Speciality</h4>
              </div>
              <div class="modal-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Sl No.</th>
                      <th>Name</th>
                    </tr>
                  </thead>
                  <tbody id="specialze-tbody">

                  </tbody>
                </table>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>