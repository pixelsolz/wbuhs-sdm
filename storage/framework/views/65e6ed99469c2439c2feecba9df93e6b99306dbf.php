<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Synopsis
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>

              <div class="clearfix"></div>
              <?php if(@\Auth::guard('admin')->user()->is_admin ==1): ?>
                <form action="<?php echo e(route('admin.student-synopsis')); ?>" method="get">
                <div class="form-group row">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-2">
                    <select class="form-control" name="search_by_status">
                      <option value="">All</option>
                      <option value="5" <?php if($request->search_by_status && $request->search_by_status ==5): ?> selected <?php endif; ?>>Not Proccesed</option>
                      <option value="1" <?php if($request->search_by_status && $request->search_by_status ==1): ?> selected <?php endif; ?>>Proccesed</option>
                      <option value="2" <?php if($request->search_by_status && $request->search_by_status ==2): ?> selected <?php endif; ?>>Approved</option>
                      <option value="3" <?php if($request->search_by_status && $request->search_by_status ==3): ?> selected <?php endif; ?>>Rejected</option>
                      <option value="4" <?php if($request->search_by_status && $request->search_by_status ==4): ?> selected <?php endif; ?>>Under Revision</option>

                    </select>
                  </div>
                  <div class="col-xs-4">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, Reg no., year, institute name or mobile" value="<?php echo e(($request->input_search? $request->input_search:'' )); ?>">
                  </div>
                  <?php $catUrl = url('admin/get-child/category'); ?>
                    <div class="col-xs-2" id="parent_div_cat">
                        <select class="form-control" name="category" onchange="getCategories(this.value, '<?php echo e($catUrl); ?>')">
                            <option value="">Select</option>
                            <?php $__currentLoopData = $parent_cats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($parent->id); ?>" <?php if($request->category &&  $request->category==$parent->id): ?> selected <?php endif; ?> ><?php echo e($parent->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </select>
                    </div>

                  </div>
                  <?php $category =@$request->category;
                  $courseId = @$request->child_sub_cat;
                  $child_catId = @$request->child_cat;
                  $from_date =  @$request->from_date;
                  $to_date = @$request->to_date;
                  $searchBy_status = @$request->search_by_status;
                  ?>
                  <div class="form-group row">
                     <div class="col-xs-2">
                   <input class="form-control datepicker" type="text" name="from_date" placeholder="From Date" autocomplete="off" <?php if(!empty($request->from_date)): ?> value="<?php echo e(@$request->from_date); ?>" <?php endif; ?>>
                  </div>

                  <div class="col-xs-2">
                   <input class="form-control datepicker" type="text" name="to_date" placeholder="To Date" autocomplete="off" <?php if(!empty($request->to_date)): ?> value="<?php echo e(@$request->to_date); ?>" <?php endif; ?>>
                  </div>
                  <div class="col-xs-2">
                    <select class="form-control" name="assign_status">
                      <option value="">select status</option>
                      <option value="1" <?php if($request->assign_status &&  $request->assign_status==1): ?> selected <?php endif; ?>>Assign</option>
                      <option value="2" <?php if($request->assign_status &&  $request->assign_status==2): ?> selected <?php endif; ?>>Notassign</option>
                    </select>
                  </div>
                    <div class="col-xs-1">
                      <button type="submit" class="btn btn-info">Search</button>
                    </div>
                    <div class="col-xs-2">
                      <a href="<?php echo e(url('admin/synopsis/assign-student/export?child_catId='.$child_catId.'&courseId='.$courseId.'&category='.$category.'&from_date='.$from_date.'&to_date='.$to_date.'&by_status='.$searchBy_status)); ?>" class="btn btn-info">Assign Students Export</a>
                    </div>
                    <div class="col-xs-2">
                      <a href="<?php echo e(url('admin/synopsis/not-assign-student/export?child_catId='.$child_catId.'&courseId='.$courseId.'&category='.$category.'&from_date='.$from_date.'&to_date='.$to_date)); ?>" class="btn btn-info">Notassign Students Export</a>
                    </div>

                  </div>
                  <div class="form-group row">
                    <div class="col-xs-2">
                      <a href="<?php echo e(url('admin/synopsis/statuswise-assign-student/export?child_catId='.$child_catId.'&courseId='.$courseId.'&category='.$category.'&from_date='.$from_date.'&to_date='.$to_date.'&by_status='.$searchBy_status)); ?>" class="btn btn-info">Students Export StatusWise</a>
                    </div>
                     <?php if($child_catId && !$courseId): ?>
                     <div class="col-xs-2">
                      <a href="<?php echo e(url('admin/synopsis/cat/student-list',$child_catId)); ?>" class="btn btn-info">Student List</a>
                    </div>
                    <?php endif; ?>
                      <?php if($courseId): ?>
                    
                    <div class="col-xs-2">
                      <a href="<?php echo e(url('admin/synopsis/student-list',$courseId)); ?>" class="btn btn-info">Student List</a>
                    </div>
                     <?php endif; ?>
                  </div>
                </form>
                <?php endif; ?>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Registration No.</th>
                  <th>Registration Year</th>
                  <th>Proposed Title</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <!--<th>Email</th>
                   <th>Guide Name</th>
                  <th>Guide Desgination</th> -->
                  <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                  <th>Assign</th>
                  <?php endif; ?>
                  <th>Document</th>
                  <th>Re Submit</th>

                  <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                  <th>Payment Status</th>
                  <?php endif; ?>
                  <th>Status</th>
                  <?php if(\Auth::guard('admin')->user()->is_admin ==1): ?>
                  <th>BOS review status</th>
                  <th>Submitted Date</th>
                  <?php else: ?>
                  <th>Review status</th>
                  <?php endif; ?>
                </tr>
                </thead>
                <tbody>
                <?php $count=1;$page = !empty($_GET['page'])?$_GET['page']-1:0; ?>
                <?php $__empty_1 = true; $__currentLoopData = $synopsis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $synop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count +($page*10)); ?></td>
                    <td>
                        <div><?php echo e($synop->name_of_student); ?></div>
                        <div class="text-info"><small><?php echo e($synop->category_name); ?></small></div>
                    </td>
                    <td><?php echo e($synop->registration_no); ?></td>
                    <td><?php echo e($synop->registration_year); ?></td>
                     <td><?php echo e($synop->proposed_title_thesis); ?></td>
                    <td><?php echo e($synop->institute_name); ?></td>
                    <td><?php echo e($synop->mobile_landline); ?></td>
                    <!--<td><?php echo e($synop->email); ?></td>
                     <td><?php echo e($synop->guide_name); ?></td>
                    <td><?php echo e($synop->guide_designation); ?></td> -->
                    <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                    <td>
                     <span class="badge bg-green" data-toggle-pop="popover" data-content="<?php echo $synop->transaction()->getEmailid(); ?>" data-html="true" title="Sent List"><?php echo e(count($synop->transaction)); ?></span>&nbsp;
                     <?php if($synop->status ==1): ?>
                     <?php
                     $assignedUser = $synop->transaction->count() ? implode($synop->transaction()->pluck('to_user_id')->toArray(),','):'';
                     ?>
                     <a href="#" data-toggle="modal" data-target="#modal-default" data-tog="tooltip" title="Assign Synopsis to BOS" id="synopsis-modal" onclick="setSynopsiId('<?php echo e($synop->id); ?>','', '<?php echo e($synop->category->name); ?>', '<?php echo e($assignedUser); ?>')" data-synopsis="<?php echo e($synop->id); ?>"><i class="fa fa-send-o"></i></a>
                     <?php endif; ?>
                    </td>
                    <?php endif; ?>

                    <td>
                    <a href="<?php echo e(url('admin/student-synopsis/viewform',['syn_id'=>$synop->id])); ?>" target="_blank" data-tog="tooltip" title="Synopsis Form Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> ||
                      <a href="<?php echo e(url('admin/student-synopsis/view',['syn_id'=>$synop->id])); ?>" target="_blank" data-tog="tooltip" title="Synopsis Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                      &nbsp;<?php if(\Auth::guard('admin')->user()->is_admin ==1): ?><a href="javascript:void(0)" data-tog="tooltip"  title="Replace Pdf" onclick="replaceSynopsisPDF('<?php echo e($synop->id); ?>')"><i class="fa fa-hand-pointer-o"></i></a><?php endif; ?>
                      <?php echo $synop->getOtherPdf($synop->id); ?>

                    </td>
                    <?php $resubmitUrl = url('admin/resubmit-synopsis/enable',$synop->id); ?>
                    <td><input type="checkbox" name="resubmit_enable" onchange="resubmitSynopsisEnable('<?php echo e($resubmitUrl); ?>',event)" <?php if($synop->resubmit_enable ==1): ?>checked <?php endif; ?>></td>

                     <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                    <td>
                    <input class="paid-status" data-id="<?php echo e($synop->id); ?>" data-on="Paid" data-off="Not Paid" data-url="<?php echo e(route('admin.payment.status-change')); ?>" data-type="synopsis" type="checkbox" <?php if($synop->payment_done ==1): ?> checked disabled <?php endif; ?>  data-toggle="toggle" data-size="small">
                    </td>
                    

                    <?php endif; ?>


                    <td><?php echo e($synop->status_text); ?> &nbsp;

                    <?php if($synop->status ==3): ?>
                      <?php if($synop->reason_reject): ?>
                      <a href="javascript:void(0)" data-tog="tooltip" title="Reason" onclick="reasonRejected('<?php echo e($synop->reason_reject); ?>')"><i class="fa fa-hand-pointer-o"></i></a>
                      <?php elseif($synop->reason_reject_doc): ?>
                        <a href="javascript:void(0)" onclick="downloadReasonDoc('<?php echo e($synop->id); ?>','synopsis')" data-tog="tooltip" title="Reason"><i class="fa fa-download"></i></a>
                      <?php endif; ?>

                    <?php endif; ?>

                    <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                    <?php if($synop->status ==1 || $synop->status ==4): ?>
                    <a href="#" data-toggle="modal" data-target="#status-modal" data-tog="tooltip" title="Change Status" ><i class="fa fa-hand-pointer-o" onclick="setSynopsisStatus('<?php echo e($synop->id); ?>','<?php echo e($synop->status); ?>','')"></i></a>
                    <?php endif; ?>
                    <?php endif; ?>
                     &nbsp; <a href="#" class="showFase" data-tog="tooltip" title="Life cycle" data-id="<?php echo e($synop->id); ?>"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>

                    <a href="#" data-id="<?php echo e($synop->id); ?>" class="hideFase" title="Hide" style="display: none;"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                    </td>
                    <td>
                       <?php $ids = @$synop->transaction ? implode(',',$synop->transaction()->pluck('id')->toArray()):'';  $reviewtatUrl = url('admin/student-synopsis/bos-review'); ?>
                      <?php if(\Auth::guard('admin')->user()->is_admin ==1): ?><a href="javascript:void(0)" onclick="getBOSReviewStatus('<?php echo e($ids); ?>','<?php echo e($reviewtatUrl); ?>')" title="BOS review status"><i class="fa fa-hand-pointer-o"></i></a>
                      <?php else: ?>
                      <?php $synop_transaction = $synop->transaction()->where('to_user_id', \Auth::guard('admin')->user()->id)->first(); $commandUrl = url('admin/student-synopsis/bos-review/command', $synop_transaction->id);  ?>
                      <a href="javascript:void(0)" onclick="setBOSReviewStatus('<?php echo e($synop->id); ?>','<?php echo e($synop_transaction->review_status); ?>','<?php echo e($commandUrl); ?>')" title="Set status"><i class="fa fa-hand-pointer-o"></i></a><?php endif; ?>
                    </td>
                     <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                    <td><?php echo e(\Carbon\Carbon::parse($synop->created_at)->format('d/m/Y')); ?></td>
                    <?php endif; ?>
                  </tr>
                  <tr class="show-tr"  data-id="<?php echo e($synop->id); ?>" style="display: none;">
                    <td colspan="11">
                      <div class="time-line-div admin-timeline">
                        <div class="legend">
                                  <ul>
                                    <li><span class="notcomplete"></span> Not Complete</li>
                                    <li><span class="complete"></span> Complete</li>
                                  </ul>
                                </div>
                        <ul class="time-line-ul">
                          <li class="time-line-li active-li">
                                  <span class="time-line-span"><?php echo e($synop->is_resubmitted?'Resubmitted':'Submitted'); ?></span><span class="circle"></span>
                                </li>
                                <?php if($synop->status !=4 && $synop->status !=3): ?>
                                <li class="time-line-li <?php if($synop->status==2): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Approved</span><span class="circle"></span>
                                </li>
                                <?php endif; ?>
                                <?php if($synop->status ==4): ?>
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Send for Revision</span><span class="circle"></span>
                                </li>
                                <?php endif; ?>
                                <?php if($synop->status ==3): ?>
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Rejected</span><span class="circle"></span>
                                </li>
                                <?php endif; ?>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
                  <td colspan="13" style="text-align: center;">No data available</td>
                 <?php else: ?>
                  <td colspan="8" style="text-align: center;">No data available</td>
                  <?php endif; ?>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
            </div>
              <div><?php echo e($synopsis->links()); ?></div>

            </div>
            <!-- /.box-body -->

            <div class="overlay" style="display: none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>

          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

      <!-- Email send Modal -->
      <div class="modal fade design-modal" id="modal-default">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Assign Synopsis To BOS <span id="assign_title"></span></h4>
              </div>
              <div class="modal-body">

                <table class="table">
                    <tbody id="send_email_tbody">
                    
                       <!-- <input type="hidden" name="synopsis_id"> -->
                    </tbody>
                 </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <?php $route= url('admin/assign-synopsis') ?>
                <button type="button" class="btn btn-primary btn-disable" onclick="sendEmail('<?php echo e($route); ?>','<?php echo e(csrf_token()); ?>','bios', 'Synopsis')">send &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- Status Change Modal-->

        <div class="modal fade design-modal" id="status-modal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Set status</h4>
              </div>
              <div class="modal-body">
              <form action="<?php echo e(route('student.synopsis.status-change')); ?>" id="synopsis_status" enctype="multipart/form-data">
              <?php echo csrf_field(); ?>
                <input type="hidden" name="synop_id" id="synop_id">
               <div class="form-group radio">
                  <input type="radio" value="2" name="status" class="minimal"><label>Approved</label>
                </div>
                <div class="form-group radio">
                  <input type="radio" value="3" name="status" class="minimal"><label>Rejected</label>
                </div>
                <div class="form-group radio">
                  <input type="radio" value="4" name="status" class="minimal"><label>Resubmitted</label>
                </div>
                <div class="form-group" id="not-accept-reason" style="display: none;">
                <label >Reason / Upload doc file <a href="javascript:void(0)" onclick="showDocs('upload_docs','not-accept-reason')">(click)</a>:</label>
                  <textarea name="reason_reject" class="form-control"></textarea>
                </div>

                <div style="display: none;" id="upload_docs">
                  <label >Upload doc file / Reason <a href="javascript:void(0)" onclick="showDocs('not-accept-reason','upload_docs')">(click)</a>:</label>
                  <input type="file" name="reason_reject_doc">
                </div>
              </form>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus(event)">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


        <!-- BOS Review -->
        <div class="modal fade design-modal" id="review-status-modal">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Set status</h4>
              </div>
              <div class="modal-body">
              <form action="<?php echo e(route('admin.bos-set.review-status')); ?>" method="post" id="review_synopsis_status" enctype="multipart/form-data">
              <?php echo csrf_field(); ?>
                <input type="hidden" name="review_synopsis_id" id="review_synopsis_id">
               <div class="form-group radio">
                  <input type="radio" value="1" name="bos_status" class="minimal"><label>Approved</label>
                </div>
                <div class="form-group radio">
                  <input type="radio" value="2" name="bos_status" class="minimal"><label>Rejected</label>
                </div>
                <div class="form-group radio">
                  <input type="radio" value="3" name="bos_status" class="minimal"><label>Resubmitted</label>
                </div>
                <div class="form-group" id="bos-not-accept-reason" style="display: none;">
                <label >Reason :</label>
                  <textarea name="bos_reason_reject" class="form-control"></textarea>
                </div>
              </form>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="bos_reviewBtn" onclick="bosReviewSubmit()" style="display: none;">Change</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


        <!-- Reson Rejected -->

         <div class="modal fade design-modal" id="reason-rejected-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Reason for Rejection</h4>
                </div>
                <div class="modal-body">
                <p id="reason_content"></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          <!-- View review status -->

        <div class="modal fade design-modal" id="view-review-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">BOS Review Status</h4>
                </div>
                <div class="modal-body">
               <table class="table">
                  <thead>
                    <th>BOS name</th>
                    <th>Status</th>
                    <th>Comment</th>
                  </thead>
                   <tbody id="review_status_tbody">

                   </tbody>
               </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>



          <!-- Replace pdf-->
<div class="modal fade design-modal" id="replacesynonp-Pdf-modal" role="dialog">
   <div class="modal-dialog modal-md">
      <div class="modal-content">
         <div class="modal-header">
          <form action="<?php echo e(url('admin/replace-synopsis/pdf')); ?>" method="post" enctype='multipart/form-data'>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Upload New PDF</h4>
         </div>
         <div class="modal-body">
         <?php echo csrf_field(); ?>
         <input type="hidden" name="synopsis_id" id="replace_synopsis_id">
         <input type="file" name="upload_new_pdf">
         </div>
         <div class="modal-footer">
         <input type="submit" class="btn btn-primary" value="Replace" style="float:left;">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
         </form>
      </div>
   </div>
</div>

  </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>