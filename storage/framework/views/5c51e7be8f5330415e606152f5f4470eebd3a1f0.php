<table>
    <thead>
    <tr>
        <th><b>Name of Student</b></th>
        <th><b>Reg. No.</b></th>
        <th><b>Reg. Year</b></th>
        <th><b>Institute name</b></th>
        <th><b>Email</b></th>
        <th><b>Phone</b></th>
        <th><b>Subject</b></th>
        <th><b>Payment Status</b></th>
        <th><b>Synopsis Status</b></th>

    </tr>
    </thead>
    <tbody>
    <?php $__currentLoopData = $synopsis_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $synopsis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e(@$synopsis->name_of_student); ?></td>
            <td><?php echo e(@$synopsis->registration_no); ?></td>
            <td><?php echo e(@$synopsis->registration_year); ?></td>
            <td><?php echo e(@$synopsis->institute_name); ?></td>
            <td><?php echo e(@$synopsis->email); ?></td>
            <td><?php echo e(@$synopsis->mobile_landline); ?></td>
            <td><?php echo e(@$synopsis->category->name); ?></td>
            <td><?php echo e(@$synopsis->is_paid ==1 ? 'paid':'Not Paid'); ?></td>
            <td><?php echo e(@$synopsis->status_text); ?></td>
            <td></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>

