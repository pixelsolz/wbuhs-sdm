<?php $__env->startSection('content'); ?>


<section class="desh-bord">
   <div class="container">
      <div class="row">
         
         <div class="col-md-12 col-sm-12 col-xs-12">
         	<div class="thankyouOuter">
         		<img src="<?php echo e(url('public/img/thankcheck.png')); ?>" alt="thankcheck">
               <?php if($type ==1): ?>
         		<p>Your Synopsis has been submitted succesfully.</p>
               <?php else: ?>
               <p>Your Dissertation has been submitted succesfully.</p>
               <?php endif; ?>

               <?php if($paid_type =='offline'): ?>
                  <p>You have paid by offline. So after paying fees your <?php echo e(($type ==1? 'Synopsis':'Dissertation')); ?> will be procced.</p>
               <?php endif; ?>
         		<a href="<?php echo e(route('dashboard.index')); ?>">Go To dashboard</a>
         	</div>
         </div>
      </div>
   </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>