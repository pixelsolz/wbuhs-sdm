<?php $__env->startSection('content'); ?>

<section class="desh-bord">
   <div class="container">
      <?php if(Session::has('error')): ?>
      <div class="alert alert-danger">
         <strong><?php echo e(Session('error')); ?></strong>
      </div>
      <?php endif; ?>
      <div class="row">
         <?php echo $__env->make('frontend.includes.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Rejected Or Resubmission</h3>
            <div class="right-pnl">
               <table class="table">
    <thead>
      <tr>
        <th>Type</th>
        <th>Date</th>
        <th>Status</th>
        <th>Reason</th>
      </tr>
    </thead>
    <tbody>
      <?php if($type=='Synopsis'): ?>
      <tr>
        <td><?php echo e($type); ?></td>
        <td><?php echo e(\Carbon\Carbon::parse($rejectedData->created_at)->format('d/m/Y')); ?></td>
        <td><?php echo e($rejectedData->status_text); ?></td>
        <td>
        <?php if($rejectedData->reason_reject): ?>
        <a href="javascript:void(0)" onclick="reasonRejectModal('<?php echo e($rejectedType); ?>','<?php echo e($rejectedData->reason_reject); ?>')"><?php echo e(strlen($rejectedData->reason_reject) >15 ? substr($rejectedData->reason_reject,0,15).'...' : $rejectedData->reason_reject); ?></a>
        <?php else: ?>
          <form action="<?php echo e(url('download-reason/doc')); ?>" method="get">
            <input type="hidden" name="id" value="<?php echo e($rejectedData->id); ?>">
            <input type="hidden" name="type" value="synopsis">
            <button class="fa fa-download" aria-hidden="true" type="submit"></button>
          </form>
          <!-- <a href="" onclick="" ><i class="fa fa-download" aria-hidden="true" data-tog="tooltip" title="Reason"></i></a> -->
        <?php endif; ?>
        </td>
      </tr>
      <?php else: ?>
        <tr>
          <td><?php echo e($type); ?></td>
          <td><?php echo e(\Carbon\Carbon::parse($rejectedData->created_at)->format('d/m/Y')); ?></td>
          <td><?php echo e($rejectedData->status_text); ?></td>
          <?php
            $rejectedReason = $rejectedData->reasonRejected($rejectedData->id);
          ?>
          <?php if($rejectedReason->reason_reject): ?>
          <td><a href="javascript:void(0)" onclick="reasonRejectModal('<?php echo e($rejectedType); ?>','<?php echo e($rejectedReason->reason_reject); ?>')"><?php echo e(strlen($rejectedReason->reason_reject) >15 ? substr($rejectedReason->reason_reject,0,15).'...' : $rejectedReason->reason_reject); ?></a></td>
          <?php else: ?>
            <form action="<?php echo e(url('download-reason/doc')); ?>" method="get">
            <input type="hidden" name="id" value="<?php echo e($rejectedReason->id); ?>">
            <input type="hidden" name="type" value="thesis">
            <button class="fa fa-download" aria-hidden="true" type="submit"></button>
          </form>
          <?php endif; ?>
        </tr>

      <?php endif; ?>

    </tbody>
  </table>

            </div>
         </div>
      </div>
   </div>
</section>
 <div class="modal fade" id="1myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reason of <span id="title"></span></h4>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>