<?php $__env->startSection('content'); ?>
<?php
$currentRoute = Route::currentRouteName();
?>
<style>
   .kbw-signature { width: 400px; height: 100px; }
   #append_more_pdf{float: right;width: 62%;margin: 10px 0 10px 10px}
   #append_more_pdf .form-group{width: 100%; margin: 0;}
   #append_more_pdf .form-group > input{width: 100%;}
</style>
<section class="desh-bord">

   <div class="container">
      <?php if(Session::has('error')): ?>
      <div class="alert alert-danger">
         <strong><?php echo e(Session('error')); ?></strong>
      </div>
      <?php endif; ?>
      <div class="row">
         <?php echo $__env->make('frontend.includes.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
         <h3 class="dashboard_title">Dissertation Form</h3>

            <div class="time-line-div">
                                <h3>Status</h3>
                                <div class="time-line-div-inner">
                                <div class="legend">
                                  <ul>
                                    <li><span class="notcomplete"></span> Not Complete</li>
                                    <li><span class="complete"></span> Complete</li>
                                  </ul>
                                </div>
                                <ul class="time-line-ul">
                              <?php if($thesis->is_resubmit): ?>
                                <li class="time-line-li <?php if($thesis->status >=5 || ($thesis->is_resubmit && $thesis->status ==3)): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Dissertation Resubmitted</span><span class="circle" data-toggle="tooltip" title="<?php echo \Carbon\Carbon::parse($thesis->created_at)->format('d-m-Y'); ?>"></span>
                                </li>
                                <li class="time-line-li <?php if($thesis->status >=6 || ($thesis->is_resubmit && $thesis->status ==3)): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Reassigend to Adjudicator</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li <?php if($thesis->status ==3): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Accepted</span><span class="circle"></span>
                                </li>
                                <?php else: ?>
                                <li class="time-line-li <?php if($thesis->status >=1): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Dissertation Submitted</span><span class="circle"></span>
                                </li>
                                <li class="time-line-li <?php if($thesis->status >=2): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Assigend to Adjudicator</span><span class="circle"></span>
                                </li>
                                <?php if($thesis->status !=4): ?>
                                 <li class="time-line-li <?php if($thesis->status ==3): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Accepted</span><span class="circle"></span>
                                </li>
                                <?php endif; ?>
                                <?php if($thesis->status ==4): ?>
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Send for Revision</span><span class="circle"></span>
                                </li>
                                <?php endif; ?>
                                <?php endif; ?>

                              </ul>
                              
                              </div>
                              <?php $subUrl=url('get/thesis-status') ?>
                              <a href="#" data-toggle="modal" data-target="#grade-modal" data-tog="tooltip" title="See detail" onclick="getFacultyStatus('<?php echo e($subUrl); ?>','<?php echo e($thesis->id); ?>')">Evaluate Dissertation Detail</a>
                              </div>





         </div>
      </div>
   </div>
</section>
<!-- The Modal -->
<div class="modal fade" id="thesisEditModal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <h4 class="modal-title">Add Category</h4>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <form action="<?php echo e(route('category.student-add')); ?>" id="category_modal" data-reload-url="<?php echo e(route('dissertation.index')); ?>">
               <?php echo csrf_field(); ?>
               <div class="form-group">
                  <label for="email">Category:</label>
                  <select class="form-control" name="parent">
                     <option value="0">Root Category</option>
                     <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <option value="<?php echo e($category['id']); ?>"><?php echo $category['name']; ?></option>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
               </div>
               <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control"  name="name">
                  <span class="error" id="name"></span>
               </div>
            </form>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="modalFormSubmit('category_modal')">Add &nbsp;<i class="fa fa-spinner fa-spin" style="font-size:20px; display: none;" id="spinner"></i></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

        <div class="modal fade design-modal" id="grade-modal">
          <div class="modal-dialog modal-md">
            <div class="modal-content modal-default">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Evaluate Dissertation Detail</h4>
              </div>
              <div class="modal-body">
              <table class="table">
              <thead>
                <tr>
                  <th>Faculty</th>
                  <th>Submitted</th>
                  <th>Status</th>
                  <th>Comment</th>
                </tr>
              </thead>
              <tbody id="gradeModalTbody">

              </tbody>
            </table>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary btn-disable" onclick="changeStatus()">Change &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button> -->
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade design-modal" id="reason-rejected-modal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Reason for Revision</h4>
                </div>
                <div class="modal-body">
                <p id="reason_content"></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>