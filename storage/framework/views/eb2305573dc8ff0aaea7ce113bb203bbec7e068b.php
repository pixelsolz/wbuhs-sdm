<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>
<style>
   .kbw-signature { width: 400px; height: 100px; }
</style>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Dissertation Evaluation
      </h1>
      
   </section>
   <!-- Main content -->
   <section class="content">
      <?php if(Session::has('error')): ?>
      <div class="alert alert-danger">
         <strong><?php echo e(Session('error')); ?></strong>
      </div>
      <?php endif; ?>
      <?php if(Session::has('msg')): ?>
      <div class="alert alert-success">
         <strong><?php echo e(Session('msg')); ?></strong>
      </div>
      <?php endif; ?>
     <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">

               <div class="box box-primary">
              <?php if(\Auth::guard('admin')->user()->is_admin ==1): ?>
               <a href="javascript:void(0)" onclick="printResult('print_div')" class="btn btn-default" style="float-right;"><i class="fa fa-print"></i> Print</a>
               <?php endif; ?>
               <form name="reviewer_form" id="reviewer_form" method="post" action="" enctype="multipart/form-data">

                <div class="box-body">
                <div class="msFormSection" id="print_div">

                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="msFormHead">
                           <h2>Confidential</h2>
                           <h4>Report of Dissertation Evaluation</h4>
                        </div>
                        <div class="msCandidateInfo">
                           <div class="formgroup">
                              <label>Name of the Candidate</label>
                              <input type="text" name="candidate_name" id="candidate_name" value="<?php echo e($reviewResult->thesis->name_of_student); ?>">
                           </div>
                           <div class="formgroup">
                              <label>Title of the Dissertation </label>
                              <textarea rows="4" name="thesis_title" id="thesis_title"><?php echo e($reviewResult->thesis->proposed_title_thesis); ?></textarea>
                              <?php if($errors->has('thesis_title')): ?>
                                  <div class="error"><?php echo e($errors->first('thesis_title')); ?></div>
                              <?php endif; ?>
                           </div>
                           <div class="formgroup">
                              <label>Course / Subject </label>
                              <input type="text" name="course_subject" id="course_subject" value="<?php echo e($reviewResult->thesis->category->name); ?>">

                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="gradeInfo">
                     <p>Adequate <span>(Ad)</span>, Marginal <span>(M)</span>, Inadequate <span>(I)</span>, Not applicable <span>(NA)</span></p>
                  </div>
                  <div class="markingPart">
                     <h3>I. Introduction</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Mention of lacunae in current knowledge </h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="lacunae_knowledge" id="lacunae_knowledge">
                                 <option value="Ad" <?php if(old('intro_rating1') ? old('intro_rating1')=='Ad': (!empty($reviewResult->lacunae_knowledge) ?$reviewResult->lacunae_knowledge=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('intro_rating1') ? old('intro_rating1')=='M': (!empty($reviewResult->lacunae_knowledge) ?$reviewResult->lacunae_knowledge=='M':'')): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('intro_rating1') ? old('intro_rating1')=='I': (!empty($reviewResult->lacunae_knowledge) ?$reviewResult->lacunae_knowledge=='I':'')): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('intro_rating1') ? old('intro_rating1')=='NA': (!empty($reviewResult->lacunae_knowledge) ?$reviewResult->lacunae_knowledge=='NA':'')): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="lacunae_knowledge_remarks" id="lacunae_knowledge_remarks"><?php echo e(old('lacunae_knowledge_remarks')? old('lacunae_knowledge_remarks'): (!empty($reviewResult->lacunae_knowledge_remarks)?$reviewResult->lacunae_knowledge_remarks: '' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>b. Justification of the present study </h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="present_study" id="present_study">
                                 <option value="Ad" <?php if(old('present_study') ? old('present_study')=='Ad': (!empty($reviewResult->present_study) ?$reviewResult->present_study=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('present_study') ? old('present_study')=='M': (!empty($reviewResult->present_study)?$reviewResult->present_study=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('present_study') ? old('present_study')=='I': (!empty($reviewResult->present_study)?$reviewResult->present_study=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('present_study') ? old('present_study')=='NA': (!empty($reviewResult->present_study)?$reviewResult->present_study=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="present_study_remarks" id="present_study_remarks"><?php echo e(old('present_study_remarks') ?old('present_study_remarks'): (!empty($reviewResult->present_study_remarks) ? $reviewResult->present_study_remarks: '' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>c. Hypothesis</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="hypothesis" id="hypothesis">
                                 <option value="Ad" <?php if(old('hypothesis') ? old('hypothesis')=='Ad': (!empty($reviewResult->hypothesis)?$reviewResult->hypothesis=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('hypothesis') ? old('hypothesis')=='M': (!empty($reviewResult->hypothesis)?$reviewResult->hypothesis=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('hypothesis') ? old('hypothesis')=='I': (!empty($reviewResult->hypothesis)?$reviewResult->hypothesis=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('hypothesis') ? old('hypothesis')=='NA': (!empty($reviewResult->hypothesis)?$reviewResult->hypothesis=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="hypothesis_remarks" id="hypothesis_remarks"><?php echo e(old('hypothesis_remarks')?old('hypothesis_remarks'):(!empty($reviewResult->hypothesis_remarks)?$reviewResult->hypothesis_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>II. Objectives </h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Expressed in observable & measurable manner (as far as practicable)</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="eom" id="eom">
                                  <option value="Ad" <?php if(old('eom') ? old('eom')=='Ad': (!empty($reviewResult->eom) ?$reviewResult->eom=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('eom') ? old('eom')=='M': (!empty($reviewResult->eom) ?$reviewResult->eom=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('eom') ? old('eom')=='I': (!empty($reviewResult->eom)?$reviewResult->eom=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('eom') ? old('eom')=='NA': (!empty($reviewResult->eom)?$reviewResult->eom=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="eom_remarks" id="eom_remarks"><?php echo e(old('eom_remarks')?old('eom_remarks'): (!empty($reviewResult->eom_remarks)? $reviewResult->eom_remarks: '')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>b. Clarity and specificity</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="clarity_specificity" id="clarity_specificity">
                                 <option value="Ad" <?php if(old('clarity_specificity') ? old('clarity_specificity')=='Ad': (!empty($reviewResult->clarity_specificity) ?$reviewResult->clarity_specificity=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('clarity_specificity') ? old('clarity_specificity')=='M': (!empty($reviewResult->clarity_specificity) ?$reviewResult->clarity_specificity=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('clarity_specificity') ? old('clarity_specificity')=='I': (!empty($reviewResult->clarity_specificity) ?$reviewResult->clarity_specificity=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('clarity_specificity') ? old('clarity_specificity')=='NA': (!empty($reviewResult->clarity_specificity) ?$reviewResult->clarity_specificity=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="clarity_specificity_remarks" id="clarity_specificity_remarks"><?php echo e(old('clarity_specificity_remarks')?old('clarity_specificity_remarks'): (!empty($reviewResult->clarity_specificity_remarks)?$reviewResult->clarity_specificity_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>III. Review of Literature</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Relevance</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="relevance" id="relevance">
                                 <option value="Ad" <?php if(old('relevance') ? old('relevance')=='Ad': (!empty($reviewResult->relevance) ?$reviewResult->relevance=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('relevance') ? old('relevance')=='M': (!empty($reviewResult->relevance)?$reviewResult->relevance=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('relevance') ? old('relevance')=='I': (!empty($reviewResult->relevance)?$reviewResult->relevance=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('relevance') ? old('relevance')=='NA': (!empty($reviewResult->relevance) ?$reviewResult->relevance=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="relevance_remarks" id="relevance_remarks"><?php echo e(old('relevance_remarks')? old('relevance_remarks'):(!empty($reviewResult->relevance_remarks)? $reviewResult->relevance_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>b. Completeness</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="completeness" id="completeness">
                                 <option value="Ad" <?php if(old('completeness') ? old('completeness')=='Ad': (!empty($reviewResult->completeness)?$reviewResult->completeness=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('completeness') ? old('completeness')=='M': (!empty($reviewResult->completeness)?$reviewResult->completeness=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('completeness') ? old('completeness')=='I': (!empty($reviewResult->completeness) ?$reviewResult->completeness=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('completeness') ? old('completeness')=='NA': (!empty($reviewResult->completeness) ?$reviewResult->completeness=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="completeness_remarks" id="completeness_remarks"><?php echo e(old('completeness_remarks')?old('completeness_remarks'):(!empty($reviewResult->completeness_remarks)? $reviewResult->completeness_remarks: '')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>c. Current and up-to-date</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="current_uptodate" id="current_uptodate">
                                  <option value="Ad" <?php if(old('current_uptodate') ? old('current_uptodate')=='Ad': (!empty($reviewResult->current_uptodate) ?$reviewResult->current_uptodate=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('current_uptodate') ? old('current_uptodate')=='M': (!empty($reviewResult->current_uptodate)?$reviewResult->current_uptodate=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('current_uptodate') ? old('current_uptodate')=='I': (!empty($reviewResult->current_uptodate) ?$reviewResult->current_uptodate=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('current_uptodate') ? old('current_uptodate')=='NA': (!empty($reviewResult->current_uptodate)?$reviewResult->current_uptodate=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="current_uptodate_remarks" id="current_uptodate_remarks"><?php echo e(old('current_uptodate_remarks')? old('current_uptodate_remarks'):(!empty($reviewResult->current_uptodate_remarks)? $reviewResult->current_uptodate_remarks: '')); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>IV. Methods</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Place of study and period of study mentioned</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="psps" id="psps">
                                 <option value="Ad" <?php if(old('psps') ? old('psps')=='Ad': (!empty($reviewResult->psps) ?$reviewResult->psps=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('psps') ? old('psps')=='M': (!empty($reviewResult->psps)?$reviewResult->psps=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('psps') ? old('psps')=='I': (!empty($reviewResult->psps) ?$reviewResult->psps=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('psps') ? old('psps')=='NA': (!empty($reviewResult->psps) ?$reviewResult->psps=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="psps_remarks" id="psps_remarks"><?php echo e(old('psps_remarks')? old('psps_remarks'):(!empty($reviewResult->psps_remarks)?$reviewResult->psps_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>b. Type of study</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="type_study" id="type_study">
                                 <option value="Ad" <?php if(old('type_study') ? old('type_study')=='Ad': (!empty($reviewResult->type_study)?$reviewResult->type_study=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('type_study') ? old('type_study')=='Ad': (!empty($reviewResult->type_study) ?$reviewResult->type_study=='Ad':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('type_study') ? old('type_study')=='Ad': (!empty($reviewResult->type_study)?$reviewResult->type_study=='Ad':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('type_study') ? old('type_study')=='Ad': (!empty($reviewResult->type_study)?$reviewResult->type_study=='Ad':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="type_study_remarks" id="type_study_remarks"><?php echo e(old('type_study_remarks')? old('type_study_remarks'):(!empty($reviewResult->type_study_remarks)? $reviewResult->type_study_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>c. Inclusion and exclusion criteria</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="inclution_exclution" id="inclution_exclution">
                                 <option value="Ad" <?php if(old('inclution_exclution') ? old('inclution_exclution')=='Ad': (!empty($reviewResult->inclution_exclution) ?$reviewResult->inclution_exclution=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('inclution_exclution') ? old('inclution_exclution')=='M': (!empty($reviewResult->inclution_exclution) ?$reviewResult->inclution_exclution=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('inclution_exclution') ? old('inclution_exclution')=='I': (!empty($reviewResult->inclution_exclution) ?$reviewResult->inclution_exclution=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('inclution_exclution') ? old('inclution_exclution')=='NA': (!empty($reviewResult->inclution_exclution) ?$reviewResult->inclution_exclution=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="inclution_exclution_remarks" id="inclution_exclution_remarks"><?php echo e(old('inclution_exclution_remarks')? old('inclution_exclution_remarks'): (!empty($reviewResult->inclution_exclution_remarks)? $reviewResult->inclution_exclution_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>d. Outcome parameters</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="outcome_parameters" id="outcome_parameters">
                                 <option value="Ad"  <?php if(old('outcome_parameters') ? old('outcome_parameters')=='Ad': (!empty($reviewResult->outcome_parameters)?$reviewResult->outcome_parameters=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('outcome_parameters') ? old('outcome_parameters')=='M': (!empty($reviewResult->outcome_parameters) ?$reviewResult->outcome_parameters=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('outcome_parameters') ? old('outcome_parameters')=='I': (!empty($reviewResult->outcome_parameters) ?$reviewResult->outcome_parameters=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('outcome_parameters') ? old('outcome_parameters')=='NA': (!empty($reviewResult->outcome_parameters) ?$reviewResult->outcome_parameters=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="outcome_parameters_remarks" id="outcome_parameters_remarks"><?php echo e(old('outcome_parameters_remarks')?old('outcome_parameters_remarks'): (!empty($reviewResult->outcome_parameters_remarks)?$reviewResult->outcome_parameters_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>e. Study variables</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="study_variables" id="study_variables">
                                 <option value="Ad" <?php if(old('study_variables') ? old('study_variables')=='Ad': (!empty($reviewResult->study_variables)?$reviewResult->study_variables=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('study_variables') ? old('study_variables')=='M': (!empty($reviewResult->study_variables)?$reviewResult->study_variables=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('study_variables') ? old('study_variables')=='I': (!empty($reviewResult->study_variables) ?$reviewResult->study_variables=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('study_variables') ? old('study_variables')=='NA': (!empty($reviewResult->study_variables)?$reviewResult->study_variables=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="study_variables_remarks" id="study_variables_remarks"><?php echo e(old('study_variables_remarks')? old('study_variables_remarks'):(!empty($reviewResult->study_variables_remarks)?$reviewResult->study_variables_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>f. Sample size, sampling design related issues</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="sampling_design" id="sampling_design">
                                 <option value="Ad" <?php if(old('sampling_design') ? old('sampling_design')=='Ad': (!empty($reviewResult->sampling_design) ?$reviewResult->sampling_design=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('sampling_design') ? old('sampling_design')=='M': (!empty($reviewResult->sampling_design) ?$reviewResult->sampling_design=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('sampling_design') ? old('sampling_design')=='I': (!empty($reviewResult->sampling_design) ?$reviewResult->sampling_design=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('sampling_design') ? old('sampling_design')=='NA': (!empty($reviewResult->sampling_design) ?$reviewResult->sampling_design=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="sampling_design_remarks" id="sampling_design_remarks"><?php echo e(old('sampling_design_remarks')?old('sampling_design_remarks'):(!empty($reviewResult->sampling_design_remarks)? $reviewResult->sampling_design_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>g. Details of materials(apparatus used,laboratory tests etc.) and experimental design</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="materials_experimental" id="materials_experimental">
                                 <option value="Ad" <?php if(old('materials_experimental') ? old('materials_experimental')=='Ad': (!empty($reviewResult->materials_experimental) ?$reviewResult->materials_experimental=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('materials_experimental') ? old('materials_experimental')=='M': (!empty($reviewResult->materials_experimental) ?$reviewResult->materials_experimental=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('materials_experimental') ? old('materials_experimental')=='I': (!empty($reviewResult->materials_experimental) ?$reviewResult->materials_experimental=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA"<?php if(old('materials_experimental') ? old('materials_experimental')=='NA': (!empty($reviewResult->materials_experimental) ?$reviewResult->materials_experimental=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="materials_experimental_remarks" id="materials_experimental_remarks"><?php echo e(old('materials_experimental_remarks')?old('materials_experimental_remarks'): (!empty($reviewResult->materials_experimental_remarks)? $reviewResult->materials_experimental_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>h. Data collection methods and tools</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="data_collection_tools" id="data_collection_tools">
                                 <option value="Ad" <?php if(old('data_collection_tools') ? old('data_collection_tools')=='Ad': ((!empty($reviewResult->data_collection_tools) )?$reviewResult->data_collection_tools=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('data_collection_tools') ? old('data_collection_tools')=='M': (!empty($reviewResult->data_collection_tools) ?$reviewResult->data_collection_tools=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('data_collection_tools') ? old('data_collection_tools')=='I': (!empty($reviewResult->data_collection_tools) ?$reviewResult->data_collection_tools=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('data_collection_tools') ? old('data_collection_tools')=='NA': (!empty($reviewResult->data_collection_tools) ?$reviewResult->data_collection_tools=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="data_collection_tools_remarks" id="data_collection_tools_remarks"><?php echo e(old('data_collection_tools_remarks')? old('data_collection_tools_remarks'):(!empty($reviewResult->data_collection_tools_remarks)?$reviewResult->data_collection_tools_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>i. Statistical methods</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="statistical_methods" id="statistical_methods">
                                 <option value="Ad" <?php if(old('statistical_methods') ? old('statistical_methods')=='Ad': (!empty($reviewResult->statistical_methods) ?$reviewResult->statistical_methods=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('statistical_methods') ? old('statistical_methods')=='M': (!empty($reviewResult->statistical_methods) ?$reviewResult->statistical_methods=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('statistical_methods') ? old('statistical_methods')=='I': (!empty($reviewResult->statistical_methods) ?$reviewResult->statistical_methods=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('statistical_methods') ? old('statistical_methods')=='NA': (!empty($reviewResult->statistical_methods) ?$reviewResult->statistical_methods=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="statistical_methods_remarks" id="statistical_methods_remarks"><?php echo e(old('statistical_methods_remarks')? old('statistical_methods_remarks'):(!empty($reviewResult->statistical_methods_remarks)?$reviewResult->statistical_methods_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>V. Results</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Logical organization in identifiable sections</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="logical_organization" id="logical_organization">
                                 <option value="Ad" <?php if(old('logical_organization') ? old('logical_organization')=='Ad': (!empty($reviewResult->logical_organization) ?$reviewResult->logical_organization=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('logical_organization') ? old('logical_organization')=='M': (!empty($reviewResult->logical_organization) ?$reviewResult->logical_organization=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('logical_organization') ? old('logical_organization')=='I': (!empty($reviewResult->logical_organization) ?$reviewResult->logical_organization=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('logical_organization') ? old('logical_organization')=='NA': (!empty($reviewResult->logical_organization) ?$reviewResult->logical_organization=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="logical_organization_remarks" id="logical_organization_remarks"><?php echo e(old('logical_organization_remarks')?old('logical_organization_remarks'):(!empty($reviewResult->logical_organization_remarks)?$reviewResult->logical_organization_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>b. Correctness of data analysis</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="correctness_data_analysis" id="correctness_data_analysis">
                                 <option value="Ad"  <?php if(old('correctness_data_analysis') ? old('correctness_data_analysis')=='Ad': (!empty($reviewResult->correctness_data_analysis) ?$reviewResult->correctness_data_analysis=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M"  <?php if(old('correctness_data_analysis') ? old('correctness_data_analysis')=='M': (!empty($reviewResult->correctness_data_analysis) ?$reviewResult->correctness_data_analysis=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I"  <?php if(old('correctness_data_analysis') ? old('correctness_data_analysis')=='I': (!empty($reviewResult->correctness_data_analysis) ?$reviewResult->correctness_data_analysis=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA"  <?php if(old('correctness_data_analysis') ? old('correctness_data_analysis')=='NA': (!empty($reviewResult->correctness_data_analysis) ?$reviewResult->correctness_data_analysis=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="correctness_data_analysis_remarks" id="correctness_data_analysis_remarks"><?php echo e(old('correctness_data_analysis_remarks')?old('correctness_data_analysis_remarks'):(!empty($reviewResult->correctness_data_analysis_remarks)? $reviewResult->correctness_data_analysis_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>c. Appropriate use of tables, charts, graphs, etc.</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="appropriate_tables" id="appropriate_tables">
                                 <option value="Ad" <?php if(old('appropriate_tables') ? old('appropriate_tables')=='Ad': (!empty($reviewResult->appropriate_tables) ?$reviewResult->appropriate_tables=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('appropriate_tables') ? old('appropriate_tables')=='M': (!empty($reviewResult->appropriate_tables) ?$reviewResult->appropriate_tables=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('appropriate_tables') ? old('appropriate_tables')=='I': (!empty($reviewResult->appropriate_tables) ?$reviewResult->appropriate_tables=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('appropriate_tables') ? old('appropriate_tables')=='Ad': (!empty($reviewResult->appropriate_tables) ?$reviewResult->appropriate_tables=='Ad':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="appropriate_tables_remarks" id="appropriate_tables_remarks"><?php echo e(old('appropriate_tables_remarks')?old('appropriate_tables_remarks'): (!empty($reviewResult->appropriate_tables_remarks)?$reviewResult->appropriate_tables_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>d. Statistical Interpretation</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="statistical_interpretation" id="statistical_interpretation">
                                 <option value="Ad" <?php if(old('statistical_interpretation') ? old('statistical_interpretation')=='Ad': (!empty($reviewResult->statistical_interpretation) ?$reviewResult->statistical_interpretation=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('statistical_interpretation') ? old('statistical_interpretation')=='Ad': (!empty($reviewResult->statistical_interpretation) ?$reviewResult->statistical_interpretation=='Ad':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('statistical_interpretation') ? old('statistical_interpretation')=='Ad': (!empty($reviewResult->statistical_interpretation) ?$reviewResult->statistical_interpretation=='Ad':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('statistical_interpretation') ? old('statistical_interpretation')=='Ad': (!empty($reviewResult->statistical_interpretation) ?$reviewResult->statistical_interpretation=='Ad':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="statistical_interpretation_remarks" id="statistical_interpretation_remarks"><?php echo e(old('statistical_interpretation_remarks')? old('statistical_interpretation_remarks'): (!empty($reviewResult->statistical_interpretation_remarks)?$reviewResult->statistical_interpretation_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>VI. Discussion</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Relevance(within framework of Study)</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="discussion_relevance" id="discussion_relevance">
                                 <option value="Ad" <?php if(old('discussion_relevance') ? old('discussion_relevance')=='Ad': (!empty($reviewResult->discussion_relevance) ?$reviewResult->discussion_relevance=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('discussion_relevance') ? old('discussion_relevance')=='M': (!empty($reviewResult->discussion_relevance) ?$reviewResult->discussion_relevance=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('discussion_relevance') ? old('discussion_relevance')=='I': (!empty($reviewResult->discussion_relevance) ?$reviewResult->discussion_relevance=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('discussion_relevance') ? old('discussion_relevance')=='NA': (!empty($reviewResult->discussion_relevance) ?$reviewResult->discussion_relevance=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="discussion_relevance_remarks" id="discussion_relevance_remarks"><?php echo e(old('discussion_relevance_remarks')? old('discussion_relevance_remarks'):(!empty($reviewResult->discussion_relevance_remarks)?$reviewResult->discussion_relevance_remarks:'' )); ?></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>b. Interpretation of results</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="interpretation_results" id="interpretation_results">
                                 <option value="Ad" <?php if(old('interpretation_results') ? old('interpretation_results')=='Ad': (!empty($reviewResult->interpretation_results) ?$reviewResult->interpretation_results=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('interpretation_results') ? old('interpretation_results')=='M': (!empty($reviewResult->interpretation_results) ?$reviewResult->interpretation_results=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('interpretation_results') ? old('interpretation_results')=='I': (!empty($reviewResult->interpretation_results) ?$reviewResult->interpretation_results=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('interpretation_results') ? old('interpretation_results')=='NA': (!empty($reviewResult->interpretation_results) ?$reviewResult->interpretation_results=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="interpretation_results_remarks" id="interpretation_results_remarks"><?php echo e(old('interpretation_results_remarks')?old('interpretation_results_remarks'):(!empty($reviewResult->interpretation_results_remarks)?$reviewResult->interpretation_results_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>VII. Summary and conclusions</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Summary and conclusions</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="summary_conclusions" id="summary_conclusions">
                                 <option value="Ad" <?php if(old('summary_conclusions') ? old('summary_conclusions')=='Ad': (!empty($reviewResult->summary_conclusions) ?$reviewResult->summary_conclusions=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                 <option value="M" <?php if(old('summary_conclusions') ? old('summary_conclusions')=='M': (!empty($reviewResult->summary_conclusions) ?$reviewResult->summary_conclusions=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                 <option value="I" <?php if(old('summary_conclusions') ? old('summary_conclusions')=='I': (!empty($reviewResult->summary_conclusions) ?$reviewResult->summary_conclusions=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                 <option value="NA" <?php if(old('summary_conclusions') ? old('summary_conclusions')=='NA': (!empty($reviewResult->summary_conclusions) ?$reviewResult->summary_conclusions=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="summary_conclusions_remarks" id="summary_conclusions_remarks"><?php echo e(old('summary_conclusions_remarks')? old('summary_conclusions_remarks'):!empty($reviewResult->summary_conclusions_remarks) ?$reviewResult->summary_conclusions_remarks:''); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>VIII. Limitations ,if any</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Limitations ,if any</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="limitations" id="limitations">
                                <option value="Ad" <?php if(old('limitations') ? old('limitations')=='Ad': (!empty($reviewResult->limitations) ?$reviewResult->limitations=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                <option value="M" <?php if(old('limitations') ? old('limitations')=='M': (!empty($reviewResult->limitations) ?$reviewResult->limitations=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                <option value="I" <?php if(old('limitations') ? old('limitations')=='I': (!empty($reviewResult->limitations) ?$reviewResult->limitations=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                <option value="NA" <?php if(old('limitations') ? old('limitations')=='NA': (!empty($reviewResult->limitations) ?$reviewResult->limitations=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="limitations_remarks" id="limitations_remarks"><?php echo e(old('limitations_remarks')? old('limitations_remarks'):(!empty($reviewResult->limitations_remarks)?$reviewResult->limitations_remarks: '')); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>IX. References</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Citation style appropriate</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="citation_appropriate" id="citation_appropriate">
                                <option value="Ad" <?php if(old('citation_appropriate') ? old('citation_appropriate')=='Ad': (!empty($reviewResult->citation_appropriate) ?$reviewResult->citation_appropriate=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                <option value="M" <?php if(old('citation_appropriate') ? old('citation_appropriate')=='M': (!empty($reviewResult->citation_appropriate) ?$reviewResult->citation_appropriate=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                <option value="I"  <?php if(old('citation_appropriate') ? old('citation_appropriate')=='I': (!empty($reviewResult->citation_appropriate) ?$reviewResult->citation_appropriate=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                <option value="NA"  <?php if(old('citation_appropriate') ? old('citation_appropriate')=='NA': (!empty($reviewResult->citation_appropriate) ?$reviewResult->citation_appropriate=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="citation_appropriate_remarks" id="citation_appropriate_remarks"><?php echo e(old('citation_appropriate_remarks')? old('citation_appropriate_remarks'):(!empty($reviewResult->citation_appropriate_remarks) ? $reviewResult->citation_appropriate_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="markingPart">
                     <h3>X. Enclosures / Annexure</h3>
                     <div class="row rowgap">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                           <h4>a. Institutional Ethics Committee certificate, data proforma / questionnaire, consent form, etc.</h4>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-12">
                           <div class="formgroup">
                              <label>Select Rating</label>
                              <select name="iecc" id="iecc">
                                <option value="Ad" <?php if(old('iecc') ? old('iecc')=='Ad': (!empty($reviewResult->iecc) ?$reviewResult->iecc=='Ad':'' )): ?> selected <?php endif; ?>>(Ad)</option>
                                <option value="M" <?php if(old('iecc') ? old('iecc')=='M': (!empty($reviewResult->iecc) ?$reviewResult->iecc=='M':'' )): ?> selected <?php endif; ?>>(M)</option>
                                <option value="I" <?php if(old('iecc') ? old('iecc')=='I': (!empty($reviewResult->iecc) ?$reviewResult->iecc=='I':'' )): ?> selected <?php endif; ?>>(I)</option>
                                <option value="NA" <?php if(old('iecc') ? old('iecc')=='NA': (!empty($reviewResult->iecc) ?$reviewResult->iecc=='NA':'' )): ?> selected <?php endif; ?>>(NA)</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                           <div class="formgroup">
                              <label>Remarks</label>
                              <textarea rows="4" placeholder="" name="iecc_remarks" id="iecc_remarks"><?php echo e(old('iecc_remarks')? old('iecc_remarks'):(!empty($reviewResult->iecc_remarks)? $reviewResult->iecc_remarks:'')); ?></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                 <!-- <div class="gradeInfo">
                     <p>Excellent <span>(A)</span>, Good <span>(B)</span>, Satisfactory <span>(C)</span>, Acceptable subject to modifications <span>(D)</span>, Not acceptable <span>(E)</span></p>
                  </div>-->
                  <div class="finalComment">
                     <h4>Final comment</h4>
                     <label>Please put your final comment</label>
                     <select name="final_comment" id="final_comment" style="width: 200px;">
                         <option value="A" <?php if(old('final_comment') ? old('final_comment')=='A': (!empty($reviewResult->final_comment) ?$reviewResult->final_comment=='A':'' )): ?> selected <?php endif; ?>>Accept</option>
                        
                        <option value="D" <?php if(old('final_comment') ? old('final_comment')=='D': (!empty($reviewResult->final_comment) ?$reviewResult->final_comment=='D':'' )): ?> selected <?php endif; ?>>Send for Revision</option>
                        
                     </select>
                     <?php if($errors->has('final_comment')): ?>
                        <div class="error"><?php echo e($errors->first('final_comment')); ?></div>
                     <?php endif; ?>
                  </div>

                  <div class="row rowgap suggestion">
                     <div class="col-md-5 col-sm-4 col-xs-12">
                        <div class="formgroup">
                           <label>Suggestion</label>
                           <textarea rows="4" placeholder="" name="suggestion" id="suggestion"><?php echo e(old('suggestion')? old('suggestion'):(!empty($reviewResult->suggestion)? $reviewResult->suggestion:'')); ?></textarea>
                        </div>
                     </div>
                  </div>

                  <div class="signatureOuter">
                   <h5>Full Signature of the Evaluator</h5>
                   <span class="view-signature-admin">
                      <img src="<?php echo e(url('public/signatures/'.$reviewResult->full_evaluator_signature)); ?>">
                   </span>
                     
                  <!--  <img src="" id="showsign" class="signature-field"> -->
                  </div>
                  <input type="hidden" name="full_evaluator_signature" id="full_evaluator_signature">


                <?php if($reviewResult->final_comment =='D' || $reviewResult->final_comment =='E'): ?>
                  <div class="msnote">
                     <h4>Note</h4>
                     <ul>
                        <!-- <li>In case of 'D', Please suggest specific areas for modification in the space provided below.</li>
                        <li>In case of 'E', evaluator may please mention the reasons for rejection</li> -->
                        <li>In case of Revision Dissertation, evaluator may please mention the reasons, send for revision</li>
                     </ul>
                     <p>Modifications suggested / Reasons for revision:(Comments)</p>
                  </div>
                  <br>
                  <div class="formgroup reason-reject-box">
                              <label>Reason, Send for Revision</label><br>
                              <textarea style="width: 100%" rows="8" cols="200" placeholder="" name="reason_reject" id="reason_reject"><?php echo e($reviewResult->reason_reject); ?></textarea>
                           </div>
                           <?php if($reviewResult->alternate_reason): ?>
                            <div class="formgroup reason-reject-box">
                            <label>Reason, Send for Revision doc file:</label>
                             <span><a href="<?php echo e(url('public/pdfs/'.$reviewResult->alternate_reason)); ?>" download>Download Doc</a></span>

                            </div>
                          <?php endif; ?>

                  <div class="signatureOuter">
                   <h5>Signature of the evaluator</h5>
                     <span class="view-signature-admin">
                      <img src="<?php echo e(url('public/signatures/'.$reviewResult->evaluator_signature)); ?>">
                     </span>
                     
                       <!-- <img src="" id="showsign2" class="signature-field"> -->
                  </div>
                  <?php endif; ?>


                   <input type="hidden" name="evaluator_signature" id="evaluator_signature">
                   <?php if(\Auth::guard('admin')->user()->is_admin !=1): ?>
                  <div class="evaluatorInfo">
                     <div class="formgroup">
                        <label>Name (in Block Letters)</label>
                        <input type="text" name="" value="<?php echo e($faculty->full_name); ?>">
                     </div>
                     <div class="formgroup">
                        <label>Designation</label>
                        <input type="text" name="">
                     </div>
                     <div class="formgroup">
                        <label>Telephone No / Mobile No</label>
                        <input type="text" name="" value="<?php echo e($faculty->userDetail->mobile); ?>">
                     </div>
                     <div class="formgroup">
                        <label>E-mail</label>
                        <input type="email" name="" value="<?php echo e($faculty->email); ?>">
                     </div>
                     <div class="formgroup">
                        <label>Address</label>
                        <textarea rows="4"><?php echo e($faculty->userDetail->address); ?></textarea>
                     </div>
                     <div class="formgroup">
                        <label class="blank">&nbsp;</label>

                     </div>
                  </div>
                  <?php endif; ?>

                  </div>
               </div>
                  <!-- <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div> -->
                  </form>
               </div>

               </div>
            </div>
   </section>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>