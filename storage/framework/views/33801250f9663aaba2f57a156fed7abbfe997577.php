

<!DOCTYPE html>

<html>
<head>
    <title>WBUHS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px 0; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="">

        <tr>
          <td align="center" valign="middle" style="position: relative;">
            <h3 style="margin: 0; color: #2b2b2b; font-size: 28px; line-height: 30px; font-weight: bold; padding: 20px 0 10px;">
              Dissertation Assign
            </h3>
          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">

            <p>Admin wants to assign you a new Dissertation. Please log into your profile and then click on the below link to provide your acknowledgement.</p>
            <p>Please follow the attached user guide if facing any difficulty. Your default login password is 'user123$' .</p>
            <a href="https://wbuhs.ac.in/sdm/admin/acknowledge/<?php echo e($userId); ?>/<?php echo e($confirmationLink); ?> " style="display: inline-block; border-radius: 8px; border: 1px solid #249d40;
            background: #249d40; color: #fff; font-weight: 600; font-size: 17px; line-height: 18px;
            text-decoration: none; padding: 15px 25px; margin-bottom: 15px; text-transform: capitalize;">click here</a>
            <br>
          </td>
        </tr>

      </td>
    </tr>

    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; ">
         NOTE: For any kind of assistance send mail to this mail id contactdevteam1@gmail.com
      </td>
    </tr>
    <tr>
    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; ">
          Copyright © 2018 WBUHS
      </td>
    </tr>
</table>
</body>
</html>



