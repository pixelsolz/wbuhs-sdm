<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Speciality List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>

              <div class="clearfix"></div>

              <div class="form-group row">
                <form action="<?php echo e(url('admin/category/user')); ?>" method="get">
                  <input type="hidden" name="search" value="search">
                  <div class="col-xs-4">
                    <select class="form-control" name="search_by_category">
                      <option value="">All</option>
                      <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($category->id); ?>" <?php if($request->search_by_category && $request->search_by_category==$category->id): ?> selected <?php endif; ?>><?php echo e($category->name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </select>
                  </div>
                  <div class="col-xs-4">
                  <input type="text" class="form-control" name="input_search" placeholder="Search by name, email or mobile" value="<?php echo e(($request->input_search? $request->input_search:'' )); ?>">
                  </div>

                  <div class="col-xs-4">
                    <button type="submit" class="btn btn-info">Search</button>
                  </div>

                </form>
              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile no.</th>
                  <th>Speciality</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count); ?></td>
                    <td><?php echo e($user->full_name); ?></td>
                    <td><?php echo e($user->email); ?></td>
                    <td><?php echo e($user->userDetail->mobile); ?></td>
                    <td><?php echo e(implode(',',$user->category()->pluck('name')->toArray())); ?></td>

                  </tr>
                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="5" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
            </div>
              <div><?php echo e($users->links()); ?></div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


  </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>