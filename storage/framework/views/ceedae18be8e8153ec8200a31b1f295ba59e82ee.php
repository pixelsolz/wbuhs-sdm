<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Update Profile
      </h1>
      <ol class="breadcrumb">
         <!-- <li><a href="<?php echo e(route('user-manage.index')); ?>"><i class="fa fa-dashboard"></i> User List</a></li> -->
         <!--  <li><a href="#">Forms</a></li> -->
         <!-- <li class="active"> profile update</li> -->
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User Form</h3>
                   <?php if(Session::has('success')): ?>
                  <div class="alert alert-success">
                     <?php echo e(Session('success')); ?>

                  </div>
                  <?php endif; ?>
                  <?php if(Session::has('error')): ?>
                  <div class="alert alert-danger">
                     <?php echo e(Session('error')); ?>

                  </div>
                  <?php endif; ?>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="<?php echo e(url('admin/profile/update')); ?>" method="POST" enctype="multipart/form-data">
                  <?php echo csrf_field(); ?>
                  <?php echo method_field('PUT'); ?>
                  <div class="box-body">
                     <div class="form-group row <?php if($errors->has('f_name')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="f_name">First Name<span class="required_field">*</span></label>
                           <input type="text" name="f_name" class="form-control" value="<?php echo e(old('f_name') ?old('f_name'):$user->f_name); ?>">
                           <?php if($errors->has('f_name')): ?>
                           <span class="error"><?php echo e($errors->first('f_name')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('l_name')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="l_name">Last Name<span class="required_field">*</span></label>
                           <input type="text" name="l_name" class="form-control" value="<?php echo e(old('l_name') ? old('l_name'): $user->l_name); ?>">
                           <?php if($errors->has('l_name')): ?>
                           <span class="error"><?php echo e($errors->first('l_name')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('email')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" name="email" class="form-control" value="<?php echo e(old('email') ? old('email'): $user->email); ?>">
                           <?php if($errors->has('email')): ?>
                           <span class="error"><?php echo e($errors->first('email')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('mobile')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="mobile">Mobile<span class="required_field">*</span></label>
                           <input type="number" name="mobile" class="form-control" value="<?php echo e(old('mobile') ? old('mobile'): $user->userDetail->mobile); ?>">
                           <?php if($errors->has('mobile')): ?>
                           <span class="error"><?php echo e($errors->first('mobile')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('address')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="address">Address<span class="required_field">*</span></label>
                           <textarea name="address" class="form-control"><?php echo e(old('address') ? old('address'): $user->userDetail->address); ?></textarea>
                           <?php if($errors->has('address')): ?>
                           <span class="error"><?php echo e($errors->first('address')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <div class="form-group row <?php if($errors->has('gender')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="gender">Gender<span class="required_field">*</span></label>
                           <select class="form-control" name="gender">
                              <option value="">select</option>
                              <option value="1" <?php if($user->userDetail->gender==1): ?> selected <?php endif; ?>>Male</option>
                              <option value="2" <?php if($user->userDetail->gender==2): ?> selected <?php endif; ?>>Female</option>
                           </select>
                           <?php if($errors->has('gender')): ?>
                           <span class="error"><?php echo e($errors->first('gender')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <div class="form-group row <?php if($errors->has('password')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="password">Password<span class="required_field">*</span></label>
                           <input type="password" name="password" class="form-control" value="">
                           <?php if($errors->has('password')): ?>
                           <span class="error"><?php echo e($errors->first('password')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <!-- <div class="form-group row <?php if($errors->has('user_type')): ?>has-error <?php endif; ?>">
                        <div class="col-xs-9">
                           <label for="user_type">User Type<span class="required_field">*</span></label>
                           <select class="form-control" name="user_type">
                              <option value="">select</option>
                              <option value="1" <?php if(old('user_type') && old('user_type')==1? 1: $user->is_admin): ?> selected <?php endif; ?>>Super Admin</option>
                              <option value="2" <?php if(old('user_type') && old('user_type')==2? 2: $user->is_admin): ?> selected <?php endif; ?>>Admin</option>
                              <option value="3" <?php if(old('user_type')==3): ?> selected <?php endif; ?>>Student</option>
                           </select>
                           <?php if($errors->has('user_type')): ?>
                           <span class="error"><?php echo e($errors->first('user_type')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div> -->

                     <div class="form-group">
                        <label for="exampleInputFile">Profile Image</label>
                        <input type="file" id="exampleInputFile" name="profile_image">
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>