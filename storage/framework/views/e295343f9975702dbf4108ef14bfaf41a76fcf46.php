<?php $__env->startSection('content'); ?>

<style type="text/css">
  .reg-error{
    color: #c11a05;
    font-size: 13px;
    line-height: 18px;
    display: block;
    margin: 0 0 20px 0;
  }

</style>
 <section class="student-thsis">
               <div class="container">
                    <div class="thsis-content">
                        <h3>Synopsis & Dissertation</h3>
                            <p>As a Student, you need to First register by filling up the Registration details. Only Registered students will be able to login to this system and then can submit their dissertation after paying the fees of Rs. 2000. All you need to do is to fill up the form and submit it. Once submitted, you will get an OTP on your registered mobile number. This is done for verification purpose. Enter the OTP to log in. </p>
                	  <p>Download Synopsis Submission Userguide <a href="<?php echo e(url('synopsis-manual')); ?>" title="Synopsis Submission Userguide"><i class="fa fa-download"></i></a></p>
		  </div>
               </div>
           </section>

           <section class="thsis-frm-rgtr-sec">
               <div class="container">

                <div class="ths-frm-wrp">

                     <div class="anchr-wrp">
                        <a href="<?php echo e(url('login')); ?>" class="btn btn-primary login">Login</a>
                        <a href="<?php echo e(url('registration')); ?>" class="btn btn-primary rgtr active">Register</a>

                    </div>


                     <div class="frm-wrpd-rgstr">

                       <form method="post" action="<?php echo e(url('registration')); ?>" enctype="multipart/form-data" id="ajax-form" data-redirectUrl="<?php echo e(url('/otp-form')); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="form-group f_name">
                            <label>First Name <em>*</em></label>
                            <input type="text" class="form-control" name="f_name" value="<?php echo e(old('f_name')); ?>" placeholder="Enter First Name">
                             <span class="error" id="f_name"></span>
                        </div>
                        <div class="form-group l_name">
                            <label>Last Name <em>*</em></label>
                            <input type="text" class="form-control "  name="l_name" value="<?php echo e(old('l_name')); ?>" placeholder="Enter Last Name">
                             <span class="error" id="l_name"></span>
                        </div>

                        <div class="form-group email">
                            <label>Email <em>*</em></label>
                            <input type="Email" class="form-control " name="email" value="<?php echo e(old('email')); ?>" placeholder="Enter Email">
                            <span class="error" id="email"></span>
                        </div>
                        <div class="form-group mobile">
                            <label>Mobile No. <em>*</em></label>
                            <input type="tel" class="form-control " name="mobile" value="<?php echo e(old('mobile')); ?>" placeholder="Enter 10 digit mobile number ">
                            <span class="error" id="mobile"></span>
                        </div>

                        <div class="form-group address">
                        <label>Address <em>*</em></label>
                      <textarea name="address" class="form-control " placeholder="Enter Address"><?php echo e(old('address')); ?></textarea>

                        <span class="error" id="address"></span>

                       </div>

                       <div class="form-group city_id">
                        <label>City <em>*</em></label>
                        <input type="text" class="form-control" name="city_id" value="<?php echo e(old('city_id')); ?>" placeholder="City">
                        

                        <span class="error" id="city_id"></span>

                       </div>

                      <div class="form-group gender">
                      <label>Gender <em>*</em></label>
                        <select class="form-control " name="gender">
                          <option value="">Select</option>
                          <option value="1">Male</option>
                          <option value="2">Female</option>
                        </select>

                        <span class="error" id="gender"></span>

                      </div>
                      <?php $registrationCheckUrl = url('registration-no/check'); ?>
                      <div class="form-group registration_no">
                            <label>Registration No. <em>*</em></label>
                            <input type="text" class="form-control " name="registration_no" placeholder="Enter Registration no." onblur="registrationCheck(event,'<?php echo e(csrf_token()); ?>','<?php echo e($registrationCheckUrl); ?>')">
                             <span class="error" id="registration_no"></span>
                        </div>

                        <div class="form-group password">
                            <label>Password <em>*</em></label>
                            <input type="password" class="form-control " name="password" placeholder="Enter Password">
                             <span class="error" id="password"></span>
                        </div>

                        <div class="form-group">
                            <label>Confirm Password <em>*</em></label>
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Enter Password confirmation">
                            <span class="error" id="password_confirmation"></span>
                        </div>
                        <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="<?php echo e(config('app.google_recaptcha_key')); ?>"></div>
                        <span class="error" id="grecaptcha_response"></span>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-primary btn-disable" type="submit"> Submit &nbsp;<i class="fa fa-spinner fa-spin" id="submit-loader" style="font-size:24px; display: none;"></i></button>
                        </div>
                    </form>
                    </div>


                </div>

               </div>
           </section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>