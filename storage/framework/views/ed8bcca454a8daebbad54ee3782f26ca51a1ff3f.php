<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dessertation Review
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
               <div class="table-responsive">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Registration No. & Year</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <th>Action</th>
                  <th>Status</th>
                  <th>Assigned Date</th>
                  <th>Final Comment</th>
                 <!--  <th>Remarks</th> -->
                  <th>Submitted Date</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $thesisData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $thesis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count); ?></td>
                    <td><?php echo e($thesis->name_of_student); ?></td>
                    <td><?php echo e($thesis->registration_no. ' '.$thesis->registration_year); ?></td>
                    <td><?php echo e($thesis->institute_name); ?></td>
                    <td><?php echo e($thesis->mobile_landline); ?></td>
                    <td>

                    <?php if($thesis->thesisResult()->facultyWiseResult($faculty->id, 'grade')): ?>
                     <a href="<?php echo e(url('admin/student-result/edit',['thesis'=>$thesis->id])); ?>" data-tog="tooltip" title="Edit Garde"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                     <?php else: ?>
                     <?php
                      $transaction = $thesis->transaction()->where('to_user_id',$faculty->id)->first();

                      $resultData ='';
                      if($transaction && $transaction->is_reassigned ==1){
                        $resultData = $thesis->thesisResult()->where('faculty_id',$faculty->id)->where('transaction_reassigned_group',$transaction->reassigned_group)->first();
                      }elseif($transaction && $transaction->status ==4){
                       $resultData = $thesis->thesisResult()->where('faculty_id',$faculty->id)->first();
                      }
                     ?>
                     <a href="<?php echo e(($resultData)?url('admin/student-result/view',['result'=>$resultData->id]) :url('admin/student-result/add',['thesis'=>$thesis->id])); ?>" data-tog="tooltip" title="See Evaluation"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></a>
                     <?php endif; ?>
                      
                    </td>
                    <td><?php echo e($thesis->transaction()->facultyData($faculty->id)->status_text); ?>

                    
                    </td>
                    <td><?php echo e(\Carbon\Carbon::parse($transaction->accknowledge_email_date)->format('d/m/Y')); ?></td>
                    <td><?php echo e($thesis->thesisResult()->facultyWiseResult($faculty->id, 'final_comment')); ?></td>
                    
                    <td><?php echo e($thesis->thesisResult()->facultyWiseResult($faculty->id, 'created_at')); ?></td>
                  </tr>
                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="10" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
            </div>
              <div><?php echo e($thesisData->links()); ?></div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>

        <div class="modal fade" id="status_modal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Status</h4>
              </div>
              <div class="modal-body">
              <form action="<?php echo e(url('admin/student-result/status-change')); ?>" id="status-change-form">
                  <?php echo csrf_field(); ?>
                  <div class="form-group">
                    <label>Status change:</label>
                    <select name="status" class="form-control">
                        <option value="">select</option>
                        <option value="1">On Process</option>
                        <option value="2">Access</option>
                        <option value="3">Submit</option>
                    </select>
                  </div>
                  <input type="hidden" name="transaction_id">
              </form>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-disable" onclick="changeThesisStatus()">Submit &nbsp; <i class="fa fa-spinner fa-spin" style="font-size:16px; display: none;" ></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>