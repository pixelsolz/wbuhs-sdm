<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        BOS Members Uploaded PDF List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>BOS. Name</th>
                  <th>Pdf</th>
                  <th>Created date</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $bosPdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pdf): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count); ?></td>
                    <td><?php echo e($pdf->user->full_name); ?></td>
                    <td><a href="<?php echo e(url('public/bos_pdfs/'. $pdf->pdf_file)); ?>" download><?php echo e($pdf->pdf_file); ?></a></td>
                    <td><?php echo e(\Carbon\Carbon::parse($pdf->created_at)->format('Y-m-d')); ?></td>
                  </tr>
                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="4" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
              <div><?php echo e($bosPdfs->links()); ?></div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>