<table>
	<thead>
		    <tr>
		        <th>Name of Student</th>
                <th>Reg. No.</th>
                <th>Reg. Year</th>
                <th>Institute name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Course</th>
                <th>Subject</th>
                <th>Synopsis Status</th>
                <th>Synopsis Submitted Date</th>
                <th>BOS</th>
                <th>BOS phone</th>
                <th>BOS Email</th>
                <th>BOS College</th>
                <th>BOS Status</th>
		    </tr>
	    </thead>
	    	    <tbody>
	    	<?php $__empty_1 = true; $__currentLoopData = $synopsis_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $synopsis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
	        <tr>
	           <td><?php echo e(@$synopsis->name_of_student); ?></td>
	            <td><?php echo e(@$synopsis->registration_no); ?></td>
	            <td><?php echo e(@$synopsis->registration_year); ?></td>
	            <td><?php echo e(@$synopsis->institute_name); ?></td>
	            <td><?php echo e(@$synopsis->email); ?></td>
	            <td><?php echo e(@$synopsis->mobile_landline); ?></td>
	            <td><?php echo e(@$synopsis->category->parentCat->name); ?></td>
	            <td><?php echo e(@$synopsis->category->name); ?></td>
	            <td><?php echo e(@$synopsis->status_text); ?></td>
	            <td><?php echo e(\Carbon\Carbon::parse(@$synopsis->created_at)->format('d-m-Y')); ?></td>
                <td>
	        		<table>
	        		<tr>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		<td></td>
	        		</tr>
	        		<?php $__currentLoopData = @$synopsis->transaction; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	        		<tr>
	        		<td><?php echo e(@$transaction->user->full_name); ?></td>
	        		<td><?php echo e(@$transaction->user->mobile); ?></td>
	        		<td><?php echo e(@$transaction->user->email); ?></td>
	        		<td><?php echo e(@$transaction->user->userDetail->college_name); ?></td>
	        		<td><?php echo e(@$transaction->review_status_text); ?></td>
	        		</tr>
	        		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        		</table>
	        	</td>

	        </tr>
	    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
	    	<tr>
	            <td></td>
	        </tr>
	    	<?php endif; ?>
	    </tbody>
</table>