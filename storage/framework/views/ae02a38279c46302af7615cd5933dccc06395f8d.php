<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
   <div class="lft-pnl">
      <a href="<?php echo e(url('dashboard')); ?>" class="<?php echo e(Request::is('dashboard')? 'active': ''); ?>"><i class="fa fa-dashboard" aria-hidden="true"></i> Dashboard</a>
      <?php if(!@Auth::user()->studentThesis && !@Auth::user()->studentThesis->is_paid): ?>

      <?php if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid): ?>
         <?php if(\Auth::user()->studentSynopsis->status ==4): ?>
         <a href="<?php echo e(route('synopsis.edit',['id'=>\Auth::user()->studentSynopsis->id])); ?>" class="<?php echo e((Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
         <?php else: ?>
         <a href="<?php echo e(url('synopsis',[\Auth::user()->studentSynopsis])); ?>" class="<?php echo e((Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
         <?php endif; ?>
      <?php elseif(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid ==0): ?>
         <a href="<?php echo e(url('synopsis/old-data',['id'=>\Auth::user()->studentSynopsis->id])); ?>" class="<?php echo e((Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
      <?php else: ?>
      <a href="<?php echo e(url('synopsis/create')); ?>" class="<?php echo e((Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
      <?php endif; ?>

      <?php endif; ?>
     

     <!-- check user has thesis-->
      

      <?php if(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid): ?>

      <a href="<?php echo e(\Auth::user()->studentThesis->status==4 ? route('dissertation.edit',['id'=>\Auth::user()->studentThesis]): url('dissertation',[\Auth::user()->studentThesis])); ?>" class="<?php echo e((Request::is('dissertation')||Request::is('dissertation/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i>Dissertation Submission</a>

       <?php elseif(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid ==0): ?>
         <a href="<?php echo e(url('dissertation/old-data',['id'=>\Auth::user()->studentThesis->id])); ?>" class="<?php echo e((Request::is('dissertation')||Request::is('dissertation/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i>Dissertation Submission</a>
      <?php else: ?>
      <a href="<?php echo e(url('dissertation/create')); ?>" class="<?php echo e((Request::is('dissertation')||Request::is('dissertation/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i>Dissertation Submission</a>
      <?php endif; ?>
      
      <?php if((@\Auth::user()->studentSynopsis || @\Auth::user()->studentThesis) &&  (@\Auth::user()->studentSynopsis->status ==3 || @\Auth::user()->studentSynopsis->status == 4 || (@\Auth::user()->studentThesis && @\Auth::user()->studentThesis->status ==4))): ?>
      <a href="<?php echo e(route('rejection-list')); ?>" class="<?php echo e((Request::is('rejection-list')||Request::is('rejection-list/*'))? 'active': ''); ?>"> <i class="fa fa-ban" aria-hidden="true"></i>Rejection or Resubmission List</a>
      <?php endif; ?>


      
      <!-- END-->
      <?php if((!empty(\Auth::user()->studentSynopsis->payements) && count(\Auth::user()->studentSynopsis->payements))|| (!empty(\Auth::user()->studentThesis->payements) && count(\Auth::user()->studentThesis->payements)) ): ?>
      <a href="<?php echo e(url('payment/history')); ?>" class="<?php echo e(Request::is('payment/history')? 'active': ''); ?>"> <i class="fa fa-money" aria-hidden="true"></i> Payment History</a>
     <?php endif; ?>

      <a href="<?php echo e(route('profile.edit',['profile'=>\Auth::user()])); ?>" class="<?php echo e((Request::is('profile')|| Request::is('profile/*'))? 'active': ''); ?>"> <i class="fa fa-user" aria-hidden="true"></i> Profile</a>
      <a href="<?php echo e(url('change-password')); ?>" class="<?php echo e(Request::is('change-password')? 'active': ''); ?>"> <i class="fa fa-key" aria-hidden="true"></i> Change Password</a> <a href="<?php echo e(url('support')); ?>" class="<?php echo e(Request::is('support')? 'active': ''); ?>"> <i class="fa fa-support" aria-hidden="true"></i> Support</a>
   </div>
</div>
