<table>
	<thead>
		    <tr>
		        <th>Name of Student</th>
                <th>Reg. No.</th>
                <th>Reg. Year</th>
                <th>Institute name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Subject</th>
                <th>Synopsis Status</th>
                <th>Synopsis Submitted Date</th>
		    </tr>
	    </thead>
	    	    <tbody>
	    	<?php $__empty_1 = true; $__currentLoopData = $synopsis_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $synopsis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
	        <tr>
	           <td><?php echo e(@$synopsis->name_of_student); ?></td>
	            <td><?php echo e(@$synopsis->registration_no); ?></td>
	            <td><?php echo e(@$synopsis->registration_year); ?></td>
	            <td><?php echo e(@$synopsis->institute_name); ?></td>
	            <td><?php echo e(@$synopsis->email); ?></td>
	            <td><?php echo e(@$synopsis->mobile_landline); ?></td>
	            
	            <td><?php echo e(@$synopsis->category->name); ?></td>
	            <td><?php echo e(@$synopsis->status_text); ?></td>
	            <td><?php echo e(\Carbon\Carbon::parse(@$synopsis->created_at)->format('d-m-Y')); ?></td>

	        </tr>
	    	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
	    	<tr>
	            <td></td>
	        </tr>
	    	<?php endif; ?>
	    </tbody>
</table>