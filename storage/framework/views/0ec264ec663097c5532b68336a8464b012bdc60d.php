<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Student Synopsis List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>

              <?php if(Session::has('err_import')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Same registration no exist - <?php echo e(session('err_import')); ?>

              </div>
              <?php endif; ?>

              <div class="clearfix"></div>

                <div class="form-group row">
                  <form action="<?php echo e(route('student-synopsis.list')); ?>" method="get">
                    <input type="hidden" name="search" value="search">

                    <div class="col-xs-3"><br/>
                       <input class="form-control" type="text" name="by_name" placeholder="search By Student Name" autocomplete="off" <?php if(!empty($request->by_name)): ?> value="<?php echo e($request->by_name); ?>" <?php endif; ?>>
                    </div>
                    <div class="col-xs-3"><br/>
                       <input class="form-control" type="text" name="phone" placeholder="search By Phone No."  <?php if(!empty($request->phone)): ?> value="<?php echo e($request->phone); ?>" <?php endif; ?>>
                    </div>
                    <div class="col-xs-3"><br/>
                       <input class="form-control" type="text" name="email" placeholder="search By Email Id"  <?php if(!empty($request->email)): ?> value="<?php echo e($request->email); ?>" <?php endif; ?>>
                    </div>
                    <div class="col-xs-1"><br/>
                      <button type="submit" class="btn btn-info">Search</button>
                    </div>
                  </form>
                </div>

                <div class="row">

                  <div class="col-xs-3">
                       <a href="<?php echo e(url('public/manuals/demo-import-synopsis-students.xlsx')); ?>" class="btn btn-success" download>Download Sample CSV</a>
                  </div>
                <form class="form-inline" method="POST" action="<?php echo e(url('admin/student-synopsis-list/import')); ?>" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>

                <div class="form-group">
                   <label for="csv_file">CSV file to import:</label>
                    <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                     <button class="btn btn-info" type="submit">submit</button>
                </div>
                </form>

              </div>

            </div>
            <!-- /.box-header -->

            <div class="box-body" id="print_area">
              <div class="table-responsive">
              <table  class="table table-bordered table-hover" width="100%">
                <thead>
                <tr>
                  <th width='5%'>Sl No.</th>
                  <th width='20%'>Name of Student</th>
                  <th width='20%'>Unique Id</th>
                  <th width='10%'>Course Name</th>
                  <!--<th width='20%'>Institute</th>-->
                  <th width='10%'>Phone</th>
                  <th width='10%'>Email</th>
                  <th width='5%'>Edit</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td width='5%'><?php echo e($count); ?></td>
                    <td width='20%'><?php echo e($student->name); ?></td>
                    <td width='20%'><?php echo e($student->unique_id); ?></td>
                    <td width='20%'><?php echo e($student->course); ?></td>
                    <!-- <td width='20%'><?php echo e($student->institute); ?></td>-->
                    <td width='10%'><?php echo e($student->phone); ?></td>
                    <td width='10%'><?php echo e($student->email); ?></td>
                    <td width="5%">

                        <a href="<?php echo e(url('admin/student-synopsis/edit',['id'=>$student->id])); ?>" class="glyphicon glyphicon-pencil" data-tog="tooltip" title="Edit"></a>
                    </td>

                  </tr>
                   <?php $count++; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                  <tr><td colspan="9" style="text-align: center;">No Data Availbale</td></tr>
                <?php endif; ?>


                </tbody>

              </table>
            </div>
              <div><?php echo e($students->links()); ?></div>

            </div>


          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>