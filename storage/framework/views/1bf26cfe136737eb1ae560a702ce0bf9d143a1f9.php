<?php $__env->startSection('content'); ?>
<section class="desh-bord">
   <div class="container">
      <?php if(Session::has('msg')): ?>
      <div class="alert alert-success">
         <strong><?php echo e(Session('msg')); ?></strong>
      </div>
      <?php endif; ?>
      <?php if(Session::has('status')): ?>
      <div class="alert alert-success" role="alert">
         <?php echo e(session('status')); ?>

      </div>
      <?php endif; ?>
      <div class="row">
         <?php echo $__env->make('frontend.includes.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Dashboard</h3>
            <?php if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->status == 3): ?>
            <div class="alert alert-danger">
              <strong>Synopsis Rejected!</strong> Your Synopsis has been Rejected.
            </div>
            <?php elseif(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->status == 4): ?>
            <div class="alert alert-danger">
              <strong>Synopsis Send for revision!</strong> Your Synopsis has not been accepted please Resubmit again.
            </div>
            <?php endif; ?>
            <?php if(Auth::user()->studentThesis && \Auth::user()->studentThesis->status ==4): ?>
            <div class="alert alert-danger">
              <strong>Dissertation Send for revision!</strong> Your Dissertation has not been accepted please Resubmit again.
            </div>
            <?php endif; ?>
            <div class="right-pnl">
               <ul>
               <!-- check user not submit thesis -->
               <?php if(!@Auth::user()->studentThesis && !@Auth::user()->studentThesis->is_paid): ?>
                  <li>
                     <div class="lft">
                        <h4><?php echo e(\Auth::user()->studentSynopsis()->count()); ?></h4>
                        <?php if(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid): ?>
                        <?php if(\Auth::user()->studentSynopsis->status ==4): ?>
                        <a href="<?php echo e(route('synopsis.edit',['id'=>\Auth::user()->studentSynopsis->id])); ?>" class="<?php echo e((Request::is('synopsis')||Request::is('synopsis/*'))? 'active': ''); ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Synopsis Submission</a>
                        <?php else: ?>
                        <span><a href="<?php echo e(url('synopsis',[\Auth::user()->studentSynopsis])); ?>">Synopsis</a></span>
                        <?php endif; ?>
                        <?php elseif(\Auth::user()->studentSynopsis && \Auth::user()->studentSynopsis->is_paid ==0): ?>
                        <span><a href="<?php echo e(url('synopsis/old-data',['id'=>\Auth::user()->studentSynopsis->id])); ?>">Synopsis</a></span>
                        <?php else: ?>
                        <span><a href="<?php echo e(url('synopsis/create')); ?>">Synopsis</a></span>
                        <?php endif; ?>
                     </div>
                     <div class="right">
                        <span class="icon"><i class="fa fa-book" aria-hidden="true"></i></span>
                     </div>
                  </li>

                  <?php endif; ?>
                  <!-- END -->

                  
                  <!-- check user has thesis-->
                 
                  <li>
                     <div class="lft">
                        <h4><?php echo e(\Auth::user()->studentThesis()->count()); ?></h4>
                        <?php if(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid): ?>
                        <span><a href="<?php echo e(\Auth::user()->studentThesis->status ==4? route('dissertation.edit',['id'=>\Auth::user()->studentThesis]): url('dissertation',[\Auth::user()->studentThesis])); ?>">Dissertation</a> </span>
                        <?php elseif(\Auth::user()->studentThesis && \Auth::user()->studentThesis->is_paid ==0): ?>
                        <span><a href="<?php echo e(url('dissertation/old-data',['id'=>\Auth::user()->studentThesis->id])); ?>">Dissertation</a></span>
                        <?php else: ?>
                        <span><a href="<?php echo e(url('dissertation/create')); ?>">Dissertation</a> </span>
                        <?php endif; ?>
                     </div>
                     <div class="right">
                        <span class="icon"><i class="fa fa-file-text-o" aria-hidden="true"></i></span>
                     </div>
                  </li>

                 
                  <!-- END -->

                  
                  <!-- <li>
                     <div class="lft">
                        <h4>14</h4>
                        <span>VAT Check Done</span>
                     </div>
                     <div class="right">
                        <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                     </div>
                  </li> -->
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>