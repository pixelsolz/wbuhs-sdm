<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>
<?php $usrCont = app('App\Http\Controllers\Admin\UserController'); ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dissertation Revision
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>

              <div class="clearfix"></div>

            </div>
            <!-- /.box-header -->

            <div class="box-body">
               <div class="table-responsive">
              <table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name of Student</th>
                  <th>Registration No. & Year</th>
                  <th>Institute name</th>
                  <th>Mobile No.</th>
                  <th>Status</th>
                  <th>Document</th>
                  <th>Assigned Date</th>
                  <th>Final Comment</th>
                  <th>Submitted Date</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1;$page = !empty($_GET['page'])?$_GET['page']-1:0; ?>
                <?php $__empty_1 = true; $__currentLoopData = $thesisData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $thesis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count +($page*10)); ?></td>
                    <td><?php echo e($thesis->name_of_student); ?></td>
                    <td><?php echo e($thesis->registration_no. ' '.$thesis->registration_year); ?></td>
                    <td><?php echo e($thesis->institute_name); ?></td>
                    <td><?php echo e($thesis->mobile_landline); ?></td>
                    <td><?php echo e($thesis->transaction()->facultyData($user->id)->status_text); ?></td>
                    <td>
                      <a href="<?php echo e(url('admin/student-dissertation/viewform',['thesis_id'=>$thesis->id])); ?>" target="_blank" data-tog="tooltip" title="Dissertation Form Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a> ||
                      <a href="<?php echo e(url('admin/student-dissertation/view',['thesis_id'=>$thesis->id])); ?>" target="_blank" data-tog="tooltip" title="Dissertation Pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                      <?php if(\Auth::guard('admin')->user()->is_admin ==1): ?>
                       &nbsp;<a href="javascript:void(0)" data-tog="tooltip"  title="Replace Pdf" onclick="replacePDF('<?php echo e($thesis->id); ?>')"><i class="fa fa-hand-pointer-o"></i></a>
                       <?php endif; ?>
                      <?php echo $thesis->getOtherPdf($thesis->id); ?>

                    </td>
                    <td><?php echo e(\Carbon\Carbon::parse($thesis->transaction()->facultyData($user->id)->accknowledge_email_date)->format('d/m/Y')); ?></td>
                    <td><?php echo e($thesis->thesisResult()->facultyWiseResult($user->id, 'final_comment')); ?></td>
                    <td><?php echo e($thesis->thesisResult()->facultyWiseResult($user->id, 'created_at')); ?></td>
                  </tr>


                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="9" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
            </div>
              <div><?php echo e($thesisData->links()); ?></div>

            </div>
            <!-- /.box-body -->

            <div class="overlay" style="display: none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->



  </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>