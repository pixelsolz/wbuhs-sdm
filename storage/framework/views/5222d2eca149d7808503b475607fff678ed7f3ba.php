<?php $__env->startSection('content'); ?>
<?php
$currentRoute = Route::currentRouteName();
?>
<style>
   .kbw-signature { width: 400px; height: 100px; }
   #append_more_pdf{float: right;width: 62%;margin: 10px 0 10px 10px}
   #append_more_pdf .form-group{width: 100%; margin: 0;}
   #append_more_pdf .form-group > input{width: 100%;}
</style>
<section class="desh-bord">
   <div class="container">
      <?php if(Session::has('error')): ?>
      <div class="alert alert-danger">
         <strong><?php echo e(Session('error')); ?></strong>
      </div>
      <?php endif; ?>
      <div class="row">
         <?php echo $__env->make('frontend.includes.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Synopsis Form</h3>

          <div class="time-line-div">
                                <h3>Status</h3>
                                <div class="time-line-div-inner">
                                <div class="legend">
                                  <ul>
                                    <li><span class="notcomplete"></span> Not Complete</li>
                                    <li><span class="complete"></span> Complete</li>
                                  </ul>
                                </div>
                              <ul class="time-line-ul">

                                  <li class="time-line-li active-li">
                                  <span class="time-line-span"><?php echo e($synopsis->is_resubmitted?'Resubmitted':'Submitted'); ?></span><span class="circle" data-toggle="tooltip" title="<?php echo \Carbon\Carbon::parse($synopsis->created_at)->format('d-m-Y'); ?>"></span>
                                </li>
                                <?php if($synopsis->status !=4 && $synopsis->status !=3): ?>
                                <li class="time-line-li <?php if($synopsis->status==2): ?> active-li <?php endif; ?>">
                                  <span class="time-line-span">Accepted</span><span class="circle" data-toggle="tooltip" title="cdcdc"></span>
                                </li>
                                <?php endif; ?>
                                <?php if($synopsis->status ==4): ?>
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Send for Revision</span><span class="circle" data-toggle="tooltip" title="<?php echo \Carbon\Carbon::parse($synopsis->updated_at)->format('d-m-Y'); ?>"></span>
                                </li>
                                <?php endif; ?>
                                <?php if($synopsis->status ==3): ?>
                                <li class="time-line-li active-li">
                                  <span class="time-line-span">Rejected</span><span class="circle" data-toggle="tooltip" title="<?php echo \Carbon\Carbon::parse($synopsis->updated_at)->format('d-m-Y'); ?>"></span>
                                </li>
                                <?php endif; ?>

                              </ul>
                              </div>
                              </div>


            <div class="right-pnl">

               
               <form action="<?php echo e(route('synopsis.update',['id'=>$synopsis->id])); ?>" method="post" enctype="multipart/form-data" id="synopsis_edit_form">
                  <?php echo csrf_field(); ?>
                  <?php echo method_field('PUT'); ?>


                  <div class="form-group <?php if($errors->first('category_id')): ?> has-error <?php endif; ?>">

                     <div class="category-label">
                        <label for="category_id">Category<span class="require-asterik">*</span>:</label>
                     </div>
                     <input class="form-control" type="text" value="<?php echo e($course->getRoot($course->parent)->name); ?>" readonly>

                  </div>

                  <?php if($course->getParent($course->parent) && $course->getParent($course->parent)->parent !=0): ?>
                  <div class="form-group <?php if($errors->first('category_id')): ?> has-error <?php endif; ?>">

                     <div class="category-label">
                        <label for="category_id">Sub category<span class="require-asterik">*</span>:</label>
                     </div>
                     <input class="form-control" type="text" value="<?php echo e($course->getParent($course->parent)->name); ?>" readonly>

                  </div>
                  <?php endif; ?>

                    <div class="form-group <?php if($errors->first('category_id')): ?> has-error <?php endif; ?>">

                     <div class="category-label">
                        <label for="category_id">Course<span class="require-asterik">*</span>:</label>
                     </div>
                     <input class="form-control" type="text" value="<?php echo e($course->name); ?>" readonly>

                  </div>

                  <div class="form-group <?php if($errors->first('name_of_student')): ?> has-error <?php endif; ?>">
                     <label for="name_of_student">Name of the Student <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="name_of_student" placeholder="Enter Student Name" name="name_of_student" value="<?php echo e(old('name_of_student') ?old('name_of_student'):$synopsis->name_of_student); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group <?php if($errors->first('registration_no')): ?> has-error <?php endif; ?>">
                     <label for="registration_no">WBUHS Reg. No./ Unique Id :</label>
                     <input type="text" class="form-control" id="registration_no" placeholder="Enter Registration No" name="registration_no" value="<?php echo e(old('registration_no')?old('registration_no'):$synopsis->registration_no); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group <?php if($errors->first('registration_year')): ?> has-error <?php endif; ?>">
                     <label for="registration_year">WBUHS Reg. Year :</label>
                     <input type="number" class="form-control" id="registration_year" placeholder="Enter Registration Year" name="registration_year" value="<?php echo e(old('registration_year')? old('registration_year'):$synopsis->registration_year); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group <?php if($errors->first('institute_name')): ?> has-error <?php endif; ?>">
                     <label for="institute_name">Name of the Institution <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="institute_name" placeholder="Enter Institute Name" name="institute_name" value="<?php echo e(old('institute_name')? old('institute_name'):$synopsis->institute_name); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group <?php if($errors->first('mobile_landline')): ?> has-error <?php endif; ?>">
                     <label for="mobile_landline">Cell Phone <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="mobile_landline" placeholder="Enter Mobile / Land No" name="mobile_landline" value="<?php echo e(old('mobile_landline')? old('mobile_landline'):$synopsis->mobile_landline); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group <?php if($errors->first('email')): ?> has-error <?php endif; ?>">
                     <label for="email">E-mail <span class="require-asterik">*</span>:</label>
                     <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" value="<?php echo e(old('email')? old('email'):$synopsis->email); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>
                     <?php if($errors->first('email')): ?>
                     <span class="error"><?php echo e($errors->first('email')); ?></span>
                     <?php endif; ?>
                  </div>
                   <div class="form-group <?php if($errors->first('dob')): ?> has-error <?php endif; ?>">
                     <label for="dob">DOB.<span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control datepicker_dob" id="dob" placeholder="DOB" name="dob" value="<?php echo e((old('dob'))?old('dob'):(@Auth::user()->userDetail->dob ? @Auth::user()->userDetail->dob: '')); ?>" readonly>

                  </div>
                  <div class="form-group <?php if($errors->first('guide_name')): ?> has-error <?php endif; ?>">
                     <label for="guide_name">Guide Name <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="guide_name" placeholder="Enter Guide Name" name="guide_name" value="<?php echo e(old('guide_name')? old('guide_name'):$synopsis->guide_name); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group <?php if($errors->first('guide_designation')): ?> has-error <?php endif; ?>">
                     <label for="guide_designation">Guide Designation <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="guide_designation" placeholder="Enter Guide Designation" name="guide_designation" value="<?php echo e(old('guide_designation') ? old('guide_designation'):$synopsis->guide_designation); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>

                  </div>
                  <div class="form-group">
                     <label for="co_guide_name">Co-Guides Name:</label>
                     <input type="text" class="form-control" id="co_guide_name" placeholder="Enter Co Guide Name" name="co_guide_name" value="<?php echo e(old('co_guide_name')? old('co_guide_name'):$synopsis->co_guide_name); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>
                  </div>
                  <div class="form-group">
                     <label for="co_guide_designation">Co-Guides Designation:</label>
                     <input type="text" class="form-control" id="co_guide_designation" placeholder="Enter Co Guide Designation" name="co_guide_designation" value="<?php echo e(old('co_guide_designation')?old('co_guide_designation'):$synopsis->co_guide_designation); ?>" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>>
                  </div>
                  <div class="form-group">
                     <label for="proposed_title_thesis">Proposed Title <span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="proposed_title_thesis" id="proposed_title_thesis" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>><?php echo e(old('proposed_title_thesis')?old('proposed_title_thesis'):$synopsis->proposed_title_thesis); ?></textarea>
                     <?php if($errors->first('proposed_title_thesis')): ?>
                     <span class="error"><?php echo e($errors->first('proposed_title_thesis')); ?></span>
                     <?php endif; ?>
                  </div>
                  <div class="form-group <?php if($errors->first('proposed_work_place')): ?> has-error <?php endif; ?>">
                     <label for="proposed_work_place">Proposed place of work <span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="proposed_work_place" id="proposed_work_place" <?php if($currentRoute=='synopsis.show'): ?> readonly <?php endif; ?>><?php echo e(old('proposed_work_place') ? old('proposed_work_place'):$synopsis->proposed_work_place); ?></textarea>

                  </div>
                 <div class="form-group <?php if($errors->first('student_pdf')): ?> has-error <?php endif; ?>">
                     <label for="proposed_work_place">Upload PDF <span class="require-asterik">*</span>:<?php if($currentRoute=='synopsis.show'): ?> <a href="<?php echo e(route('synopsis.view-pdf',['id'=>$synopsis->id])); ?>" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px"></i></a>
                      <?php else: ?> <span>(Add more PDF)<a href="javascript:void(0)" onclick="addMorePdf()"> <i class="fa fa-plus" style="font-size:18px"></i></a></span> <?php endif; ?>
                      <?php if($synopsis->resubmit_enable==1): ?><span>(Add more PDF)<a href="javascript:void(0)" onclick="addMorePdf()"> <i class="fa fa-plus" style="font-size:18px"></i></a></span><?php endif; ?>
                    </label>
                     <input class="form-control" type="file" name="student_pdf" data-url="<?php echo e(url('synopsis/file-upload')); ?>">
                     <div id="append_more_pdf">
                     </div>
                  </div>
                  <?php if(($currentRoute=='synopsis.show' && $synopsis->more_pdfs) || $synopsis->resubmit_enable !=0): ?>
                  <div class="form-group">
                      <label for="proposed_work_place">Other PDF:</label>
                      <?php $__currentLoopData = explode(',',$synopsis->more_pdfs); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $other_pdf): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <a href="<?php echo e(url('public/pdfs/'.$other_pdf)); ?>" download><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px"></i></a>
                       &nbsp;
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
                  <?php endif; ?>

                  <input type="hidden" name="student_signature" id="student_signature">
                  <div>
                     <?php if($currentRoute!='synopsis.show'): ?>
                     <label for="sig" class="signature-field">Signature <span class="require-asterik">*</span>: (or Upload Signature)<a href="javascript:void(0)" onclick="showSignUpload('signature_off')"> click</a></label>
                     <label for="sig" style="display: none;" class="signature-upload">Upload Signature <span class="require-asterik">*</span>: (or Signature)<a href="javascript:void(0)" onclick="showSignUpload('signature_on')"> click</a></label>
                     <div class="form-group signature-field">
                        <div id="sig"></div>
                     </div>
                     <p style="clear: both;" class="signature-field">
                         <a href="#" id="saveImage" data-url="<?php echo e(url('synopsis/save-signature')); ?>"  onclick="signatureUpload('<?php echo e(csrf_token()); ?>', event)">Save Signature</a>
                        <a href="#" id="clear">Clear</a>


                     </p>
                     <?php endif; ?>
                     <?php if($currentRoute =='synopsis.show'): ?>
                     <label class="signature-field">Signature:</label>
                     <span class="view-signature">
                     <img src="<?php echo e(url('public/signatures/'.$synopsis->student_signature)); ?>" id="showsign" class="signature-field">
                     </span>
                     <?php else: ?>
                     
                     <?php endif; ?>
                     <div class="form-group signature-upload" id="signature-image" style="display: none;">
                        <input type="file" name="" class="form-control">
                     </div>
                  </div>
                  <!-- <div id="sig"></div> -->
                  <div class="submitOuter">
                  <?php if($currentRoute!='synopsis.show'): ?>
                  <button type="submit" class="btn btn-primary submit-student">Edit Synopsis</button>
                  <?php elseif($synopsis->resubmit_enable==1): ?>
                  <?php $updateSynopUrl = route('update.synopsis-file', $synopsis->id) ?>
                   <button type="submit" class="btn btn-primary submit-student" onclick="updateSynopsisFile('<?php echo e($updateSynopUrl); ?>', event)">Edit Synopsis Files</button>
                  <?php endif; ?>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- The Modal -->
<div class="modal fade" id="synopEditModal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal Header -->
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            <h4 class="modal-title">Add Category</h4>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <form action="<?php echo e(route('category.student-add')); ?>" id="category_modal" data-reload-url="<?php echo e(route('synopsis.index')); ?>">
               <?php echo csrf_field(); ?>
               <div class="form-group">
                  <label for="email">Category:</label>
                  <select class="form-control" name="parent">
                     <option value="0">Root Category</option>
                     <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     <option value="<?php echo e($category['id']); ?>"><?php echo $category['name']; ?></option>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
               </div>
               <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control"  name="name">
                  <span class="error" id="name"></span>
               </div>
            </form>
         </div>
         <!-- Modal footer -->
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="modalFormSubmit('category_modal')">Add &nbsp;<i class="fa fa-spinner fa-spin" style="font-size:20px; display: none;" id="spinner"></i></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>