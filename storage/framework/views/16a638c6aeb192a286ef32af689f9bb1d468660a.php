<footer class="site-footer">

                <div class="footer-bottom">
                    <div class="container">
                        <p>Copyright &copy; 2018 <a href="https://wbuhs.ac.in/">wbuhs.ac.in</a>  Privacy Policy. <!-- |  Developed by
                        <a class="pixel" href="http://pixelsolutionz.com">Pixel Solutionz</a></p>  -->
                    </div>
                </div>
        </footer>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="<?php echo e(asset('public/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/bower_components/bootstrap-sweetalert/dist/sweetalert.min.js')); ?>"></script>
    <script src='//www.google.com/recaptcha/api.js'></script>
     <script src="<?php echo e(asset('public/js/jquery.signature.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/js/frontend.js')); ?>"></script>
  </body>
</html>