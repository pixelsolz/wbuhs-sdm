<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

<style>
ul, #myUL {
  list-style-type: none;
}

#myUL {
  margin: 0;
  padding: 0;
}

.boxes {
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
}

.boxes::before {
  content: "\2610";
  color: black;
  display: inline-block;
  margin-right: 6px;
}

.check-box::before {
  content: "\2611";
  color: dodgerblue;
}


.nested {
  display: block ;
}

.active-cat {
  display:none;
}
</style>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Speciality List

      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li class="active">Banner</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('success')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('success')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>
              <a href="<?php echo e(route('category.create')); ?>" class="btn btn-primary">Create New Speciality</a>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>
            <!-- /.box-header -->

            <div class="box-body">

            <h4>Speciality Tree View</h4>
                <p>A tree view represents a hierarchical view of information, where each item can have a number of subitems.</p>
                <p>Click on the box(es) to open or close the tree branches.</p>
                <ul id="myUL">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php echo $category; ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
var toggler = document.getElementsByClassName("boxes");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].classList.add('check-box');
  toggler[i].addEventListener("click", function() {

    this.parentElement.querySelector(".nested").classList.toggle("active-cat");
    this.classList.toggle("check-box");
  });
}
</script>
  <?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>