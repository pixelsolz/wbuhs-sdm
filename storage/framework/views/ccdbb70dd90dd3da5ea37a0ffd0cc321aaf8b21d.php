<?php $__env->startSection('content'); ?>
	<section class="desh-bord">
	<div class="container">
			<?php if(Session::has('error')): ?>
			<div class="alert alert-danger">
			  <strong><?php echo e(Session('error')); ?></strong>
			</div>
			<?php endif; ?>

	<div class="row">
		<?php echo $__env->make('frontend.includes.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
		<h3 class="dashboard_title">Payment Form</h3>
		<div class="right-pnl">
			
			<form method='get' action='<?php echo e(url("/test/payment-check")); ?>'>
				
				<input type='hidden' name='msg' id='msg' value="<?php echo ($newstr); ?>">
				<div style="text-align: center;">
					 
				 <button type="submit"  type="submit" class="btn btn-primary">Online Pay</button>
			 	</div>
			</form>


		</div>
		</div>
	</div>



	</div>
	</section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>