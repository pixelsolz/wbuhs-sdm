

<!DOCTYPE html>

<html>
<head>
    <title>WBUHS</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
</head>

<body style="font-family: 'arial', helvetica ,sans serif ; margin: 0;">
<table cellspacing="0" border="0" align="center" width="700" cellspadding="0" style=" margin: 10px auto;
font-size: 16px; color: #5c5a5b;  line-height: 20px; border: 1px solid #ddd; padding: 5px 0; box-shadow:  0 2px 6px #ddd;">
    <tr>
      <td align="center" valign="middle" style="">

        <tr>
          <td align="center" valign="middle" style="position: relative;">
            <h3 style="margin: 0; color: #2b2b2b; font-size: 28px; line-height: 30px; font-weight: bold; padding: 20px 0 10px;">WBUHS New User Create</h3>
          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="position: relative; text-align: justify; padding: 0 20px;">
            <p style="margin-bottom: 20px;">
             This is your credential detail of login into WBUHS SDM system.Please click below mentioned link to access the system with this credential.
					  <br> Login Email: <?php echo e($email); ?>

					<br>Login Password: <?php echo e($password); ?></p>

          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="position: relative; padding: 10px 0; ">
            <a href="<?php echo e(route('admin.login')); ?>" style="display: inline-block; border-radius: 8px; border: 1px solid #249d40;
            background: #249d40; color: #fff; font-weight: 600; font-size: 17px; line-height: 18px;
            text-decoration: none; padding: 15px 25px; margin-bottom: 15px; text-transform: capitalize;">click here to log in</a>
          </td>
        </tr>
      </td>
    </tr>

    <tr>
      <td align="center" valign="middle" style="position: relative; padding:15px 0; background: #f5f5f5; position: relative; top: 5px; font-size: 14px; font-weight: 600; ">
          Copyright © 2018 WBUHS
      </td>
    </tr>
</table>
</body>
</html>








