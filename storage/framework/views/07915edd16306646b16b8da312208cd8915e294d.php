  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo e((\Auth::guard('admin')->user()->userDetail && \Auth::guard('admin')->user()->userDetail->profile_image) ? url('public/'.\Auth::guard('admin')->user()->userDetail->profile_image):url('public/img/img_logo.png')); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo e(\Auth::guard('admin')->user()->name); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     <!--  <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
       <!--  <li class="header">MAIN NAVIGATION</li> -->

        <li class="<?php echo e(Request::is('admin')? 'active': ''); ?>">
          <a href="<?php echo e(url('admin')); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>

        </li>
        <?php if(Helper::sidenavAccess('role-list')): ?>
        <li class="<?php echo e((Request::is('admin/manage-role') || Request::is('admin/manage-role/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/manage-role')); ?>">
            <i class="fa fa-cogs"></i> <span>Manage Role</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('user-list')): ?>
         <li class="<?php echo e((Request::is('admin/user-manage') || Request::is('admin/user-manage/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/user-manage')); ?>">
            <i class="fa fa-group"></i> <span>Manage User</span>
           <!--  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('category-list')): ?>
        <li class="treeview <?php echo e((Request::is('admin/category') || Request::is('admin/category/*'))? 'active': ''); ?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Speciality</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo e((Request::is('admin/category'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/category')); ?>"><i class="fa fa-circle-o"></i>List</a></li>
            <li class="<?php echo e((Request::is('admin/category/create'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/category/create')); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
            <li class="<?php echo e((Request::is('admin/category/user'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/category/user')); ?>"><i class="fa fa-circle-o"></i>User Speciality List</a></li>
          </ul>
        </li>



         
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('student-synopsis')): ?>

        <li class="treeview <?php echo e((Request::is('admin/student-synopsis') || Request::is('admin/student-synopsis/*') || Request::is('admin/view/other-bos/review'))? 'active': ''); ?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Synopsis</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo e((Request::is('admin/student-synopsis'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-synopsis')); ?>"><i class="fa fa-circle-o"></i>List</a></li>
             <?php if(in_array('bos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
             <li class="<?php echo e((Request::is('admin/view/other-bos/review'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/view/other-bos/review')); ?>"><i class="fa fa-circle-o"></i>Other Bos Report</a></li>
             <?php endif; ?>
            <?php if(\Auth::guard('admin')->user()->is_admin ==1 || in_array('secretarybos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
            <li class="<?php echo e((Request::is('admin/student-synopsis/report'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-synopsis/report')); ?>"><i class="fa fa-circle-o"></i>Report</a></li>
            <?php endif; ?>
          </ul>
        </li>

        

        <li class="<?php echo e((Request::is('admin/student-synopsis-title')) ? 'active': ''); ?>">
          <a href="<?php echo e(route('admin.student-synopsis-title')); ?>">
            <i class="fa fa-files-o"></i> 
            <span>Synopsis <br/> <b>Title  Management</b> </span>
  
          </a>
        </li>

        
       
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('student-thesis')): ?>

        <li class="treeview <?php echo e((Request::is('admin/student-dissertation') || Request::is('admin/student-dissertation/*'))? 'active': ''); ?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Dissertation</span>
            <span class="pull-right-container">
             <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo e((Request::is('admin/student-dissertation'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-dissertation')); ?>"><i class="fa fa-circle-o"></i>List</a></li>
            <?php if(\Auth::guard('admin')->user()->is_admin !=1): ?>
            <li class="<?php echo e((Request::is('admin/student-dissertation/completed'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-dissertation/completed')); ?>"><i class="fa fa-circle-o"></i> Completed List</a></li>
            <li class="<?php echo e((Request::is('admin/student-dissertation/revision'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-dissertation/revision')); ?>"><i class="fa fa-circle-o"></i>Revision List</a></li>
            <li class="<?php echo e((Request::is('admin/student-dissertation/archive'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-dissertation/archive')); ?>"><i class="fa fa-circle-o"></i>Archive List</a></li>
            <?php endif; ?>
            <?php if(\Auth::guard('admin')->user()->is_admin ==1): ?>
            <li class="<?php echo e((Request::is('admin/student-dissertation/report'))? 'active': ''); ?>"><a href="<?php echo e(url('admin/student-dissertation/report')); ?>"><i class="fa fa-circle-o"></i>Report</a></li>
            <?php endif; ?>
          </ul>
        </li>

        <?php endif; ?>

        <?php if(\Auth::guard('admin')->user()->is_admin !=1 && Helper::sidenavAccess('student-result-list')): ?>
        <li class="<?php echo e((Request::is('admin/student-result') || Request::is('admin/student-result/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/student-result')); ?>">
            <i class="fa fa-files-o"></i> <span>Dissertation Review</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('leave-list')): ?>
         <li class="<?php echo e((Request::is('admin/leave') || Request::is('admin/leave/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/leave')); ?>">
            <i class="fa fa-hand-paper-o"></i> <span><?php echo e((\Auth::guard('admin')->user()->is_admin ==1)?'Adjudicator':'My'); ?> Availability</span>
          </a>
        </li>
        <?php endif; ?>
        <?php if(Helper::sidenavAccess('renumeration-create') && \Auth::guard('admin')->user()->is_admin !=1): ?>
        <li class="<?php echo e((Request::is('admin/renumeration') || Request::is('admin/renumeration/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/renumeration')); ?>">
            <i class="fa fa-money"></i> <span>Renumeration</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

         <?php if(Helper::sidenavAccess('reviewerPayment-list')): ?>
        <li class="<?php echo e((Request::is('admin/reviewerPayment') || Request::is('admin/reviewerPayment/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/reviewerPayment')); ?>">
            <i class="fa fa-money"></i> <span>Adjudicator payment</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>
     <?php if(Helper::sidenavAccess('role-list')): ?>
        <li class="<?php echo e((Request::is('admin/student-payments') || Request::is('admin/student-payments/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/student-payments')); ?>">
            <i class="fa fa-money"></i> <span>Students payment</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('role-list')): ?>
        <li class="<?php echo e((Request::is('admin/student-list') || Request::is('admin/student-list/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/student-list')); ?>">
            <i class="fa fa-group"></i> <span>Student Dessertation<br/><b> List Upload</b></span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('role-list')): ?>
        <li class="<?php echo e((Request::is('admin/student-synopsis-list') || Request::is('admin/student-synopsis-list/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/student-synopsis-list')); ?>">
            <i class="fa fa-group"></i> <span>Student Synopsis<br/><b> List Upload</b></span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        

        <?php if(\Auth::guard('admin')->user()->is_admin !=1): ?>
        <li class="<?php echo e((Request::is('admin/profile/edit') || Request::is('admin/profile/edit/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/profile/edit')); ?>">
            <i class="fa fa-user"></i> <span>Profile Update</span>
          </a>
        </li>
<<<<<<< HEAD:storage/framework/views/07915edd16306646b16b8da312208cd8915e294d.php
          <?php if(in_array('bos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
=======
         <?php if(in_array('bos', \Auth::guard('admin')->user()->roles()->pluck('slug')->toArray())): ?>
>>>>>>> 6cbf08e15cf4686f42635acf7fe6c2503342e973:storage/framework/views/7f08167e7a78c9bfd16f0174e8eea74bdbdd52b9.php
          <li class="<?php echo e((Request::is('admin/bos-pdf/form') || Request::is('admin/bos-pdf/form/*'))? 'active': ''); ?>">
            <a href="<?php echo e(url('admin/bos-pdf/form')); ?>">
              <i class="fa fa-files-o"></i> <span>Upload Pdf</span>
            </a>
          </li>
          <?php endif; ?>
        <?php endif; ?>


        <?php if(Helper::sidenavAccess('role-list')): ?>
        <li class="<?php echo e((Request::is('admin/student-support-list') || Request::is('admin/student-support-list/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/student-support-list')); ?>">
            <i class="fa fa-support"></i> <span>Student Support List</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <?php if(Helper::sidenavAccess('role-list')): ?>
        <li class="<?php echo e((Request::is('admin/bos-pdf/list') || Request::is('admin/bos-pdf/list/*'))? 'active': ''); ?>">
          <a href="<?php echo e(url('admin/bos-pdf/list')); ?>">
            <i class="fa fa-files-o"></i> <span>BOS Members Pdf List</span>
            <!-- <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> -->
          </a>
        </li>
        <?php endif; ?>

        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li> -->






       <!--  <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
