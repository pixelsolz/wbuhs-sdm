<?php $__env->startSection('content'); ?>

<section class="desh-bord">
   <div class="container">
      <?php if(Session::has('error')): ?>
      <div class="alert alert-danger">
         <strong><?php echo e(Session('error')); ?></strong>
      </div>
      <?php endif; ?>
      <div class="row">
         <?php echo $__env->make('frontend.includes.sidemenu', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <h3 class="dashboard_title">Profile</h3>
            <div class="right-pnl">

               <form action="<?php echo e(route('profile.update',['id'=>$user->id])); ?>" method="post" enctype="multipart/form-data">
                  <?php echo csrf_field(); ?>
                  <?php echo method_field('put'); ?>

                  <div class="form-group <?php if($errors->first('f_name')): ?> has-error <?php endif; ?>">
                     <label for="f_name">First Name <span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="f_name" placeholder="Enter first name" name="f_name" value="<?php echo e(old('f_name')? old('f_name'): $user->f_name); ?>">
                     
                  </div>
                  <div class="form-group <?php if($errors->first('l_name')): ?> has-error <?php endif; ?>">
                     <label for="l_name">Last Name<span class="require-asterik">*</span>:</label>
                     <input type="text" class="form-control" id="l_name" placeholder="Enter last name" name="l_name" value="<?php echo e(old('l_name')? old('l_name'): $user->l_name); ?>">
                     
                  </div>
                  <div class="form-group <?php if($errors->first('email')): ?> has-error <?php endif; ?>">
                     <label for="email">Email <span class="require-asterik">*</span>:</label>
                     <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" value="<?php echo e(old('email')? old('email'): $user->email); ?>" readonly="true">
                     
                  </div>
                  <div class="form-group <?php if($errors->first('mobile')): ?> has-error <?php endif; ?>">
                     <label for="mobile">Mobile <span class="require-asterik">*</span>:</label>
                     <input type="number" class="form-control" id="mobile" placeholder="Enter mobile no." name="mobile" value="<?php echo e(old('mobile')? old('mobile'): $user->userDetail->mobile); ?>" readonly="true">
                     
                  </div>


                  <div class="form-group <?php if($errors->first('address')): ?> has-error <?php endif; ?>">
                     <label for="address">Address<span class="require-asterik">*</span>:</label>
                     <textarea class="form-control" name="address" id="address"><?php echo e(old('address')? old('address'): $user->userDetail->address); ?></textarea>
                     
                  </div>
                  <div class="form-group <?php if($errors->first('gender')): ?> has-error <?php endif; ?>">
                     <label for="gender">Gender<span class="require-asterik">*</span>:</label>
                     <select name="gender" class="form-control">
                        <option value="">select</option>
                        <option value="1" <?php if(old('gender')?old('gender')==1: ($user->userDetail->gender ? $user->userDetail->gender==1: '' ) ): ?> selected <?php endif; ?>>Male</option>
                        <option value="2" <?php if(old('gender')?old('gender')==2: ($user->userDetail->gender ? $user->userDetail->gender==2: '' ) ): ?> selected <?php endif; ?>>Female</option>
                     </select>
                     
                  </div>
                  <div class="form-group <?php if($errors->first('profile_image')): ?> has-error <?php endif; ?>">
                     <label for="profile_image">Image Upload:</label>
                     <input class="form-control" type="file" name="profile_image">
                     
                  </div>

                    <div class="submitOuter">
                  <button type="submit" class="btn btn-primary">Update</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</section>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>