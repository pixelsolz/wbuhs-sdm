<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Create User
      </h1>
      
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">User Form</h3>
                  <?php if(Session::has('error')): ?>
                  <div class="alert alert-danger">
                     <?php echo e(Session('error')); ?>

                  </div>
                  <?php endif; ?>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="<?php echo e(url('admin/user-manage')); ?>" method="POST" enctype="multipart/form-data">
                  <?php echo csrf_field(); ?>
                  <div class="box-body">
                     <div class="form-group row <?php if($errors->has('f_name')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="f_name">First Name<span class="required_field">*</span></label>
                           <input type="text" name="f_name" class="form-control" value="<?php echo e(old('f_name')); ?>">
                           <?php if($errors->has('f_name')): ?>
                           <span class="error"><?php echo e($errors->first('f_name')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('l_name')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="l_name">Last Name<span class="required_field">*</span></label>
                           <input type="text" name="l_name" class="form-control" value="<?php echo e(old('l_name')); ?>">
                           <?php if($errors->has('l_name')): ?>
                           <span class="error"><?php echo e($errors->first('l_name')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('email')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="email">Email<span class="required_field">*</span></label>
                           <input type="email" name="email" class="form-control" value="<?php echo e(old('email')); ?>">
                           <?php if($errors->has('email')): ?>
                           <span class="error"><?php echo e($errors->first('email')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('mobile')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="mobile">Mobile<span class="required_field">*</span></label>
                           <input type="number" name="mobile" class="form-control" value="<?php echo e(old('mobile')); ?>">
                           <?php if($errors->has('mobile')): ?>
                           <span class="error"><?php echo e($errors->first('mobile')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row <?php if($errors->has('address')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="address">Address<span class="required_field">*</span></label>
                           <textarea name="address" class="form-control"><?php echo e(old('address')); ?></textarea>
                           <?php if($errors->has('address')): ?>
                           <span class="error"><?php echo e($errors->first('address')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <div class="form-group row <?php if($errors->has('state_id')): ?>has-error <?php endif; ?>">
                        <div class="col-xs-9">
                           <label for="state_id">State<span class="required_field">*</span></label>
                           <select class="form-control" name="state_id">
                              <option value="">select state</option>
                              <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($state->id); ?>" <?php if(old('state_id')==$state->id): ?> selected <?php endif; ?>><?php echo e($state->state_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <?php if($errors->has('state_id')): ?>
                           <span class="error"><?php echo e($errors->first('state_id')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <div class="form-group row <?php if($errors->has('city_id')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="city_id">City<span class="required_field">*</span></label>
                           <input type="text"  class="form-control" name="city_id" value="<?php echo e(old('city_id')); ?>">

                           <?php if($errors->has('city_id')): ?>
                           <span class="error"><?php echo e($errors->first('city_id')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <div class="form-group row <?php if($errors->has('gender')): ?>has-error <?php endif; ?>">
                        <div class="col-lg-9 col-xs-12">
                           <label for="gender">Gender<span class="required_field">*</span></label>
                           <select class="form-control" name="gender">
                              <option value="">select</option>
                              <option value="1" <?php if(old('gender')==1): ?> selected <?php endif; ?>>Male</option>
                              <option value="2" <?php if(old('gender')==2): ?> selected <?php endif; ?>>Female</option>
                           </select>
                           <?php if($errors->has('gender')): ?>
                           <span class="error"><?php echo e($errors->first('gender')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>

                     <!-- <div class="form-group row <?php if($errors->has('user_type')): ?>has-error <?php endif; ?>">
                        <div class="col-xs-9">
                           <label for="user_type">User Type<span class="required_field">*</span></label>
                           <select class="form-control" name="user_type">
                              <option value="">select</option>
                              <option value="1" <?php if(old('user_type')==1): ?> selected <?php endif; ?>>Super Admin</option>
                              <option value="2" <?php if(old('user_type')==2): ?> selected <?php endif; ?>>Admin</option>
                              <option value="3" <?php if(old('user_type')==3): ?> selected <?php endif; ?>>Student</option>
                           </select>
                           <?php if($errors->has('user_type')): ?>
                           <span class="error"><?php echo e($errors->first('user_type')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div> -->

                     <div class="form-group row">
                        <div class="col-lg-9 col-xs-12">
                           <label>Role<span class="required_field">*</span></label>
                            <?php if($errors->has('roles')): ?>
                            <br>
                           <span class="error"><?php echo e($errors->first('roles')); ?></span>
                           <?php endif; ?>

                           <div class="form-group">
                              <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($role->slug !='superadmin'): ?>
                              <input type="checkbox" class="minimal" name="roles[]" id="roles" value="<?php echo e($role->id); ?>" onclick="showCategory('<?php echo e($role->slug); ?>')" <?php if(old('roles') && in_array($role->id, old('roles'))): ?> checked <?php endif; ?>>&nbsp;
                              <label><?php echo e($role->name); ?></label>&nbsp;
                              <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </div>

                        </div>
                     </div>
                     <div class="form-group row forAdjudicator" <?php if(old('roles') && in_array(2, old('roles'))): ?> <?php else: ?> style="display: none;" <?php endif; ?> >
                        <div class="col-lg-9 col-xs-12">
                           <label for="designation">Designation<span class="required_field">*</span></label>
                           <select class="form-control" name="designation">
                              <option value="">select</option>
                              <option value="professor">Professor</option>
                              <option value="associate-professor">Associate Professor</option>
                              <option value="assistant-professor">Assistant Professor</option>
                           </select>
                        </div>
                     </div>

                     <div class="form-group row"  id="category-div" <?php if(old('roles') && in_array(2, old('roles'))): ?> <?php else: ?> style="display: none;" <?php endif; ?> >
                        <div class="col-lg-9 col-xs-12">
                           <label for="user_type">Category<span class="required_field">*</span></label>
                           <select class="form-control" name="category[]" multiple="true">
                              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($category['id']); ?>"><?php echo $category['name']; ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                     </div>

                     <div class="form-group row <?php if($errors->has('college_name')): ?>has-error <?php endif; ?>" style="display: none;" id="user_college_name">
                        <div class="col-lg-9 col-xs-12">
                           <label for="college_name">College Name<span class="required_field">*</span> <a href="javascript:void(0)" id="show_other_college">Show other</a> <a href="javascript:void(0)" style="display: none;" id="hide_other_college">Hide other</a></label>
                            <select class="form-control" name="college_name" id="college_name">
                              <option value="">Select College Name</option>
                              <?php $__currentLoopData = $colleges; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $college): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($college['id']); ?>"><?php echo $college['college_name']; ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <textarea name="other_college_name" id="other_college_name" class="form-control" style="display: none;"><?php echo e(old('other_college_name')); ?></textarea>
                           <?php if($errors->has('other_college_name')): ?>
                           <span class="error"><?php echo e($errors->first('college_name')); ?></span>
                           <?php endif; ?>
                        </div>

                     </div>
                     <div class="form-group row" style="display: none;" id="select_state_div">
                        <div class="col-lg-9 col-xs-12">
                           <label for="college_name">Select State<span class="required_field">*</span></label>
                            <select class="form-control" name="other_state" id="other_state">
                              <option value="">Select State Name</option>
                              <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($state['id']); ?>"><?php echo $state['state_name']; ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </div>
                     </div>

                    

                    <!--  <div class="form-group row <?php if($errors->has('role')): ?>has-error <?php endif; ?>">
                        <div class="col-xs-9">
                           <label for="role">Role<span class="required_field">*</span></label>
                           <select class="form-control" name="role">
                              <option value="">select</option>
                              <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($role->id); ?>" <?php if(old('role')==$role->id): ?> selected <?php endif; ?>><?php echo e($role->name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <?php if($errors->has('role')): ?>
                           <span class="error"><?php echo e($errors->first('role')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div> -->
                     <div class="form-group">
                        <label for="exampleInputFile">Profile Image</label>
                        <input type="file" id="exampleInputFile" name="profile_image">
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>