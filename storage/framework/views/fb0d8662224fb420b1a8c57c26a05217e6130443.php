<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Upload Pdf
      </h1>
      <ol class="breadcrumb">
         <!-- <li><a href="<?php echo e(route('user-manage.index')); ?>"><i class="fa fa-dashboard"></i> User List</a></li> -->
         <!--  <li><a href="#">Forms</a></li> -->
         <!-- <li class="active"> profile update</li> -->
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title">Upload Pdf</h3>
                   <?php if(Session::has('success')): ?>
                  <div class="alert alert-success alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <?php echo e(Session('success')); ?>

                  </div>
                  <?php endif; ?>
                  <?php if(Session::has('error')): ?>
                  <div class="alert alert-danger">
                     <?php echo e(Session('error')); ?>

                  </div>
                  <?php endif; ?>
               </div>
               <!-- /.box-header -->
               <!-- form start -->
               <form role="form" action="<?php echo e(url('admin/bos-pdf/submit')); ?>" method="POST" enctype="multipart/form-data">
                  <?php echo csrf_field(); ?>
                  <div class="box-body">

                     <div class="form-group">
                        <label for="exampleInputFile">Upload Pdf</label>
                        <input type="file" id="exampleInputFile" name="pdf_file">
                         <?php if($errors->has('pdf_file')): ?>
                           <span class="error"><?php echo e($errors->first('pdf_file')); ?></span>
                         <?php endif; ?>
                     </div>
                  </div>
                  <div class="box-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>

              <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Pdf</th>
                  <th>Created date</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $bosPdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pdf): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count); ?></td>
                    <td><a href="<?php echo e(url('public/bos_pdfs/'. $pdf->pdf_file)); ?>" download><?php echo e($pdf->pdf_file); ?></a></td>
                    <td><?php echo e(\Carbon\Carbon::parse($pdf->created_at)->format('Y-m-d')); ?></td>
                  </tr>
                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="4" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
              <div><?php echo e($bosPdfs->links()); ?></div>

            </div>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <!-- right column -->
         <!--/.col (right) -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>