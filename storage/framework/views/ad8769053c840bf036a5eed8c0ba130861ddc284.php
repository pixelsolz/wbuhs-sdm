<?php $__env->startSection('title', $title); ?>
<?php $__env->startSection('content'); ?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Role List
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <?php if(Session::has('msg')): ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('msg')); ?>

              </div>
              <?php elseif(Session::has('error')): ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo e(session('error')); ?>

              </div>

              <?php endif; ?>
              <a href="<?php echo e(route('manage-role.create')); ?>" class="btn btn-primary">Create New Role</a>
              <!-- <h3 class="box-title">Hover Data Table</h3> -->
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <table id="example" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sl No.</th>
                  <th>Name</th>
                  <th>Slug</th>
                  <th>Permissions</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $count=1; ?>
                <?php $__empty_1 = true; $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                  <tr>
                    <td><?php echo e($count); ?></td>
                    <td><?php echo e($role->name); ?></td>
                    <td><?php echo e($role->slug); ?></td>
                    <td><?php echo e($role->getPermissions($role->permission_array)); ?></td>
                    <td><?php echo e($role->status_text); ?></td>
                    <td>
                      <?php if($role->slug!= 'superadmin'): ?>
                      <a href="<?php echo e(route('manage-role.edit', ['id'=>$role->id])); ?>" class="glyphicon glyphicon-pencil"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                      <?php
                        $deleteroute = route('manage-role.destroy', ['id'=>$role->id]);
                      ?>
                         
                      <?php endif; ?>
                    </td>
                  </tr>
                  <?php $count++; ?>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <tr>
                  <td colspan="6" style="text-align: center;">No data available</td>
                </tr>
               <?php endif; ?>


                </tbody>

              </table>
              <div><?php echo e($roles->links()); ?></div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>