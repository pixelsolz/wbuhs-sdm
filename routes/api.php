<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('add/student', 'ApiServiceController@addStudentDetail');
Route::get('validate/dissertation-complete/{regis_no}/{course_id}', 'ApiServiceController@validateCompleteDissertation');

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
