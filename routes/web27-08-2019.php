<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/**  FRONTEND SECION **/
Route::get('', function () {
	return redirect('/login');
});
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/registration', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/registration', 'Auth\RegisterController@register');
Route::get('/otp-form', 'Auth\LoginController@showOtpForm');
Route::get('otp-resend', 'Auth\LoginController@otpResend')->name('otp-resend');
Route::post('otp-verification', 'Auth\LoginController@otpVerification')->name('otp-verification');
Route::get('/synopsis-manual', 'SynopsisController@downloadSynossisManual');
Route::post('registration-no/check', 'Auth\RegisterController@checkRegistrationNo');

/*Route::post('/registration', function () {
echo 'test';
});*/
Route::get('/email-verification/{token}'
	, 'Auth\LoginController@emailVerifyLogin');
Route::get('/forgot-password', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLink');
Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('reset-password', 'Auth\ResetPasswordController@storeResetPassword');

Route::get('/payment/history')->name('student-payment.history');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('dashboard', 'DashboardController');
	Route::get('rejection-list', 'StudentProfileController@rejectionList')->name('rejection-list');
	Route::post('/return-payment', 'paymentController@returnPayment');
	Route::post('/category/add', 'SynopsisController@addCategory')->name('category.student-add');

	Route::get('/synopsis/testpdf', 'SynopsisController@test');
	Route::get('/set-registration-no', 'paymentController@setRegistrationNo');
	Route::get('/check-registration-no', 'paymentController@checkRegistration');

	Route::get('/check-unique-no', 'SynopsisController@setUniqueNo');

	Route::post('synopsis/file-upload', 'SynopsisController@uploadPDF');
	Route::get('/synopsis/other-pdf/{pdf}', 'SynopsisController@viewOherpdf');
	Route::get('/dissertation/other-pdf/{pdf}', 'ThesisController@viewOherpdf');
	Route::post('/synopsis/save-signature', 'SynopsisController@saveSignature');
	Route::post('/dissertation/save-signature', 'SynopsisController@saveSignature');

	Route::get('/get-subcategory', 'paymentController@getSubCategory');
	Route::get('synopsis/old-data/{id}', 'SynopsisController@oldSynopsisPayment');
	Route::get('dissertation/old-data/{id}', 'ThesisController@oldThesisPayment');
	Route::get('synopsis/view-pdf', 'SynopsisController@viewPdf')->name('synopsis.view-pdf');
	Route::get('dissertation/view-pdf', 'ThesisController@viewPdf')->name('thesis.view.pdf');
	Route::resource('synopsis', 'SynopsisController');
	Route::resource('dissertation', 'ThesisController');
	Route::resource('profile', 'StudentProfileController');
	Route::get('change-password', 'StudentProfileController@changePassword');
	Route::post('change-password', 'StudentProfileController@changePasswordStore');

	Route::get('payment/history', 'paymentController@getSudentPayment')->name('student-pay.history');

	//Route::resource('payment', 'paymentController');
	Route::get('synopsis/payment/create', 'paymentController@createSynopsis')->name('synopsis.payment.create');
	Route::get('dissertation/payment/create', 'paymentController@create')->name('thesis.payment.create');
	Route::post('/payment', 'paymentController@store')->name('payment.store');
	Route::get('logout', 'Auth\LoginController@logout')->name('logout');
	Route::get('/success-message/{type}/{paid_type}', 'paymentController@successMessage')->name('success-message');
	Route::get('/download-reason/doc', 'StudentProfileController@downloadReason');

	Route::get('/check', 'paymentController@check');
});

/**  ADMIN SECTION **/

Route::get('/admin/login', 'Admin\AuthController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Admin\AuthController@storeLogin');

Route::get('/admin/forget-password', 'Admin\AuthController@forgetpassword');
Route::post('/admin/forget-password', 'Admin\AuthController@forgetPasswordStore');

Route::get('/admin/reset-password/{token}', 'Admin\AuthController@showResetForm');
Route::post('admin/reset-password', 'Admin\AuthController@storeResetPassword');

Route::get('/admin/registration', 'Admin\AuthController@showRegisterForm')->name('admin.register');
Route::post('/admin/registration', 'Admin\AuthController@storeRegistration')->name('admin.register.store');
Route::get('admin/reviewer-success', 'Admin\AuthController@registerSuccess')->name('admin.messages');
Route::get('admin/email-verification/{token}'
	, 'Admin\AuthController@emailVerifyLogin');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
	Route::get('/mail-send/adjudicator', 'UserController@sendLoginCredentialToAdjudicator');
	Route::get('/', 'DashboardController@index')->name('admin.dashboard');
	Route::get('/dashboard-data', 'DashboardController@dashboardData');
	Route::get('/logout', 'AuthController@logout')->name('admin.logout');

	Route::get('/user-manage', 'UserController@index')->name('user-manage.index')->middleware('admin.roles:user-list');
	Route::get('/user-manage/profile', 'UserController@Profile')->name('user-manage.profile')->middleware('admin.roles:user-list');
	Route::get('/user-manage/create', 'UserController@create')->name('user-manage.create')->middleware('admin.roles:user-create');
	Route::post('/user-manage', 'UserController@store')->name('user-manage.store')->middleware('admin.roles:user-create');
	Route::get('/user-manage/{user_manage}/edit', 'UserController@edit')->name('user-manage.edit')->middleware('admin.roles:user-edit');
	Route::put('/user-manage/{user_manage}', 'UserController@update')->name('user-manage.update')->middleware('admin.roles:user-edit');
	Route::delete('/user-manage/{user_manage}', 'UserController@destroy')->name('user-manage.destroy')->middleware('admin.roles:user-delete');

	Route::get('/withdraw-dissertation', 'UserController@DissertationWithdraw')->name('admin.dissertation.withdraw');

	Route::get('/view-category/{id}', 'UserController@viewSpeciality');

	Route::post('/send-email', 'UserController@sendEmail');
	Route::post('/synopsis/change-status', 'UserController@synopsisChangeStatus')->name('student.synopsis.status-change');

	Route::get('/reason-rejected/view', 'UserController@viewReasonReject');

	Route::get('/manage-role', 'RoleController@index')->name('manage-role.index')->middleware('admin.roles:role-list');
	Route::get('/manage-role/create', 'RoleController@create')->name('manage-role.create')->middleware('admin.roles:role-create');
	Route::post('/manage-role', 'RoleController@store')->name('manage-role.store')->middleware('admin.roles:role-create');
	Route::get('/manage-role/{manage_role}/edit', 'RoleController@edit')->name('manage-role.edit')->middleware('admin.roles:role-edit');
	Route::put('/manage-role/{manage_role}', 'RoleController@update')->name('manage-role.update')->middleware('admin.roles:role-edit');
	Route::delete('/manage-role/{manage_role}', 'RoleController@destroy')->name('manage-role.destroy')->middleware('admin.roles:role-delete');

	Route::post('/assign-synopsis', 'UserController@assingSynopsis');
	Route::get('/student-payment/status-change', 'UserController@changePaymentStatus')->name('admin.payment.status-change');

	/* category */
	Route::get('/category', 'CategoryController@index')->name('category.index')->middleware('admin.roles:category-list');
	Route::get('/category/create', 'CategoryController@create')->name('category.create')->middleware('admin.roles:category-create');
	Route::post('/category', 'CategoryController@store')->name('category.store')->middleware('admin.roles:category-create');
	Route::get('/category/{category}/edit', 'CategoryController@edit')->name('category.edit')->middleware('admin.roles:category-edit');
	Route::put('/category/{category}', 'CategoryController@update')->name('category.update')->middleware('admin.roles:category-edit');
	Route::delete('/category/{category}', 'CategoryController@destroy')->name('category.destroy')->middleware('admin.roles:category-delete');
	Route::get('/category/user', 'CategoryController@categoryWiseUser');

	/* student synopsis*/
	Route::get('/student-synopsis', 'UserController@studentSynopsis')->name('admin.student-synopsis')->middleware('admin.roles:student-synopsis');
	Route::get('/student-synopsis/report', 'UserController@studentSynopsisReport')->name('admin.student-synopsis-report')->middleware('admin.roles:student-synopsis-report');

	Route::get('/synopsis-bos/report/{id}', 'UserController@synopsisBOSReport')->name('admin.synopsis-bos-report')->middleware('admin.roles:student-synopsis-report');

	Route::get('/student-dissertation/report', 'UserController@studentDissertationReport')->name('admin.dissertation-report');

	Route::get('/student-dissertation/adjudicator/{id}', 'UserController@synopsisAdjudicatorReport')->name('admin.dissertation-adjudicator');

	Route::get('/student-synopsis/viewform/{syn_id}', 'UserController@viewSynpFormPdf');
	Route::get('/student-synopsis/view/{syn_id}', 'UserController@viewPDf');
	Route::get('student-otherpdf/view/{pdf}', 'UserController@viewOtherPdf')->name('student-other-pdf');

	Route::get('student-dissertation/viewform/{thesis_id}', 'UserController@viewThesisFormPdf');
	Route::get('/student-dissertation/view/{thesis_id}', 'UserController@viewThesisPdf');

	Route::get('/profile/edit', 'UserController@profileEdit');
	Route::put('/profile/update', 'UserController@profileUpdate');
	Route::get('/synopsis/users', 'UserController@getBOSUsers');

//
	Route::get('student-dissertation', 'UserController@studentThesis')->name('admin.student-thesis')->middleware('admin.roles:student-thesis');

	Route::get('student-result', 'ReviewerController@showReviewList')->name('admin.student-result')->middleware('admin.roles:student-result-list');

	Route::get('student-result/add/{thesis}', 'ReviewerController@addReviewResult')->middleware('admin.roles:student-result-add');

	Route::post('student-result/store', 'ReviewerController@storeReviewReult')->middleware('admin.roles:student-result-add');

	Route::get('student-result/edit/{thesis}', 'ReviewerController@editReviewResult')->middleware('admin.roles:student-result-edit');

	Route::get('student-result/view/{result}', 'ReviewerController@editReviewResult')->middleware('admin.roles:student-result-edit');

	Route::post('student-result/status-change', 'ReviewerController@reviewerStatusChange')->middleware('admin.roles:student-result-edit');

	Route::get('leave/user-leave', 'AdminLeaveController@getLeaves');

	//Route::resource('leave', 'AdminLeaveController');

	Route::get('/leave', 'AdminLeaveController@index')->name('leave.index')->middleware('admin.roles:leave-list');
	Route::get('/leave/create', 'AdminLeaveController@create')->name('leave.create')->middleware('admin.roles:leave-create');
	Route::post('/leave', 'AdminLeaveController@store')->name('leave.store')->middleware('admin.roles:leave-create');
	Route::get('/leave/{leave}/edit', 'AdminLeaveController@edit')->name('leave.edit')->middleware('admin.roles:leave-edit');
	Route::put('/leave/{leave}', 'AdminLeaveController@update')->name('leave.update')->middleware('admin.roles:leave-edit');
	Route::delete('/leave/{leave}', 'AdminLeaveController@destroy')->name('leave.destroy')->middleware('admin.roles:leave-delete');

	Route::get('/test', 'ReviewerController@reviewerReminder');

	Route::post('/signature-save', 'ReviewerController@saveSignature');

	Route::get('notification-read', 'UserController@markNotificationRead');

	Route::get('available-reviewers', 'UserController@getAvailableReviewer');

	Route::get('/renumeration', 'ReviewerPaymentController@thesisRenumeration')->name('renumeration.create')->middleware('admin.roles:renumeration-create');

	Route::get('renumeration/form/{th_rr}', 'ReviewerPaymentController@renumerationForm')->name('renumeration.show-form');

	Route::post('/renumeration/store', 'ReviewerPaymentController@renumerationStore')->name('renumeration.store')->middleware('admin.roles:renumeration-create');

	Route::get('reviewerpayment/update', 'ReviewerPaymentController@updatePayment');

	Route::get('/reason-reject/download', 'UserController@viewReasonRejectDownload');

	Route::get('/reviewer-payment/all-update', 'ReviewerPaymentController@updateAllPyments')->name('reviwer.allpayment-done');
	Route::get('/pending-reviwer/payment-export', 'ReviewerPaymentController@exportAccounts')->name('reviewer.paymentexport');

	//Route::resource('reviewerPayment', 'ReviewerPaymentController');

	Route::get('/reviewerPayment', 'ReviewerPaymentController@index')->name('reviewerPayment.index')->middleware('admin.roles:reviewerPayment-list');
	Route::get('/reviewerPayment/create', 'ReviewerPaymentController@create')->name('reviewerPayment.create')->middleware('admin.roles:reviewerPayment-create');
	Route::post('/reviewerPayment', 'ReviewerPaymentController@store')->name('reviewerPayment.store')->middleware('admin.roles:reviewerPayment-create');
	Route::get('/reviewerPayment/{reviewerPayment}/edit', 'ReviewerPaymentController@edit')->name('reviewerPayment.edit')->middleware('admin.roles:reviewerPayment-edit');
	Route::put('/reviewerPayment/{reviewerPayment}', 'ReviewerPaymentController@update')->name('reviewerPayment.update')->middleware('admin.roles:reviewerPayment-edit');
	Route::delete('/reviewerPayment/{reviewerPayment}', 'ReviewerPaymentController@destroy')->name('reviewerPayment.destroy')->middleware('admin.roles:reviewerPayment-delete');

	//Route::resource('bonus', 'BonusMasterController');
	Route::get('/bonus', 'BonusMasterController@index')->name('bonus.index')->middleware('admin.roles:bonus-master-list');
	Route::get('/bonus/create', 'BonusMasterController@create')->name('bonus.create')->middleware('admin.roles:bonus-master-add');
	Route::post('/bonus', 'BonusMasterController@store')->name('bonus.store')->middleware('admin.roles:bonus-master-add');
	Route::get('/bonus/{bonus}/edit', 'BonusMasterController@edit')->name('bonus.edit')->middleware('admin.roles:bonus-master-edit');
	Route::put('/bonus/{bonus}', 'BonusMasterController@update')->name('bonus.update')->middleware('admin.roles:bonus-master-edit');
	Route::delete('/bonus/{bonus}', 'BonusMasterController@destroy')->name('bonus.destroy')->middleware('admin.roles:bonus-master-delete');

	Route::get('/faculty/submitted-status', 'UserController@submittedStatus');

	Route::get('/reviewer-accept', 'ReviewerController@reviewerAccept')->name('admin.reviewer-accept');

	Route::get('/view/reviewer-result/{user_id}/{result}', 'ReviewerController@adminViewResult')->name('admin.view-reviewer.result');
	Route::get('/accounts/reminder', 'ReviewerPaymentController@accountsReminder');

	Route::get('/synopsis/rewiew-status', 'UserController@bosReviewStatus')->name('synopsis.review-status');

	Route::get('/student-payments', 'UserController@getStudentPayments')->name('student.payment-history');

	Route::get('/get-assigned-user', 'UserController@getAssignedUser');

	Route::post('/replace-new/pdf', 'UserController@replaceThesisPDF');

	Route::post('/replace-synopsis/pdf', 'UserController@replaceSynopsisPDF');
});
