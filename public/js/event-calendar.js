$(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
              let eventArray =[];
               $.ajax({
                type: 'GET',
                url: 'https://wbuhs.ac.in/sdm/admin/leave/user-leave',
                async: false,
                success: (response) => {
                      //let calendarData = JSON.parse(response);
                      console.log(response.length);
                      for (var i =0; i<response.length; i++) {
                        let st_year = new Date(response[i].start_date).getFullYear();
                        let st_month = new Date(response[i].start_date).getMonth();
                        let st_date = new Date(response[i].start_date).getDate();
                        let end_year = new Date(response[i].end_date).getFullYear();
                        let end_month = new Date(response[i].end_date).getMonth();
                        let end_date = new Date(response[i].end_date).getDate();
                        eventArray.push({title:response[i].reason,start:new Date(st_year, st_month, st_date),
                                       end:new Date(end_year, end_month, end_date),backgroundColor: '#f56954',borderColor    : '#f56954'});
                        /*eventArray =[{title:response[i].reason,start:new Date(st_year, st_month, st_date),
                                       end:new Date(end_year, end_month, end_date),backgroundColor: '#f56954',borderColor    : '#f56954'}];*/
                      }


                            $('#calendar').fullCalendar({
                                header    : {
                                  left  : 'prev,next today',
                                  center: 'title',
                                  right : 'month,agendaWeek,agendaDay'
                                },
                                buttonText: {
                                  today: 'today',
                                  month: 'month',
                                  week : 'week',
                                  day  : 'day'
                                },
                                //Random default events
                                events    : eventArray,
                               
                              });

                            $('.fc-time').remove();

                                                console.log(eventArray);
                                          },
                                          error: () => {

                                          }

                                      });



 

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    var colorChooser = $('#color-chooser-btn')
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      init_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  });