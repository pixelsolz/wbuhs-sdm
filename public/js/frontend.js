/********************************************************
 *
 * Custom Javascript code for Flexor Bootstrap theme
 *
 *******************************************************/
//var serverUrl ="http://localhost/root/wbuhs-sdm/";
//var serverUrl ="http://pxlsysdev.in/sdm/";
var serverUrl ="https://wbuhs.ac.in/sdm/";

$(document).ready(function() {

//stickeyheader

$(window).scroll(function(){
    if($(this).scrollTop() > 0){
         if($(window).width() > 0){
            $('body').addClass('sticky')
         }
    }
    else{
        $('body').removeClass('sticky')
    }

});


$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});


//datepicker
$('.datepicker').datepicker();

  $('.datepicker_dob').datepicker({
    autoclose: true
  });

//ui-accordian
 $( "#accordion" ).accordion();

// Counter
        $('.counter').bind('inview', function(event, visible) {
          if (visible) {
                $('.counter').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');

                $({ countNum: $this.text()}).animate({
                  countNum: countTo
                },

                {

                  duration: 8000,
                  easing:'linear',
                  step: function() {
                    $this.text(Math.floor(this.countNum));
                  },
                  complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                  }

                });
            });
          } else {

          }
        });

//menu click
$(".hashchild .dp-icon").click(function(){
    $(".hashchild .subchild").show();
});

$("[name='signUpload']").change(function(e){
    console.log(e);
    if($(this).val()){
        $('.submit-student').prop('disabled', false);
    }
});

// $(".menu-item-has-children").click(function(e){
//  $('.sub-menu').removeAttr('style');
//     $(this).children('.sub-menu').show();
// });
});

// $(document).click(function(e) {

//   if( $(e.target).attr('class') != 'dp-icon') {

//     $(".sub-menu").hide();
//   }
// });

//in-view-js
var $animation_elements = $('.animation-element');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$window.on('scroll resize', check_if_in_view);
$window.trigger('scroll');


/* CUSTOM JS END*/

$(function() {
  var sig = $('#sig').signature();
  $('#disable').click(function() {
    var disable = $(this).text() === 'Disable';
    $(this).text(disable ? 'Enable' : 'Disable');
    sig.signature(disable ? 'disable' : 'enable');
  });
  $('#clear').click(function(e) {
    e.preventDefault();
    sig.signature('clear');
    $('#showsign').attr('src','');
    $('[name="student_signature"]').val('');
    $('.submit-student').prop('disabled', true);
  });
  $('#json').click(function() {
    alert(sig.signature('toJSON'));
  });
  $('#saveImage').click(function(e) {
    e.preventDefault();
    let image = sig.signature('toDataURL', 'image/jpeg');
    $('#showsign').attr('src',image);
    //alert(sig.signature('toDataURL', 'image/jpeg'));
  });

  $('#sig1').signature();
});




$('#ajax-form').on('submit', function(e){
    e.preventDefault();
    var jsonData = getFormData($(this));
    $('.error').html('');
    $('.reg-error').html('');
    let redirectUrl = $(this).attr('data-redirectUrl');
    $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: jsonData,
                beforeSend: function() {
                $('.fa-spin').show();
                $('.btn-disable').prop('disabled', true);
                $('.form-group').removeClass('has-error');
                },
                success: (response) => {
                  $('.fa-spin').hide()
                  $('.btn-disable').prop('disabled', false);
                  if(response.status ==422 && response.error_type =='registration'){
                    $('#registration_no').find('span').text(response.status_text);
                  }
                    if (response.status == 200) {
                        swal({
                            title: "Successfully Registered",
                            text: "You have registered successfuly, OTP has been sent to your mobile & email has been sent to your email address",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                                window.location.href = redirectUrl;
                            }
                        });
                    }
                },
                error: (err) => {
                    $('.fa-spin').hide()
                    $('.btn-disable').prop('disabled', false);
                    let error = err.responseJSON.errors;

                    var output = Object.keys(error).map(key => {
                              return {
                                key: key,
                                value: error[key]
                              };
                            });
                    output.forEach((value, key)=>{
                        console.log(value);
                        $('[name="'+ value.key +'"]').after('<span class="reg-error">'+ value.value[0]+'</span>');
                    });



                    /*var result = Object.keys(error).map(function(key) {

                        if(key=='g-recaptcha-response'){
                            $('#grecaptcha_response').append('Please ensure that you are a human!');
                        }else{
                            $('[name="'+ key +'"]').after('<span class="error"></span>');
                            $('#' + key).append(error[key]);
                            $('.' + key).addClass('has-error');
                        }

                    });*/



                }

            });

        });




var getFormData = function ($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function registrationCheck(event, token, url){
  $.ajax({
      type:'POST',
      url: url,
      data: {'_token':token,registration_no: event.target.value},
      success:(response)=>{
          if(response.status == 422){
            $('[name="registration_no"]').val('');
            swal({
              title: "Error",
              type: "error",
              text: response.status_text
            });
          }
      }

  });
}

function modalFormSubmit(form_name){
    let form = $('#' + form_name);
    let formdata = getFormData(form);
    console.log(formdata);
    $('.error').html('');
    let reloadUrl = form.attr('data-reload-url');
    $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: formdata,
        async: false,
        success: (response)=>{
            console.log(response);
            if(response.status == 200){
                location.reload();
            }
        },
        error: (err)=>{
            console.log(err);
            if(err.status === 422){
                let error = err.responseJSON.errors;
                Object.keys(error).map(function(key) {
                        $('#' + key).append(error[key]);
                    });
            }

        }
    });

}




function signatureUpload(token, e){
    e.preventDefault();
    var sig = $('#sig').signature();
    let image = sig.signature('toDataURL', 'image/jpeg');
    $.ajax({
        type: 'POST',
        url: $('#saveImage').attr('data-url'),
        data: {'_token':token,image: image},
        async: false,
        success: (response)=>{
            console.log(response);
            $('#student_signature').val(response.image);
            $('.submit-student').prop('disabled', false);
        },
        error: (err)=>{

        }
    });
}

function showSignUpload(param){
    if(param =='signature_off'){
        $('.signature-field').hide();
        $('.signature-upload').show();
    }else{
       $('.signature-field').show();
       $('.signature-upload').hide();
    }
}

function paymentCheck(event, formId,payType){
    event.preventDefault();
    $('[name="pay_type"]').val(payType);
    $('#' + formId).submit();
}

function reasonRejectModal(title,reason){
    $('#title').text(title);
    $('.modal-body').text(reason);
    $('#1myModal').modal("show");
}

$('[name="student_pdf"]').change(function(e){
    let file;
    if((file = this.files[0])){
       //console.log(file.type);
       if(file.type !='application/pdf'){
            swal("Warning!", "File type only PDF allow", "warning");
            $('[name="student_pdf"]').val('');
            return false;
       }
    }
});


var _URL = window.URL || window.webkitURL;
$('[name="signUpload"]').change(function (e) {
    var file, img;
    if ((file = this.files[0])) {
        let fileSize = Math.round(file.size / 1024 / 1024);
        if(fileSize > 2){
            swal("Warning!", "Image size couldn't be more than 2MB", "warning");
            $('[name="signUpload"]').val('');
            $('.submit-student').prop('disabled', true);
            return false;
        }
        if(file.type !='image/jpeg' && file.type !='image/png'){
            swal("Warning!", "Image type only allow jpg or png", "warning");
            $('[name="signUpload"]').val('');
            $('.submit-student').prop('disabled', true);
            return false;
        }

        img = new Image();
        img.onload = function () {
            if(this.width >400 || this.width <350){
                swal("Warning!", "Image width should be between 350 - 400 pixel", "warning");
                $('[name="signUpload"]').val('');
                $('.submit-student').prop('disabled', true);
                return false;
            }
            if(this.height > 110 || this.height < 80){
                swal("Warning!", "Image height should be between 80 - 110 pixel", "warning");
                $('[name="signUpload"]').val('');
                $('.submit-student').prop('disabled', true);
                return false;
            }

        };
        img.src = _URL.createObjectURL(file);
    }
});

function addMorePdf(field_name){
    $('#append_more_pdf').append('<div class="form-group"><a><i class="fa fa-trash" aria-hidden="true" onclick="removeAppend(event)" title="remove"></i></a><input class="form-control" type="file" name="student_pdf_more[]"><div class="file-size">Max file size : 25 MB</div><br></div>');
}

/*function addMorePdf(field_name){
    $('#append_more_pdf').append('<div class="form-group"><a><i class="fa fa-trash" aria-hidden="true" onclick="removeAppend(event)" title="remove"></i></a><input class="form-control" type="file" name="student_pdf" id="upload_file"><div class="file-size">Max file size : 25 MB</div><br></div>');
}*/

function removeAppend(event){
  $(event.target).parent().parent().remove();
//  console.log($(event.target).parent().parent());
}

$(document).on('change','input[name="student_pdf"]',function(e){
    var file = this.files[0];
    var formData = new FormData();
    var url = e.target.dataset.url;
    let fileLocation = serverUrl + 'public/pdfs';
    console.log(url);
     formData.append("upload_file", file);
     formData.append("_token", $('[name="_token"]').val());
     console.log(formData);
     $.ajax({
         type: 'POST',
         url: url,
         data: formData,
         processData: false,
         contentType: false,
         beforeSend: function() {
         $('.ajax-loader').show();
         },
         success: (response)=>{
          
          console.log(fileLocation);
           $('.ajax-loader').hide();
           $('[name="student_pdf"]').after('<span class="set-pdf-position"><a href="'+fileLocation+'/'+response +'" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px"></i></a><a href="javascript:void(0)" class="remove-pdf"><i class="fa fa-times" aria-hidden="true" onclick="removePDF(event)"></i></a></span>');
           $('[name="student_pdf"]').after('<input type="hidden" name="student_pdf" id="upload_file" value="'+ response +'">');
           $('[name="student_pdf"]').remove();
         },
         error: (err)=>{
           $('.ajax-loader').hide();
         }
     });
});

$(document).on('change','[name="student_pdf"]',function(e){
    var file = this.files[0];
    var formData = new FormData();
    var url = e.target.dataset.url;
    let fileLocation = serverUrl + 'public/pdfs';
     formData.append("student_pdf", file);
     formData.append("_token", $('[name="_token"]').val());
     console.log(formData);
     $.ajax({
         type: 'POST',
         url: url,
         data: formData,
         processData: false,
         contentType: false,
         beforeSend: function() {
         $('.ajax-loader').show();
         },
         success: (response)=>{
           $('.ajax-loader').hide();
           $('[name="student_pdf"]').after('<span class="set-pdf-position"><a href="'+fileLocation+'/'+response +'" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:24px"></i></a><a href="javascript:void(0)" class="remove-pdf"><i class="fa fa-times" aria-hidden="true" onclick="removePDF(event)"></i></a></span>');
           $('[name="student_pdf"]').after('<input type="hidden" name="student_pdf_file" value="'+ response +'">');
            $('[name="student_pdf"]').remove();
         },
         error: (err)=>{
           $('.ajax-loader').hide();
         }
     });
});


function removePDF(e){
   let url = serverUrl + 'synopsis/file-upload';
    $('[name="student_pdf_file"]').after('<input class="form-control" type="file" name="student_pdf" data-url="'+ url +'">');
    $('[name="student_pdf_file"]').remove();
    $(e.target).parent().parent().remove();
}

function studentform(e){
  let validation = true;
  if($('[name="category_id"]').val() ==''){
    $('[name="category_id"]').parent().addClass('has-error');
       validation = false;
  }
  if($('[name="course_name"]').val() ==''){
    $('[name="course_name"]').parent().addClass('has-error');
       validation = false;
  }
 if($('[name="registration_no"]').val() ==''){
    $('[name="registration_no"]').parent().addClass('has-error');
       validation = false;
  }
    /* if($('[name="registration_year"]').val() ==''){
    $('[name="registration_year"]').parent().addClass('has-error');
       validation = false;
  }*/
  if($('[name="dob"]').val() ==''){
    $('[name="dob"]').parent().addClass('has-error');
       validation = false;
  }
    if($('[name="institute_name"]').val() ==''){
    $('[name="institute_name"]').parent().addClass('has-error');
       validation = false;
  }
    if($('[name="guide_name"]').val() ==''){
    $('[name="guide_name"]').parent().addClass('has-error');
       validation = false;
  }
    if($('[name="guide_designation"]').val() ==''){
    $('[name="guide_designation"]').parent().addClass('has-error');
       validation = false;
  }
    if($('[name="proposed_title_thesis"]').val() ==''){
    $('[name="proposed_title_thesis"]').parent().addClass('has-error');
       validation = false;
  }
  if($('[name="proposed_work_place"]').val() ==''){
    $('[name="proposed_work_place"]').parent().addClass('has-error');
       validation = false;
  }
  if($('[name="proposed_work_place"]').val() ==''){
    $('[name="proposed_work_place"]').parent().addClass('has-error');
       validation = false;
  }
  if($('[name="student_pdf"]').val() ==''){
    $('[name="student_pdf"]').parent().addClass('has-error');
       validation = false;
  }
    return validation;

 // e.preventDefault();
 // $('#student_form').submit();
}


$('[name="category_id"]').change(function(){
  $('[name="course_name"]').html('<option value="">select course</option>');
});
function getSubCategory(event, url){
        if(event.value ==76 ){
          $('#subcat_div').hide();
        }else{
          $('#subcat_div').show();
        }
       $.ajax({
         type: 'GET',
         url: url,
         data: {id: event.value},
         success: (response)=>{

          //$('#add_course').html('');

          if(response.status == 200 && response.is_sub_cat==0){
            let options='';
            for (var i = 0; i < response.data.length; i++) {
              options += '<option value="'+ response.data[i].id +'">'+response.data[i].name +'</option>';
            }
            $('[name="course_name"]').html('<option value="">select course</option>' +options);
            //$('#add_course').html('<div class="form-group"><div class="category-label"><label for="course_id">Course <span class="require-asterik">*</span>:</label></div><select class="form-control" name="course_name"><option value="">select course</option>'+ options +'</select></div>');
          }else {
            let options='';
            for (var i = 0; i < response.data.length; i++) {
              options += '<option value="'+ response.data[i].id +'">'+response.data[i].name +'</option>';
            }
            let localurl= serverUrl + 'get-subcategory';
            $('[name="sub_category"]').html('<option value="">select sub category</option>' +options);
            //$('#add_sub_cat').html('<div class="form-group"><div class="category-label"><label for="course_id">Sub Category <span class="require-asterik">*</span>:</label></div><select class="form-control" name="sub_category" onchange="getSubCategory(this, \'' + localurl + '\')"><option value="">select category</option>'+ options +'</select></div>');
          }
          },
         error: (err)=>{
         }
     });
}

function otpResend(url, event){
  event.preventDefault();
  let mobile = $('[name="otp_mobile"]').val();
  $.ajax({
    type: 'GET',
    url: url,
    data:{mobile:mobile},
    beforeSend: function() {
    $('.btn-disable').prop('disabled', true);
    },
    success: (response)=>{
      setTimeout(function(){ $('.btn-disable').prop('disabled', false); }, 72000);
      console.log(response);

        swal("Success!", "sent OTP to your mobile", "success");
     },
    error: (err)=>{
      $('.btn-disable').prop('disabled', false);
    }
});
}

function setRegistrationNo(value,url){
  $.ajax({
    type: 'GET',
    url: url,
    data:{value:value},
    success: (response)=>{
        if(response.status ==200){
            //swal("Success!","" +response.status_text + "", "success");
        }else if(response.status ==422){
          //$('[name="registration_no"]').val('');
          swal("Warning!", ""+response.status_text +"", "warning");
        }

     },
    error: (err)=>{
    }
  });
}

function checkRegistration(url){
    let name_student = $('[name="name_of_student"]').val();
  $.ajax({
    type: 'GET',
    url: url,
    data:{value:name_student},
    success: (response)=>{
        if(response.status ==200){
            $('[name="registration_no"]').val(response.data);
        }else if(response.status ==422){
          swal("Warning!", ""+response.status_text +"", "warning");
          $('[name="registration_no"]').val('');
        }

     },
    error: (err)=>{
    }
  });
}

function checkFileSize(event){
  console.log(event);
  let fileSize = event.target.files[0].size;
  let exactsize = Math.round(fileSize/1024);
  let maxFilesize = (1024*8);
  if(exactsize > maxFilesize){
      $(event.target.name).after('<span class="has-error file_exis">File size could not be more than 8MB.</span>');
      event.target.value ='';
      return false;

  }else{
    $('.file_exis').remove();
    return true;
  }

}

function updateSynopsisFile(url,event){
    event.preventDefault();
    $('#synopsis_edit_form').attr('action',url);
    $('#synopsis_edit_form').submit();

}


function updateThesisFile(url,event){
    event.preventDefault();
    $('#updateThesisFile').attr('action',url);
    $('#updateThesisFile').submit();

}

function getFacultyStatus(url, thesis_id){
  $('#gradeModalTbody').html('');
      $.ajax({
        type: 'GET',
        url: url,
        data: {'thesis_id':thesis_id},
        async: false,
        success: (response)=>{
            let resData = response.data;
            var trimmedString = '';
            for (var i =0; i<resData.length; i++) {
              let reAssigned = resData[i].reAssigned ==1? ' (revision)':'';
              if(resData[i].grade =='Accepted'){

                if(resData[i].suggestion)
                {
                  var trimmedString = resData[i].suggestion.substr(0, 50);
                
                  //re-trim if we are in the middle of a word
                  trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
                
                
                
                  $('#gradeModalTbody').append('<tr><td>'+ 'adjudicator'+ parseInt(i+1) +'</td><td>'+ resData[i].submitted + reAssigned +'</td><td>'+ resData[i].grade +'</td><td><span id="dots'+resData[i].id+'">'+ trimmedString +'...</span><span id="more'+resData[i].id+'" style="display:none">'+resData[i].suggestion +'</span></br><button onclick="myFunction('+resData[i].id+')" id="myBtn'+resData[i].id+'"> Read more</button></td></tr>');
                }
                else
                {
                  $('#gradeModalTbody').append('<tr><td>'+ 'adjudicator'+ parseInt(i+1) +'</td><td>'+ resData[i].submitted + reAssigned +'</td><td>'+ resData[i].grade +'</td><td></td></tr>');
                }
              }else{
                if(resData[i].reason_reject){
                  $('#gradeModalTbody').append('<tr><td>'+ 'adjudicator'+ parseInt(i+1) +'</td><td>'+ resData[i].submitted + reAssigned +'</td><td>'+ (resData[i].grade? resData[i].grade:'') +' (<a href="#" onclick="reasonRejected(\''+ resData[i].reason_reject +'\')">Reason</a>)</td><td></td></tr>');
                }else if(resData[i].alternate_reason){
                  $('#gradeModalTbody').append('<tr><td>'+ 'adjudicator'+ parseInt(i+1) +'</td><td>'+ resData[i].submitted+ reAssigned +'</td><td>'+ (resData[i].grade ? resData[i].grade: '') +' &nbsp;<a href="#" onclick="downloadReasonDoc(\''+resData[i].id +'\',\'thesis\')"><i class="fa fa-download" aria-hidden="true" data-tog="tooltip" title="Reason"></i></a></td></tr>');
                }else{
                  $('#gradeModalTbody').append('<tr><td>'+ 'adjudicator'+ parseInt(i+1) +'</td><td>'+ resData[i].submitted+ reAssigned +'</td><td>'+ (resData[i].grade ? resData[i].grade: '') +'</td><td></td></tr>');
                }

              }

            }
        },
        error: (err)=>{

        }
    });
}

function myFunction(commentId) {
  
  var dots = document.getElementById("dots"+commentId);
  var moreText = document.getElementById("more"+commentId);
  var btnText = document.getElementById("myBtn"+commentId);

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}

function reasonRejected(reason){
  $('#reason-rejected-modal').modal("show");
  $('#reason_content').text(reason);
}

function downloadReasonDoc(id, type){
  let form  = $('<form action="'+ serverUrl +'/get/download-reason/doc" method="get"></form>');
    form.append('<input type="hidden" name="id" value="'+ id+'">');
    form.append('<input type="hidden" name="type" value="'+ type+'">');
    $('body').append(form);
    form.submit();
}