$(document).ready( function() {
    $('#example').dataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });

    $(document).on('click','#show_other_college', function(e){
      $('#other_college_name').show();
      $('#hide_other_college').show();
      $('#select_state_div').show();
      $('#college_name').hide();
      $('#show_other_college').hide();
    });

    $(document).on('click','#hide_other_college', function(){
      $('#other_college_name').hide();
      $('#hide_other_college').hide();
      $('#select_state_div').hide();
      $('#college_name').show();
      $('#show_other_college').show();
    });


    $("input:radio[name='status']").click(function(){
      $('#upload_docs').hide();
      if($(this).val() == 3 || $(this).val() == 4){
        $('#not-accept-reason').show();
      }else{
        $('#not-accept-reason').hide();
      }
    });

    $('.all_checked').change(function(e){
      if(e.target.checked){
        $('.paychecked').prop('checked', true);
      }else{
        $('.paychecked').prop('checked', false);
      }
    });

    $('.paychecked').change(function(e){
        if($('.paychecked:checked').length == $('.paychecked').length){
          $('.all_checked').prop('checked', true);
        }else{
          $('.all_checked').prop('checked', false);
        }

    });

    $('#status_modal_open').click(function(){

      let transId = $(this).attr('data-transId');
      let transStatus = $(this).attr('data-trans-status');
      document.getElementById("status-change-form").elements.namedItem("transaction_id").value =transId;
      document.getElementById("status-change-form").elements.namedItem("status").value=transStatus;
    });
    /*$('.btn-disable').click(function(){
      $(this).prop('disabled',true);
      $('.fa-spin').show();
    });*/

    $('#reservationtime').daterangepicker({ timePicker: false,locale: {
            format: 'DD/MM/YYYY'
        }});
    $('.datepicker').datepicker({format: 'dd/mm/yyyy',autoclose: true});
    $('[data-toggle-pop="popover"]').popover({trigger: "hover"});

    $('#final_comment').change(function(e){
      if($(this).val() == 'D' || $(this).val() == 'E'){
        $('#show_for_DE').show();
      }else{
        $('#show_for_DE').hide();
      }
    });

    $('.hide-div').click(function(){
      $('.box-show').hide();
    });

    $('.paid-status').change(function(e){
    let status = (e.target.checked)? 1:0;
    let id = e.target.dataset.id;
    let type = e.target.dataset.type;
         $.ajax({
                type: 'GET',
                url: e.target.dataset.url,
                data: { 'status':status, 'id': id, 'type':type},
                //async: false,
                beforeSend: function() {
                $('.overlay').show();
                },
                success:function(response){
                   if(response.status == 200){
                      $('.overlay').hide();
                        swal({
                              title: "Successfuly changed payment status",
                              type: "success",
                              showCancelButton: false,
                              confirmButtonClass: "btn-success",
                              confirmButtonText: "Ok",
                              closeOnConfirm: false,
                              closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                 window.location.reload();
                              }
                          });
                     }
                },
                error:function(){
                  $('.overlay').hide();
                }

            });
    console.log(e);
  });

 } );
var locationProtocol = window.location.protocol;
var hostname = window.location.hostname;
console.log(locationProtocol + '//' + hostname);
//var serverUrl ="http://pxlsysdev.in/sdm/";
//var serverUrl ="https://wbuhs.ac.in/sdm/";
var serverUrl ="http://localhost/root/wbuhs-sdm/";

function  deleteData(url, token){
event.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You want to delete this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
        function () {
            $.ajax({
                type: 'POST',
                url: url,
                data: { '_method': 'delete', '_token': token},
                async: false,
                success: (response) => {

                    //swal("Deleted!", "Your has been deleted.", "success");
                    window.location.reload();
                },
                error: () => {

                }

            });

        });

}

function showCategory(role){
     var values = $("#roles:checked").map(function(){
      return $(this).val();
    }).get();

     if(values.indexOf('2') != -1){
      $('#bankinformation').show();
     }else{
      $('#bankinformation').hide();
     }

     if(values.indexOf('2') != -1 || values.indexOf('4') != -1){
       $('#category-div').show();
       $('.forAdjudicator').show();
       $('#user_college_name').show();
     }else{
       $('#category-div').hide();
        $('.forAdjudicator').hide();
       $('#user_college_name').hide();
     }

}

$(document).ready(function(){
    $('[data-tog="tooltip"]').mouseover(function(){
      $(this).tooltip('show');
    });
    $('[data-tog="tooltip"]').click(function(){
      $(this).tooltip('destroy');
    });
   // $('[data-tog="tooltip"]').tooltip({ trigger:'hover focus' });
});

function sendEmail(url,token, type, sub_type){

    let send_email = $("#send_email:checked").map(function(){
      return $(this).val();
    }).get();

    let synop_id = $('[name="synopsis_id"]').val();
        console.log(send_email);
    $.ajax({
                type: 'POST',
                url: url,
                data: {'_token': token, type:type, send_to:send_email.toString(),synop_id: synop_id},
                 beforeSend: function() {
                  $('.fa-spin').show();
                  $('.btn-disable').prop('disabled', true);
                },
                success: (response) => {
                   $('.fa-spin').hide()
                   $('.btn-disable').prop('disabled', false);

                    if(response.status === 422){
                      swal(response.status_text);
                      return false;
                    }
                    else if(response.status === 500){
                      swal(response.status_text);
                      return false;
                    }

                    console.log(response);

                     swal({
                              title: "Successfully Assigned " + sub_type,
                              type: "success",
                              showCancelButton: false,
                              confirmButtonClass: "btn-success",
                              confirmButtonText: "Ok",
                              closeOnConfirm: false,
                              closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                 window.location.reload();
                              }
                          });
                },
                error: (err) => {
                  $('.fa-spin').hide()
                  $('.btn-disable').prop('disabled', false);
                }

            });

}

function setSynopsiId(id, users, category, assignedUserId){
  $('#send_email_tbody').html('');

          $.ajax({
                type: 'GET',
                url: serverUrl + `admin/category-wise/bos/${id}`,
                success: (response) => {
                    if (response.status == 200) {
                      let assignedUserArray = assignedUserId ? assignedUserId.split(',').map(function(item) {
                        return parseInt(item, 10);
                    }):[];
                    let data = response.data;  
                $('#assign_title').html( ' for (' + category + ') ');
                
                  for (var i =0;i<data.length; i++) {
                    if(assignedUserArray.indexOf(data[i].id) != -1){
                      $('#send_email_tbody').append('<tr><td>'+data[i].full_name +' ('+data[i].speciality  +')'+ '&nbsp;<i class="fa fa-check" aria-hidden="true" style="color:green" data-tog="tooltip" title="Already Assigned"></i>&nbsp;<span class="badge bg-green" data-tog="tooltip" title="Total assigned synopsis">'+ data[i].synopsis_count+'</span>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="withDrawSynopsis('+ id +','+ data[i].id +')" class="btn btn-info btn-xs">Withdraw</a></td><td><input type="checkbox" name="send_email[]" id="send_email" value="'+data[i].id+'" checked disabled></td></tr>');
                    }else{
                    $('#send_email_tbody').append('<tr><td>'+data[i].full_name +' ('+data[i].speciality  +')'+ '&nbsp;<span class="badge bg-green" data-tog="tooltip" title="Total assigned synopsis">'+ data[i].synopsis_count+'</span></td><td><input type="checkbox" name="send_email[]" id="send_email" value="'+data[i].id+'"></td></tr>');
                    }

                  }

                  $('#send_email_tbody').append('<input type="hidden" name="synopsis_id" value="'+ id +'">');
                    }
                },
                error: (err) => {
                }

            });


  /*console.log(data);
  let assignedUserArray = assignedUserId ? assignedUserId.split(',').map(function(item) {
          return parseInt(item, 10);
      }):[];
  $('#assign_title').html( ' for (' + category + ') ');
    for (var i =0;i<data.length; i++) {
      if(assignedUserArray.indexOf(data[i].id) != -1){
        $('#send_email_tbody').append('<tr><td>'+data[i].full_name +' ('+data[i].speciality  +')'+ '&nbsp;<i class="fa fa-check" aria-hidden="true" style="color:green" data-tog="tooltip" title="Already Assigned"></i>&nbsp;<span class="badge bg-green" data-tog="tooltip" title="Total assigned synopsis">'+ data[i].synopsis_count+'</span>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="withDrawSynopsis('+ id +','+ data[i].id +')" class="btn btn-info btn-xs">Withdraw</a></td><td><input type="checkbox" name="send_email[]" id="send_email" value="'+data[i].id+'" checked disabled></td></tr>');
      }else{
      $('#send_email_tbody').append('<tr><td>'+data[i].full_name +' ('+data[i].speciality  +')'+ '&nbsp;<span class="badge bg-green" data-tog="tooltip" title="Total assigned synopsis">'+ data[i].synopsis_count+'</span></td><td><input type="checkbox" name="send_email[]" id="send_email" value="'+data[i].id+'"></td></tr>');
      }

    }
    $('#send_email_tbody').append('<input type="hidden" name="synopsis_id" value="'+ id +'">');*/
}

 function withDrawSynopsis(synopId, bosId){
          $.ajax({
                type: 'GET',
                url: serverUrl + 'admin/synopsis/withdraw',
                data: {synpId:synopId, bosId:bosId},
                success: (response) => {
                    if (response.status == 200) {
                        swal({
                            title: "Successfully Withdraw Synopsis",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                    }
                },
                error: (err) => {
                }

            });
 }

function getAvailableReviewer(id, url, category, status){
        $('#send_email_tbody').html('');
        $('#assign_thesis_title').html( ' for (' + category + ') ');

        if(status ==3){
          $('#reviewer_assign_btn').prop('disabled', true);
        }
        $.ajax({
                type: 'GET',
                url: url,
                data: {'thesis_id': id},
                success: (response) => {
                  let transactionIds = response.send_transaction_ids;
                  console.log(transactionIds[290]);
                  let data = response.users;
                  console.log(data);
                  let sendedUser = response.sendedUser.map(function(item) {
                          return parseInt(item, 10);
                      });
                  let rejectedUser = response.rejectedUser.map(function(item) {
                          return parseInt(item, 10);
                      });
                  console.log(sendedUser);
                  let sendedAcknowledge = response.sendedAcknowledge;
                  console.log(sendedAcknowledge);
                  
                  let sendedRequest = response.sendedRequest;
                  console.log(sendedRequest);
                   var x =0;
                    for (var i =0;i<data.length; i++) {
                      if(sendedUser.indexOf(data[i].id) != -1){
                       
                        if (sendedAcknowledge.includes(data[i].id))
                          {
                            $('#send_email_tbody').append('<tr><td>'+data[i].full_name+' ('+data[i].speciality  +') &nbsp;<i class="fa fa-check" aria-hidden="true" style="color:green" data-tog="tooltip" title="Already Assigned"></i></td><td><input type="checkbox" disabled="disabled" checked="checked" name="send_email[]" id="send_email" value="'+data[i].id+'">&nbsp; <a href="javascript:void(0)" onclick="withdrawDissertationAssign('+ transactionIds[data[i].id]+')" class="label label-warning">withdraw</a></td></tr>');
                          }
                        else
                          {
                            $('#send_email_tbody').append('<tr><td>'+data[i].full_name+' ('+data[i].speciality  +') </td><td><input type="checkbox" disabled="disabled" name="send_email[]" id="send_email" value="'+data[i].id+'"></td><td>Request Send on ('+sendedRequest[x]+') &nbsp; <a href="javascript:void(0)" onclick="withdrawDissertationAssign('+ transactionIds[data[i].id]+')" class="label label-warning">withdraw <i class="fa fa-spinner fa-spin" style="font-size:12px;display: none;" id="spiner_'+ transactionIds[data[i].id]+'"></i></a></td></tr>');
                           x++;
                          }  
                        
                      }
                      else if(rejectedUser.indexOf(data[i].id) != -1){
                        $('#send_email_tbody').append('<tr><td>'+data[i].full_name+' ('+data[i].speciality  +') &nbsp; <i class="fa fa-close" style="color:red" data-tog="tooltip" title="Not accepted"></i></td><td></td></tr>');
                      }
                      else{
                        if(data[i].leave_list !=''){
                          $('#send_email_tbody').append('<tr><td>'+data[i].full_name+' ('+data[i].speciality  +') Not Available ('+ data[i].leave_list +')</td><td><input type="checkbox" name="send_email[]" id="send_email" value="'+data[i].id+'"></td></tr>');
                        }else{
                            $('#send_email_tbody').append('<tr><td>'+data[i].full_name+' ('+data[i].speciality  +') </td><td><input type="checkbox" name="send_email[]" id="send_email" value="'+data[i].id+'"></td></tr>');
                        }

                      }

                    }
                    $('#send_email_tbody').append('<input type="hidden" name="synopsis_id" value="'+id+'">')
                    //$('[name="synopsis_id"]').val(id);
                },
                error: (err) => {

                }

            });
}

function withdrawDissertationAssign(trans_id){
          $.ajax({
                type: 'GET',
                url: serverUrl + 'admin/withdraw-dissertation',
                data: {trans_id:trans_id},
                beforeSend: function() {
                $('#spiner_' + trans_id).show();
                },
                success: (response) => {
                    $('#spiner_' + trans_id).hide();
                    if (response.status == 200) {
                        swal({
                            title: "Successfully Withdraw Dissertation",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                    }
                },
                error: (err) => {
                   $('#spiner_' + trans_id).hide();
                }

            });
}

function setSynopsisStatus(synop_id,status, reason){
  $("input[name='status']").attr('checked', false);
  $('#not-accept-reason').hide();
  if(status == '3' || status == '4'){
    $('#not-accept-reason').show();
    $('[name="reason_reject"]').val(reason);
  }
  $("input[name='status'][value="+ status +"]").attr('checked', true);
  $('#synop_id').val(synop_id);
  $('.error').empty();
}

$('[name="reason_reject_doc"]').change(function(e){
    var file = this.files[0];
    if(file.type !='application/vnd.openxmlformats-officedocument.wordprocessingml.document' && file.type !='application/msword'){
      swal("Warning!", "File type only doc or docx allow", "warning");
      $('[name="reason_reject_doc"]').val('');
      return false;
    }
});

let formdataArray = new FormData();
function changeStatus(event){
        event.preventDefault();
        let data = getFormData($('#synopsis_status'));
        //console.log($('[name="reason_reject_doc"]').val());
        $('.error').empty();
        if((data.status == 3 || data.status == 4) && ( $('[name="reason_reject"]').val()=='' && $('[name="reason_reject_doc"]').val() =='')){
          $('[name="reason_reject"]').after('<span class="error">Please enter reason of reject</span>');
          $('[name="reason_reject_doc"]').after('<span class="error">Please enter reason of reject</span>');
          return false;
        }

        formdataArray.append('status', data.status);
        formdataArray.append('_token', $('[name="_token"]').val());
        formdataArray.append('synop_id', $('[name="synop_id"]').val());
        if($('[name="reason_reject_doc"]').val()){
          formdataArray.append('reason_reject_doc', $('[name="reason_reject_doc"]')[0].files[0]);
        }else if($('[name="reason_reject"]').val()){
           formdataArray.append('reason_reject', $('[name="reason_reject"]').val());
        }
        $.ajax({
                type: 'POST',
                url: $('#synopsis_status').attr('action'),
                data: formdataArray,
                processData: false,
                contentType: false,
                beforeSend: function() {
                $('.fa-spin').show();
                $('.btn-disable').prop('disabled', true);
                },
                success: (response) => {
                  console.log(response);
                  $('.fa-spin').hide()
                  $('.btn-disable').prop('disabled', false);
                    if(response.status == 422){
                        swal("Warning!",response.status_text, "warning");
                        return false;
                    }
                    if (response.status == 200) {
                        swal({
                            title: "Successfully change status",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                    }
                },
                error: (err) => {
                  $('.fa-spin').hide()
                  $('.btn-disable').prop('disabled', false);
                }

            });
}

var getFormData = function ($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

/* Show and Hide */
$('.showFase').each(function () {
    $(this).click(function () {
        const dataId = $(this).attr('data-id');
        $('.hideFase[data-id= ' + dataId + ']').show();
        $('.show-tr[data-id= ' + dataId + ']').show();
        $(this).hide();
    });
});

$('.hideFase').each(function () {
    $(this).click(function () {
        const dataId = $(this).attr('data-id');
        $('.showFase[data-id= ' + dataId + ']').show();
        $('.show-tr[data-id= ' + dataId + ']').hide();
        $(this).hide();
    });
});

function changeThesisStatus(){
          let data = getFormData($('#status-change-form'));
          $.ajax({
                type: 'POST',
                url: $('#status-change-form').attr('action'),
                data: data,
                beforeSend: function() {
                $('.fa-spin').show();
                $('.btn-disable').prop('disabled', true);
                },
                success: (response) => {
                  console.log(response);
                  $('.fa-spin').hide()
                  $('.btn-disable').prop('disabled', false);
                    if (response.status == 200) {
                        swal({
                            title: "Successfully change status",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                    }
                },
                error: (err) => {
                  $('.fa-spin').hide()
                  $('.btn-disable').prop('disabled', false);
                }

            });
}

function updatepayment(url, pay_id){


        swal({
          title: "Update Payment!",
          //text: "Write something interesting:",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          inputPlaceholder: "Enter Amount"
        }, function (inputValue) {
          if (inputValue === false) return false;
          if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
          }/*else if(Number.isInteger(inputValue) == false){
            swal.showInputError("Please enter number!");
            return false
          }*/
           $.ajax({
                  type:'GET',
                  url:url,
                  data:{amount: inputValue, pay_id:pay_id},
                  success:function(response){
                    if (response.status == 200) {

                        swal({
                            title: "Successfully update payment",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                    }
                    else if(response.status == 422){
                        swal.showInputError(response.status_text);
                        return false;
                    }
                  },
                  error: function(err){
                    console.log(err);
                    swal.showInputError(err.responseJSON.errors.amount[0]);
                    return false

                  }
            });

        });


}

$(function() {
  var sig = $('#sig').signature();
   $('#sig1').signature();

  $('#disable').click(function() {
    var disable = $(this).text() === 'Disable';
    $(this).text(disable ? 'Enable' : 'Disable');
    sig.signature(disable ? 'disable' : 'enable');
  });

  $('.clear-signature').click(function(e) {
    e.preventDefault();

    $('#'+ e.target.dataset.signfield).signature('clear');

    $('#' + e.target.dataset.showid).attr('src','');
  });

  $('#json').click(function() {
    alert(sig.signature('toJSON'));
  });

  $('.saveImage-signature').click(function(e) {
    e.preventDefault();
    let image = $('#' + e.target.dataset.signfield).signature('toDataURL', 'image/jpeg');
    $('#' + e.target.dataset.showid).attr('src',image);
    //alert(sig.signature('toDataURL', 'image/jpeg'));
  });


});

function signatureUpload(token, e, setfield, id){
    e.preventDefault();
    var sig = $('#' + id).signature();
    let image = sig.signature('toDataURL', 'image/jpeg');
    $.ajax({
        type: 'POST',
        url: $('#saveImage').attr('data-url'),
        data: {'_token':token,image: image},
        async: false,
        success: (response)=>{
          $('.submit-result-btn').prop('disabled', false);
          $('[name="signature_upload1"]').val('');
            console.log(response);
            $('#' + setfield).val(response.image);

        },
        error: (err)=>{

        }
    });
}

function getFacultyStatus(url, thesis_id){
  $('#gradeModalTbody').html('');
      $.ajax({
        type: 'GET',
        url: url,
        data: {'thesis_id':thesis_id},
        async: false,
        success: (response)=>{
            let resData = response.data;
            var trimmedString = '';
            for (var i =0; i<resData.length; i++) {
              let reAssigned = resData[i].reAssigned ==1? ' (revision)':'';
              if(resData[i].grade =='Accepted'){

                if(resData[i].suggestion)
                {
                  var trimmedString = resData[i].suggestion.substr(0, 50);
                
                  //re-trim if we are in the middle of a word
                  trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
                
                
                
                  $('#gradeModalTbody').append('<tr><td>'+ resData[i].name +'</td><td>'+ resData[i].submitted + reAssigned +'</td><td>'+ resData[i].grade +'</td><td><span id="dots'+resData[i].id+'">'+ trimmedString +'...</span><span id="more'+resData[i].id+'" style="display:none">'+resData[i].suggestion +'</span></br><button onclick="myFunction('+resData[i].id+')" id="myBtn'+resData[i].id+'"> Read more</button></td><td><a href="'+ resData[i].reviewUrl+'" target="_blank">click</a></td></tr>');
                }
                else
                {
                  $('#gradeModalTbody').append('<tr><td>'+ resData[i].name +'</td><td>'+ resData[i].submitted + reAssigned +'</td><td>'+ resData[i].grade +'</td><td></td><td><a href="'+ resData[i].reviewUrl+'" target="_blank">click</a></td></tr>');
                }
              }else{
                if(resData[i].reason_reject){
                  $('#gradeModalTbody').append('<tr><td>'+ resData[i].name +'</td><td>'+ resData[i].submitted + reAssigned +'</td><td>'+ (resData[i].grade? resData[i].grade:'') +' (<a href="#" onclick="reasonRejected(\''+ resData[i].reason_reject +'\')">Reason</a>)</td><td><a href="'+ resData[i].reviewUrl+'" target="_blank">click</a></td></tr>');
                }else if(resData[i].alternate_reason){
                  $('#gradeModalTbody').append('<tr><td>'+ resData[i].name +'</td><td>'+ resData[i].submitted+ reAssigned +'</td><td>'+ (resData[i].grade ? resData[i].grade: '') +' &nbsp;<a href="#" onclick="downloadReasonDoc(\''+resData[i].id +'\',\'thesis\')"><i class="fa fa-download" aria-hidden="true" data-tog="tooltip" title="Reason"></i></a></td><td><a href="'+ resData[i].reviewUrl+'" target="_blank">click</a></td></tr>');
                }else{
                  $('#gradeModalTbody').append('<tr><td>'+ resData[i].name +'</td><td>'+ resData[i].submitted+ reAssigned +'</td><td>'+ (resData[i].grade ? resData[i].grade: '') +'</td><td></td></tr>');
                }

              }

            }
        },
        error: (err)=>{

        }
    });
}

function myFunction(commentId) {
  
  var dots = document.getElementById("dots"+commentId);
  var moreText = document.getElementById("more"+commentId);
  var btnText = document.getElementById("myBtn"+commentId);

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}
function reviewerAcceptStatus(value, url, thesisId){

    if(value ==3){
        swal({
          title: "Reason Of Reject",
          //text: "Reason for reject",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          inputPlaceholder: "Reason for reject"
        }, function (inputValue) {
          if (inputValue === false) return false;
          if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false;
          }
       $.ajax({
                type: 'GET',
                url: url,
                data: { 'status':value, 'thesis_id': thesisId, reason:inputValue},
                //async: true,
                beforeSend: function() {
                $('.overlay').show();
                },
                success: (response) => {
                   $('.overlay').hide();
                     if(response.status == 200){
                        swal({
                            title: "Successfully change status",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                     }
                },
                error: () => {
                   $('.overlay').hide();
                }

            });
        });
    }else{
       $.ajax({
                type: 'GET',
                url: url,
                data: { 'status':value, 'thesis_id': thesisId, reason:''},
                beforeSend: function() {
                $('.overlay').show();
                },
                success: (response) => {
                    $('.overlay').hide();
                     if(response.status == 200){
                        swal({
                            title: "Successfully change status",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
                     }
                },
                error: () => {
                  $('.overlay').hide();
                }

            });
    }

}




$(function() {
   $('#toggle-one').bootstrapToggle({
      on: 'Paid',
      off: 'Notpaid',
      size:'mini',
    });
    //$('#toggle-one').bootstrapToggle('on');
   $('#toggle-one').change(function(e){
    //alert();
    let status = '';
       status = (e.target.checked)? 'accept': 'notaccept';
     $.ajax({
                type: 'GET',
                url: e.target.dataset.url,
                data: { 'status':status, 'thesis_id': e.target.dataset.thesis_id},
                async: false,
                success: (response) => {
                     if(response.status == 200){
                      window.location.reload();
                     }
                },
                error: () => {

                }

            });
    //console.log(e.target.dataset.thesis_id);
   });
  });

function viewSpeciality(url){
         $('#specialze-tbody').html('');
         $.ajax({
                type: 'GET',
                url: url,
                async: false,
                success: (response) => {
                     if(response.status == 200){
                      let data = response.categories;
                      for (var i =0; i<data.length; i++) {
                        $('#specialze-tbody').append('<tr><td>'+ (i+1) +'</td><td>' + data[i].name + '</td></tr>');
                      }

                     }
                },
                error: () => {

                }

            });
}

function uploadSignature(show_id, hide_id){

    $('.' + show_id).show();
    $('.' + hide_id).hide();
    if(show_id != 'signature_upload2' && show_id != 'evulator_sign_for_fail'){
      $('.submit-result-btn').prop('disabled', true);
    }


}

function getDashboardData(type, url){
          $('.box-show').show();
          $('#dashboard_tbody').html('');
          let labelStatus = '';
          let headingText = '';
          if(type =='accepted_reviewer'){
            labelStatus ='label label-info';
            headingText = 'Thesis accepted Reviewers';
          }else if(type =='submitted_reviewer'){
            labelStatus = 'label label-success';
            headingText = 'Thesis submitted Reviewers';
          }else if(type =='evaluation_pending'){
            labelStatus = 'label label-danger';
            headingText = 'Evaluation Pending';
          }
          else{
            labelStatus = 'label label-danger';
            headingText = 'Thesis onprocess Reviewers';
          }

           $.ajax({
                type: 'GET',
                url: url,
                async: false,
                data: {type:type},
                success: (response) => {
                     if(response.status == 200){
                        let data = response.data;
                        $('.set-heading').text(headingText);
                        if(data.length ==0){
                         $('#dashboard_tbody').append('<tr><td colspan="4" style="text-align:center">No Data Available</td></tr>');
                        }else{
                          for (var i =0; i<data.length; i++) {
                            $('#dashboard_tbody').append('<tr><td>'+ (i+1) +'</td><td>' + data[i].user_name + '</td> <td><span class="'+ labelStatus +'">'+ data[i].status_text +'</span></td><td>'+ data[i].last_em_date +'</td></tr>');
                          }
                        }


                     }
                },
                error: () => {

                }

            });
}

function showDocs(show_id, hide_id){
  $('#'+ show_id).show();
  $('#' + hide_id).hide();
}

function reasonRejected(reason){
  //alert();
  $('#reason-rejected-modal').modal("show");
  $('#reason_content').text(reason);
}

function downloadReasonDoc(id, type){
  let form  = $('<form action="'+ serverUrl +'/admin/reason-reject/download" method="get"></form>');
    form.append('<input type="hidden" name="id" value="'+ id+'">');
    form.append('<input type="hidden" name="type" value="'+ type+'">');
    $('body').append(form);
    form.submit();
}

function synopsisReviewStatus(value, synopId, usrId, url){
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: function() {
        $('.overlay').show();
        },
        data: {status:value, synop_id:synopId, user_id: usrId},
        success:(response)=>{
          $('.overlay').hide();
          if(response.status == 200){
                      swal({
                            title: "Successfully change status",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false
                        }, function (isConfirm) {
                            if (isConfirm) {
                               window.location.reload();
                            }
                        });
          }
        },
        error:(error)=>{
          $('.overlay').hide();
        }
    });
}

function getBOSReviewStatus(data, url){
  $('#review_status_tbody').html('');

    $.ajax({
        type: 'GET',
        url: url,
        data: {transaction_ids:data},
        success:(response)=>{
          if(response.status == 200){
            let jsonData = response.data;
            if(jsonData.length > 0){
              let test='';
                for (var i =0; i< jsonData.length; i++) {
               test +='<tr> <td>'+ jsonData[i].user.full_name+'</td> <td>'+ jsonData[i].review_status_text+'</td><td>'+ (jsonData[i].comment ? jsonData[i].comment:"")+'</td></tr>';
              }
              $('#review_status_tbody').html(test);
            }
            $('#view-review-modal').modal('show');
          }
        },
        error:(error)=>{
          $('.overlay').hide();
        }
    });



}

var _URL = window.URL || window.webkitURL;
$('[name="signature_upload1"]').change(function (e) {
    var file, img;
     $('.submit-result-btn').prop('disabled', false);
     $('[name="full_evaluator_signature"]').val('');
/*    if ((file = this.files[0])) {
        let fileSize = Math.round(file.size / 1024 / 1024);
        if(fileSize > 2){
            swal("Warning!", "Image size couldn't be more than 2MB", "warning");
            $('[name="signature_upload1"]').val('');
            $('.submit-result-btn').prop('disabled', true);
            return false;
        }
        if(file.type !='image/jpeg' && file.type !='image/png'){
            swal("Warning!", "Image type only allow jpg or png", "warning");
            $('[name="signature_upload1"]').val('');
            $('.submit-result-btn').prop('disabled', true);
            return false;
        }

        img = new Image();
        img.onload = function () {
            if(this.width >400 || this.width <350){
                swal("Warning!", "Image width can't be less than 350 and more than 400", "warning");
                $('[name="signature_upload1"]').val('');
                $('.submit-result-btn').prop('disabled', true);
                return false;
            }
            if(this.height > 110 || this.height < 80){
                swal("Warning!", "Image height can't be less than 80 and more than 110", "warning");
                $('[name="signature_upload1"]').val('');
                $('.submit-result-btn').prop('disabled', true);
                return false;
            }
            $('.submit-result-btn').prop('disabled', false);
            $('[name="full_evaluator_signature"]').val('');
        };
        img.src = _URL.createObjectURL(file);
    }*/
});

$('[name="signature_upload2"]').change(function (e) {
    var file, img;
    if ((file = this.files[0])) {
        let fileSize = Math.round(file.size / 1024 / 1024);
        if(fileSize > 2){
            swal("Warning!", "Image size couldn't be more than 2MB", "warning");
            $('[name="signature_upload2"]').val('');
            return false;
        }
        if(file.type !='image/jpeg' && file.type !='image/png'){
            swal("Warning!", "Image type only allow jpg or png", "warning");
            $('[name="signature_upload2"]').val('');
            return false;
        }

        img = new Image();
        img.onload = function () {
            if(this.width >400 || this.width <350){
                swal("Warning!", "Image width can't be less than 350 and more than 400", "warning");
                $('[name="signature_upload2"]').val('');
                //$('.submit-result-btn').prop('disabled', true);
                return false;
            }
            if(this.height > 110 || this.height < 80){
                swal("Warning!", "Image height can't be less than 80 and more than 110", "warning");
                $('[name="signature_upload2"]').val('');
                //$('.submit-result-btn').prop('disabled', true);
                return false;
            }

        };
        //img.src = _URL.createObjectURL(file);
    }
});

function getAssignedUser(type,id,url){
  $.ajax({
                type: 'GET',
                url: url,
                data: { 'type':type, 'id': id},
                async: false,
                success:function(response){
                   if(response.status == 200){
                      $('#send_email_tbody').html('');
                      $('.modal-title').html('Assigned Dissertation to Adjudicator for ('+ response.category.name+ ')');
                      let data = response.data;
                      for (var i =0;i<data.length; i++) {
                     $('#send_email_tbody').append('<tr><td>'+data[i].full_name+' ('+data[i].speciality  +') &nbsp;<i class="fa fa-check" aria-hidden="true" style="color:green" data-tog="tooltip" title="Already Assigned"></i></td><td><input type="checkbox" disabled="disabled" checked="checked" name="send_email[]" id="send_email" value="'+data[i].id+'"></td></tr>');
                      }
                     }
                },
                error:function(){

                }

            });
}

function printData(){
     var data = '<div id="div_print"><h2 class="headerClass" style="text-align:center">Student Payment History</h2>';
        data += $('#print_area').html();
        data += '</div>';
        var originalContents = document.body.innerHTML;      
        document.body.innerHTML = data;
        window.print();
        document.body.innerHTML = originalContents;
}

function renumerationFormSubmit(e){
  e.preventDefault();
  var ifs = $('[name="ifs_code"]').val();
  $('#ifsc_error').html('');
  if(ifs.length && ifs.length !=11){
    $('#ifsc_error').html('Please enter 11 digit.');
    return false;
  }else if(ifs ==''){
    $('#ifsc_error').html('This is required field.');
    return false;
  }else{
    $('#renumation_form').submit();
  }
}

function updateCheckedPayment(url){
  let values =[];
  if($('.paychecked:checked').length ==0){
    swal("Warning!", "Your had not checked any checkbox.", "warning");
    return false;
  }
  $('.paychecked:checked').each(function(){
    values.push(this.value);
  });
  console.log(values);
  if(values.length){
      $.ajax({
                type: 'GET',
                url: url,
                data: { 'value_ids':values.toString()},
                beforeSend: function() {
                $('.overlay').show();
                },
                success:function(response){
                  if(response.status == 200){
                      $('.overlay').hide();
                        swal({
                              title: "Successfully changed payment status",
                              type: "success",
                              showCancelButton: false,
                              confirmButtonClass: "btn-success",
                              confirmButtonText: "Ok",
                              closeOnConfirm: false,
                              closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                 window.location.reload();
                              }
                          });
                     }
                },
                error:function(err){
                   $('.overlay').hide();
                }

            });
  }
  console.log(values);
}

function reviewerPaymentExport(event){
  event.preventDefault();
   let values =[];
  if($('.paychecked:checked').length ==0){
    swal("Warning!", "Your had not checked any checkbox.", "warning");
    return false;
  }
  $('.paychecked:checked').each(function(){
    values.push(this.value);
  });
  console.log(values);
  if(values.length){
    $('[name="idsValue"]').val(values.toString());
    $('#export_form').submit();
  }
}

function reviewerPaymentExportPDF(event){
  event.preventDefault();
   let values =[];
  if($('.paychecked:checked').length ==0){
    swal("Warning!", "Your had not checked any checkbox.", "warning");
    return false;
  }
  $('.paychecked:checked').each(function(){
    values.push(this.value);
  });
  if(values.length){
    $('[name="idsValuePdf"]').val(values.toString());
    $('#export_form_pdf').submit();
  }
}


function printResult(printDiv){
     var printContents = document.getElementById(printDiv).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function replacePDF(thesis_id){
    $('#replace_thesis_id').val(thesis_id);
    $("#replacePdf-modal").modal("show");
}

function replaceSynopsisPDF(synop_id){
   $('#replace_synopsis_id').val(synop_id);
   $("#replacesynonp-Pdf-modal").modal("show");
}

function getCategories(value, url){
 
  $('#child_cat').remove();
  $('#child_sub_cat').remove();
  //$('[name="sub_cat_ids"]').remove();
        $.ajax({
                type: 'GET',
                url: url,
                data: {'id':value},
                success:function(response){
                  if(response.status == 200){
                    let options = '';
                    let sub_catArr= [];
                    for (var i =0; i<response.data.length; i++) {
                      sub_catArr.push(response.data[i].id);
                      options += '<option value="'+ response.data[i].id +'">'+ response.data[i].name +'</option>';
                    }
                    if($('[name="sub_cat_ids"]').length){
                      $('[name="sub_cat_ids"]').val(sub_catArr);
                    }else{
                      $('[name="category"]').after('<input type="hidden" value="'+ sub_catArr+'" name="sub_cat_ids">');
                    }
                    
                    
                    $('#parent_div_cat').after('<div class="col-xs-2" id="child_cat"><select class="form-control" name="child_cat" onchange="getSubCategories(event.target.value)"><option value="">All</option>'+ options +'</select></div>');
                     var urlParams = new URLSearchParams(location.search);
                         if(urlParams.get('child_cat')){
                            $('[name="child_cat"]').val(urlParams.get('child_cat'));
                          }
                     }
                },
                error:function(err){
                }

            });
}

function getSubCategories(value){
  let url = serverUrl + 'admin/get-child/category'; 
  var urlParamsFor = new URLSearchParams(location.search);
  if(urlParamsFor.get('category') != 76){
      $('#child_sub_cat').remove();
        $.ajax({
                type: 'GET',
                url: url,
                data: {'id':value},
                success:function(response){
                  if(response.status == 200){
                    let options = '';
                    for (var i =0; i<response.data.length; i++) {
                      options += '<option value="'+ response.data[i].id +'">'+ response.data[i].name +'</option>';
                    }
                    
                    $('#child_cat').after('<div class="col-xs-2" id="child_sub_cat"><select class="form-control" name="child_sub_cat" ><option value="">All</option>'+ options +'</select></div>');
                        var urlParams = new URLSearchParams(location.search);
                        if(urlParams.get('child_sub_cat')){
                            $('[name="child_sub_cat"]').val(urlParams.get('child_sub_cat'));
                        }
                    }
                },
                error:function(err){
                }

            });
  }

}

$(document).ready(function() {
  var urlParams = new URLSearchParams(location.search);
    let value = $('[name="category"]').val();
    let url = serverUrl + 'admin/get-child/category';
    getCategories(value, url);
    if(urlParams.get('child_cat')){
      getSubCategories(urlParams.get('child_cat'));
    }

});



function reSubmitPermission( url, thesisId){

  var status = $('#reSubmit_'+thesisId).prop('checked') == true ? '1' : '0'; 
  var check = confirm("Are you sure change the Re-Submit status?");
    if (check == true) {
      $.ajax({
                type: 'GET',
                url: url,
                data: { 'status':status, 'thesis_id': thesisId},
                //async: true,
               
                success: (response) => {
                  console.log(response.status);
                   if(response.status != 200){

                    alert('The Dissertation has already been assigned!');
                      if(status==1)
                      {
                        var currentStatus = false;
                      }
                      else
                      {
                        var currentStatus = true;
                      }
                            $('#reSubmit_'+thesisId).prop('checked', currentStatus); 
                            return false;
                   }
                }

              });
    }
    else {
      if(status==1)
      {
        var currentStatus = false;
      }
      else
      {
        var currentStatus = true;
      }
            $('#reSubmit_'+thesisId).prop('checked', currentStatus); 
            return false;

        }


}

function resubmitSynopsisEnable(url, event){
  let status = event.target.checked ? 1:0;
        $.ajax({
                type: 'GET',
                url: url,
                data: { 'status':status},
                //async: true,
               
                success: (response) => {
                  if(response.status ==200){
                    swal({
                              title: "Successfuly changed status",
                              type: "success",
                              showCancelButton: false,
                              confirmButtonClass: "btn-success",
                              confirmButtonText: "Ok",
                              closeOnConfirm: false,
                              closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                 window.location.reload();
                              }
                          });

                  }
                  
                }

              });


}

function setBOSReviewStatus(synp_id, status, url){

  $('#review_synopsis_id').val(synp_id);
  $('[name="bos_status"]').prop('checked', false);
  $('#bos-not-accept-reason').hide();
  $('[name="bos_reason_reject"]').val('');
  if(status!=0){
    if(status ==1){$('[name="bos_status"]').attr('disabled', true);}else{
      $('[name="bos_status"]').attr('disabled', false);
    }
    $('[name="bos_status"]').attr('disabled', true);
    $('[name="bos_status"][value="' + status + '"]').prop('checked', true);
    if(status == 2 || status == 3){
              $.ajax({
                type: 'GET',
                url: url,
                //async: true,               
                success: (response) => {
                  if(response.status ==200){
               $('#bos-not-accept-reason').show();
              $('[name="bos_status"]').attr('disabled', false);
              $('[name="bos_reason_reject"]').val(response.data);                   

                  }
                  
                }

              });
    }
  }else{
    $('[name="bos_status"]').attr('disabled', false);
  }
  $('#review-status-modal').modal('show');
}

function bosReviewSubmit(){
  $('#review_synopsis_status').submit();
}

$(document).ready(function(){
  $('[name="bos_status"]').change(function(event){
      console.log(event.target.value);
      if(event.target.checked){
        $('#bos_reviewBtn').show();
      }else{
       $('#bos_reviewBtn').hide(); 
      }
      if(event.target.value ==2 || event.target.value ==3){
        $('#bos-not-accept-reason').show();        
      }else{
         $('#bos-not-accept-reason').hide();
      }
  });

});

function resultInitialSave(event, no){
  event.preventDefault();
 let data = getFormData($('#reviewer_form'));
 let url = serverUrl + 'admin/student-result/init-save/' + no; 
           $.ajax({
                type: 'POST',
                url: url,
                data: data,
                success: (response) => {
                    if(response.status == 422){
                        swal("Warning!",response.status_text, "warning");
                        return false;
                    }
                    if (response.status == 200) {

                    }
                },
                error: (err) => {
                }

            });
}

function setToPreviusState(url){
                          swal({
                            title: "Are you sure change",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                              window.location.href = url;
                            }
                        });
}


/*function sendPushNotification(){
  {
"to": "ExponentPushToken[b9Oo4eAGrPEDUrJUVqZr2K]",
"title": "Title of the Notification",
"body": "Body of your Notification",
"channelId":"default"
}
}*/



