$(document).ready(function(){

//owl-banner
$('.highlighted-slider').owlCarousel({
      loop:true,
      margin:0,
      autoplay:true,
      animateOut: 'fadeOut',
      autoplayTimeout:1500,
      slideSpeed: 300,
      responsiveClass:true,
      autoplayHoverPause: false,
      smartspeed:1000,
      responsive:{
          0:{
              items:1,
              nav:true
          },
          600:{
              items:1,
              nav:true
          },
          1000:{
              items:1,
              nav:true,
              loop:true
          }
      }
});
//mmenu
      api = $("#menu").mmenu({
        //slidingSubmenus: false,
         offCanvas: {
           position  : "right"
         },
         navbar: {
             
         },
         extensions: [
             "pagedim-black"
         ]
     }); 


$('.notifiaction_slider').owlCarousel({
              loop:true,
              margin:0,
              autoplay:true,
              autoplayTimeout:1500,
              slideSpeed: 300,
              responsiveClass:true,
              autoplayHoverPause: true,
              responsive:{
                  0:{
                      items:1,
                      nav:true
                  },
                  600:{
                      items:1,
                      nav:true
                  },
                  1000:{
                      items:1,
                      nav:true,
                      loop:true
                  }
              }
        });

$('#horizontalTab').easyResponsiveTabs({
            type: 'default', 
            width: 'auto', 
            fit: true, 
            tabidentify: 'hor_1',
            activate: function(event) { 
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

$('#marquee-vertical').marquee();
$('#marquee-vertical1').marquee();
$('#marquee-vertical2').marquee();

});
